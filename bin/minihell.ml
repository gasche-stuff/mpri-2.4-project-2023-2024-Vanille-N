(* We instantiate the machinery with the Empty functor,
   which forbids any use of the Do constructor. *)
module Untyped = Untyped.Make (Utils.Empty)
module UntypedPrinter = UntypedPrinter.Make (Utils.Empty)
module Constraint = Constraint.Make (Utils.Empty)
module Infer = Infer.Make (Utils.Empty)
module ConstraintPrinter = ConstraintPrinter.Make (Utils.Empty)
module Solver = Solver.Make (Utils.Empty)

type config = {
  show_source : bool;
  show_constraint : bool;
  log_solver : bool;
  show_type : bool;
  show_typed_term : bool;
}

module LexUtil = MenhirLib.LexerUtil

module Processing = struct
  type doc = PPrint.document
  type 'a t = 'a option * doc option

  let concat_opt_opt a b =
      match (a, b) with
      | None, b -> b
      | a, None -> a
      | Some a, Some b -> Some PPrint.(a ^/^ hardline ^^ b)

  let concat_doc_opt a b =
      match b with None -> a | Some b -> PPrint.(a ^/^ hardline ^^ b)

  let concat_opt_doc a b =
      match a with None -> b | Some a -> PPrint.(a ^/^ hardline ^^ b)

  let return (x : 'a) : 'a t = (Some x, None)
  let error e = (None, Some e)

  let bind (fn : 'a -> 'b t) (a : 'a t) : 'b t =
      let a, doc = a in
      match a with
      | Some a ->
          let b, doc' = fn a in
          (b, concat_opt_opt doc doc')
      | None -> (None, doc)

  let finish (nb_fail : int ref) (a : unit t) : doc =
      let a, doc = a in
      (match a with None -> incr nb_fail | Some _ -> ());
      Option.value ~default:PPrint.empty doc

  let of_result : ('a, doc) result -> 'a t = function
      | Ok a -> return a
      | Error e -> error e

  let of_result_with (efn : 'e -> doc) (a : ('a, 'e) result) : 'a t =
      a |> Result.map_error efn |> of_result

  let write_ok (doc : doc) (a : 'a t) : 'a t =
      bind (fun x -> (Some x, Some doc)) a

  let write_ok_if (cond : bool) (doc : unit -> doc) (a : 'a t) : 'a t =
      if cond then write_ok (doc ()) a else a

  let and_then_write (doc : 'a -> doc) (a : 'a t) : 'a t =
      bind (fun x -> return x |> write_ok (doc x)) a

  let and_then_write_if (cond : bool) (doc : 'a -> doc) (a : 'a t) : 'a t =
      if cond then and_then_write doc a else a

  let error_section doc = Printer.(with_header "Fatal error" doc)

  let of_loc file (loc, msg) =
      error_section
        PPrint.(
          string (Printf.sprintf "Syntax error in %s" (Filename.quote file))
          ^/^ string "help:"
          ^/^ L.highlight_range file loc ~label:PPrint.(string msg))

  let of_exn (file : string) (e : exn) : doc =
      error_section
        (match e with
        | Invalid_argument info -> PPrint.string info
        | Failure fail -> PPrint.string fail
        | Utils.Not_found_at (msg, localizer) ->
            PPrint.(string msg ^/^ localizer file)
        | Utils.Invalid_argument_at (msg, localizer) ->
            PPrint.(string msg ^/^ localizer file)
        | _ -> raise e)

  let catch fmt fn =
      match fn () with res -> res | exception e -> error (fmt e)
end

type doc = PPrint.document

let append_section doc header inner =
    PPrint.(doc ^^ Printer.with_header header inner ^^ hardline ^^ hardline)

let call_parser parser_fun input_path : Untyped.raw_term L.t Processing.t list =
    let ch = open_in input_path in
    Fun.protect ~finally:(fun () -> close_in ch) @@ fun () ->
    let lexbuf = Lexing.from_channel ch in
    let lexbuf = LexUtil.init input_path lexbuf in
    let terms =
        parser_fun lexbuf UntypedParser.Incremental.batch_term_elem
          lexbuf.lex_curr_p
    in
    terms |> List.map (Processing.of_result_with (Processing.of_loc input_path))

let call_freshen ~config ~file (raw_term : Untyped.raw_term L.t) :
    Untyped.term L.t Processing.t =
    let fresh () =
        L.map Untyped.freshen raw_term
        |> Processing.return
        |> Processing.(
             and_then_write_if config.show_source (fun term ->
                 Printer.with_header "Input term"
                   (UntypedPrinter.print_term (L.destruct term))))
    in
    Processing.(catch (of_exn file) fresh)

let gen_constraint ~config ~file (term : Untyped.term L.t) : _ Processing.t =
    let cst () =
        let w = Constraint.Var.fresh "final_type" in
        Processing.return
          Constraint.(
            Exist
              ( w,
                L.toplevel,
                None,
                Top,
                Conj
                  ( Infer.has_type Untyped.Var.Map.empty Infer.AliasEnv.empty
                      term w,
                    Infer.decode w ) ))
    in
    Processing.(
      catch (of_exn file) cst
      |> and_then_write_if config.show_constraint (fun cst ->
             Printer.with_header "Generated constraint"
               (ConstraintPrinter.print_constraint cst)))

let format_typing_failure file err : doc =
    Printer.with_header "Error"
      (match err with
      | Infer.Clash cl ->
          let ty1, ty2, doc = cl () in
          PPrint.(
            Printer.incompatible
              (L.map_destruct STLCPrinter.print_ty ty1)
              (L.map_destruct STLCPrinter.print_ty ty2)
            ^/^ PPrint.string "help:"
            ^/^ L.highlight_range file (L.aux ty1)
                  ~label:(PPrint.string "found to impose structure (1)")
            ^/^ L.highlight_range file (L.aux ty2)
                  ~label:(PPrint.string "incompatible with structure (2)")
            ^/^ string "hint: " ^^ doc ())
      | Infer.Cycle cyc ->
          let cyc = cyc () in
          Printer.(
            cycle
              (Structure.cycle_pp cyc.ty)
              (List.map Printer.source_var cyc.user_vars)
            ^/^ PPrint.string "help:"
            ^/^ L.highlight_many file
                  (List.map snd cyc.user_vars)
                  ~label:(string "these variables are involved in the cycle")))

let solve_constraint ~config ~file (cst : (_, _) Constraint.t) : _ Processing.t
    =
    let logs, result =
        let logs, env, nf =
            Solver.eval ~log:config.log_solver Unif.Env.empty cst
        in
        let result =
            match nf with
            | NRet v ->
                let decode = Decode.make_decoder env () in
                Ok (v decode)
            | NErr e -> Error e
            | NDo _ -> .
        in
        (logs, result)
    in
    Processing.(
      return ()
      |> write_ok_if config.log_solver (fun () ->
             Printer.with_header "Constraint solving log"
               PPrint.(separate hardline logs))
      |> bind (fun () -> of_result_with (format_typing_failure file) result))

let call_typer ~config ~file (term : Untyped.term L.t) : _ Processing.t =
    Processing.(
      return term
      |> bind (gen_constraint ~config ~file)
      |> bind (solve_constraint ~config ~file))

let format_success ~config (term, ty) : unit Processing.t =
    Processing.(
      return ()
      |> write_ok_if config.show_type (fun () ->
             Printer.with_header "Inferred type"
               (STLCPrinter.print_ty (L.destruct ty)))
      |> write_ok_if config.show_typed_term (fun () ->
             Printer.with_header "Elaborated term"
               (STLCPrinter.print_term (L.destruct term))))

let process_one file ~config (raw_term : Untyped.raw_term L.t) :
    unit Processing.t =
    Processing.(
      return raw_term
      |> bind (call_freshen ~file ~config)
      |> bind (call_typer ~config ~file)
      |> bind (format_success ~config))

let process_all ~config input_path : unit Processing.t Seq.t =
    let raw_terms = call_parser MenhirDriver.parse input_path in
    raw_terms |> List.to_seq
    |> Seq.map (Processing.bind (process_one input_path ~config))

let parse_args () =
    let show_source = ref false in
    let show_constraint = ref false in
    let log_solver = ref false in
    let show_type = ref true in
    let show_typed_term = ref true in
    let check_wf = ref false in
    let parseable_output = ref false in
    let annot_level = ref 0 in
    let inputs = Queue.create () in
    let add_input path = Queue.add path inputs in
    let usage = Printf.sprintf "Usage: %s [options] <filenames>" Sys.argv.(0) in
    let spec =
        Arg.align
          [
            ("--wf", Arg.Set check_wf, " Run extra well-formedness checks");
            ("--show-source", Arg.Set show_source, " Show input source");
            ( "--show-constraint",
              Arg.Set show_constraint,
              " Show the generated constraint" );
            ( "--log-solver",
              Arg.Set log_solver,
              " Log intermediate constraints during solving" );
            ( "--show-type",
              Arg.Set show_type,
              " Show the inferred type (or error)" );
            ( "--show-typed-term",
              Arg.Set show_typed_term,
              " Show the inferred type (or error)" );
            ( "--parseable_output",
              Arg.Set parseable_output,
              " Make sure that the elaborated term is parseable again" );
            ( "--annot-level",
              Arg.Set_int annot_level,
              " Control the level of annotations. 0: no annotations (default). \
               1: keep annotations already in the source. 2: insert \
               annotations everywhere." );
          ]
    in
    Arg.parse spec add_input usage;
    Config.(OnceCell.set enable_wf_check !check_wf);
    Config.(OnceCell.set parseable_elaboration !parseable_output);
    Config.(OnceCell.set annotation_level !annot_level);
    let config =
        {
          show_source = !show_source;
          show_constraint = !show_constraint;
          log_solver = !log_solver;
          show_type = !show_type;
          show_typed_term = !show_typed_term;
        }
    in
    let input_paths = inputs |> Queue.to_seq |> List.of_seq in
    (config, input_paths)

let () =
    let config, input_paths = parse_args () in
    let nb_fail = ref 0 in
    input_paths |> List.to_seq
    |> Seq.concat_map (process_all ~config)
    |> Seq.map (Processing.finish nb_fail)
    |> TermSeq.of_seq
    |> TermSeq.punctuate (fun () -> PPrint.(string "----------" ^^ hardline))
    |> TermSeq.iter
         (fun d -> d |> Utils.string_of_doc |> print_endline)
         ~finish:(fun () -> ());
    if !nb_fail > 0 then exit (100 + min !nb_fail 99)
