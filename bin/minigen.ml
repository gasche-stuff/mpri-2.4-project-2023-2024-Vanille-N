type config = {
  exhaustive : bool;
  depth : int;
  count : int;
  seed : int option;
  full_type : bool;
}

let config =
    let exhaustive = ref false in
    let depth = ref 10 in
    let count = ref 1 in
    let seed = ref None in
    let check_wf = ref false in
    let usage = Printf.sprintf "Usage: %s [options]" Sys.argv.(0) in
    let ext_features = ref false in
    let full_type = ref false in
    let annot_level = ref 0 in
    let parseable_output = ref false in
    let spec =
        Arg.align
          [
            ("--wf", Arg.Set check_wf, " Run extra well-formedness checks");
            ("--exhaustive", Arg.Set exhaustive, " Exhaustive enumeration");
            ("--depth", Arg.Set_int depth, "<int> Depth of generated terms");
            ("--count", Arg.Set_int count, "<int> Number of terms to generate");
            ( "--seed",
              Arg.Int (fun s -> seed := Some s),
              "<int> Fixed seed for the random number generator" );
            ( "--ext-features",
              Arg.Set ext_features,
              " Include (some) language extensions to generated terms" );
            ( "--full-type",
              Arg.Set full_type,
              " Also print the full type of generated terms" );
            ( "--parseable-output",
              Arg.Set parseable_output,
              " Ensure that the output of `minigen` can be parsed as an input \
               to `minihell`" );
            ( "--annot-level",
              Arg.Set_int annot_level,
              " Control the level of annotations. 0: no annotations (default). \
               2: insert annotations everywhere." );
          ]
    in
    Arg.parse spec (fun s -> raise (Arg.Bad s)) usage;
    Config.(OnceCell.set enable_wf_check !check_wf);
    Config.(OnceCell.set generator_ext_features !ext_features);
    Config.(OnceCell.set parseable_elaboration !parseable_output);
    Config.(OnceCell.set annotation_level !annot_level);
    {
      exhaustive = !exhaustive;
      depth = !depth;
      count = !count;
      seed = !seed;
      full_type = !full_type;
    }

let () =
    match config.seed with
      | None -> Random.self_init ()
      | Some s -> Random.init s

let generate (module M : Utils.MonadPlus) =
    let module Gen = Generator.Make (M) in
    M.run @@ Gen.typed ~depth:config.depth

let () =
    generate (if config.exhaustive then (module MSeq) else (module MRand))
    |> Seq.map (fun (term, ty) ->
           if config.full_type then
             PPrint.(
               surround 2 0 lparen
                 (STLCPrinter.print_term (L.destruct term)
                 ^/^ string " : "
                 ^^ STLCPrinter.print_ty (L.destruct ty))
                 rparen)
           else STLCPrinter.print_term (L.destruct term))
    |> TermSeq.take_from_seq config.count
    |> TermSeq.punctuate (fun () ->
           PPrint.(
             if Config.(OnceCell.get parseable_elaboration) then
               hardline ^^ string "--%" ^^ hardline
             else string ""))
    |> TermSeq.iter
         (fun d -> d |> Utils.string_of_doc |> print_endline)
         ~finish:(fun term ->
           let d = TermSeq.seq_take_termin_pp term in
           PPrint.(hardline ^^ d) |> Utils.string_of_doc |> print_endline)
