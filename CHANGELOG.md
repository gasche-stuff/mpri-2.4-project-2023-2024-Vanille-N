# Changelog

Here are recorded the extensions and modifications that occured after
the completion of the mandatory part of the project on 2024-01-17.
The features are recorded by order of implementation.


## Sum types
**Kind:** new feature \
**Scope:** parsing, inference, unification, generation, printing \
**Impl:**
[2847e55](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/2847e5556111c3e2b05a46150b8418c14ff411c8) \
**Tests:** sumannot, case, swap, orelim, distrib

Added sum types to the language, for now only of arity 2.

The syntax of annotations and printing is `[a + b]`

The syntax for elimination is
```
case t of
| x => f x
| y => g y
esac
```
and for introduction is
```
left x
right x
```

The generator can now produce much more low-depth terms because `left _` and `right _`
are a depth-1 way of having an `App`-like constructor.

Having binary sums and products we can already do some nontrivial manipulations
like the standard proof of `A /\ (B \/ C) <-> (A /\ B) \/ (A /\ C)` implemented
in `distrib.test`.


## Towards n-ary sums
**Kind:** refactoring / bugfixes \
**Scope:** parsing, inference, unification \
**Impl:**
[9709096](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/9709096ec271eca06731f9b4a43bd7eb0d00d740)
[56e90f7](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/56e90f719101f2cbab411ca560815d145c9bc6d9)

Changed the syntax of sums from `left x`, `right x` to `'0 x`, `'1 x`
in anticipation for sum types of higher arity.

Solved issues that would have made unification of `Sum` structures incorrect
in the presence of sums of different lengths being merged together.

In the process the type of sums has been extended from `[a + b]` only
to `[a + b]` or `[a + b + ...]` in anticipation of having `case` impose
an upper bound on the length and `'n` only a lower bound on the length.


## Wildcard annotations
**Kind:** improvement / cosmetics \
**Scope:** parsing, unification, printing \
**Impl:**
[1b2ce12](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/1b2ce123835efa9cde5c62059e23f799c68c51ce) \
**Tests:** wildcard

Type annotations should support partial structures, and these partial annotations
should be merged together when relevant.

For example we want to be able to have the following annotated (untyped) term
`lambda f. lambda x. (f : _ -> int) x` where `f : _ -> int` is only partially annotated,
and have it type to `(a -> int) -> a -> int`.

To this end we introduce the `Wildcard` constructor of constraints that is a neutral
element for constraint unification.



## Full n-ary sums
**Kind:** improvement \
**Scope:** inference, generation \
**Impl:**
[9d2b70f](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/9d2b70ff3b841f2c1b9d8f2d152b3691475804b7)
[ff32206](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/ff32206ff7daf39a80d33c1b1f4fa7a24a8a68d2)
[d4c9d58](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/d4c9d58707debb4425b3ec3c997a09f7ab54c218) \
**Tests:** unary-sum-projection, explosion, double-negation, contrapositive

We now have sums of arbitrary arity, and all the standard functions that come with them,
most notably the explosion principle `[] -> a` for which a term is
`lambda x. case x of esac`

The inference has been made much more complex, I am working towards a refactoring
in the form of a fold-like function for constraint generation.
The generation can only produce sizes 2 or lower, it may be improved later.



## Type hint heuristics
**Kind:** cosmetics \
**Scope:** unification, printing \
**Impl:**
[7756535](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/7756535d8fe279148ae8c47a7c8dcb6071b168fd) \
**Tests:** ignored-variant, ignored-field, ignored-input, sparse, ignored-twice

When type variables appear only once in the type such as `b` in `a -> b -> a`,
it is obvious that the actual implementation ignores them.
When a variant of a sum type is used, other variants are never generated and
can thus be instanciated with any type later, such as `b` in `a -> [b + a + ...]`.

This feature changes the naming convention of variables such that
- variants that are never instanciated have a type variable named "ø"
- arguments that are never used have a type variable named "\_"

This makes the typer prefer types such as `a -> _ -> a` and `a -> [ø + a + ...]`
instead of `a -> b -> a` and `a -> [b + a + ...]` and makes it more explicit
which variables are actually relevant.

Note that this is only a hint and affects exclusively the user-facing representation,
there is nothing changed to the actual unification. This is also a heuristic
and it may be imperfect.
The following known imperfections exist:
- error messages may type `lambda y. y` as `_ -> a` if the internal constraints
  have not yet been solved.
- wildcards sometimes cancel each other out too much, e.g. in `ignored-twice.test`
  where `lambda x. lambda y. let a = x in y` considers `x` to be used
- patterns and annotations may cause false negatives

Simultaneously the printing of freshened variables is adjusted from
`a/1` to the less intrusive `a₁`.


## Tuples of arbitrary arity
**Kind:** improvement \
**Scope:** inference \
**Impl:**
[438c919](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/438c9193d42bb43d807995dc8b2441c4cb437d94) \
**Tests:** bigprod, unit, lazy

Higher-arity tuples were indeed simpler than higher-arity sums, so no additional
difficulty here.
I won't expand on this much because although I implemented it in full, it was
immediately superseded by the next feature.

Still, one interesting new thing that we have at this stage is a unit type!
This means that `minigen --depth 2 --exhaustive` is now nonempty, and we can
have types like option (`[a + {}]`) and lazy (`[a + {} -> a]`)


## Patterns
**Kind:** new feature
**Scope:** parsing, inference
**Impl:**
[04e4def](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/04e4defebcbf3de17acc36462339ad132057b725) \
**Tests:** {case,lambda,let}-pat-{unit,nested}, pat-annot

Full support for annotated patterns of products such as
- `let (a, _, (b : _ -> _)) = ... in ...`
- `lambda ((a : A), (b : B)). ...`
- `case ... of | (_ : {}) => ... esac`

More precisely the patterns support
- arbitrary nesting
- n-ary tuples
- wildcards ("`_`")
- annotations of subpatterns

and are allowed on
- function arguments
- `let` bindings
- match arms binders

A new constructor of terms (both typed and untyped) is introduced: `LetPat`.
Later both `Let` and `LetTuple` are removed.

This feature has caused somewhat of an explosion of the size of the generated constraints,
I might try to optimize the constraints slightly later.


## Field accesses
**Kind:** new feature \
**Scope:** parsing, inference \
**Impl:**
[8414145](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/8414145b15cc5331fad7e47d2717b8c56174669e) \
**Tests:** field-access

Added the construct `x.n` (where `n` is an integer) for accessing fields of tuples.
Because this is very similar in spirit to work that has already been done on sums
(inference for for the projection `x.n` has exactly the same difficulties as
inference for the injection `'n x`, namely the approximation of the size),
most of the inference work is almost copy-pasted.

The machinery for length lower bounds has now proven useful twice, I should refactor it.


## Refactoring arity handling into `Unsized`
**Kind:** major refactoring \
**Scope:** utils \
**Impl:**
[e7eb312](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/e7eb3124e9ef385371c96010d67c7345b6314494)
[0105bf5](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/0105bf57a436c97b4cd78c6ce782a71c8f8e1cb5)

The `Unsized` module provides what was previously manually implemented with
`list` and `bool` in `Sum`, that is it lets us generically manipulate sequences
and is optimized for binding operations.

The interface of `Unsized` was difficult to get right because of the continuation
style on constraint generation, but the actual implementation is very type-guided.


## Named variants and fields
**Kind:** new feature \
**Scope:** parsing \
**Impl:**
[af41718](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/af41718debb1df0b7882831c0d505d8081aaa17b)
**Tests:** named-field, named-lettup, named-match, named-tup, named-variant, option

Now that `Unsized` is fully abstract, we get almost for free a huge new feature.
This feature is all the more remarkable that *there was no change to the inference procedure*.

By abstracting over product and sum indices, we can perform a textual replacement
of `int` into `string` and we get named fields and named variants.
The following additional term constructors are now available:
- `x.foo` projection of `{foo:a * …}` to `a`
- `'foo x` injection of `a` into `[foo:a + …]`

and all the associated forms:
- `let (x=a, y=b) = tup in ...`
- `case x of | 'a a => a | 'b b => b esac`
- etc.

In exchange for a lot of work in `Unsized` -- and nontrivial parsing --,
the implementation of inference was found to be literally copy-pasting the
inference arms for the integer version and replacing `int` with `string` in
type annotations. This huge amount of copy-pasted code will soon be refactored.

`tests.t/option.test` shows that we now have enough features to implement something
that truly looks like a library, as it is one term of type
```
{bind:(α → [None:{} + …]) → [None:{} + Some:α] → [None:{} + …]
* default:[None:{} + Some:β] → β → β
* flatten:[None:{} + Some:[None:{} + Some:γ]] → [None:{} + Some:γ + …]
* iter:(δ → {}) → [None:{} + Some:δ] → {}
* map:(β₁ → α₁) → [None:{} + Some:β₁] → [None:{} + Some:α₁ + …]
* xor:[None:{} + Some:γ₁] → [None:{} + Some:γ₁] → [None:{} + Some:γ₁]}
```
in which one will recognize the standard library features of the `option` type.

Annotations for these types are available starting with
[055a638](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/055a6383535ee6c6816e4c0edb8f92389c621b1f)


## Several rounds of refactoring and documentation

Most notably
- [ec862bf](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/ec862bfc1b5a6f94755d2069ca0cce968344d290) for the printer,
- [0bdb90b](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/0bdb90b42f4d9b7763d1222410db259bc3e18ccb) simplify mutual recursion in `Infer.ml`,
- [87c5a68](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/87c5a68e727bc20effe67fc04cef32d2205850c3) documentation of `Unsized.ml`,
- [7eaadcc](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/7eaadccdb7eae722290b25f93046973b14fd6d13) documentation of `Hint.ml`,
- [ed6cdfb](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/ed4cdfb7c75c812657390b55e6b63364b2cfe964) fully reuse all the identical logic on sum and product types
- [64e87be](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/64e87befe7f983a1f85538b5955c88bea408c70b) recategorization of all test files per feature.


## Extra command line flags to enable runtime wf checks
**Kind:** improvement \
**Scope:** command line \
**Impl:**
[957fff8](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/957fff800c525022f4fbec35be3a532738341a92) \

Several types, most notably `Unsized.t` and `MRand.t`, have nontrivial well-formedness
invariants.

The command-line flag `--wf` is added, which when set enables runtime well-formedness
checks of the invariants of these types (off by default).

The extra cost is
- very minor when the flag is turned off (one `ref bool` lookup per constructor invocation).
- `O(n)` when the flag is turned on, as it may perform several `List.length` computations
  per constructor invocation.
  Even then, `n` here is typically a constant of the size of the program, such as
  the number of arms in a `case` expression, or the number of elements in a tuple,
  so the impact on performance should be negligible.

It is thus chosen to have this flag
- **active** in **inference** tests of `run.t`.
- **inactive** in **generation** tests of `run.t` which are more performance critical.


## Additional constructors and destructors
**Kind:** improvement \
**Scope:** parsing, inference, printing \
**Impl:**
[840339f](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/840339f2e09d123661ac7c4da27e23446912ba1c)
[14efea8](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/14efea8157e26a695d4ea8afbc7dda89a11da74f)
[55b8485](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/55b84859a7765f61b8f38659794a45a9adcbac9f) \
**Tests:** `pattern/drop-fields`, `pattern/drop-all-fields`, `sum/default-case`, `sum/labeled-empty`, `library/bool`, `sum/iflet`

The availability of `Unsized` and the parallels in inference logic that it makes apparent makes more
self-evident the features that are "missing" in that e.g. there is a language construct that does
not have a dual for some type construction.

- We have a partial destructor for term products `x.1` and `x.foo`, but we do not
  have one for pattern products.
  * we add `let (x, y, ...) = tup in term` and `let (x=x, y=y, ...) = tup in term`
- We have a partial constructor `case t of esac` for sums but we do not have a
  partial destructor.
  * we add `case t of | … ⇒ default esac`
  * we also add `if let 'foo x = sum then term else term`
- We have a partial destructor for products but we do not have a partial constructor.
  * we add `(x with a=a)`

These additional language features are interesting mainly because their addition to
the language requires remarkably little new logic, and we get all of them simultaneously
for integer labels and for string labels for the same amount of work, showing that
the `Unsized` library already has enough features and is abstract enough.


For the specific case of the `with` operator, notice that its signature is a lot
weaker than it could theoretically be. Currently `(x with a=y)` is more like Rust's
`..` than OCaml's `with`. That is, the function `lambda x y. (x with a=y)` could
be given one of the types
1. `{a:a * ...} -> a -> {a:a * ...}` (current semantics, and also what Rust does
  with its corresponding construct)
2. `{a:a * ...} -> b -> {a:b * ...}` (OCaml equivalent)
3. `{...} -> b -> {a:b * ...}` (most general signature)

Though 1. is a bit restrictive, it is interesting to observe that it comes completely
for free with the current architecture of the project.
On the other hand 2. and 3. cannot be expressed with our current constraints,
Several failed attempts have confirmed that this is tricky to implement.

It is also a good time to note that in general our signatures are not as precise
as they could be, because there is a small amount of ambiguity in the `...` of types.
The term `lambda x. let _ = x.1 in x` will be inferred to be of type
`{_₁ * _₂ * ...} -> {_₁ * _₂ * ...}` but implicitly the two `...` are equal
and this is not fully reflected in the signature. There is no unsoundness here
because we are just inferring a slightly more general type, but it could be a
good small improvement to have variables to distinguish the `...` from each other.



## Inductive algebraic datatypes
**Kind**: new feature \
**Scope**: parsing, inference, printing \
**Impl**: \
**Tests**: `rec/simple`, `rec/should-fail/unequal`, `rec/list`, `rec/emptylist`,
`rec/func`, `parsing/alias-precedence`, `rec/is-empty.test`, `rec/let.test`

A new syntax is introduced to declare types:
`type <args>Name of definition in term`

for example
- `type <>Bool of [True:{} + False:{}]`
- `type <a>Pair of {a * a}`
- `type <a,l>Tree of [Leaf:l + Node:{<a,l>Tree * a * <a,l>Tree}]`

Terms can be built through `Tree'Node (Tree'Leaf, (), Tree'Leaf)`, and
`Tree _` is an irrefutable pattern that can be used in `let`.
Annotations for these types are not yet supported.

There is a possibility that this could make inductive functions possible and
interesting without resorting to primitive types such as builtin lists or
integers.

Named types are equipped with an operation called "unfold" that removes
the constructor: in the context `type <a>T of a`,
if `v : a` then `T v : <a>T`;
The unfolding is the reciprocal operation where if
`t : <a>T` then `T/t : a`. This is possible because all our inductive types
have exactly one constructor and it drastically reduces the amount of
`let`s required to destructure such values.


## Recursive functions
**Kind**: improvement \
**Scope**: inference \
**Impl**: \
**Tests**: `rec/rev`, `rec/list-map`, `rec/tree`

`let rec` is basically a copy-paste of `let` that uses the updated environment
to type the term being bound. This trivially enables recursive function definitions,
but what makes it an actually interesting addition is that we can now traverse
inductively defined datatypes.

I am particularly proud of
```ml
-- tests.t/rec/list-map.test
type <a>List of [Nil:{} + Cons:{a * <a>List}] in
let rec map = lambda f (List l).
  List (case l of
  | 'Nil => 'Nil
  | 'Cons (a, tail) => 'Cons (f a, map f tail)
  esac)
in map
```
that is properly inferred to be of type
`(β → α) → <β>List → <α>List`

See also `tests.t/library/list.test` for a good chunk of the `OCaml` standard
library for `List`


## Complex libraries using indutive types
**Kind**: demo \
**Scope**: tests \
**Impl**: \
**Tests**: `library.t/*`

In short I went to
- [`Bool` (OCaml)](https://v2.ocaml.org/api/Bool.html)
- [`Option` (Rust)](https://doc.rust-lang.org/std/option/)
- [`Either` (OCaml)](https://v2.ocaml.org/api/Either.html)
- [`Lazy` (OCaml)](https://v2.ocaml.org/api/Lazy.html)
- [`Result` (Rust)](https://doc.rust-lang.org/std/result/)
- [`List` (OCaml)](https://v2.ocaml.org/api/List.html)
- [`Seq` (OCaml)](https://v2.ocaml.org/api/Seq.html)
- [`Map` (OCaml)](https://v2.ocaml.org/api/Map.S.html)

and implemented everything that looked interesting.
This results in about 2000 lines of code that have been placed in the
`library.t` test suite.


## Improve error message for cyclic types
**Kind**: pretty-printing \
**Scope**: unification \
**Impl**: \
**Tests**: `rec/should-fail/cycle`, `rec/should-fail/alias-cycle`

When a cycle is detected we lazily decode a partial type with a loop,
to show an error message such as
```
Cycle detected: ω occurs in
ω = ω → _
```


## Pure `Decode.decode`
**Kind**: critical bugfix \
**Scope**: elaboration \
**Impl**: [bbf22b4](https://gitlab.com/Vanille-N/mpri-2.4-project-2023-2024/-/commit/bbf22b4407163a182d427d92ff350fa3d4d81f06) \
**Tests**: `bugfix.t/*`

See explanations in `bugfix-003.t`

In short `Decode.decode` having side effects causes problems during elaboration.
As protection against this bug I have implemented a way to pipe the output of the
generator back into the typechecker.
