type <>Int of ['Z + 'S:<>Int] in
type <>Bool of ['T + 'F] in
type <a>Option of ['None + 'Some:a] in

let rec int_eq = lambda (Int m) (Int n).
  case m of
  | 'Z =>
    case n of
    | 'Z => Bool'T
    | … => Bool'F
    esac
  | 'S m =>
    case n of
    | 'Z => Bool'F
    | 'S n => int_eq m n
    esac
  esac
in

type <>Var of <>Int in

let var_eq = lambda (Var v) (Var w).
  int_eq v w
in

type <a>List of [{} + {a * <a>List}] in

let rec assoc_opt = lambda eq k0 (List kvs).
  case kvs of
  | () => Option'None
  | ((k, v), rest) =>
    if let 'T = Bool/eq k0 k
    then Option'Some v
    else assoc_opt eq k0 rest
  esac
in

type <e>Val of [
    'Int:<>Int
  + 'Fun:<e>Val -> <<e>Val, e>Result
] in

type <a,e>Result of ['Ok:a + 'Err:e] in
type <>Failure of [
    'UnboundVariable
  + 'InvalidApplication
  + 'Unimplemented
] in

type <a>Env of <{<>Var * a}>List in

let fetch = lambda (Env env) k.
  case Option/assoc_opt var_eq k env of
  | 'None => Result'Err (Failure'UnboundVariable)
  | 'Some v => Result'Ok v
  esac
in

let add = lambda (Env env) k v.
  Env (List'1 ((k, v), env))
in

type <>Term of [
    'Var:<>Var
  + 'Int:<>Int
  + 'Abs:{<>Var * <>Term}
  + 'App:{<>Term * <>Term}
] in

let aux_apply = lambda (Val f) x.
  case f of
  | 'Int _ => Result'Err (Failure'InvalidApplication)
  | 'Fun f => f x
  esac
in

let eval = lambda t.
  let rec eval = lambda env (Term t).
    case t of
    | 'Var v => fetch env v
    | 'Int i => Result'Ok (Val'Int i)
    | 'Abs (k, body) =>
       Result'Ok (Val'Fun (lambda v.
         eval (add env k v) body
       ))
    | 'App (f, x) =>
      case Result/eval env f of
      | 'Err e => Result'Err e
      | 'Ok f =>
        case Result/eval env x of
        | 'Err e => Result'Err e
        | 'Ok x => aux_apply f x
        esac
      esac
    esac
  in eval (Env (List'0)) t
in eval
