type <>Bool of ['T + 'F] in
type <>Ord of ['Lt + 'Eq + 'Gt] in

let not = lambda (Bool b).
  Bool case b of
  | 'T => 'F
  | 'F => 'T
  esac
in

let and = lambda (Bool b1) b2.
  if let 'T = b1 then b2 else Bool'F
in

let or = lambda (Bool b1) b2.
  if let 'F = b1 then b2 else Bool'T
in

let equal = lambda b1 b2.
  case Bool/b1 of
  | 'T => b2
  | 'F =>
    Bool case Bool/b2 of
    | 'T => 'F
    | 'F => 'T
    esac
  esac
in

let xor = lambda b1 b2.
  case Bool/b1 of
  | 'T =>
    Bool case Bool/b2 of
    | 'T => 'F
    | 'F => 'T
    esac
  | 'F => b2
  esac
in

let compare = lambda (Bool b1) (Bool b2).
  Ord case b1 of
  | 'F =>
    case b2 of
    | 'F => 'Eq
    | 'T => 'Lt
    esac
  | 'T =>
    case b2 of
    | 'F => 'Gt
    | 'T => 'Eq
    esac
  esac
in

let if_ = lambda (Bool b) yes no.
  case b of
  | 'T => yes ()
  | 'F => no ()
  esac
in

(
  not=not,
  and=and,
  or=or,
  xor=xor,
  equal=equal,
  compare=compare,
  if_=if_,
)
