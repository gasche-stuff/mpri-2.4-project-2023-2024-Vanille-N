# TL;DR

To run these tests, execute

```
dune build @library
```

from the root of the project directory. If this outputs
nothing, the testsuite passes. If this outputs a diff, it
means that there is a mismatch between the recorded/reference
output and the behavior of your program.

To *promote* the tests outputs (that is, to modify the reference
output to match the current behavior of your program), run

```
dune build
dune promote
```

If you use the `justfile` that comes with the project, there are the aliases
`just test-lib` and `just bless` for those two commands.


On these terms the generated constraint is huge, and these tests are not suitable
for debugging anyway. We turn off printing the constraint and input type
and just print the type and elaborated term.

  $ alias minihell='minihell --wf'


# Intro

These tests are much more involved than those in `infer.t`, but they are also
less focused on edge cases and as such less suitable for debugging.
These tests should be read as more of a showcase of what the language can express.

I basically went to

- [`Bool` (OCaml)](https://v2.ocaml.org/api/Bool.html)
- [`Option` (Rust)](https://doc.rust-lang.org/std/option/)
- [`Either` (OCaml)](https://v2.ocaml.org/api/Either.html)
- [`Lazy` (OCaml)](https://v2.ocaml.org/api/Lazy.html)
- [`Result` (Rust)](https://doc.rust-lang.org/std/result/)
- [`List` (OCaml)](https://v2.ocaml.org/api/List.html)
- [`Seq` (OCaml)](https://v2.ocaml.org/api/Seq.html)
- [`Map` (OCaml)](https://v2.ocaml.org/api/Map.S.html)

and implemented everything that looked somewhat interesting.

After writing this much code (nearly 2000 lines in total!) in the language I did gain some insights
- not having nested refutable patterns is by far the biggest thing that slows down development;
- explicit fold and unfold operations are not as bad as they seem, the ergonomy is fine;
- not having line numbers on `Clash` errors is a bit of a problem.


## `Bool`

Booleans are naturally represented by a type isomorphic to `[{} + {}]`.

  $ minihell bool.test
  Inferred type:
    {and:<>Bool → <>Bool → <>Bool
    * compare:<>Bool → <>Bool → <>Ord
    * equal:<>Bool → <>Bool → <>Bool
    * if_:<>Bool → ({} → α) → ({} → α) → α
    * not:<>Bool → <>Bool
    * or:<>Bool → <>Bool → <>Bool
    * xor:<>Bool → <>Bool → <>Bool}
  
  Elaborated term:
    type <>Bool of ['F:{} + 'T:{}] in
    type <>Ord of ['Eq:{} + 'Gt:{} + 'Lt:{}] in
    let (not : <>Bool → <>Bool) =
      λ (Bool (b₁ : ['F:{} + 'T:{}])).
          (Bool case b₁ of | 'F () ⇒ 'T () | 'T () ⇒ 'F () esac)
    in
    let (and : <>Bool → <>Bool → <>Bool) =
      λ (Bool (b1₄ : ['F:{} + 'T:{}])).
          λ (b2₄ : <>Bool).
            if let 'T () = b1₄ then b2₄ else (Bool 'F ()) fi
    in
    let (or : <>Bool → <>Bool → <>Bool) =
      λ (Bool (b1₃ : ['F:{} + 'T:{}])).
          λ (b2₃ : <>Bool).
            if let 'F () = b1₃ then b2₃ else (Bool 'T ()) fi
    in
    let (equal : <>Bool → <>Bool → <>Bool) =
      λ (b1₂ : <>Bool).
          λ (b2₂ : <>Bool).
            case (Bool/ b1₂) of
            | 'F () ⇒
              (
                Bool case (Bool/ b2₂) of
                | 'F () ⇒ 'T ()
                | 'T () ⇒ 'F ()
                esac
              )
            | 'T () ⇒ b2₂
            esac
    in
    let (xor : <>Bool → <>Bool → <>Bool) =
      λ (b1₁ : <>Bool).
          λ (b2₁ : <>Bool).
            case (Bool/ b1₁) of
            | 'F () ⇒ b2₁
            | 'T () ⇒
              (
                Bool case (Bool/ b2₁) of
                | 'F () ⇒ 'T ()
                | 'T () ⇒ 'F ()
                esac
              )
            esac
    in
    let (compare : <>Bool → <>Bool → <>Ord) =
      λ (Bool (b1 : ['F:{} + 'T:{}])).
          λ (Bool (b2 : ['F:{} + 'T:{}])).
            (
              Ord case b1 of
              | 'F () ⇒ case b2 of | 'F () ⇒ 'Eq () | 'T () ⇒ 'Lt () esac
              | 'T () ⇒ case b2 of | 'F () ⇒ 'Gt () | 'T () ⇒ 'Eq () esac
              esac
            )
    in
    let (if_ : <>Bool → ({} → α) → ({} → α) → α) =
      λ (Bool (b : ['F:{} + 'T:{}])).
          λ (yes : {} → α).
            λ (no : {} → α).
              case b of | 'F () ⇒ no () | 'T () ⇒ yes () esac
    in
    (
      and=and,
      compare=compare,
      equal=equal,
      if_=if_,
      not=not,
      or=or,
      xor=xor
    )


## `Option`

Optional values with `[{} + a]`.
This library is inspired more by Rust's `Option` than by OCaml's.

  $ minihell option.test
  Inferred type:
    {and:<_>Option → <α>Option → <α>Option
    * and_then:<β>Option → (β → <γ>Option) → <γ>Option
    *
    cmp:(δ → α₁ → <>Ord) → <δ>Option → <α₁>Option → <>Ord
    *
    eq:(β₁ → γ₁ → <>Bool)
      → <β₁>Option → <γ₁>Option → <>Bool
    * filter:<δ₁>Option → (δ₁ → <>Bool) → <δ₁>Option
    * flatten:<<α₂>Option>Option → <α₂>Option
    * inspect:<β₂>Option → (β₂ → {}) → {}
    * is_none:<_₁>Option → <>Bool
    * is_some:<_₂>Option → <>Bool
    * is_some_and:<γ₂>Option → (γ₂ → <>Bool) → <>Bool
    * map:<δ₂>Option → (δ₂ → α₃) → <α₃>Option
    * map_or:<β₃>Option → γ₃ → (β₃ → γ₃) → γ₃
    *
    map_or_else:<δ₃>Option
      → ({} → α₄) → (δ₃ → α₄) → α₄
    * ok_or:<β₄>Option → γ₄ → <β₄, γ₄>Result
    * ok_or_else:<δ₄>Option → ({} → α₅) → <δ₄, α₅>Result
    * or:<_₃>Option → <_₃>Option → <_₃>Option
    * or_else:<_₄>Option → ({} → <_₄>Option) → <_₄>Option
    *
    transpose:<<β₅, γ₅>Result>Option → <<β₅>Option, γ₅>Result
    * unwrap_or:<δ₅>Option → δ₅ → δ₅
    * unwrap_or_else:<α₆>Option → ({} → α₆) → α₆
    * unzip:<{β₆ * γ₆}>Option → {<β₆>Option * <γ₆>Option}
    * xor:<δ₆>Option → <δ₆>Option → <δ₆>Option
    * zip:<α₇>Option → <β₇>Option → <{α₇ * β₇}>Option
    *
    zip_with:<γ₇>Option
      → <δ₇>Option → (γ₇ → δ₇ → α₈) → <α₈>Option}
  
  Elaborated term:
    type <>Bool of ['F:{} + 'T:{}] in
    type <a, e>Result of ['Err:e + 'Ok:a] in
    type <>Ord of ['Eq:{} + 'Gt:{} + 'Lt:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    let (is_some : <_₂>Option → <>Bool) =
      λ (Option (t₂₈ : ['None:{} + 'Some:_₂])).
          (Bool if let 'Some _ = t₂₈ then 'T () else 'F () fi)
    in
    let (is_some_and : <γ₂>Option → (γ₂ → <>Bool) → <>Bool) =
      λ (Option (t₂₇ : ['None:{} + 'Some:γ₂])).
          λ (f₆ : γ₂ → <>Bool).
            if let 'Some (x₁₀ : γ₂) =
              t₂₇ then f₆ x₁₀ else (Bool 'F ()) fi
    in
    let (is_none : <_₁>Option → <>Bool) =
      λ (Option (t₂₆ : ['None:{} + 'Some:_₁])).
          (Bool if let 'None () = t₂₆ then 'T () else 'F () fi)
    in
    let (unwrap_or : <δ₅>Option → δ₅ → δ₅) =
      λ (Option (t₂₅ : ['None:{} + 'Some:δ₅])).
          λ (dflt₃ : δ₅).
            if let 'Some (x₉ : δ₅) = t₂₅ then x₉ else dflt₃ fi
    in
    let (unwrap_or_else : <α₆>Option → ({} → α₆) → α₆) =
      λ (Option (t₂₄ : ['None:{} + 'Some:α₆])).
          λ (dflt₂ : {} → α₆).
            if let 'Some (x₈ : α₆) = t₂₄ then x₈ else dflt₂ () fi
    in
    let (map : <δ₂>Option → (δ₂ → α₃) → <α₃>Option) =
      λ (Option (t₂₃ : ['None:{} + 'Some:δ₂])).
          λ (f₅ : δ₂ → α₃).
            (
              Option if let 'Some (x₇ : δ₂) =
                t₂₃ then 'Some (f₅ x₇) else 'None () fi
            )
    in
    let (inspect : <β₂>Option → (β₂ → {}) → {}) =
      λ (Option (t₂₂ : ['None:{} + 'Some:β₂])).
          λ (f₄ : β₂ → {}).
            if let 'Some (x₆ : β₂) = t₂₂ then f₄ x₆ else () fi
    in
    let (map_or : <β₃>Option → γ₃ → (β₃ → γ₃) → γ₃) =
      λ (Option (t₂₁ : ['None:{} + 'Some:β₃])).
          λ (dflt₁ : γ₃).
            λ (f₃ : β₃ → γ₃).
              if let 'Some (x₅ : β₃) = t₂₁ then f₃ x₅ else dflt₁ fi
    in
    let (
      map_or_else :
        <δ₃>Option → ({} → α₄) → (δ₃ → α₄) → α₄
    ) =
      λ (Option (t₂₀ : ['None:{} + 'Some:δ₃])).
          λ (dflt : {} → α₄).
            λ (f₂ : δ₃ → α₄).
              if let 'Some (x₄ : δ₃) = t₂₀ then f₂ x₄ else dflt () fi
    in
    let (ok_or : <β₄>Option → γ₄ → <β₄, γ₄>Result) =
      λ (Option (t₁₉ : ['None:{} + 'Some:β₄])).
          λ (e₃ : γ₄).
            (
              Result if let 'Some (x₃ : β₄) =
                t₁₉ then 'Ok x₃ else 'Err e₃ fi
            )
    in
    let (
      ok_or_else : <δ₄>Option → ({} → α₅) → <δ₄, α₅>Result
    ) =
      λ (Option (t₁₈ : ['None:{} + 'Some:δ₄])).
          λ (e₂ : {} → α₅).
            (
              Result if let 'Some (x₂ : δ₄) =
                t₁₈ then 'Ok x₂ else 'Err (e₂ ()) fi
            )
    in
    let (and : <_>Option → <α>Option → <α>Option) =
      λ (Option (t₁₇ : ['None:{} + 'Some:_])).
          λ (u₁₂ : <α>Option).
            if let 'None () = t₁₇ then u₁₂ else (Option 'None ()) fi
    in
    let (and_then : <β>Option → (β → <γ>Option) → <γ>Option) =
      λ (Option (t₁₆ : ['None:{} + 'Some:β])).
          λ (f₁ : β → <γ>Option).
            if let 'Some (x₁ : β) =
              t₁₆ then f₁ x₁ else (Option 'None ()) fi
    in
    let (filter : <δ₁>Option → (δ₁ → <>Bool) → <δ₁>Option) =
      λ (Option (t₁₅ : ['None:{} + 'Some:δ₁])).
          λ (p : δ₁ → <>Bool).
            (
              Option if let 'Some (x : δ₁) =
                t₁₅
                  then
                  case (Bool/ p x) of
                  | 'F () ⇒ 'None ()
                  | 'T () ⇒ 'Some x
                  esac
                    else
                    'None () fi
            )
    in
    let (or : <_₃>Option → <_₃>Option → <_₃>Option) =
      λ (t₁₄ : <_₃>Option).
          λ (u₁₁ : <_₃>Option).
            if let 'None () = (Option/ t₁₄) then u₁₁ else t₁₄ fi
    in
    let (or_else : <_₄>Option → ({} → <_₄>Option) → <_₄>Option) =
      λ (t₁₃ : <_₄>Option).
          λ (u₁₀ : {} → <_₄>Option).
            if let 'None () = (Option/ t₁₃) then u₁₀ () else t₁₃ fi
    in
    let (xor : <δ₆>Option → <δ₆>Option → <δ₆>Option) =
      λ (t₁₂ : <δ₆>Option).
          λ (u₉ : <δ₆>Option).
            if let 'Some _ =
              (Option/ t₁₂)
                then
                if let 'Some _ =
                  (Option/ u₉) then (Option 'None ()) else t₁₂ fi
                  else
                  u₉ fi
    in
    let (zip : <α₇>Option → <β₇>Option → <{α₇ * β₇}>Option) =
      λ (Option (t₁₀ : ['None:{} + 'Some:α₇])).
          λ (Option (u₇ : ['None:{} + 'Some:β₇])).
            (
              Option if let 'Some (t₁₁ : α₇) =
                t₁₀
                  then
                  if let 'Some (u₈ : β₇) =
                    u₇ then 'Some (t₁₁, u₈) else 'None () fi
                    else
                    'None () fi
            )
    in
    let (
      zip_with :
        <γ₇>Option
          → <δ₇>Option → (γ₇ → δ₇ → α₈) → <α₈>Option
    ) =
      λ (Option (t₈ : ['None:{} + 'Some:γ₇])).
          λ (Option (u₅ : ['None:{} + 'Some:δ₇])).
            λ (f : γ₇ → δ₇ → α₈).
              (
                Option if let 'Some (t₉ : γ₇) =
                  t₈
                    then
                    if let 'Some (u₆ : δ₇) =
                      u₅ then 'Some (f t₉ u₆) else 'None () fi
                      else
                      'None () fi
              )
    in
    let (
      unzip : <{β₆ * γ₆}>Option → {<β₆>Option * <γ₆>Option}
    ) =
      λ (Option (t₇ : ['None:{} + 'Some:{β₆ * γ₆}])).
          case t₇ of
          | 'None () ⇒ ((Option 'None ()), (Option 'None ()))
          | 'Some ((u₄ : β₆), (v : γ₆)) ⇒
            ((Option 'Some u₄), (Option 'Some v))
          esac
    in
    let (
      transpose :
        <<β₅, γ₅>Result>Option → <<β₅>Option, γ₅>Result
    ) =
      λ (Option (t₆ : ['None:{} + 'Some:<β₅, γ₅>Result])).
          case t₆ of
          | 'None () ⇒ (Result 'Ok (Option 'None ()))
          | 'Some (Result (r : ['Err:γ₅ + 'Ok:β₅])) ⇒
            case r of
            | 'Err (e₁ : γ₅) ⇒ (Result 'Err e₁)
            | 'Ok (a : β₅) ⇒ (Result 'Ok (Option 'Some a))
            esac
          esac
    in
    let (flatten : <<α₂>Option>Option → <α₂>Option) =
      λ (Option (t₄ : ['None:{} + 'Some:<α₂>Option])).
          if let 'Some (t₅ : <α₂>Option) =
            t₄ then t₅ else (Option 'None ()) fi
    in
    let (
      eq :
        (β₁ → γ₁ → <>Bool)
          → <β₁>Option → <γ₁>Option → <>Bool
    ) =
      λ (e : β₁ → γ₁ → <>Bool).
          λ (Option (t₂ : ['None:{} + 'Some:β₁])).
            λ (Option (u₂ : ['None:{} + 'Some:γ₁])).
              case t₂ of
              | 'None () ⇒
                case u₂ of
                | 'None () ⇒ (Bool 'T ())
                | … ⇒ (Bool 'F ())
                esac
              | 'Some (t₃ : β₁) ⇒
                case u₂ of
                | 'Some (u₃ : γ₁) ⇒ e t₃ u₃
                | … ⇒ (Bool 'F ())
                esac
              esac
    in
    let (
      cmp :
        (δ → α₁ → <>Ord) → <δ>Option → <α₁>Option → <>Ord
    ) =
      λ (c : δ → α₁ → <>Ord).
          λ (Option (t : ['None:{} + 'Some:δ])).
            λ (Option (u : ['None:{} + 'Some:α₁])).
              case t of
              | 'None () ⇒
                case u of
                | 'None () ⇒ (Ord 'Eq ())
                | 'Some _ ⇒ (Ord 'Lt ())
                esac
              | 'Some (t₁ : δ) ⇒
                case u of
                | 'None () ⇒ (Ord 'Gt ())
                | 'Some (u₁ : α₁) ⇒ c t₁ u₁
                esac
              esac
    in
    (
      and=and,
      and_then=and_then,
      cmp=cmp,
      eq=eq,
      filter=filter,
      flatten=flatten,
      inspect=inspect,
      is_none=is_none,
      is_some=is_some,
      is_some_and=is_some_and,
      map=map,
      map_or=map_or,
      map_or_else=map_or_else,
      ok_or=ok_or,
      ok_or_else=ok_or_else,
      or=or,
      or_else=or_else,
      transpose=transpose,
      unwrap_or=unwrap_or,
      unwrap_or_else=unwrap_or_else,
      unzip=unzip,
      xor=xor,
      zip=zip,
      zip_with=zip_with
    )


## `Either`

Binary choice with `[a + b]`.

  $ minihell either.test
  Inferred type:
    {compare:(α → β → <>Ord)
      → (γ → δ → <>Ord)
        → <α, γ>Either → <β, δ>Either → <>Ord
    *
    equal:(α₁ → β₁ → <>Bool)
      → (γ₁ → δ₁ → <>Bool)
        → <α₁, γ₁>Either → <β₁, δ₁>Either → <>Bool
    * find_left:<α₂, _>Either → <α₂>Option
    * find_right:<_₁, β₂>Either → <β₂>Option
    *
    fold:(γ₂ → δ₂)
      → (α₃ → δ₂) → <γ₂, α₃>Either → δ₂
    * is_left:<_₂, _₃>Either → <>Bool
    * is_right:<_₄, _₅>Either → <>Bool
    * left:β₃ → <β₃, γ₃>Either
    *
    map:(δ₃ → α₄)
      → (β₄ → γ₄) → <δ₃, β₄>Either → <α₄, γ₄>Either
    *
    map_left:(δ₄ → α₅)
      → <δ₄, β₅>Either → <α₅, β₅>Either
    *
    map_right:(γ₅ → δ₅)
      → <α₆, γ₅>Either → <α₆, δ₅>Either
    * right:β₆ → <γ₆, β₆>Either
    * swap:<δ₆, α₇>Either → <α₇, δ₆>Either}
  
  Elaborated term:
    type <>Bool of ['F:{} + 'T:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    type <>Ord of ['Eq:{} + 'Gt:{} + 'Lt:{}] in
    type <a, b>Either of [a + b] in
    let (left : β₃ → <β₃, γ₃>Either) =
      λ (x₁₈ : β₃). (Either '0 x₁₈)
    in
    let (right : β₆ → <γ₆, β₆>Either) =
      λ (x₁₇ : β₆). (Either '1 x₁₇)
    in
    let (is_left : <_₂, _₃>Either → <>Bool) =
      λ (Either (x₁₆ : [_₂ + _₃])).
          (Bool if let '0 _ = x₁₆ then 'T () else 'F () fi)
    in
    let (is_right : <_₄, _₅>Either → <>Bool) =
      λ (Either (x₁₅ : [_₄ + _₅])).
          (Bool if let '1 _ = x₁₅ then 'T () else 'F () fi)
    in
    let (find_left : <α₂, _>Either → <α₂>Option) =
      λ (Either (x₁₃ : [α₂ + _])).
          (
            Option if let '0 (x₁₄ : α₂) =
              x₁₃ then 'Some x₁₄ else 'None () fi
          )
    in
    let (find_right : <_₁, β₂>Either → <β₂>Option) =
      λ (Either (x₁₁ : [_₁ + β₂])).
          (
            Option if let '1 (x₁₂ : β₂) =
              x₁₁ then 'Some x₁₂ else 'None () fi
          )
    in
    let (
      map_left :
        (δ₄ → α₅) → <δ₄, β₅>Either → <α₅, β₅>Either
    ) =
      λ (f₃ : δ₄ → α₅).
          λ (Either (x₁₀ : [δ₄ + β₅])).
            (
              Either case x₁₀ of
              | (a₄ : δ₄) ⇒ '0 (f₃ a₄)
              | (b₄ : β₅) ⇒ '1 b₄
              esac
            )
    in
    let (
      map_right :
        (γ₅ → δ₅) → <α₆, γ₅>Either → <α₆, δ₅>Either
    ) =
      λ (f₂ : γ₅ → δ₅).
          λ (Either (x₉ : [α₆ + γ₅])).
            (
              Either case x₉ of
              | (a₃ : α₆) ⇒ '0 a₃
              | (b₃ : γ₅) ⇒ '1 (f₂ b₃)
              esac
            )
    in
    let (
      map :
        (δ₃ → α₄)
          → (β₄ → γ₄) → <δ₃, β₄>Either → <α₄, γ₄>Either
    ) =
      λ (f₁ : δ₃ → α₄).
          λ (g₁ : β₄ → γ₄).
            λ (Either (x₈ : [δ₃ + β₄])).
              (
                Either case x₈ of
                | (a₂ : δ₃) ⇒ '0 (f₁ a₂)
                | (b₂ : β₄) ⇒ '1 (g₁ b₂)
                esac
              )
    in
    let (
      fold :
        (γ₂ → δ₂)
          → (α₃ → δ₂) → <γ₂, α₃>Either → δ₂
    ) =
      λ (f : γ₂ → δ₂).
          λ (g : α₃ → δ₂).
            λ (Either (x₇ : [γ₂ + α₃])).
              case x₇ of
              | (a₁ : γ₂) ⇒ f a₁
              | (b₁ : α₃) ⇒ g b₁
              esac
    in
    let (swap : <δ₆, α₇>Either → <α₇, δ₆>Either) =
      λ (Either (x₆ : [δ₆ + α₇])).
          (Either case x₆ of | (a : δ₆) ⇒ '1 a | (b : α₇) ⇒ '0 b esac)
    in
    let (
      compare :
        (α → β → <>Ord)
          → (γ → δ → <>Ord)
            → <α, γ>Either → <β, δ>Either → <>Ord
    ) =
      λ (lcmp : α → β → <>Ord).
          λ (rcmp : γ → δ → <>Ord).
            λ (Either (x₃ : [α + γ])).
              λ (Either (y₃ : [β + δ])).
                case x₃ of
                | (x₄ : α) ⇒
                  case y₃ of
                  | (y₄ : β) ⇒ lcmp x₄ y₄
                  | _ ⇒ (Ord 'Lt ())
                  esac
                | (x₅ : γ) ⇒
                  case y₃ of
                  | _ ⇒ (Ord 'Gt ())
                  | (y₅ : δ) ⇒ rcmp x₅ y₅
                  esac
                esac
    in
    let (
      equal :
        (α₁ → β₁ → <>Bool)
          → (γ₁ → δ₁ → <>Bool)
            → <α₁, γ₁>Either → <β₁, δ₁>Either → <>Bool
    ) =
      λ (leq : α₁ → β₁ → <>Bool).
          λ (req : γ₁ → δ₁ → <>Bool).
            λ (Either (x : [α₁ + γ₁])).
              λ (Either (y : [β₁ + δ₁])).
                case x of
                | (x₁ : α₁) ⇒
                  case y of
                  | (y₁ : β₁) ⇒ leq x₁ y₁
                  | _ ⇒ (Bool 'F ())
                  esac
                | (x₂ : γ₁) ⇒
                  case y of
                  | _ ⇒ (Bool 'F ())
                  | (y₂ : δ₁) ⇒ req x₂ y₂
                  esac
                esac
    in
    (
      compare=compare,
      equal=equal,
      find_left=find_left,
      find_right=find_right,
      fold=fold,
      is_left=is_left,
      is_right=is_right,
      left=left,
      map=map,
      map_left=map_left,
      map_right=map_right,
      right=right,
      swap=swap
    )


## `Lazy`

An implementation of the `Lazy` module of OCaml.

This was implementable as soon as I had binary sums and nary tuples,
and was later improved with type definitions. No induction is needed here.
Lazyness is represented by the term `[a + {} -> a]`.

  $ minihell lazy.test
  Inferred type:
    {force:<α>Lazy → α
    * is_val:<_>Lazy → <>Bool
    * later:({} → β) → <β>Lazy
    * make_val:<γ>Lazy → <γ>Lazy
    * map:(δ → α₁) → <δ>Lazy → <α₁>Lazy
    * map_val:(β₁ → γ₁) → <β₁>Lazy → <γ₁>Lazy
    * return:δ₁ → <δ₁>Lazy}
  
  Elaborated term:
    type <a>Lazy of [a + {} → a] in
    type <>Bool of ['F:{} + 'T:{}] in
    let (return : δ₁ → <δ₁>Lazy) = λ (x : δ₁). (Lazy '0 x)
    in
    let (later : ({} → β) → <β>Lazy) =
      λ (f₄ : {} → β). (Lazy '1 f₄)
    in
    let (map : (δ → α₁) → <δ>Lazy → <α₁>Lazy) =
      λ (f₃ : δ → α₁).
          λ (Lazy (l₄ : [δ + {} → δ])).
            (
              Lazy '1 (λ ().
                case l₄ of
                | (a₄ : δ) ⇒ f₃ a₄
                | (a₅ : {} → δ) ⇒ f₃ (a₅ ())
                esac)
            )
    in
    let (map_val : (β₁ → γ₁) → <β₁>Lazy → <γ₁>Lazy) =
      λ (f₂ : β₁ → γ₁).
          λ (Lazy (l₃ : [β₁ + {} → β₁])).
            (
              Lazy case l₃ of
              | (a₂ : β₁) ⇒ '0 (f₂ a₂)
              | (a₃ : {} → β₁) ⇒ '1 (λ (). f₂ (a₃ ()))
              esac
            )
    in
    let (is_val : <_>Lazy → <>Bool) =
      λ (Lazy (l₂ : [_ + {} → _])).
          (Bool case l₂ of | _ ⇒ 'T () | _ ⇒ 'F () esac)
    in
    let (force : <α>Lazy → α) =
      λ (Lazy (l₁ : [α + {} → α])).
          case l₁ of
          | (a₁ : α) ⇒ a₁
          | (f₁ : {} → α) ⇒ f₁ ()
          esac
    in
    let (make_val : <γ>Lazy → <γ>Lazy) =
      λ (Lazy (l : [γ + {} → γ])).
          (
            Lazy case l of
            | (a : γ) ⇒ '1 (λ (). a)
            | (f : {} → γ) ⇒ '1 f
            esac
          )
    in
    (
      force=force,
      is_val=is_val,
      later=later,
      make_val=make_val,
      map=map,
      map_val=map_val,
      return=return
    )

## `Result`

When we start to interpret `Either` as representing specifically
a type for faillible operations, some more interesting operations appear.
The following is inspired more by Rust's `Result` than by OCaml's.

  $ minihell result.test
  Inferred type:
    {and:<_, α>Result → <β, α>Result → <β, α>Result
    *
    and_then:<γ, δ>Result
      → (γ → <α₁, δ>Result) → <α₁, δ>Result
    * err:β₁ → <γ₁, β₁>Result
    * find:<δ₁, _₁>Result → <δ₁>Option
    * find_err:<_₂, α₂>Result → <α₂>Option
    * flatten:<<β₂, γ₂>Result, γ₂>Result → <β₂, γ₂>Result
    * into_err:<[], δ₂>Result → δ₂
    * into_ok:<α₃, []>Result → α₃
    * is_err:<_₃, _₄>Result → <>Bool
    * is_err_and:<_₅, β₃>Result → (β₃ → <>Bool) → <>Bool
    * is_ok:<_₆, _₇>Result → <>Bool
    * is_ok_and:<γ₃, _₈>Result → (γ₃ → <>Bool) → <>Bool
    *
    map:<δ₃, α₄>Result → (δ₃ → β₄) → <β₄, α₄>Result
    *
    map_err:<γ₄, δ₄>Result
      → (δ₄ → α₅) → <γ₄, α₅>Result
    * map_or:<β₅, _₉>Result → (β₅ → γ₅) → γ₅ → γ₅
    *
    map_or_else:<δ₅, α₆>Result
      → (δ₅ → β₆) → (α₆ → β₆) → β₆
    * ok:γ₆ → <γ₆, δ₆>Result
    *
    or:<α₇, _₁₀>Result
      → <α₇, β₇>Result → <α₇, β₇>Result
    *
    or_else:<γ₇, δ₇>Result
      → (δ₇ → <γ₇, α₈>Result) → <γ₇, α₈>Result
    *
    transpose:<<β₈>Option, γ₈>Result → <<β₈, γ₈>Result>Option
    * unwrap:<δ₈, _₁₁>Result → δ₈ → δ₈
    * unwrap_err:<_₁₂, α₉>Result → α₉ → α₉
    * unwrap_or_else:<β₉, γ₉>Result → (γ₉ → β₉) → β₉}
  
  Elaborated term:
    type <a, b>Result of ['Err:b + 'Ok:a] in
    type <>Bool of ['F:{} + 'T:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    let (ok : γ₆ → <γ₆, δ₆>Result) =
      λ (a₁₆ : γ₆). (Result 'Ok a₁₆)
    in
    let (err : β₁ → <γ₁, β₁>Result) =
      λ (b : β₁). (Result 'Err b)
    in
    let (is_ok : <_₆, _₇>Result → <>Bool) =
      λ (Result (r₁₆ : ['Err:_₇ + 'Ok:_₆])).
          case r₁₆ of | 'Err _ ⇒ (Bool 'F ()) | 'Ok _ ⇒ (Bool 'T ()) esac
    in
    let (is_err : <_₃, _₄>Result → <>Bool) =
      λ (Result (r₁₅ : ['Err:_₄ + 'Ok:_₃])).
          case r₁₅ of | 'Err _ ⇒ (Bool 'T ()) | 'Ok _ ⇒ (Bool 'F ()) esac
    in
    let (is_ok_and : <γ₃, _₈>Result → (γ₃ → <>Bool) → <>Bool) =
      λ (Result (r₁₄ : ['Err:_₈ + 'Ok:γ₃])).
          λ (f₈ : γ₃ → <>Bool).
            case r₁₄ of
            | 'Ok (a₁₅ : γ₃) ⇒ f₈ a₁₅
            | … ⇒ (Bool 'F ())
            esac
    in
    let (is_err_and : <_₅, β₃>Result → (β₃ → <>Bool) → <>Bool) =
      λ (Result (r₁₃ : ['Err:β₃ + 'Ok:_₅])).
          λ (f₇ : β₃ → <>Bool).
            case r₁₃ of
            | 'Err (e₁₃ : β₃) ⇒ f₇ e₁₃
            | … ⇒ (Bool 'F ())
            esac
    in
    let (find : <δ₁, _₁>Result → <δ₁>Option) =
      λ (Result (r₁₂ : ['Err:_₁ + 'Ok:δ₁])).
          (
            Option case r₁₂ of
            | 'Ok (a₁₄ : δ₁) ⇒ 'Some a₁₄
            | … ⇒ 'None ()
            esac
          )
    in
    let (find_err : <_₂, α₂>Result → <α₂>Option) =
      λ (Result (r₁₁ : ['Err:α₂ + 'Ok:_₂])).
          (
            Option case r₁₁ of
            | 'Err (e₁₂ : α₂) ⇒ 'Some e₁₂
            | … ⇒ 'None ()
            esac
          )
    in
    let (
      map :
        <δ₃, α₄>Result → (δ₃ → β₄) → <β₄, α₄>Result
    ) =
      λ (Result (r₁₀ : ['Err:α₄ + 'Ok:δ₃])).
          λ (f₆ : δ₃ → β₄).
            (
              Result case r₁₀ of
              | 'Err (e₁₁ : α₄) ⇒ 'Err e₁₁
              | 'Ok (a₁₃ : δ₃) ⇒ 'Ok (f₆ a₁₃)
              esac
            )
    in
    let (
      map_err :
        <γ₄, δ₄>Result → (δ₄ → α₅) → <γ₄, α₅>Result
    ) =
      λ (Result (r₉ : ['Err:δ₄ + 'Ok:γ₄])).
          λ (f₅ : δ₄ → α₅).
            (
              Result case r₉ of
              | 'Err (e₁₀ : δ₄) ⇒ 'Err (f₅ e₁₀)
              | 'Ok (a₁₂ : γ₄) ⇒ 'Ok a₁₂
              esac
            )
    in
    let (
      map_or : <β₅, _₉>Result → (β₅ → γ₅) → γ₅ → γ₅
    ) =
      λ (Result (r₈ : ['Err:_₉ + 'Ok:β₅])).
          λ (f₄ : β₅ → γ₅).
            λ (dflt₃ : γ₅).
              case r₈ of
              | 'Ok (a₁₁ : β₅) ⇒ f₄ a₁₁
              | … ⇒ dflt₃
              esac
    in
    let (
      map_or_else :
        <δ₅, α₆>Result
          → (δ₅ → β₆) → (α₆ → β₆) → β₆
    ) =
      λ (Result (r₇ : ['Err:α₆ + 'Ok:δ₅])).
          λ (f₃ : δ₅ → β₆).
            λ (dflt₂ : α₆ → β₆).
              case r₇ of
              | 'Err (e₉ : α₆) ⇒ dflt₂ e₉
              | 'Ok (a₁₀ : δ₅) ⇒ f₃ a₁₀
              esac
    in
    let (unwrap : <δ₈, _₁₁>Result → δ₈ → δ₈) =
      λ (Result (r₆ : ['Err:_₁₁ + 'Ok:δ₈])).
          λ (dflt₁ : δ₈).
            case r₆ of | 'Ok (a₉ : δ₈) ⇒ a₉ | … ⇒ dflt₁ esac
    in
    let (
      unwrap_or_else : <β₉, γ₉>Result → (γ₉ → β₉) → β₉
    ) =
      λ (Result (r₅ : ['Err:γ₉ + 'Ok:β₉])).
          λ (f₂ : γ₉ → β₉).
            case r₅ of
            | 'Err (e₈ : γ₉) ⇒ f₂ e₈
            | 'Ok (a₈ : β₉) ⇒ a₈
            esac
    in
    let (unwrap_err : <_₁₂, α₉>Result → α₉ → α₉) =
      λ (Result (r₄ : ['Err:α₉ + 'Ok:_₁₂])).
          λ (dflt : α₉).
            case r₄ of | 'Err (e₇ : α₉) ⇒ e₇ | … ⇒ dflt esac
    in
    let (into_ok : <α₃, []>Result → α₃) =
      λ (Result (r₃ : ['Err:[] + 'Ok:α₃])).
          case r₃ of
          | 'Err (e₆ : []) ⇒ case e₆ of  esac
          | 'Ok (a₇ : α₃) ⇒ a₇
          esac
    in
    let (into_err : <[], δ₂>Result → δ₂) =
      λ (Result (r₂ : ['Err:δ₂ + 'Ok:[]])).
          case r₂ of
          | 'Err (e₅ : δ₂) ⇒ e₅
          | 'Ok (a₆ : []) ⇒ case a₆ of  esac
          esac
    in
    let (and : <_, α>Result → <β, α>Result → <β, α>Result) =
      λ (Result (r1₃ : ['Err:α + 'Ok:_])).
          λ (r2₁ : <β, α>Result).
            case r1₃ of
            | 'Err (e₄ : α) ⇒ (Result 'Err e₄)
            | 'Ok _ ⇒ r2₁
            esac
    in
    let (
      and_then :
        <γ, δ>Result → (γ → <α₁, δ>Result) → <α₁, δ>Result
    ) =
      λ (Result (r1₂ : ['Err:δ + 'Ok:γ])).
          λ (f₁ : γ → <α₁, δ>Result).
            case r1₂ of
            | 'Err (e₃ : δ) ⇒ (Result 'Err e₃)
            | 'Ok (a₅ : γ) ⇒ f₁ a₅
            esac
    in
    let (
      or :
        <α₇, _₁₀>Result → <α₇, β₇>Result → <α₇, β₇>Result
    ) =
      λ (Result (r1₁ : ['Err:_₁₀ + 'Ok:α₇])).
          λ (r2 : <α₇, β₇>Result).
            case r1₁ of
            | 'Err _ ⇒ r2
            | 'Ok (a₄ : α₇) ⇒ (Result 'Ok a₄)
            esac
    in
    let (
      or_else :
        <γ₇, δ₇>Result
          → (δ₇ → <γ₇, α₈>Result) → <γ₇, α₈>Result
    ) =
      λ (Result (r1 : ['Err:δ₇ + 'Ok:γ₇])).
          λ (f : δ₇ → <γ₇, α₈>Result).
            case r1 of
            | 'Err (e₂ : δ₇) ⇒ f e₂
            | 'Ok (a₃ : γ₇) ⇒ (Result 'Ok a₃)
            esac
    in
    let (
      transpose :
        <<β₈>Option, γ₈>Result → <<β₈, γ₈>Result>Option
    ) =
      λ (Result (r₁ : ['Err:γ₈ + 'Ok:<β₈>Option])).
          (
            Option case r₁ of
            | 'Err (e₁ : γ₈) ⇒ 'Some (Result 'Err e₁)
            | 'Ok (a₁ : <β₈>Option) ⇒
              case (Option/ a₁) of
              | 'None () ⇒ 'None ()
              | 'Some (a₂ : β₈) ⇒ 'Some (Result 'Ok a₂)
              esac
            esac
          )
    in
    let (
      flatten : <<β₂, γ₂>Result, γ₂>Result → <β₂, γ₂>Result
    ) =
      λ (Result (r : ['Err:γ₂ + 'Ok:<β₂, γ₂>Result])).
          (
            Result case r of
            | 'Err (e : γ₂) ⇒ 'Err e
            | 'Ok (a : <β₂, γ₂>Result) ⇒ (Result/ a)
            esac
          )
    in
    (
      and=and,
      and_then=and_then,
      err=err,
      find=find,
      find_err=find_err,
      flatten=flatten,
      into_err=into_err,
      into_ok=into_ok,
      is_err=is_err,
      is_err_and=is_err_and,
      is_ok=is_ok,
      is_ok_and=is_ok_and,
      map=map,
      map_err=map_err,
      map_or=map_or,
      map_or_else=map_or_else,
      ok=ok,
      or=or,
      or_else=or_else,
      transpose=transpose,
      unwrap=unwrap,
      unwrap_err=unwrap_err,
      unwrap_or_else=unwrap_or_else
    )


## List

Finite lists are the first inductive type shown here.
Defined by `<a>List = [{} + <a>List]`, we can manipulate them thanks to
the addition of `lec rec` to the language.

  $ minihell list.test
  Inferred type:
    {append:<α>List → <α>List → <α>List
    * concat:<<α>List>List → <α>List
    * cons:β → <β>List → <β>List
    * exists:(γ → <>Bool) → <γ>List → <>Bool
    * filter_map:(δ → <α₁>Option) → <δ>List → <α₁>List
    *
    fold_left:(β₁ → γ₁ → β₁)
      → β₁ → <γ₁>List → β₁
    *
    fold_left_map:(δ₁ → α₂ → {δ₁ * β₂})
      → δ₁ → <α₂>List → {δ₁ * <β₂>List}
    * for_all:(γ → <>Bool) → <γ>List → <>Bool
    * hd:<γ₂>List → <γ₂>Option
    * is_empty:<_>List → <>Bool
    * iter:(δ₂ → {}) → <δ₂>List → {}
    * iteri:(<>Int → α₃ → {}) → <α₃>List → {}
    * length:<_₁>List → <>Int
    * map:(β₃ → γ₃) → <β₃>List → <γ₃>List
    * mapi:(<>Int → δ₃ → α₄) → <δ₃>List → <α₄>List
    * nth:<>Int → <β₄>List → <β₄>Option
    *
    partition_map:(γ₄ → <δ₄, α₅>Either)
      → <γ₄>List → {<δ₄>List * <α₅>List}
    * rev:<β₅>List → <β₅>List
    * tl:<_₂>List → <<_₂>List>Option
    * zip:<γ₅>List → <δ₅>List → <<γ₅, δ₅>Zip>List}
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <>Int of ['S:<>Int + 'Z:{}] in
    type <>Bool of ['F:{} + 'T:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    type <a, b>Zip of ['Both:{a * b} + 'Left:a + 'Right:b] in
    type <a, b>Either of ['Left:a + 'Right:b] in
    let rec (length : <_₁>List → <>Int) =
      λ (List (l₁₉ : ['Cons:{_₁ * <_₁>List} + 'Nil:{}])).
          (
            Int case l₁₉ of
            | 'Cons (_, (tail₁₆ : <_₁>List)) ⇒ 'S (length tail₁₆)
            | 'Nil () ⇒ 'Z ()
            esac
          )
    in
    let (is_empty : <_>List → <>Bool) =
      λ (List (l₁₈ : ['Cons:{_ * <_>List} + 'Nil:{}])).
          (Bool case l₁₈ of | 'Cons _ ⇒ 'F () | 'Nil () ⇒ 'T () esac)
    in
    let (cons : β → <β>List → <β>List) =
      λ (a₁₅ : β).
          λ (tail₁₅ : <β>List). (List 'Cons (a₁₅, tail₁₅))
    in
    let (hd : <γ₂>List → <γ₂>Option) =
      λ (List (l₁₇ : ['Cons:{γ₂ * <γ₂>List} + 'Nil:{}])).
          (
            Option case l₁₇ of
            | 'Cons ((a₁₄ : γ₂), _) ⇒ 'Some a₁₄
            | 'Nil () ⇒ 'None ()
            esac
          )
    in
    let (tl : <_₂>List → <<_₂>List>Option) =
      λ (List (l₁₆ : ['Cons:{_₂ * <_₂>List} + 'Nil:{}])).
          (
            Option case l₁₆ of
            | 'Cons (_, (tl₁ : <_₂>List)) ⇒ 'Some tl₁
            | 'Nil () ⇒ 'None ()
            esac
          )
    in
    let (rev : <β₅>List → <β₅>List) =
      λ (l₁₄ : <β₅>List).
          let rec (aux₃ : <β₅>List → <β₅>List → <β₅>List) =
            λ (acc₄ : <β₅>List).
                λ (List (l₁₅ : ['Cons:{β₅ * <β₅>List} + 'Nil:{}])).
                  case l₁₅ of
                  | 'Cons ((a₁₃ : β₅), (tail₁₄ : <β₅>List)) ⇒
                    aux₃ (List 'Cons (a₁₃, acc₄)) tail₁₄
                  | 'Nil () ⇒ acc₄
                  esac
          in aux₃ (List 'Nil ()) l₁₄
    in
    let rec (append : <α>List → <α>List → <α>List) =
      λ (List (fst : ['Cons:{α * <α>List} + 'Nil:{}])).
          λ (snd : <α>List).
            case fst of
            | 'Cons ((a₁₂ : α), (tail₁₃ : <α>List)) ⇒
              (List 'Cons (a₁₂, append tail₁₃ snd))
            | 'Nil () ⇒ snd
            esac
    in
    let rec (concat : <<α>List>List → <α>List) =
      λ (List (ls : ['Cons:{<α>List * <<α>List>List} + 'Nil:{}])).
          case ls of
          | 'Cons ((a₁₁ : <α>List), (tail₁₂ : <<α>List>List)) ⇒
            append a₁₁ (concat tail₁₂)
          | 'Nil () ⇒ (List 'Nil ())
          esac
    in
    let rec (iter : (δ₂ → {}) → <δ₂>List → {}) =
      λ (f₆ : δ₂ → {}).
          λ (List (l₁₃ : ['Cons:{δ₂ * <δ₂>List} + 'Nil:{}])).
            case l₁₃ of
            | 'Cons ((a₁₀ : δ₂), (tail₁₁ : <δ₂>List)) ⇒
              let () = f₆ a₁₀ in iter f₆ tail₁₁
            | 'Nil () ⇒ ()
            esac
    in
    let rec (iteri : (<>Int → α₃ → {}) → <α₃>List → {}) =
      λ (f₅ : <>Int → α₃ → {}).
          λ (l₁₁ : <α₃>List).
            let rec (aux₂ : <>Int → <α₃>List → {}) =
              λ (count₁ : <>Int).
                  λ (List (l₁₂ : ['Cons:{α₃ * <α₃>List} + 'Nil:{}])).
                    case l₁₂ of
                    | 'Cons ((a₉ : α₃), (tail₁₀ : <α₃>List)) ⇒
                      let () = f₅ count₁ a₉
                      in aux₂ (Int 'S count₁) tail₁₀
                    | 'Nil () ⇒ ()
                    esac
            in aux₂ (Int 'Z ()) l₁₁
    in
    let rec (map : (β₃ → γ₃) → <β₃>List → <γ₃>List) =
      λ (f₄ : β₃ → γ₃).
          λ (l₉ : <β₃>List).
            let rec (aux₁ : <β₃>List → <γ₃>List) =
              λ (List (l₁₀ : ['Cons:{β₃ * <β₃>List} + 'Nil:{}])).
                  (
                    List case l₁₀ of
                    | 'Cons ((a₈ : β₃), (tail₉ : <β₃>List)) ⇒
                      'Cons (f₄ a₈, aux₁ tail₉)
                    | 'Nil () ⇒ 'Nil ()
                    esac
                  )
            in aux₁ l₉
    in
    let rec (
      mapi : (<>Int → δ₃ → α₄) → <δ₃>List → <α₄>List
    ) =
      λ (f₃ : <>Int → δ₃ → α₄).
          λ (l₇ : <δ₃>List).
            let rec (aux : <>Int → <δ₃>List → <α₄>List) =
              λ (count : <>Int).
                  λ (List (l₈ : ['Cons:{δ₃ * <δ₃>List} + 'Nil:{}])).
                    (
                      List case l₈ of
                      | 'Cons ((a₇ : δ₃), (tail₈ : <δ₃>List)) ⇒
                        'Cons (f₃ count a₇, aux (Int 'S count) tail₈)
                      | 'Nil () ⇒ 'Nil ()
                      esac
                    )
            in aux (Int 'Z ()) l₇
    in
    let rec (
      filter_map : (δ → <α₁>Option) → <δ>List → <α₁>List
    ) =
      λ (f₂ : δ → <α₁>Option).
          λ (List (l₆ : ['Cons:{δ * <δ>List} + 'Nil:{}])).
            case l₆ of
            | 'Cons ((a₆ : δ), (tail₇ : <δ>List)) ⇒
              let (Option (res₁ : ['None:{} + 'Some:α₁])) = f₂ a₆
              in
              let (rest : <α₁>List) = filter_map f₂ tail₇
              in
              case res₁ of
              | 'None () ⇒ rest
              | 'Some (b₄ : α₁) ⇒ (List 'Cons (b₄, rest))
              esac
            | 'Nil () ⇒ (List 'Nil ())
            esac
    in
    let rec (
      fold_left :
        (β₁ → γ₁ → β₁) → β₁ → <γ₁>List → β₁
    ) =
      λ (f₁ : β₁ → γ₁ → β₁).
          λ (acc₃ : β₁).
            λ (List (l₅ : ['Cons:{γ₁ * <γ₁>List} + 'Nil:{}])).
              case l₅ of
              | 'Cons ((a₅ : γ₁), (tail₆ : <γ₁>List)) ⇒
                fold_left f₁ (f₁ acc₃ a₅) tail₆
              | 'Nil () ⇒ acc₃
              esac
    in
    let rec (
      fold_left_map :
        (δ₁ → α₂ → {δ₁ * β₂})
          → δ₁ → <α₂>List → {δ₁ * <β₂>List}
    ) =
      λ (f : δ₁ → α₂ → {δ₁ * β₂}).
          λ (acc : δ₁).
            λ (List (l₄ : ['Cons:{α₂ * <α₂>List} + 'Nil:{}])).
              case l₄ of
              | 'Cons ((a₄ : α₂), (tail₄ : <α₂>List)) ⇒
                let ((acc₁ : δ₁), (b₃ : β₂)) = f acc a₄
                in
                let ((acc₂ : δ₁), (tail₅ : <β₂>List)) =
                  fold_left_map f acc₁ tail₄
                in (acc₂, (List 'Cons (b₃, tail₅)))
              | 'Nil () ⇒ (acc, (List 'Nil ()))
              esac
    in
    let rec (zip : <γ₅>List → <δ₅>List → <<γ₅, δ₅>Zip>List) =
      λ (List (l1 : ['Cons:{γ₅ * <γ₅>List} + 'Nil:{}])).
          λ (List (l2 : ['Cons:{δ₅ * <δ₅>List} + 'Nil:{}])).
            case l1 of
            | 'Cons ((a1 : γ₅), (tail1 : <γ₅>List)) ⇒
              case l2 of
              | 'Cons ((a2₁ : δ₅), (tail2₁ : <δ₅>List)) ⇒
                (List 'Cons ((Zip 'Both (a1, a2₁)), zip tail1 tail2₁))
              | 'Nil () ⇒ (List 'Cons ((Zip 'Left a1), zip tail1 (List l2)))
              esac
            | 'Nil () ⇒
              case l2 of
              | 'Cons ((a2 : δ₅), (tail2 : <δ₅>List)) ⇒
                (List 'Cons ((Zip 'Right a2), zip (List l1) tail2))
              | 'Nil () ⇒ (List 'Nil ())
              esac
            esac
    in
    let rec (for_all : (γ → <>Bool) → <γ>List → <>Bool) =
      λ (pred₁ : γ → <>Bool).
          λ (List (l₃ : ['Cons:{γ * <γ>List} + 'Nil:{}])).
            case l₃ of
            | 'Cons ((a₃ : γ), (tail₃ : <γ>List)) ⇒
              let (Bool (b₂ : ['F:{} + 'T:{}])) = pred₁ a₃
              in
              if let 'F () =
                b₂ then (Bool 'F ()) else for_all pred₁ tail₃ fi
            | 'Nil () ⇒ (Bool 'T ())
            esac
    in
    let rec (exists : (γ → <>Bool) → <γ>List → <>Bool) =
      λ (pred : γ → <>Bool).
          λ (List (l₂ : ['Cons:{γ * <γ>List} + 'Nil:{}])).
            case l₂ of
            | 'Cons ((a₂ : γ), (tail₂ : <γ>List)) ⇒
              let (Bool (b₁ : ['F:{} + 'T:{}])) = pred a₂
              in
              if let 'T () = b₁ then (Bool 'T ()) else for_all pred tail₂ fi
            | 'Nil () ⇒ (Bool 'F ())
            esac
    in
    let rec (nth : <>Int → <β₄>List → <β₄>Option) =
      λ (Int (n : ['S:<>Int + 'Z:{}])).
          λ (List (l₁ : ['Cons:{β₄ * <β₄>List} + 'Nil:{}])).
            case l₁ of
            | 'Cons ((a₁ : β₄), (tail₁ : <β₄>List)) ⇒
              case n of
              | 'S (n₁ : <>Int) ⇒ nth n₁ tail₁
              | 'Z () ⇒ (Option 'Some a₁)
              esac
            | 'Nil () ⇒ (Option 'None ())
            esac
    in
    let rec (
      partition_map :
        (γ₄ → <δ₄, α₅>Either)
          → <γ₄>List → {<δ₄>List * <α₅>List}
    ) =
      λ (fn : γ₄ → <δ₄, α₅>Either).
          λ (List (l : ['Cons:{γ₄ * <γ₄>List} + 'Nil:{}])).
            case l of
            | 'Cons ((a : γ₄), (tail : <γ₄>List)) ⇒
              let (Either (res : ['Left:δ₄ + 'Right:α₅])) = fn a
              in
              let ((left : <δ₄>List), (right : <α₅>List)) =
                partition_map fn tail
              in
              case res of
              | 'Left (b : δ₄) ⇒ ((List 'Cons (b, left)), right)
              | 'Right (c : α₅) ⇒ (left, (List 'Cons (c, right)))
              esac
            | 'Nil () ⇒ ((List 'Nil ()), (List 'Nil ()))
            esac
    in
    (
      append=append,
      concat=concat,
      cons=cons,
      exists=exists,
      filter_map=filter_map,
      fold_left=fold_left,
      fold_left_map=fold_left_map,
      for_all=for_all,
      hd=hd,
      is_empty=is_empty,
      iter=iter,
      iteri=iteri,
      length=length,
      map=map,
      mapi=mapi,
      nth=nth,
      partition_map=partition_map,
      rev=rev,
      tl=tl,
      zip=zip
    )


## `Seq`

Slightly more complex than `List` are the possibly infinite
lazy sequences. `<a>Stream` is represented by `{} -> [{} + {a * <a>Stream}]`.

  $ minihell stream.test
  Inferred type:
    {append:<α>Stream → <α>Stream → <α>Stream
    * compare:(β → γ → <>Ord) → <β>Stream → <γ>Stream → <>Ord
    * concat:<<δ>Stream>Stream → <δ>Stream
    * cons:α₁ → <α₁>Stream → <α₁>Stream
    * count:<>Int → <<>Int>Stream
    * cycle:<β₁>Stream → <β₁>Stream
    * drop:<>Int → <γ₁>Stream → <γ₁>Stream
    * drop_while:(δ₁ → <>Bool) → <δ₁>Stream → <δ₁>Stream
    * empty:<α₂>Stream
    *
    equal:(β₂ → γ₂ → <>Bool)
      → <β₂>Stream → <γ₂>Stream → <>Bool
    * exists:(δ₂ → <>Bool) → <δ₂>Stream → <>Bool
    * filter:(α₃ → <>Bool) → <α₃>Stream → <α₃>Stream
    *
    filter_map:(β₃ → <γ₃>Option) → <β₃>Stream → <γ₃>Stream
    * find:(δ₃ → <>Bool) → <δ₃>Stream → <δ₃>Option
    * find_index:(α₄ → <>Bool) → <α₄>Stream → <<>Int>Option
    * find_map:(β₄ → <γ₄>Option) → <β₄>Stream → <γ₄>Option
    * flat_map:(δ₄ → <α₅>Stream) → <δ₄>Stream → <α₅>Stream
    *
    fold_left:(β₅ → γ₅ → β₅)
      → β₅ → <γ₅>Stream → β₅
    *
    fold_left_map:(δ₅ → α₆ → {δ₅ * β₆})
      → δ₅ → <α₆>Stream → <β₆>Stream
    *
    fold_lefti:(γ₆ → <>Int → δ₆ → γ₆)
      → γ₆ → <δ₆>Stream → γ₆
    * for_all:(α₇ → <>Bool) → <α₇>Stream → <>Bool
    * forever:({} → β₇) → <β₇>Stream
    * is_empty:<_>Stream → <>Bool
    * length:<_₁>Stream → <>Int
    * map:(γ₇ → δ₇) → <γ₇>Stream → <δ₇>Stream
    * once:α₈ → <α₈>Stream
    * repeat:β₈ → <β₈>Stream
    *
    sorted_merge:(γ₈ → γ₈ → <>Ord)
      → <γ₈>Stream → <γ₈>Stream → <γ₈>Stream
    * splice:<δ₈>Stream → <δ₈>Stream → <δ₈>Stream
    *
    split:(α₉ → <β₉, γ₉>Either)
      → <α₉>Stream → {<β₉>Stream * <γ₉>Stream}
    * take:<>Int → <δ₉>Stream → <δ₉>Stream
    * take_while:(δ₁ → <>Bool) → <δ₁>Stream → <δ₁>Stream
    * uncons:<α₁₀>Stream → <{α₁₀ * <α₁₀>Stream}>Option
    *
    zip:<β₁₀>Stream
      → <γ₁₀>Stream → <<β₁₀, γ₁₀>Zip>Stream}
  
  Elaborated term:
    type <a>Stream of {} → [{} + {a * <a>Stream}] in
    type <>Bool of ['F:{} + 'T:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    type <a, b>Either of [a + b] in
    type <a, b>Zip of ['Both:{a * b} + 'Left:a + 'Right:b] in
    type <>Int of [{} + <>Int] in
    type <>Ord of ['Eq:{} + 'Gt:{} + 'Lt:{}] in
    let (empty : <α₂>Stream) = (Stream λ (). '0 ())
    in
    let (is_empty : <_>Stream → <>Bool) =
      λ (Stream (s₆₁ : {} → [{} + {_ * <_>Stream}])).
          (Bool case s₆₁ () of | () ⇒ 'T () | _ ⇒ 'F () esac)
    in
    let (
      uncons : <α₁₀>Stream → <{α₁₀ * <α₁₀>Stream}>Option
    ) =
      λ (Stream (s₅₉ : {} → [{} + {α₁₀ * <α₁₀>Stream}])).
          (
            Option case s₅₉ () of
            | () ⇒ 'None ()
            | ((a₂₇ : α₁₀), (s₆₀ : <α₁₀>Stream)) ⇒
              'Some (a₂₇, s₆₀)
            esac
          )
    in
    let rec (length : <_₁>Stream → <>Int) =
      λ (Stream (s₅₇ : {} → [{} + {_₁ * <_₁>Stream}])).
          (
            Int case s₅₇ () of
            | () ⇒ '0 ()
            | (_, (s₅₈ : <_₁>Stream)) ⇒ '1 (length s₅₈)
            esac
          )
    in
    let rec (repeat : β₈ → <β₈>Stream) =
      λ (x₁ : β₈). (Stream λ (). '1 (x₁, repeat x₁))
    in
    let (once : α₈ → <α₈>Stream) =
      λ (x : α₈). (Stream λ (). '1 (x, (Stream λ (). '0 ())))
    in
    let rec (map : (γ₇ → δ₇) → <γ₇>Stream → <δ₇>Stream) =
      λ (f₁₆ : γ₇ → δ₇).
          λ (Stream (s₅₅ : {} → [{} + {γ₇ * <γ₇>Stream}])).
            (
              Stream λ ().
                case s₅₅ () of
                | () ⇒ '0 ()
                | ((a₂₆ : γ₇), (s₅₆ : <γ₇>Stream)) ⇒
                  '1 (f₁₆ a₂₆, map f₁₆ s₅₆)
                esac
            )
    in
    let rec (
      zip :
        <β₁₀>Stream
          → <γ₁₀>Stream → <<β₁₀, γ₁₀>Zip>Stream
    ) =
      λ (Stream (s₅₃ : {} → [{} + {β₁₀ * <β₁₀>Stream}])).
          λ (Stream (z₁₀ : {} → [{} + {γ₁₀ * <γ₁₀>Stream}])).
            (
              Stream λ ().
                case s₅₃ () of
                | () ⇒
                  case z₁₀ () of
                  | () ⇒ '0 ()
                  | ((b₈ : γ₁₀), (z₁₁ : <γ₁₀>Stream)) ⇒
                    '1 ((Zip 'Right b₈), zip (Stream s₅₃) z₁₁)
                  esac
                | ((a₂₅ : β₁₀), (s₅₄ : <β₁₀>Stream)) ⇒
                  case z₁₀ () of
                  | () ⇒ '1 ((Zip 'Left a₂₅), zip s₅₄ (Stream z₁₀))
                  | ((b₉ : γ₁₀), (z₁₂ : <γ₁₀>Stream)) ⇒
                    '1 ((Zip 'Both (a₂₅, b₉)), zip s₅₄ z₁₂)
                  esac
                esac
            )
    in
    let rec (
      filter : (α₃ → <>Bool) → <α₃>Stream → <α₃>Stream
    ) =
      λ (f₁₅ : α₃ → <>Bool).
          λ (Stream (s₅₁ : {} → [{} + {α₃ * <α₃>Stream}])).
            (
              Stream λ ().
                case s₅₁ () of
                | () ⇒ '0 ()
                | ((a₂₄ : α₃), (s₅₂ : <α₃>Stream)) ⇒
                  case (Bool/ f₁₅ a₂₄) of
                  | 'F () ⇒ (Stream/ filter f₁₅ s₅₂) ()
                  | 'T () ⇒ '1 (a₂₄, filter f₁₅ s₅₂)
                  esac
                esac
            )
    in
    let rec (
      filter_map :
        (β₃ → <γ₃>Option) → <β₃>Stream → <γ₃>Stream
    ) =
      λ (f₁₄ : β₃ → <γ₃>Option).
          λ (Stream (s₄₉ : {} → [{} + {β₃ * <β₃>Stream}])).
            (
              Stream λ ().
                case s₄₉ () of
                | () ⇒ '0 ()
                | ((a₂₃ : β₃), (s₅₀ : <β₃>Stream)) ⇒
                  case (Option/ f₁₄ a₂₃) of
                  | 'None () ⇒ (Stream/ filter_map f₁₄ s₅₀) ()
                  | 'Some (b₇ : γ₃) ⇒ '1 (b₇, filter_map f₁₄ s₅₀)
                  esac
                esac
            )
    in
    let rec (splice : <δ₈>Stream → <δ₈>Stream → <δ₈>Stream) =
      λ (Stream (s₄₇ : {} → [{} + {δ₈ * <δ₈>Stream}])).
          λ (z₉ : <δ₈>Stream).
            (
              Stream λ ().
                case s₄₇ () of
                | () ⇒ (Stream/ z₉) ()
                | ((a₂₂ : δ₈), (s₄₈ : <δ₈>Stream)) ⇒
                  '1 (a₂₂, splice z₉ s₄₈)
                esac
            )
    in
    let rec (
      fold_left_map :
        (δ₅ → α₆ → {δ₅ * β₆})
          → δ₅ → <α₆>Stream → <β₆>Stream
    ) =
      λ (f₁₃ : δ₅ → α₆ → {δ₅ * β₆}).
          λ (acc₅ : δ₅).
            λ (Stream (s₄₅ : {} → [{} + {α₆ * <α₆>Stream}])).
              (
                Stream λ ().
                  case s₄₅ () of
                  | () ⇒ '0 ()
                  | ((a₂₁ : α₆), (s₄₆ : <α₆>Stream)) ⇒
                    let ((acc₆ : δ₅), (b₆ : β₆)) =
                      f₁₃ acc₅ a₂₁
                    in '1 (b₆, fold_left_map f₁₃ acc₆ s₄₆)
                  esac
              )
    in
    let rec (
      fold_left :
        (β₅ → γ₅ → β₅) → β₅ → <γ₅>Stream → β₅
    ) =
      λ (f₁₂ : β₅ → γ₅ → β₅).
          λ (acc₃ : β₅).
            λ (Stream (s₄₃ : {} → [{} + {γ₅ * <γ₅>Stream}])).
              case s₄₃ () of
              | () ⇒ acc₃
              | ((a₂₀ : γ₅), (s₄₄ : <γ₅>Stream)) ⇒
                let (acc₄ : β₅) = f₁₂ acc₃ a₂₀
                in fold_left f₁₂ acc₄ s₄₄
              esac
    in
    let (
      fold_lefti :
        (γ₆ → <>Int → δ₆ → γ₆)
          → γ₆ → <δ₆>Stream → γ₆
    ) =
      λ (f₁₁ : γ₆ → <>Int → δ₆ → γ₆).
          λ (acc : γ₆).
            λ (s₄₀ : <δ₆>Stream).
              let rec (aux₄ : γ₆ → <>Int → <δ₆>Stream → γ₆) =
                λ (acc₁ : γ₆).
                    λ (count₁ : <>Int).
                      λ (
                        Stream (s₄₁ : {} → [{} + {δ₆ * <δ₆>Stream}])
                      ).
                        case s₄₁ () of
                        | () ⇒ acc₁
                        | ((a₁₉ : δ₆), (s₄₂ : <δ₆>Stream)) ⇒
                          let (acc₂ : γ₆) = f₁₁ acc₁ count₁ a₁₉
                          in aux₄ acc₂ (Int '1 count₁) s₄₂
                        esac
              in aux₄ acc (Int '0 ()) s₄₀
    in
    let rec (for_all : (α₇ → <>Bool) → <α₇>Stream → <>Bool) =
      λ (f₁₀ : α₇ → <>Bool).
          λ (Stream (s₃₈ : {} → [{} + {α₇ * <α₇>Stream}])).
            case s₃₈ () of
            | () ⇒ (Bool 'T ())
            | ((a₁₈ : α₇), (s₃₉ : <α₇>Stream)) ⇒
              if let 'T () =
                (Bool/ f₁₀ a₁₈)
                  then
                  for_all f₁₀ s₃₉ else (Bool 'F ()) fi
            esac
    in
    let rec (exists : (δ₂ → <>Bool) → <δ₂>Stream → <>Bool) =
      λ (f₉ : δ₂ → <>Bool).
          λ (Stream (s₃₆ : {} → [{} + {δ₂ * <δ₂>Stream}])).
            case s₃₆ () of
            | () ⇒ (Bool 'F ())
            | ((a₁₇ : δ₂), (s₃₇ : <δ₂>Stream)) ⇒
              if let 'T () =
                (Bool/ f₉ a₁₇) then (Bool 'T ()) else exists f₉ s₃₇ fi
            esac
    in
    let rec (find : (δ₃ → <>Bool) → <δ₃>Stream → <δ₃>Option) =
      λ (f₈ : δ₃ → <>Bool).
          λ (Stream (s₃₄ : {} → [{} + {δ₃ * <δ₃>Stream}])).
            case s₃₄ () of
            | () ⇒ (Option 'None ())
            | ((a₁₆ : δ₃), (s₃₅ : <δ₃>Stream)) ⇒
              if let 'T () =
                (Bool/ f₈ a₁₆)
                  then
                  (Option 'Some a₁₆) else find f₈ s₃₅ fi
            esac
    in
    let rec (
      find_index : (α₄ → <>Bool) → <α₄>Stream → <<>Int>Option
    ) =
      λ (f₇ : α₄ → <>Bool).
          λ (s₃₁ : <α₄>Stream).
            let rec (aux₃ : <>Int → <α₄>Stream → <<>Int>Option) =
              λ (idx : <>Int).
                  λ (Stream (s₃₂ : {} → [{} + {α₄ * <α₄>Stream}])).
                    case s₃₂ () of
                    | () ⇒ (Option 'None ())
                    | ((a₁₅ : α₄), (s₃₃ : <α₄>Stream)) ⇒
                      if let 'T () =
                        (Bool/ f₇ a₁₅)
                          then
                          (Option 'Some idx) else aux₃ (Int '1 idx) s₃₃ fi
                    esac
            in aux₃ (Int '0 ()) s₃₁
    in
    let rec (
      find_map : (β₄ → <γ₄>Option) → <β₄>Stream → <γ₄>Option
    ) =
      λ (f₆ : β₄ → <γ₄>Option).
          λ (Stream (s₂₉ : {} → [{} + {β₄ * <β₄>Stream}])).
            case s₂₉ () of
            | () ⇒ (Option 'None ())
            | ((a₁₄ : β₄), (s₃₀ : <β₄>Stream)) ⇒
              if let 'Some (b₅ : γ₄) =
                (Option/ f₆ a₁₄)
                  then
                  (Option 'Some b₅) else find_map f₆ s₃₀ fi
            esac
    in
    let rec (
      equal :
        (β₂ → γ₂ → <>Bool)
          → <β₂>Stream → <γ₂>Stream → <>Bool
    ) =
      λ (eq : β₂ → γ₂ → <>Bool).
          λ (Stream (s₂₇ : {} → [{} + {β₂ * <β₂>Stream}])).
            λ (Stream (z₇ : {} → [{} + {γ₂ * <γ₂>Stream}])).
              case s₂₇ () of
              | () ⇒
                case z₇ () of | () ⇒ (Bool 'T ()) | _ ⇒ (Bool 'F ()) esac
              | ((a₁₃ : β₂), (s₂₈ : <β₂>Stream)) ⇒
                case z₇ () of
                | () ⇒ (Bool 'F ())
                | ((b₄ : γ₂), (z₈ : <γ₂>Stream)) ⇒
                  if let 'T () =
                    (Bool/ eq a₁₃ b₄)
                      then
                      equal eq s₂₈ z₈ else (Bool 'F ()) fi
                esac
              esac
    in
    let rec (
      compare : (β → γ → <>Ord) → <β>Stream → <γ>Stream → <>Ord
    ) =
      λ (cmp : β → γ → <>Ord).
          λ (Stream (s₂₅ : {} → [{} + {β * <β>Stream}])).
            λ (Stream (z₅ : {} → [{} + {γ * <γ>Stream}])).
              case s₂₅ () of
              | () ⇒
                case z₅ () of | () ⇒ (Ord 'Eq ()) | _ ⇒ (Ord 'Lt ()) esac
              | ((a₁₂ : β), (s₂₆ : <β>Stream)) ⇒
                case z₅ () of
                | () ⇒ (Ord 'Gt ())
                | ((b₃ : γ), (z₆ : <γ>Stream)) ⇒
                  case (Ord/ cmp a₁₂ b₃) of
                  | 'Eq () ⇒ compare cmp s₂₆ z₆
                  | 'Gt () ⇒ (Ord 'Gt ())
                  | 'Lt () ⇒ (Ord 'Lt ())
                  esac
                esac
              esac
    in
    let rec (
      split :
        (α₉ → <β₉, γ₉>Either)
          → <α₉>Stream → {<β₉>Stream * <γ₉>Stream}
    ) =
      λ (f₅ : α₉ → <β₉, γ₉>Either).
          λ (s₂₀ : <α₉>Stream).
            let rec (left : <α₉>Stream → <β₉>Stream) =
              λ (Stream (s₂₃ : {} → [{} + {α₉ * <α₉>Stream}])).
                  (
                    Stream λ ().
                      case s₂₃ () of
                      | () ⇒ '0 ()
                      | ((a₁₁ : α₉), (s₂₄ : <α₉>Stream)) ⇒
                        case (Either/ f₅ a₁₁) of
                        | (b₂ : β₉) ⇒ '1 (b₂, left s₂₄)
                        | _ ⇒ (Stream/ left s₂₄) ()
                        esac
                      esac
                  )
            in
            let rec (right : <α₉>Stream → <γ₉>Stream) =
              λ (Stream (s₂₁ : {} → [{} + {α₉ * <α₉>Stream}])).
                  (
                    Stream λ ().
                      case s₂₁ () of
                      | () ⇒ '0 ()
                      | ((a₁₀ : α₉), (s₂₂ : <α₉>Stream)) ⇒
                        case (Either/ f₅ a₁₀) of
                        | _ ⇒ (Stream/ right s₂₂) ()
                        | (b₁ : γ₉) ⇒ '1 (b₁, right s₂₂)
                        esac
                      esac
                  )
            in (left s₂₀, right s₂₀)
    in
    let (cons : α₁ → <α₁>Stream → <α₁>Stream) =
      λ (a₉ : α₁).
          λ (s₁₉ : <α₁>Stream). (Stream λ (). '1 (a₉, s₁₉))
    in
    let rec (count : <>Int → <<>Int>Stream) =
      λ (n₄ : <>Int). (Stream λ (). '1 (n₄, count (Int '1 n₄)))
    in
    let rec (take : <>Int → <δ₉>Stream → <δ₉>Stream) =
      λ (n₂ : <>Int).
          λ (Stream (s₁₇ : {} → [{} + {δ₉ * <δ₉>Stream}])).
            (
              Stream λ ().
                case s₁₇ () of
                | () ⇒ '0 ()
                | ((a₈ : δ₉), (s₁₈ : <δ₉>Stream)) ⇒
                  case (Int/ n₂) of
                  | () ⇒ '0 ()
                  | (n₃ : <>Int) ⇒ '1 (a₈, take n₃ s₁₈)
                  esac
                esac
            )
    in
    let rec (forever : ({} → β₇) → <β₇>Stream) =
      λ (f₄ : {} → β₇). (Stream λ (). '1 (f₄ (), forever f₄))
    in
    let (cycle : <β₁>Stream → <β₁>Stream) =
      λ (s₁₆ : <β₁>Stream).
          let rec (aux₂ : <β₁>Stream → <β₁>Stream) =
            λ (Stream (z₃ : {} → [{} + {β₁ * <β₁>Stream}])).
                (
                  Stream λ ().
                    case z₃ () of
                    | () ⇒ (Stream/ aux₂ s₁₆) ()
                    | ((a₇ : β₁), (z₄ : <β₁>Stream)) ⇒
                      '1 (a₇, aux₂ z₄)
                    esac
                )
          in
          (
            Stream λ ().
              case (Stream/ s₁₆) () of
              | () ⇒ '0 ()
              | _ ⇒ (Stream/ aux₂ s₁₆) ()
              esac
          )
    in
    let rec (drop : <>Int → <γ₁>Stream → <γ₁>Stream) =
      λ (n : <>Int).
          λ (Stream (s₁₄ : {} → [{} + {γ₁ * <γ₁>Stream}])).
            (
              Stream λ ().
                case s₁₄ () of
                | () ⇒ '0 ()
                | ((a₆ : γ₁), (s₁₅ : <γ₁>Stream)) ⇒
                  case (Int/ n) of
                  | () ⇒ (Stream/ s₁₅) ()
                  | (n₁ : <>Int) ⇒ (Stream/ drop n₁ s₁₅) ()
                  esac
                esac
            )
    in
    let rec (
      take_while : (δ₁ → <>Bool) → <δ₁>Stream → <δ₁>Stream
    ) =
      λ (f₃ : δ₁ → <>Bool).
          λ (Stream (s₁₂ : {} → [{} + {δ₁ * <δ₁>Stream}])).
            (
              Stream λ ().
                case s₁₂ () of
                | () ⇒ '0 ()
                | ((a₅ : δ₁), (s₁₃ : <δ₁>Stream)) ⇒
                  if let 'T () =
                    (Bool/ f₃ a₅)
                      then
                      '1 (a₅, take_while f₃ s₁₃) else '0 () fi
                esac
            )
    in
    let rec (
      drop_while : (δ₁ → <>Bool) → <δ₁>Stream → <δ₁>Stream
    ) =
      λ (f₂ : δ₁ → <>Bool).
          λ (Stream (s₁₀ : {} → [{} + {δ₁ * <δ₁>Stream}])).
            (
              Stream λ ().
                case s₁₀ () of
                | () ⇒ '0 ()
                | ((a₄ : δ₁), (s₁₁ : <δ₁>Stream)) ⇒
                  if let 'T () =
                    (Bool/ f₂ a₄)
                      then
                      (Stream/ take_while f₂ s₁₁) ()
                        else
                        (Stream/ s₁₁) () fi
                esac
            )
    in
    let rec (append : <α>Stream → <α>Stream → <α>Stream) =
      λ (Stream (s₈ : {} → [{} + {α * <α>Stream}])).
          λ (z₂ : <α>Stream).
            (
              Stream λ ().
                case s₈ () of
                | () ⇒ (Stream/ z₂) ()
                | ((a₃ : α), (s₉ : <α>Stream)) ⇒
                  '1 (a₃, append s₉ z₂)
                esac
            )
    in
    let rec (concat : <<δ>Stream>Stream → <δ>Stream) =
      λ (Stream (ss₃ : {} → [{} + {<δ>Stream * <<δ>Stream>Stream}])).
          let rec (aux₁ : <δ>Stream → <<δ>Stream>Stream → <δ>Stream) =
            λ (Stream (s₆ : {} → [{} + {δ * <δ>Stream}])).
                λ (ss₅ : <<δ>Stream>Stream).
                  (
                    Stream λ ().
                      case s₆ () of
                      | () ⇒ (Stream/ concat ss₅) ()
                      | ((a₂ : δ), (s₇ : <δ>Stream)) ⇒
                        '1 (a₂, aux₁ s₇ ss₅)
                      esac
                  )
          in
          (
            Stream λ ().
              case ss₃ () of
              | () ⇒ '0 ()
              | ((s₅ : <δ>Stream), (ss₄ : <<δ>Stream>Stream)) ⇒
                (Stream/ aux₁ s₅ ss₄) ()
              esac
          )
    in
    let rec (
      flat_map : (δ₄ → <α₅>Stream) → <δ₄>Stream → <α₅>Stream
    ) =
      λ (f₁ : δ₄ → <α₅>Stream).
          λ (Stream (ss : {} → [{} + {δ₄ * <δ₄>Stream}])).
            let rec (aux : <α₅>Stream → <δ₄>Stream → <α₅>Stream) =
              λ (Stream (s₃ : {} → [{} + {α₅ * <α₅>Stream}])).
                  λ (ss₂ : <δ₄>Stream).
                    (
                      Stream λ ().
                        case s₃ () of
                        | () ⇒ (Stream/ flat_map f₁ ss₂) ()
                        | ((a₁ : α₅), (s₄ : <α₅>Stream)) ⇒
                          '1 (a₁, aux s₄ ss₂)
                        esac
                    )
            in
            (
              Stream λ ().
                case ss () of
                | () ⇒ '0 ()
                | ((s₂ : δ₄), (ss₁ : <δ₄>Stream)) ⇒
                  (Stream/ aux (f₁ s₂) ss₁) ()
                esac
            )
    in
    let rec (
      sorted_merge :
        (γ₈ → γ₈ → <>Ord)
          → <γ₈>Stream → <γ₈>Stream → <γ₈>Stream
    ) =
      λ (f : γ₈ → γ₈ → <>Ord).
          λ (Stream (s : {} → [{} + {γ₈ * <γ₈>Stream}])).
            λ (Stream (z : {} → [{} + {γ₈ * <γ₈>Stream}])).
              (
                Stream λ ().
                  case s () of
                  | () ⇒ z ()
                  | ((a : γ₈), (s₁ : <γ₈>Stream)) ⇒
                    case z () of
                    | () ⇒ '1 (a, s₁)
                    | ((b : γ₈), (z₁ : <γ₈>Stream)) ⇒
                      if let 'Gt () =
                        (Ord/ f a b)
                          then
                          '1 (b, sorted_merge f (Stream λ (). '1 (a, s₁)) z₁)
                            else
                            '1 (
                              a,
                              sorted_merge f s₁ (Stream λ (). '1 (b, z₁))
                            )
                              fi
                    esac
                  esac
              )
    in
    (
      append=append,
      compare=compare,
      concat=concat,
      cons=cons,
      count=count,
      cycle=cycle,
      drop=drop,
      drop_while=drop_while,
      empty=empty,
      equal=equal,
      exists=exists,
      filter=filter,
      filter_map=filter_map,
      find=find,
      find_index=find_index,
      find_map=find_map,
      flat_map=flat_map,
      fold_left=fold_left,
      fold_left_map=fold_left_map,
      fold_lefti=fold_lefti,
      for_all=for_all,
      forever=forever,
      is_empty=is_empty,
      length=length,
      map=map,
      once=once,
      repeat=repeat,
      sorted_merge=sorted_merge,
      splice=splice,
      split=split,
      take=take,
      take_while=take_while,
      uncons=uncons,
      zip=zip
    )

## `Map`

What I consider to be the most difficult term written here, the following
is an (not very optimized) implementation of OCaml's `Map` module with binary
search trees. For the first time here this involves trees and nested type aliases.

  $ minihell map.test
  Inferred type:
    {empty:<α>Compare → <α, β>Map
    * find:<γ, δ>Map → γ → <δ>Option
    *
    first:<α₁, β₁>Map
      → (α₁ → <>Bool) → <{α₁ * β₁}>Option
    * insert:<γ₁, δ₁>Map → γ₁ → δ₁ → <γ₁, δ₁>Map
    * is_empty:<_, _₁>Map → <>Bool
    * iter:<α₂, β₂>Map → (α₂ → β₂ → {}) → {}
    *
    last:<γ₂, δ₂>Map
      → (γ₂ → <>Bool) → <{γ₂ * δ₂}>Option
    *
    map:<α₃, β₃>Map
      → (α₃ → β₃ → γ₃) → <α₃, γ₃>Map
    * max:<δ₃, α₄>Map → <{δ₃ * α₄}>Option
    * min:<β₄, γ₄>Map → <{β₄ * γ₄}>Option
    *
    remove:<δ₄, α₅>Map
      → δ₄ → {<α₅>Option * <δ₄, α₅>Map}
    * singleton:<β₅>Compare → β₅ → γ₅ → <β₅, γ₅>Map
    * size:<_₂, _₃>Map → <>Int
    * to_list:<δ₅, α₆>Map → <{δ₅ * α₆}>List}
  
  Elaborated term:
    type <>Ordering of ['Equal:{} + 'Greater:{} + 'Less:{}] in
    type <a>Compare of a → a → <>Ordering in
    type <k, v>BST of
    ['Emp:{} + 'Node:{gt:<k, v>BST * key:k * lt:<k, v>BST * val:v}]
    in
    type <k, v>Map of {cmp:<k>Compare * vals:<k, v>BST} in
    type <a>Option of ['None:{} + 'Some:a] in
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <>Bool of ['F:{} + 'T:{}] in
    type <>Int of [{} + <>Int] in
    let (empty : <α>Compare → <α, β>Map) =
      λ (cmp₄ : <α>Compare). (Map (cmp=cmp₄, vals=(BST 'Emp ())))
    in
    let (is_empty : <_, _₁>Map → <>Bool) =
      λ (m₁₁ : <_, _₁>Map).
          (
            Bool if let 'Emp () =
              (BST/ (Map/ m₁₁).vals) then 'T () else 'F () fi
          )
    in
    let (
      singleton : <β₅>Compare → β₅ → γ₅ → <β₅, γ₅>Map
    ) =
      λ (cmp₃ : <β₅>Compare).
          λ (k₃ : β₅).
            λ (v₁ : γ₅).
              (
                Map (
                  cmp=cmp₃,
                  vals=(
                    BST 'Node (
                      gt=(BST 'Emp ()),
                      key=k₃,
                      lt=(BST 'Emp ()),
                      val=v₁
                    )
                  )
                )
              )
    in
    let (size : <_₂, _₃>Map → <>Int) =
      λ (m₁₀ : <_₂, _₃>Map).
          let rec (size_aux : <>Int → <_₂, _₃>BST → <>Int) =
            λ (acc₂ : <>Int).
                λ (t₁₁ : <_₂, _₃>BST).
                  case (BST/ t₁₁) of
                  | 'Emp () ⇒ acc₂
                  | 'Node (
                    n₁₁ :
                      {gt:<_₂, _₃>BST
                      * key:_₂
                      * lt:<_₂, _₃>BST
                      * val:_₃}
                  ) ⇒
                    let (acc₃ : <>Int) = size_aux acc₂ n₁₁.lt
                    in size_aux (Int '1 acc₃) n₁₁.gt
                  esac
          in size_aux (Int '0 ()) (Map/ m₁₀).vals
    in
    let (find : <γ, δ>Map → γ → <δ>Option) =
      λ (m₉ : <γ, δ>Map).
          λ (k₂ : γ).
            let (cmp₂ : γ → γ → <>Ordering) = (Compare/ (Map/ m₉).cmp)
            in
            let rec (find_aux : <γ, δ>BST → <δ>Option) =
              λ (t₁₀ : <γ, δ>BST).
                  case (BST/ t₁₀) of
                  | 'Emp () ⇒ (Option 'None ())
                  | 'Node (
                    n₁₀ : {gt:<γ, δ>BST * key:γ * lt:<γ, δ>BST * val:δ}
                  ) ⇒
                    case (Ordering/ cmp₂ k₂ n₁₀.key) of
                    | 'Equal () ⇒ (Option 'Some n₁₀.val)
                    | 'Greater () ⇒ find_aux n₁₀.gt
                    | 'Less () ⇒ find_aux n₁₀.lt
                    esac
                  esac
            in find_aux (Map/ m₉).vals
    in
    let (
      insert : <γ₁, δ₁>Map → γ₁ → δ₁ → <γ₁, δ₁>Map
    ) =
      λ (m₈ : <γ₁, δ₁>Map).
          λ (k₁ : γ₁).
            λ (v : δ₁).
              let (cmp₁ : γ₁ → γ₁ → <>Ordering) =
                (Compare/ (Map/ m₈).cmp)
              in
              let rec (insert_aux : <γ₁, δ₁>BST → <γ₁, δ₁>BST) =
                λ (t₉ : <γ₁, δ₁>BST).
                    (
                      BST case (BST/ t₉) of
                      | 'Emp () ⇒
                        'Node (
                          gt=(BST 'Emp ()),
                          key=k₁,
                          lt=(BST 'Emp ()),
                          val=v
                        )
                      | 'Node (
                        n₉ :
                          {gt:<γ₁, δ₁>BST
                          * key:γ₁
                          * lt:<γ₁, δ₁>BST
                          * val:δ₁}
                      ) ⇒
                        case (Ordering/ cmp₁ k₁ n₉.key) of
                        | 'Equal () ⇒ 'Node (n₉ with val=v)
                        | 'Greater () ⇒ 'Node (n₉ with gt=insert_aux n₉.gt)
                        | 'Less () ⇒ 'Node (n₉ with lt=insert_aux n₉.lt)
                        esac
                      esac
                    )
              in
              let (vals₂ : <γ₁, δ₁>BST) = insert_aux (Map/ m₈).vals
              in (Map (cmp=(Compare cmp₁), vals=vals₂))
    in
    let (
      remove :
        <δ₄, α₅>Map → δ₄ → {<α₅>Option * <δ₄, α₅>Map}
    ) =
      λ (m₇ : <δ₄, α₅>Map).
          λ (k : δ₄).
            let (cmp : δ₄ → δ₄ → <>Ordering) =
              (Compare/ (Map/ m₇).cmp)
            in
            let rec (
              delmin :
                <δ₄, α₅>BST
                  → {<{δ₄ * α₅}>Option * <δ₄, α₅>BST}
            ) =
              λ (t₈ : <δ₄, α₅>BST).
                  case (BST/ t₈) of
                  | 'Emp () ⇒ ((Option 'None ()), (BST 'Emp ()))
                  | 'Node (
                    n₈ :
                      {gt:<δ₄, α₅>BST
                      * key:δ₄
                      * lt:<δ₄, α₅>BST
                      * val:α₅}
                  ) ⇒
                    let ((del₂ : <{δ₄ * α₅}>Option),
                    (lt₁ : <δ₄, α₅>BST)) = delmin n₈.lt
                    in
                    case (Option/ del₂) of
                    | 'None () ⇒ ((Option 'Some (n₈.key, n₈.val)), n₈.gt)
                    | 'Some (del₃ : {δ₄ * α₅}) ⇒
                      ((Option 'Some del₃), (BST 'Node (n₈ with lt=lt₁)))
                    esac
                  esac
            in
            let rec (
              remove_aux :
                <δ₄, α₅>BST → {<α₅>Option * <δ₄, α₅>BST}
            ) =
              λ (t₇ : <δ₄, α₅>BST).
                  case (BST/ t₇) of
                  | 'Emp () ⇒ ((Option 'None ()), (BST 'Emp ()))
                  | 'Node (
                    n₇ :
                      {gt:<δ₄, α₅>BST
                      * key:δ₄
                      * lt:<δ₄, α₅>BST
                      * val:α₅}
                  ) ⇒
                    case (Ordering/ cmp k n₇.key) of
                    | 'Equal () ⇒
                      let (ans₁ : <α₅>Option) = (Option 'Some n₇.val)
                      in
                      let ((subst : <{δ₄ * α₅}>Option),
                      (gt₁ : <δ₄, α₅>BST)) = delmin n₇.gt
                      in
                      case (Option/ subst) of
                      | 'None () ⇒ (ans₁, n₇.lt)
                      | 'Some (subst₁ : {δ₄ * α₅}) ⇒
                        (
                          ans₁,
                          (
                            BST 'Node (
                              n₇
                              with
                              gt=gt₁,
                              key=subst₁.0,
                              val=subst₁.1
                            )
                          )
                        )
                      esac
                    | 'Greater () ⇒
                      let ((del₁ : <α₅>Option), (gt : <δ₄, α₅>BST)) =
                        remove_aux n₇.gt
                      in (del₁, (BST 'Node (n₇ with gt=gt)))
                    | 'Less () ⇒
                      let ((del : <α₅>Option), (lt : <δ₄, α₅>BST)) =
                        remove_aux n₇.lt
                      in (del, (BST 'Node (n₇ with lt=lt)))
                    esac
                  esac
            in
            let ((ans : <α₅>Option), (vals₁ : <δ₄, α₅>BST)) =
              remove_aux (Map/ m₇).vals
            in (ans, (Map (cmp=(Compare cmp), vals=vals₁)))
    in
    let (min : <β₄, γ₄>Map → <{β₄ * γ₄}>Option) =
      λ (m₆ : <β₄, γ₄>Map).
          let rec (
            min_aux :
              <{β₄ * γ₄}>Option
                → <β₄, γ₄>BST → <{β₄ * γ₄}>Option
          ) =
            λ (prev₁ : <{β₄ * γ₄}>Option).
                λ (t₆ : <β₄, γ₄>BST).
                  case (BST/ t₆) of
                  | 'Emp () ⇒ prev₁
                  | 'Node (
                    n₆ :
                      {gt:<β₄, γ₄>BST
                      * key:β₄
                      * lt:<β₄, γ₄>BST
                      * val:γ₄}
                  ) ⇒ min_aux (Option 'Some (n₆.key, n₆.val)) n₆.lt
                  esac
          in min_aux (Option 'None ()) (Map/ m₆).vals
    in
    let (max : <δ₃, α₄>Map → <{δ₃ * α₄}>Option) =
      λ (m₅ : <δ₃, α₄>Map).
          let rec (
            max_aux :
              <{δ₃ * α₄}>Option
                → <δ₃, α₄>BST → <{δ₃ * α₄}>Option
          ) =
            λ (prev : <{δ₃ * α₄}>Option).
                λ (t₅ : <δ₃, α₄>BST).
                  case (BST/ t₅) of
                  | 'Emp () ⇒ prev
                  | 'Node (
                    n₅ :
                      {gt:<δ₃, α₄>BST
                      * key:δ₃
                      * lt:<δ₃, α₄>BST
                      * val:α₄}
                  ) ⇒ max_aux (Option 'Some (n₅.key, n₅.val)) n₅.gt
                  esac
          in max_aux (Option 'None ()) (Map/ m₅).vals
    in
    let (
      first :
        <α₁, β₁>Map → (α₁ → <>Bool) → <{α₁ * β₁}>Option
    ) =
      λ (m₄ : <α₁, β₁>Map).
          λ (f₃ : α₁ → <>Bool).
            let rec (
              first_aux :
                <{α₁ * β₁}>Option
                  → <α₁, β₁>BST → <{α₁ * β₁}>Option
            ) =
              λ (best₁ : <{α₁ * β₁}>Option).
                  λ (t₄ : <α₁, β₁>BST).
                    case (BST/ t₄) of
                    | 'Emp () ⇒ best₁
                    | 'Node (
                      n₄ :
                        {gt:<α₁, β₁>BST
                        * key:α₁
                        * lt:<α₁, β₁>BST
                        * val:β₁}
                    ) ⇒
                      case (Bool/ f₃ n₄.key) of
                      | 'F () ⇒ first_aux best₁ n₄.gt
                      | 'T () ⇒
                        first_aux (Option 'Some (n₄.key, n₄.val)) n₄.lt
                      esac
                    esac
            in first_aux (Option 'None ()) (Map/ m₄).vals
    in
    let (
      last :
        <γ₂, δ₂>Map → (γ₂ → <>Bool) → <{γ₂ * δ₂}>Option
    ) =
      λ (m₃ : <γ₂, δ₂>Map).
          λ (f₂ : γ₂ → <>Bool).
            let rec (
              last_aux :
                <{γ₂ * δ₂}>Option
                  → <γ₂, δ₂>BST → <{γ₂ * δ₂}>Option
            ) =
              λ (best : <{γ₂ * δ₂}>Option).
                  λ (t₃ : <γ₂, δ₂>BST).
                    case (BST/ t₃) of
                    | 'Emp () ⇒ best
                    | 'Node (
                      n₃ :
                        {gt:<γ₂, δ₂>BST
                        * key:γ₂
                        * lt:<γ₂, δ₂>BST
                        * val:δ₂}
                    ) ⇒
                      case (Bool/ f₂ n₃.key) of
                      | 'F () ⇒ last_aux best n₃.lt
                      | 'T () ⇒
                        last_aux (Option 'Some (n₃.key, n₃.val)) n₃.gt
                      esac
                    esac
            in last_aux (Option 'None ()) (Map/ m₃).vals
    in
    let (iter : <α₂, β₂>Map → (α₂ → β₂ → {}) → {}) =
      λ (m₂ : <α₂, β₂>Map).
          λ (f₁ : α₂ → β₂ → {}).
            let rec (iter_aux : <α₂, β₂>BST → {}) =
              λ (t₂ : <α₂, β₂>BST).
                  case (BST/ t₂) of
                  | 'Emp () ⇒ ()
                  | 'Node (
                    n₂ :
                      {gt:<α₂, β₂>BST
                      * key:α₂
                      * lt:<α₂, β₂>BST
                      * val:β₂}
                  ) ⇒
                    let () = iter_aux n₂.lt
                    in
                    let () = f₁ n₂.key n₂.val
                    in let () = iter_aux n₂.gt in ()
                  esac
            in iter_aux (Map/ m₂).vals
    in
    let (
      map :
        <α₃, β₃>Map → (α₃ → β₃ → γ₃) → <α₃, γ₃>Map
    ) =
      λ (m₁ : <α₃, β₃>Map).
          λ (f : α₃ → β₃ → γ₃).
            let rec (map_aux : <α₃, β₃>BST → <α₃, γ₃>BST) =
              λ (t₁ : <α₃, β₃>BST).
                  (
                    BST case (BST/ t₁) of
                    | 'Emp () ⇒ 'Emp ()
                    | 'Node (
                      n₁ :
                        {gt:<α₃, β₃>BST
                        * key:α₃
                        * lt:<α₃, β₃>BST
                        * val:β₃}
                    ) ⇒
                      'Node (
                        gt=map_aux n₁.gt,
                        key=n₁.key,
                        lt=map_aux n₁.lt,
                        val=f n₁.key n₁.val
                      )
                    esac
                  )
            in
            let (vals : <α₃, γ₃>BST) = map_aux (Map/ m₁).vals
            in (Map (cmp=(Map/ m₁).cmp, vals=vals))
    in
    let (to_list : <δ₅, α₆>Map → <{δ₅ * α₆}>List) =
      λ (m : <δ₅, α₆>Map).
          let rec (
            to_list_aux :
              <{δ₅ * α₆}>List
                → <δ₅, α₆>BST → <{δ₅ * α₆}>List
          ) =
            λ (acc : <{δ₅ * α₆}>List).
                λ (t : <δ₅, α₆>BST).
                  case (BST/ t) of
                  | 'Emp () ⇒ acc
                  | 'Node (
                    n :
                      {gt:<δ₅, α₆>BST
                      * key:δ₅
                      * lt:<δ₅, α₆>BST
                      * val:α₆}
                  ) ⇒
                    let (acc₁ : <{δ₅ * α₆}>List) = to_list_aux acc n.gt
                    in to_list_aux (List 'Cons ((n.key, n.val), acc₁)) n.lt
                  esac
          in to_list_aux (List 'Nil ()) (Map/ m).vals
    in
    (
      empty=empty,
      find=find,
      first=first,
      insert=insert,
      is_empty=is_empty,
      iter=iter,
      last=last,
      map=map,
      max=max,
      min=min,
      remove=remove,
      singleton=singleton,
      size=size,
      to_list=to_list
    )


## `Logic`

We finish this test suite with something quite different.
There are a more "logic-oriented" interpretation of sum and product types
and we can do some theorems with them.

  $ minihell logic.test
  Inferred type:
    {and_assoc_l:<α, <β, γ>And>And → <<α, β>And, γ>And
    * and_assoc_r:<<δ, α₁>And, β₁>And → <δ, <α₁, β₁>And>And
    * and_comm:<γ₁, δ₁>And → <δ₁, γ₁>And
    * contradiction:α₂ → <α₂>Neg → <>False
    * contrapositive:(β₂ → γ₂) → <γ₂>Neg → <β₂>Neg
    * curry:(δ₂ → α₃ → β₃) → <δ₂, α₃>And → β₃
    * double_neg:γ₃ → <<γ₃>Neg>Neg
    * double_neg_elim:<<<δ₃>Neg>Neg>Neg → <δ₃>Neg
    * explosion:<>False → α₄
    * inj0:β₄ → <β₄, γ₄>Or
    * inj1:δ₄ → <α₅, δ₄>Or
    * or_assoc_l:<β₅, <γ₅, δ₅>Or>Or → <<β₅, γ₅>Or, δ₅>Or
    * or_assoc_r:<<α₆, β₆>Or, γ₆>Or → <α₆, <β₆, γ₆>Or>Or
    * or_comm:<δ₆, α₇>Or → <α₇, δ₆>Or
    * proj0:<β₇, _>And → β₇
    * proj1:<_₁, γ₇>And → γ₇
    * uncurry:(<δ₇, α₈>And → β₈) → δ₇ → α₈ → β₈}
  
  Elaborated term:
    type <>False of [] in
    type <>True of {} in
    type <a>Neg of a → [] in
    type <a, b>Or of [a + b] in
    type <a, b>And of {a * b} in
    let (explosion : <>False → α₄) =
      λ (False (f₂ : [])). case f₂ of  esac
    in
    let (contradiction : α₂ → <α₂>Neg → <>False) =
      λ (a₁₃ : α₂).
          λ (Neg (na₂ : α₂ → [])). (False na₂ a₁₃)
    in
    let (
      curry : (δ₂ → α₃ → β₃) → <δ₂, α₃>And → β₃
    ) =
      λ (f₁ : δ₂ → α₃ → β₃).
          λ (And ((a₁₂ : δ₂), (b₉ : α₃))). f₁ a₁₂ b₉
    in
    let (
      uncurry : (<δ₇, α₈>And → β₈) → δ₇ → α₈ → β₈
    ) =
      λ (f : <δ₇, α₈>And → β₈).
          λ (a₁₁ : δ₇). λ (b₈ : α₈). f (And (a₁₁, b₈))
    in
    let (contrapositive : (β₂ → γ₂) → <γ₂>Neg → <β₂>Neg) =
      λ (ab₂ : β₂ → γ₂).
          λ (nb : <γ₂>Neg).
            (Neg λ (a₁₀ : β₂). (Neg/ nb) (ab₂ a₁₀))
    in
    let (double_neg : γ₃ → <<γ₃>Neg>Neg) =
      λ (a₉ : γ₃). (Neg λ (na₁ : <γ₃>Neg). (Neg/ na₁) a₉)
    in
    let (double_neg_elim : <<<δ₃>Neg>Neg>Neg → <δ₃>Neg) =
      λ (nnna : <<<δ₃>Neg>Neg>Neg).
          (
            Neg λ (a₈ : δ₃).
              (Neg/ nnna) (Neg λ (na : <δ₃>Neg). (Neg/ na) a₈)
          )
    in
    let (or_comm : <δ₆, α₇>Or → <α₇, δ₆>Or) =
      λ (Or (ab₁ : [δ₆ + α₇])).
          (
            Or case ab₁ of
            | (a₇ : δ₆) ⇒ '1 a₇
            | (b₇ : α₇) ⇒ '0 b₇
            esac
          )
    in
    let (
      or_assoc_l : <β₅, <γ₅, δ₅>Or>Or → <<β₅, γ₅>Or, δ₅>Or
    ) =
      λ (Or (abc₁ : [β₅ + <γ₅, δ₅>Or])).
          case abc₁ of
          | (a₆ : β₅) ⇒ (Or '0 (Or '0 a₆))
          | (Or (bc : [γ₅ + δ₅])) ⇒
            case bc of
            | (b₆ : γ₅) ⇒ (Or '0 (Or '1 b₆))
            | (c₃ : δ₅) ⇒ (Or '1 c₃)
            esac
          esac
    in
    let (
      or_assoc_r : <<α₆, β₆>Or, γ₆>Or → <α₆, <β₆, γ₆>Or>Or
    ) =
      λ (Or (abc : [<α₆, β₆>Or + γ₆])).
          case abc of
          | (Or (ab : [α₆ + β₆])) ⇒
            case ab of
            | (a₅ : α₆) ⇒ (Or '0 a₅)
            | (b₅ : β₆) ⇒ (Or '1 (Or '0 b₅))
            esac
          | (c₂ : γ₆) ⇒ (Or '1 (Or '1 c₂))
          esac
    in
    let (and_comm : <γ₁, δ₁>And → <δ₁, γ₁>And) =
      λ (And ((a₄ : γ₁), (b₄ : δ₁))). (And (b₄, a₄))
    in
    let (and_assoc_l : <α, <β, γ>And>And → <<α, β>And, γ>And) =
      λ (And ((a₃ : α), (And ((b₃ : β), (c₁ : γ))))).
          (And ((And (a₃, b₃)), c₁))
    in
    let (
      and_assoc_r : <<δ, α₁>And, β₁>And → <δ, <α₁, β₁>And>And
    ) =
      λ (And ((And ((a₂ : δ), (b₂ : α₁))), (c : β₁))).
          (And (a₂, (And (b₂, c))))
    in
    let (proj0 : <β₇, _>And → β₇) =
      λ (And ((a₁ : β₇), _)). a₁
    in
    let (proj1 : <_₁, γ₇>And → γ₇) =
      λ (And (_, (b₁ : γ₇))). b₁
    in
    let (inj0 : β₄ → <β₄, γ₄>Or) = λ (a : β₄). (Or '0 a)
    in
    let (inj1 : δ₄ → <α₅, δ₄>Or) = λ (b : δ₄). (Or '1 b)
    in
    (
      and_assoc_l=and_assoc_l,
      and_assoc_r=and_assoc_r,
      and_comm=and_comm,
      contradiction=contradiction,
      contrapositive=contrapositive,
      curry=curry,
      double_neg=double_neg,
      double_neg_elim=double_neg_elim,
      explosion=explosion,
      inj0=inj0,
      inj1=inj1,
      or_assoc_l=or_assoc_l,
      or_assoc_r=or_assoc_r,
      or_comm=or_comm,
      proj0=proj0,
      proj1=proj1,
      uncurry=uncurry
    )

  $ minihell lambda.test
  Inferred type:
    <>Term → <<<>Failure>Val, <>Failure>Result
  
  Elaborated term:
    type <>Int of ['S:<>Int + 'Z:{}] in
    type <>Bool of ['F:{} + 'T:{}] in
    type <a>Option of ['None:{} + 'Some:a] in
    let rec (int_eq : <>Int → <>Int → <>Bool) =
      λ (Int (m : ['S:<>Int + 'Z:{}])).
          λ (Int (n : ['S:<>Int + 'Z:{}])).
            case m of
            | 'S (m₁ : <>Int) ⇒
              case n of
              | 'S (n₁ : <>Int) ⇒ int_eq m₁ n₁
              | 'Z () ⇒ (Bool 'F ())
              esac
            | 'Z () ⇒
              case n of | 'Z () ⇒ (Bool 'T ()) | … ⇒ (Bool 'F ()) esac
            esac
    in
    type <>Var of <>Int in
    let (var_eq : <>Var → <>Var → <>Bool) =
      λ (Var (v₅ : <>Int)). λ (Var (w : <>Int)). int_eq v₅ w
    in
    type <a>List of [{} + {a * <a>List}] in
    let rec (
      assoc_opt :
        (<>Var → <>Var → <>Bool)
          → <>Var → <{<>Var * <<>Failure>Val}>List → <<<>Failure>Val>Option
    ) =
      λ (eq : <>Var → <>Var → <>Bool).
          λ (k0 : <>Var).
            λ (
              List (
                kvs :
                  [{}
                  + {{<>Var * <<>Failure>Val} * <{<>Var * <<>Failure>Val}>List}]
              )
            ).
              case kvs of
              | () ⇒ (Option 'None ())
              | (((k₃ : <>Var), (v₄ : <<>Failure>Val)),
              (rest : <{<>Var * <<>Failure>Val}>List)) ⇒
                if let 'T () =
                  (Bool/ eq k0 k₃)
                    then
                    (Option 'Some v₄) else assoc_opt eq k0 rest fi
              esac
    in
    type <e>Val of ['Fun:<e>Val → <<e>Val, e>Result + 'Int:<>Int] in
    type <a, e>Result of ['Err:e + 'Ok:a] in
    type <>Failure of
    ['InvalidApplication:{} + 'UnboundVariable:{} + 'Unimplemented:{}]
    in
    type <a>Env of <{<>Var * a}>List in
    let (
      fetch :
        <<<>Failure>Val>Env → <>Var → <<<>Failure>Val, <>Failure>Result
    ) =
      λ (Env (env₂ : <{<>Var * <<>Failure>Val}>List)).
          λ (k₂ : <>Var).
            case (Option/ assoc_opt var_eq k₂ env₂) of
            | 'None () ⇒ (Result 'Err (Failure 'UnboundVariable ()))
            | 'Some (v₃ : <<>Failure>Val) ⇒ (Result 'Ok v₃)
            esac
    in
    let (
      add :
        <<<>Failure>Val>Env → <>Var → <<>Failure>Val → <<<>Failure>Val>Env
    ) =
      λ (Env (env₁ : <{<>Var * <<>Failure>Val}>List)).
          λ (k₁ : <>Var).
            λ (v₂ : <<>Failure>Val). (Env (List '1 ((k₁, v₂), env₁)))
    in
    type <>Term of
    ['Abs:{<>Var * <>Term}
    + 'App:{<>Term * <>Term}
    + 'Int:<>Int
    + 'Var:<>Var]
    in
    let (
      aux_apply :
        <<>Failure>Val → <<>Failure>Val → <<<>Failure>Val, <>Failure>Result
    ) =
      λ (
          Val (
            f₂ :
              ['Fun:<<>Failure>Val → <<<>Failure>Val, <>Failure>Result
              + 'Int:<>Int]
          )
        ).
          λ (x₂ : <<>Failure>Val).
            case f₂ of
            | 'Fun (f₃ : <<>Failure>Val → <<<>Failure>Val, <>Failure>Result) ⇒
              f₃ x₂
            | 'Int _ ⇒ (Result 'Err (Failure 'InvalidApplication ()))
            esac
    in
    let (eval : <>Term → <<<>Failure>Val, <>Failure>Result) =
      λ (t : <>Term).
          let rec (
            eval₁ :
              <<<>Failure>Val>Env → <>Term → <<<>Failure>Val, <>Failure>Result
          ) =
            λ (env : <<<>Failure>Val>Env).
                λ (
                  Term (
                    t₁ :
                      ['Abs:{<>Var * <>Term}
                      + 'App:{<>Term * <>Term}
                      + 'Int:<>Int
                      + 'Var:<>Var]
                  )
                ).
                  case t₁ of
                  | 'Abs ((k : <>Var), (body : <>Term)) ⇒
                    (
                      Result 'Ok (
                        Val 'Fun (λ (v₁ : <<>Failure>Val).
                          eval₁ (add env k v₁) body)
                      )
                    )
                  | 'App ((f : <>Term), (x : <>Term)) ⇒
                    case (Result/ eval₁ env f) of
                    | 'Err (e : <>Failure) ⇒ (Result 'Err e)
                    | 'Ok (f₁ : <<>Failure>Val) ⇒
                      case (Result/ eval₁ env x) of
                      | 'Err (e₁ : <>Failure) ⇒ (Result 'Err e₁)
                      | 'Ok (x₁ : <<>Failure>Val) ⇒ aux_apply f₁ x₁
                      esac
                    esac
                  | 'Int (i : <>Int) ⇒ (Result 'Ok (Val 'Int i))
                  | 'Var (v : <>Var) ⇒ fetch env v
                  esac
          in eval₁ (Env (List '0 ())) t
    in eval
