# Mini{hell,gen} -- Project report

Neven Villani

## Extra dependencies

The following dependencies have been added to `constrained-generation.opam`:
- `ocamlformat` provides `dune build @fmt`
- `odoc` provides `dune build @doc`
- `bisect_ppx` makes `--instrument-with bisect_ppx` available during `runtest`
  which enables code coverage analysis

Strictly speaking none of these are necessary to build the project and run tests,
but I wanted the project to be self-contained.

A `justfile` is provided, that defines in particular the recipes `build`, `test`
and `doc`, but it is not required to interact with it to build the project and you
can still fall back to
- `dune build`
- `dune runtest`
- `dune build @doc`


## Overview of extensions

Note: by default none of the features that add term constuctors are enabled in the
generator. Use the flag `--ext-features` in `minigen` to enable more (not all, in
part because there are infinitely many) constructors.

### Minor cosmetic/convenience changes

- Partial type annotations `(x : _ -> _)`
- Naming convention for inference variables:
  - `ø` for uninstantiated sum variants
  - `_` for unused tuple fields
- adjustments to printing, including subscripts for the unique identifiers that
  generate fresh variables
- improved error message for cyclic types
- batch mode for inference

### Inductive algebraic datatypes

- Products and sums of arbitrary arity
- Constructors and destructors for algebraic datatypes
  - nested irrefutable patterns allowed in
    - `let` binders
    - `lambda` binders
    - `case` arms
  - shallow refutable patterns through `if let`
  - a match `case x of | 'A a => a | 'B b => b esac`, optionally with a default arm
  - `tup.1` and `tup.foo` tuple field accesses
  - tuple update `tup with foo=v`
- Inductive aliases `type <a>List of [Nil + Cons:{a * <a>List}]`
- Recursive function definitions with `let rec`

Note that sum and product types are implemented generically over the type of the
discriminant and the parser instanciates string and integer labels,
so a non-negligible amount of complexity in `Unsized` and `Infer`
-- but also a greater confidence that they are free of bugs because they share
the same inference logic -- comes from
the desire to have the exact same handling for
- `(a=x, b=y, c=z)` and `(x, y, z)`
  (parsed respectively as `[("a",x); ("b",y); ("c",z)]` and `[(0,x); (1,y); (2,z)]`)
- `t.a` and `t.0`
  (parsed respectively as `(t, "a")` and `(t, 0)`)
- `case x of | a => a esac` and `case x of | 'A a => a esac`
  (parsed respectively as `(x, [(0, a, a)])` and `(x, [("A", a, a)])`
- etc

Ironically, OCaml struggles with the inference of `'label` so some functions
in `Infer.ml` need lots of annotations in their signatures.


### Limitations

The following possible improvements have been either unsuccessfully attempted
or intentionally avoided because of their (perceived ?) difficulty

#### Polymorphism

Although I'm very happy with how they turned out in the end, inductive types started
mostly as a consolation prize after I repeatedly failed to implement polymorphism. <br>
One of my approaches was to duplicate the inferred structure of polymorphic
functions once per use site so that their instanciations would be independent
from each other, but I quickly found it that this would still require rethinking
most of the unification due to the order of resolution of `Eq` constraints now
being significant. <br>
I thought of creating a new kind of constraint that would be like a one-way
equality, but this introduced a lot of complexity in managing the environment.


#### Generalized `with` operator

By `with`, I'm referring to the operator on tuples that updates one or several
fields. <br>
My reference implementations are OCaml's `with` syntax
`{ t with a=x }` and Rust's `..` notation `T { a: x, ..t }`.
Both take a term `t` whose type is a labeled product and update the field labeled
`a` to replace whatever previous value it had to the new value `x`. <br>

Consider the signature given to the fuction `lambda x. x with a=x`.
- The most general signature that my type system can express is `{…} → {a:t * …}`,
  where `with` would thus have the effect of _adding_ a field,
  i.e. `(b=y,) with a=x` is `(a=x, b=y)` and
  `(a=z, b=y) with a=x` is `(a=x, b=y)`.
- The signature that OCaml would give it is equivalent to
  `{a:u * …} → {a:t * …}` (although OCaml doesn't have anonymous labeled tuples).
  In this point of view `(b=y,) with a=x` is a type error, and
  `(a=z, b=y) with a=x` is `(a=x, b=y)` with no further constraint.
  That is, `with` can substitute but not add fields.
- What my project can handle -- and incidentally also the signature that Rust
  gives it -- is `{a:t * …} → {a:t * …}`, that is `with` has the same
  type signature as the identity on tuples. `(b=y,) with a=x` is a type error
  and `(a=z, b=y) with a=x` imposes the further constraint that `z` and `x`
  have the same type.

Although this is sufficient for most usages, it's a much weaker operator than
what I initially aimed for and I didn't manage to make it fully general.
Fixing this limitation would probably require an additional environment during
unification, modifications to `Unsized.t`, and unification having side-effects.


#### Nested refutable patterns and exhaustiveness checks

Checking the exhaustiveness of a `match` statement is known to be a tricky problem.
- (OCaml, paper) [GADTs and Exhaustiveness: Looking for the Impossible](https://arxiv.org/pdf/1702.02281.pdf),
  J. Garrigue and J. Le Normand, EPTCS 2015;
- (Rust, blog) [Compiling Rust is NP-Hard](https://compilercrim.es/rust-np/);

The issue always comes from the combination of sum and product types,
and that's not a difficulty I want to go into. To simplify things I decided
that all my refutable patterns would be shallow. This affects the
`if let` and `case` language constructs where refutable patterns are expected.
They may contain nested _irrefutable_ patterns, but refutable patterns must
always be toplevel and shallow. This means that the following is **NOT SUPPORTED**:
```
-- Syntax error
lambda x.
  if let 'Some ('Some v) = x then
    'Some v
  else 'None
```
instead you have to write it in two steps
```
-- OK
lambda x.
  if let 'Some w = x then
    if let 'Some v = w then
      'Some v
    else 'None
  else 'None
```
There is also no way of doing a `case` on two values at once (no equivalent
of `match x, y with ...`).

Even unary sums are not exempt from this restriction: for values of type `[{}]`
(sum type with one variant containing unit) the pattern `'0 ()` is *not* syntactically
irrefutable, even though it is in practice impossible for a value of the correct
type not to match it. <br>
Note: the closest OCaml equivalent would be `type unit = U0 of ()` where `U0 y`
is an irrefutable pattern. Meanwhile in Rust this would be written
`enum Unit { U0(()) }`, and in that case `Unit::U0(y)` is also an irrefutable pattern,
so the behavior of my project on this point is a downgrade compared to both languages.


#### Variant name uniqueness

OCaml allows using the same variant name or record field for two types,
without namespaces, and this opens annoying questions such as shadowing of
type constructors, and asymetrical inference.
My least favorite example of this is
```ml
type ab = A | B
type a = A

B = A (* - : bool = false *)
A = B (* type error: no constructor B in type a *)
```
and its dual for products
```ml
type ab = { a:unit; b:unit }
type a = { a:unit }

fun x -> x.b = x.a (* - : ab -> bool = <fun> *)
fun x -> x.a = x.b (* type error: no field b within type a *)
```
This shows that because of type constructor partial shadowing and limitations of
type inference, equality is not syntactically symmetrical even for pure values.

This is a can of worms that I chose not to open for my project, and the
escape route I found is to completely disconnect the variant labels from the type
constructors. The above example would be written
```
type <>AB of [A:{} + B:{}] in
type <>A of [A:{}] in
...
```
in which context `'A` is of type `[A:{} + ...]` compatible with
*both* `[A:{} + B:{}]` and `[A:{}]`.
A term of type `<>AB` is obtained by `AB'A` which removes the ambiguity
on which type to construct. This is admittedly a bit of an easy way out
since this syntax gives a lot of information to the inference algorithm,
but given I already did the work for anonymous sum and product types,
it seems like a good compromise.


#### Ambiguous type aliases

In OCaml one can define a new type without binding it to any constructor,
such as `type 'a arrow = 'a -> 'a`. This can only be used in annotations,
because if `fun x -> x` is inferred to be of type `'a arrow` then there is
now an ambiguity on which type alias to choose. This way of defining types
is thus viable only for annotations.

Earlier I said that I chose to disconnect the type constructor from the variant,
but instead I am now making it impossible to separate the type constructor
from the type name: any new type defined comes with a constructor which
*must* wrap all values of the type.
In the context
```
type <a>Arrow of a -> a in
...
```
the term `lambda x. x` does *not* have type `<α>Arrow`, it merely has
type `α → α`. A term of type `<α>Arrow` is only given by
`Arrow (lambda x. x)`. There is thus never a need to fold or unfold the types
for unification because named types are fully unambiguous in the syntax.
The fact that the name of the type is identical to the constructor simplifies
the process quite a bit by eliminating map accesses to translate from one to the
other.


#### Exhaustive or random generation of inductive datatypes

Inductive algebraic datatypes are a late addition to the project and I haven't
put much thought into how to integrate them to the generator.
I could provide a header of predefined types with fixed constructors,
but it wouldn't be very interesting.

More generally the generator doesn't generate all existing terms at all
because it only emits tuples and sums of arity 2 or less,
only labeled by `int`, and does not use the term constructors
`IfLet`, recursive `LetPat`, `Annot`, `Field` or `With`.
These additional constructors are a nice showcase of how extensible my inference
facilities are due to how easy it was to implement them, but honestly they
don't add that much to the expressiveness of the language and because they
significantly increase the frequency of dead-ends during random generation,
adding them to the generator would have a terrible impact on performance.


### Known bugs

#### Naming heuristics

The heuristics defined by `Hint.t` are imperfect, and I know of one
false positive and one false negative.

Reminder: `Hint.t` implements variable naming hints so that
- `lambda x. lambda y. x` is given type `α → _ → α` which is more
  explicit than `α → β → α`
- `'1 ()` is given type `[ø + {} + …]` which is more explicit
  than `[α + {} + …]`.

This is only a naming convention that happens at `decode` time, it does
not have any influence on the inference.

The `_` hint is not perfect:
- `lambda x. lambda y. let a = y in x` is typed as `α → β → α`
  even though `y` is transitively unused.
- `lambda ((_,_) as y). y` is typed as `{_ * _₁} → {_ * _₁}`,
  marking the types as irrelevant even though there is still an equality
  constraint between them.

Because these signatures -- although slightly suboptimal -- are not
*incorrect* (notice in the second case the subscript `₁` that shows that
the input and output tuples have the same fields `_` and `_₁` distinguishable from
each other), this bug is in my opinion very low priority and I decided it wasn't
worth introducing more complexity to the unification or full-blown dead code
analysis just to fix this.


## Notable changes to the project structure

### Files added

- `Hint.ml{,i}` added to support type variable naming conventions
- `Pattern.ml{,i}` implements the structure of irrefutable patterns
- `Unsized.ml{,i}` handles tuples of arbitrary arity


### Extra command line arguments

- `minihell`
  - `--wf` runs extra well-formedness checks in `Unsized`

- `minigen`
  - `--wf` runs extra well-formedness checks in `MRand`
  - `--ext-features` enables some of the new features (field accesses, sums, unit tuple)
  - `--full-type` print the type of generated terms
  - `--parseable-output` disable some pretty printing conventions so that the output
    of `minigen` can be parsed as an input to `minihell`. See `bugfix-003.t` for a use of this.


### Signatures changed

- in `STLC` and `Untyped`
  - `Let` and `LetTup` constructors merged into `LetPat`
  - `Tuple` constructor moved to the `adt` type, and is now parameterized
    by the labels. `int adt` contains what used to be `Tuple`.
  - in `Abs`, `(TeVar.t * ty, void) Pattern.t` replaces `TeVar.t * ty`:
    in nested patterns we place annotations on all variable bindings
- in `Constraint`
  - replaced a `structure option` with `structure Hint.t`
- in `Structure`
  - `merge` changed from `(a -> b -> c) -> a t -> b t -> c t option`
    to `((a, b) either_or_both -> c) -> a t -> b t -> c t option`
- in `Infer`
  - `bind` renamed `( let-: )` and changed from
    `STLC.ty -> (Constraint.variable -> ('a, 'e t)) -> ('a, 'e) t`
    to `(STLC.ty * foreign) -> (Constraint.variable -> ('a, 'e) t) -> ('a, 'e) t`
    where `type foreign = (Structure.TyVar.t * variable) list` describes
    type variable substitutions for generic datatypes
  - `has_type` changed from
    `env -> Untyped.term -> variable -> (STLC.term, err) t`
    to `env -> aliases -> Untyped.term -> variable -> (STLC.term, err) t`
    where `type aliases = (Structure.TyVar.t list * Structure.TyVar.t STLC.ty_) (Map.S with type key = string).t`
- instances of the `Cycle` exception everywhere now contain
  not a variable but a `unit -> (string * Lexing.position) list * cyclic_ty` that lazily
  computes a cyclic type to display to the user and a list of variables in the source
  code that are involved in the cycle.
  Instead of merely printing "cycle on variable ?x" we can now state
  e.g. "ω occurs in ω = ω → \_".
- `Clash` has also become lazy

There are also new constructors in several types.
The parsing and printing modules have been reworked a lot.


### New tests

It should be no surprise that I added a few tests for the new features.

The test suite grew from
- 6 → 93 files (83 in `infer.t`)
- 7 → 1874 lines (350 in `infer.t`)

The core part of these tests (`infer.t`) contains surprisingly little duplication:
thanks to the logic being fully generic I only had to test the parsing and printing
of e.g. labeled tuples to be confident that they were handled correctly once indexed
tuples were good, because past the parsing step the inference is oblivious to labels.


#### "Library" tests

More than 80% of the lines of code of tests are due to `library.t/`, which I had a lot of
fun writing. It contains the culmination of all the features I added to the language
and there are files in there that implement most of OCaml's `Option`, `Lazy`, `List`,
`Seq`, `Bool`, `Either`, `Result` and `Map` modules, along with some simple lemmas
in `library.t/logic.test`.

`library.t/map.test` for example implements (a not very optimized version of) OCaml's
`Map` with binary search trees in the form of one term of inferred type
```
{ empty:<α>Compare → <α, β>Map
* find:<γ, δ>Map → γ → <δ>Option
* first:<α₁, β₁>Map → (α₁ → <>Bool) → <{α₁ * β₁}>Option
* insert:<γ₁, δ₁>Map → γ₁ → δ₁ → <γ₁, δ₁>Map
* is_empty:<α₂, β₂>Map → <>Bool
* iter:<γ₂, δ₂>Map → (γ₂ → δ₂ → {}) → {}
* last:<α₃, β₃>Map → (α₃ → <>Bool) → <{α₃ * β₃}>Option
* map:<γ₃, δ₃>Map → (γ₃ → δ₃ → α₄) → <γ₃, α₄>Map
* max:<β₄, γ₄>Map → <{β₄ * γ₄}>Option
* min:<δ₄, α₅>Map → <{δ₄ * α₅}>Option
* remove:<β₅, γ₅>Map → β₅ → {<γ₅>Option * <β₅, γ₅>Map}
* singleton:<δ₅>Compare → δ₅ → α₆ → <δ₅, α₆>Map
* size:<β₆, γ₆>Map → <>Int
* to_list:<δ₆, α₇>Map → <{δ₆ * α₇}>List }
```


#### New test structure

This growth required a reorganization of the `tests.t` folder, which besides
being renamed into `infer.t` is now sorted by feature.
Most of the tests that were part of the initial skeleton
ended up in `infer.t/lambda`, `infer.t/tuple` and `infer.t/annot`.

I decided to split the inference from the generation tests because only the latter
is slightly more of a performance bottleneck now that i've increased the depth at
which the test generates terms. Generation is moved to `gen.t`.

For reference the test suites on my machine take the following amount of time:
- `dune build @gen` (runs generation tests in `gen.t`): 12s
- `dune build @infer` (runs inference tests in `infer.t/run.t`): 250ms
- `dune build @library` (runs inference tests in `library.t/run.t`): 1.1s
- `dune build @bugfix-003` (runs the pipeline from generation back into inference): 5.6s

## Syntax

With how many language features have been added, an index of language constructs
is due.

### Example code snippets

These should be enough to get an overview of the language extensions.

```
-- infer.t/pattern/let-pat-nested.test

lambda a.
  let (x, y, (_, z, (w,))) = a in
  (x, y, z, w)


: {α * β * {_ * γ * {δ}}} → {α * β * γ * δ}
```

```
-- infer.t/label/named-field.test

lambda x.
  let y = x.foo in
  let z = x.bar in
  (y, z)

: {bar:α * foo:β * …} → {β * α}
```

```
-- infer.t/sum/default-case.test

lambda x.
  case x of
  | 'Yes y => 'Yes y
  | 'No n => 'No n
  | ... => 'Other ()
  esac

: ['No:α + 'Yes:β + …] → ['No:α + 'Other:{} + 'Yes:β + …]
```

```
-- infer.t/rec/list-map.test

type <a>List of ['Nil + 'Cons:{a * <a>List}] in
let rec map = lambda f (List l).
  List (case l of
  | 'Nil => 'Nil
  | 'Cons (a, tail) => 'Cons (f a, map f tail)
  esac)
in map

: (α → β) → <α>List → <β>List
```

```
-- infer.t/rec/dne.test

type <a>Neg of a -> [] in
let double_neg = lambda x (Neg nx). nx x in
lambda (Neg nnnx).
  Neg (lambda x. nnnx (Neg (double_neg x)))

: <<<α>Neg>Neg>Neg → <α>Neg
```

### Listing of language features

#### Unchanged

| Name        | Examples        | Notes                                           |
|-------------|-----------------|-------------------------------------------------|
| Variable    | `x`, `foo`      | The first letter has to be lowercase alphabetic |
| Abstraction | `lambda x. x`   |                                                 |
| Application | `f x`           |                                                 |
| Arrow type  | `a → b`         |                                                 |

#### Tuples

The following are (most of them atomic) terms

| Name          | Summary                        | Examples                                        | Notes                                                       |
|---------------|--------------------------------|-------------------------------------------------|-------------------------------------------------------------|
| Indexed tuple | standard tuple, size 0 or more | `(x, y, z)`, `()`                               |                                                             |
| Product type  | type of indexed tuples         | `{t * u * v}`, `{}`, `{t * …}`                  | `{t * …}` coerces to both `{t}` and `{t * u * …}`           |
| Labeled tuple | named record, size 0 or more   | `(a=x, b=y, c=z)`, `()`                         | The labeled unit coerces to the indexed unit and vice versa |
| Record type   | type of named records          | `{a:t * b:u * c:v)`, `{…}`, `{a:t * …}`           | `{a:t * …}` coerces to both `{a:t}` and `{a:t * b:u * …}`   |
| With          | tuple update                   | `(x, y, z) with 1=w` is `(x, w, z)`             | Unfortunately limited to `y` and `w` having the same type   |
|               |                                | `(a=x, b=y, c=z) with b=w` is `(a=x, b=w, c=z)` |                                                             |
| Field access  | projection to a single field   | `(x, y, z).1` is `y`                            |                                                             |
|               |                                | `(a=x, b=y, c=z).b` is `y`                      |                                                             |

#### Irrefutable patterns


The following are patterns and can be used in the following places:
- `let <pattern> = <term> in <term>`
- `lambda <pattern>. <term>`
- `case <term> of | <pattern> => <term> esac`

| Name          | Examples           | Matches            | Binds                   | Asserts                                            | Notes                                         |
|---------------|--------------------|--------------------|-------------------------|----------------------------------------------------|-----------------------------------------------|
| Wildcard      | `_`                | anything           |                         |                                                    | There is also a wildcard for type annotations |
| Variable      | `a`, `b`           | anything           | entire term             |                                                    |                                               |
| Annotation    | `(pat : ty)`       | same as `pat`      | same as `pat`           | term that matches `pat` has the structure of `ty`  |                                               |
| As            | `pat as v`         | same as `pat`      | `v` AND same as `pat`   |                                                    |                                               |
| Indexed tuple | `(pat1, pat2)`     | indexed tuple      | both `pat1` and `pat2`  | tuple has the correct size                         | Supports trailing `…`                         |
| Labeled tuple | `(a=pat1, b=pat2)` | labeled tuple      | both `pat1` and `pat2`  | tuple has the correct size                         | Supports trailing `…`                         |
| Alias         | `Tree pat`         | inductive datatype | same as `pat`           | proper instanciation of the generic datatype       |                                               |

Some self-contained examples:
```
lambda (x, _). x

lambda x. let (a, (b, (c, d))) = x in (d, c, b, a)

lambda x. let (((a : int), _) as pair) = x in (pair, a)

lambda x. let (a=y, b=z, c=_, ...) = x in (y, z)
```


#### Sums

| Name         | Summary                                       | Examples                                          | Notes                                                         |
|--------------|-----------------------------------------------|---------------------------------------------------|---------------------------------------------------------------|
| Injection    | Sum constructor                               | `'0 x`, `'1 x`                                    | When the grammar allows `'0 ()` can be abbreviated `'0`       |
|              |                                               | `'Foo x`, `'Nil ()`                               | When the grammar allows, `'Nil ()` can be abbreviated `'Nil`  |
| Match        | Sum destructor                                | `case <term> of \| <pattern> => <term> esac`      | Allows a default arm `\| … => <term>`                         |
|              |                                               | `case <term> of \| 'Foo <pattern> => <term> esac` |                                                               |
| Sum type     | `'0 x` has type `[t + …]` when `x : t`        | `[t + u + v]`, `[t + u + …]`, `[]`                | `[t + …]` coerces to both `[t]` and `[t + u + …]`             |
|              | `'Foo x` has type `['Foo:t + …]` when `x : t` | `['A:t + 'B:u + 'C:v]`, `['A:t + …]`, `[]`        | `['A:t + …]` coerces to both `['A:t]` and `['A:t + 'B:u + …]` |
|              |                                               |                                                   | The indexed `[]` and the labeled `[]` coerce to each other    |
|              |                                               |                                                   | `['A:{} + …]` can be abbreviated to `['A + …]`                |

#### Refutable patterns

A refutable pattern is shallow and takes the shape `<refut_pattern> ::= ' <label> <pattern>`
where `<label>` is a sum type constructor, i.e. an integer or a string,
and `<pattern>` is a (nested) irrefutable pattern.
`<pattern>` can be elided and is then assumed to be the unit tuple pattern `()`.

The usage is `if let <refut_pattern> = <term> then <term> else <term>` where
the variables bound by `<refut_pattern>` are available only in the `then` branch.

Examples:
```
let not = lambda b.
  if let 'True = x then 'False else 'True

: ['True:{} + …] → ['False:{} + 'True:{} + …]
```

```
let is_some = lambda v.
  if let 'Some _ = v then 'True else 'False

: [Some:a + …] → ['False:{} + 'True:{} + …]
```

```
let unwrap = lambda v dflt.
  if let 'Some inner = v then inner else dflt

: ['Some:a + …] → a → a
```


#### Recursive functions

The syntax `let rec <pattern> = <term> in <term>` is implemented and makes the bound variables of
`<pattern>` available during the typing of both `<term>`s.

#### Inductive datatypes

The annotation for an inductive datatype takes the form `<params>Type`, and
it is declared as `type <params>Type of <structure> in <term>`

For example
```
type <>Bool of ['T + 'F] in
Bool'T

: <>Bool
```

```
type <a>List of ['Nil + 'Cons:{a * <a>List}] in
lambda a b c. List'Cons (a, List'Cons (b, List'Cons (c, List'Nil)))

: a -> a -> a -> <a>List
```

```
type <n,l>Tree of ['Leaf:l + 'Node:{<n,l>Tree * n * <n,l>Tree}] in
lambda v. Tree'Node (Tree'Leaf, v, Tree'Leaf)

: a -> <a,b>Tree
```



## Closing remarks

- Due to how many features I implemented that add infinite choices to
  term generation, the language accessible to inference is much richer than
  the language accessible to generation. I haven't thought much of how to make
  the generator produce interesting inductive types, so it's stuck with basic
  fixed-arity products and sums.

- The generator in general is not as efficient as when there were few term
  constructors, because there are a lot of nested constructors that are
  incompatible with each other. Overall I have spent a disproportionately
  large portion of the time working on this project improving the inference
  rather than the generation because that's the part I found more fun.

- For having tried both and succeeded in only one, inductive types are *much*
  easier to implement than polymorphism. I made several failed attempts at the
  latter and still don't have a successful implementation, but the former worked
  on the first try with barely any changes to unification.

- On the topic of extensions, I'm surprised that sum types weren't even suggested.
  Nary sums are tricky (even more so than nary tuples, I found), but adding to the
  language a type isomorphic to `Either.t` is very easy.

- I'm sorry about how many custom `let` operators I added in `Infer.ml`.
  They genuinely did make the implementation easier, but they might be an obstacle
  to reading the code.

