# TL;DR

This is the inference part of the test suite

To run all tests, execute
```
dune runtest
```
To run only *these* tests, execute
```
dune build @infer
```

Both commands expect to be launched from the root of the project directory.
If this outputs nothing, the testsuite passes. If this outputs a diff, it
means that there is a mismatch between the recorded/reference
output and the behavior of your program.

To *promote* the tests outputs (that is, to modify the reference
output to match the current behavior of your program), run

```
dune build
dune promote
```


If you are using the associated `justfile` for this project, then
`just test`, `just test-infer` and `just bless` are
aliases for the three commands above respectively.


# The tests

The tests below use the `minihell` program defined in
../bin/minihell.ml, called on the *.test files stored in (subfolder of)
the present directory.

`minihell` takes untyped programs as input and will
type-check and elaborate them. It can show many things
depending on the input flags passed. By default we ask
`minihell` to repeat the source file (to make the recorded
output here more pleasant to read) and to show the generated
constraint. It will also show the result type and the
elaborated term.

  $ alias minihell='minihell --show-source --show-constraint --wf'

The tests that we run here are cheap, so we are enabling the `--wf` flag
which activates some runtime well-formedness checks (e.g. some hidden
invariants on the internal representation of tuples).

Remark: You can call minihell from the command-line yourself
by using either
> dune exec bin/minihell.exe -- <arguments>
or
> dune exec minihell -- <arguments>
(The latter short form, used in the tests below, is available thanks
to the bin/dune content.)



## Tests about abstraction and application

`id_poly` is just the polymorphic identity.

  $ minihell lambda/id_poly.test
  Input term:
    λ x. x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    α → α
  
  Elaborated term:
    λ (x : α). x

`id_int` is the monomorphic identity on the type `int`. Note
that we have not implemented support for a built-in `int`
type, this is just an abstract/rigid type variable: `Constr
(Var ...)` at type `STLC.ty`.

  $ minihell annot/id_int.test
  Input term:
    λ x. (x : int)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃(?int = int).
          (∃_. ?x₁ = ?x ∧ ?x = ?int) ∧ ?int = ?wt ∧ decode ?int))
      ∧ decode ?final_type
  
  Inferred type:
    int → int
  
  Elaborated term:
    λ (x : int). x

Checks the fix for an early bug where the `Let` case of `Infer` did not properly
bind `final_type`, giving all `let _ = _ in _` expressions an arbitrary type.

  $ minihell lambda/letfun.test
  Input term:
    let f = λ i. i in f
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?f = ?wpat
        ∧ decode ?f
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?i = ?wpat₁ ∧ decode ?i ∧ ?warr = ?wt ∧ (∃_. ?i₁ = ?i ∧ ?i = ?wt₁))
        ∧ (∃_. ?f₁ = ?f ∧ ?f = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    α → α
  
  Elaborated term:
    let (f : α → α) = λ (i : α). i in f


Some negative tests for good measure:


  $ minihell annot/should-fail/poly-conflict.test
  Input term:
    λ x. let y1 = (x : t1) in let y2 = (x : t2) in (y1, y2)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _.
          ?y1 = ?wpat₁
          ∧ decode ?y1
          ∧ (∃(?t1 = t1). (∃_. ?x₁ = ?x ∧ ?x = ?t1) ∧ ?t1 = ?wt₁ ∧ decode ?t1)
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _.
            ?y2 = ?wpat₂
            ∧ decode ?y2
            ∧ (∃(?t2 = t2).
              (∃_. ?x₂ = ?x ∧ ?x = ?t2) ∧ ?t2 = ?wt₂ ∧ decode ?t2)
            ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
              (∃_. ?y2₁ = ?y2 ∧ ?y2 = ?s₁)
              ∧ (∃_. ?y1₁ = ?y1 ∧ ?y1 = ?s)
              ∧ ?prod = ?wu₁)
            ∧ ?wu₁ = ?wu
            ∧ ?wpat₂ = ?wt₂)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Error:
      (1)  t1
    incompatible with
      (2)  t2
    help:
       2|     let y1 = (x : t1) in
                            ^^found to impose structure (1)
       3|     let y2 = (x : t2) in
                            ^^incompatible with structure (2)
    hint: rigid type variables `t1` and `t2` are unequal
  [101]


  $ minihell lambda/should-fail/error.test
  Input term:
    (λ x. (x : int)) (λ y. y)
  
  Generated constraint:
    ∃?final_type.
      (∃?wu ?wr (?wt = ?wu → ?wr).
        (∃?wpat ?wt₁ (?warr = ?wpat → ?wt₁) _.
          ?x = ?wpat
          ∧ decode ?x
          ∧ ?warr = ?wt
          ∧ (∃(?int = int).
            (∃_. ?x₁ = ?x ∧ ?x = ?int) ∧ ?int = ?wt₁ ∧ decode ?int))
        ∧ (∃?wpat₁ ?wt₂ (?warr₁ = ?wpat₁ → ?wt₂) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wu
          ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wt₂))
        ∧ ?wr = ?final_type)
      ∧ decode ?final_type
  
  Error:
      (1)  _ → α
    incompatible with
      (2)  int
    help:
       1| (lambda x. (x : int)) (lambda y. y)
                                ^^^^^^^^^^^^^found to impose structure (1)
       1| (lambda x. (x : int)) (lambda y. y)
                          ^^^incompatible with structure (2)
    hint: rigid type variable `int` is only compatible with itself
  [101]

  $ minihell lambda/should-fail/not-found.test
  Fatal error:
    Find failed: y not found.
       1| lambda x. y
                    ^this variable is unbound in the current term
  [101]


## Batch mode

I have implemented a form of batch parsing.
This will later allow us to much more easily pipe the output of the generator
into the typechecker as a final sanity check.

  $ minihell parsing/simple.batch.test
  Input term:
    λ x. x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    α → α
  
  Elaborated term:
    λ (x : α). x
  ----------
  
  Fatal error:
    Find failed: x not found.
       3| lambda y. x
                    ^this variable is unbound in the current term
  ----------
  
  Input term:
    let x₁ = () in x₁ ()
  
  Generated constraint:
    ∃?final_type₁.
      (∃?wpat₁ ?wt₁ ?wu _.
        ?x₂ = ?wpat₁
        ∧ decode ?x₂
        ∧ (∃(?prod = {}). ?prod = ?wt₁)
        ∧ (∃?wu₁ ?wr (?wt₂ = ?wu₁ → ?wr).
          (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?wt₂)
          ∧ (∃(?prod₁ = {}). ?prod₁ = ?wu₁)
          ∧ ?wr = ?wu)
        ∧ ?wu = ?final_type₁
        ∧ ?wpat₁ = ?wt₁)
      ∧ decode ?final_type₁
  
  Error:
      (1)  {} → β
    incompatible with
      (2)  {}
    help:
       5| let x = () in x ()
                        ^found to impose structure (1)
       5| let x = () in x ()
                  ^^incompatible with structure (2)
    hint: an arrow type can only match another arrow type
  [102]


## Logging the constraint-solving process

You can ask `minihell` to show how the constraint evolves as
the solver progresses and accumulates more information on
the inference variables with the `--log-solver` flag.

  $ minihell --log-solver annot/id_int.test
  Input term:
    λ x. (x : int)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃(?int = int).
          (∃_. ?x₁ = ?x ∧ ?x = ?int) ∧ ?int = ?wt ∧ decode ?int))
      ∧ decode ?final_type
  
  Constraint solving log:
    ∃?final_type.
      decode ?final_type
      ∧ (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?int = int). decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
        ∧ ?warr = ?final_type
        ∧ decode ?x
        ∧ ?x = ?wpat)
    ∃?final_type.
      decode ?final_type
      ∧ (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?int = int). decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
        ∧ ?warr = ?final_type
        ∧ decode ?x
        ∧ ?x = ?wpat)
    ∃?wpat ?final_type.
      decode ?final_type
      ∧ (∃?wt (?warr = ?wpat → ?wt) _.
        (∃(?int = int). decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
        ∧ ?warr = ?final_type
        ∧ decode ?x
        ∧ ?x = ?wpat)
    ∃?wt ?wpat ?final_type.
      decode ?final_type
      ∧ (∃(?warr = ?wpat → ?wt) _.
        (∃(?int = int). decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
        ∧ ?warr = ?final_type
        ∧ decode ?x
        ∧ ?x = ?wpat)
    ∃?wt ?wpat (?warr = ?wpat → ?wt) ?final_type.
      decode ?final_type
      ∧ (∃_.
        (∃(?int = int). decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
        ∧ ?warr = ?final_type
        ∧ decode ?x
        ∧ ?x = ?wpat)
    ∃_ ?wt ?wpat (?warr = ?wpat → ?wt) ?final_type.
      decode ?final_type
      ∧ (∃(?int = int).
        decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
      ∧ ?warr = ?final_type
      ∧ decode ?x
      ∧ ?x = ?wpat
    ∃_ ?wt (?warr = ?x → ?wt) ?final_type.
      decode ?final_type
      ∧ (∃(?int = int).
        decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
      ∧ ?warr = ?final_type
      ∧ decode ?x
    ∃_ ?wt (?warr = ?x → ?wt).
      decode ?warr
      ∧ (∃(?int = int).
        decode ?int ∧ ?int = ?wt ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x))
      ∧ decode ?x
    ∃_ ?wt (?warr = ?x → ?wt) (?int = int).
      decode ?warr
      ∧ decode ?int
      ∧ ?int = ?wt
      ∧ (∃_. ?x = ?int ∧ ?x₁ = ?x)
      ∧ decode ?x
    ∃_ _ ?wt (?warr = ?x → ?wt) (?int = int).
      decode ?warr
      ∧ decode ?int
      ∧ ?int = ?wt
      ∧ ?x = ?int
      ∧ ?x₁ = ?x
      ∧ decode ?x
    ∃_ ?wt (?warr = ?x₁ → ?wt) (?int = int).
      decode ?warr ∧ decode ?int ∧ ?int = ?wt ∧ ?x₁ = ?int ∧ decode ?x₁
    ∃(?x₁ = int) ?wt (?warr = ?x₁ → ?wt).
      decode ?warr ∧ decode ?x₁ ∧ ?x₁ = ?wt ∧ decode ?x₁
    ∃(?x₁ = int) (?warr = ?x₁ → ?x₁).
      decode ?warr ∧ decode ?x₁ ∧ decode ?x₁
  
  Inferred type:
    int → int
  
  Elaborated term:
    λ (x : int). x



## Binary products

  $ minihell tuple/curry.test
  Input term:
    λ f. λ x. λ y. f (x, y)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?f = ?wpat
        ∧ decode ?f
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?x = ?wpat₁
          ∧ decode ?x
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?y = ?wpat₂
            ∧ decode ?y
            ∧ ?warr₂ = ?wt₁
            ∧ (∃?wu ?wr (?wt₃ = ?wu → ?wr).
              (∃_. ?f₁ = ?f ∧ ?f = ?wt₃)
              ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
                (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
                ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
                ∧ ?prod = ?wu)
              ∧ ?wr = ?wt₂))))
      ∧ decode ?final_type
  
  Inferred type:
    ({α * β} → γ) → α → β → γ
  
  Elaborated term:
    λ (f : {α * β} → γ). λ (x : α). λ (y : β). f (x, y)


  $ minihell tuple/uncurry.test
  Input term:
    λ f. λ p. let (x, y) = p in f x y
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?f = ?wpat
        ∧ decode ?f
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?p = ?wpat₁
          ∧ decode ?p
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ ?wu _ _.
            (∃_ _ (?prod = {?p₁ * ?p₂}).
              ?prod = ?wpat₂ ∧ ?y = ?p₂ ∧ decode ?y ∧ ?x = ?p₁ ∧ decode ?x)
            ∧ (∃_. ?p₃ = ?p ∧ ?p = ?wt₂)
            ∧ (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
              (∃?wu₂ ?wr₁ (?wt₄ = ?wu₂ → ?wr₁).
                (∃_. ?f₁ = ?f ∧ ?f = ?wt₄)
                ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₂)
                ∧ ?wr₁ = ?wt₃)
              ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wu₁)
              ∧ ?wr = ?wu)
            ∧ ?wu = ?wt₁
            ∧ ?wpat₂ = ?wt₂)))
      ∧ decode ?final_type
  
  Inferred type:
    (α → β → γ) → {α * β} → γ
  
  Elaborated term:
    λ (f : α → β → γ).
      λ (p : {α * β}). let ((x : α), (y : β)) = p in f x y




I noticed that there was no provided test that covered the `Arrow`
and `Prod` cases of `Structure.t` flattening (the `(let-:)` operator,
formerly `bind`). With the following tests, one positive and one negative,
we now have full coverage for annotation flattening.

  $ minihell annot/funannot.test
  Input term:
    (λ x. (x, x) : i → {i * i})
  
  Generated constraint:
    ∃?final_type.
      (∃ (?i = i) (?i₁ = i) (?i₂ = i) (?prod = {?i₁ * ?i₂})
        (?arr = ?i → ?prod)
      .
        (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
          ?x = ?wpat
          ∧ decode ?x
          ∧ ?warr = ?arr
          ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
            (∃_. ?x₁ = ?x ∧ ?x = ?s₁)
            ∧ (∃_. ?x₂ = ?x ∧ ?x = ?s)
            ∧ ?prod₁ = ?wt))
        ∧ ?arr = ?final_type
        ∧ decode ?arr)
      ∧ decode ?final_type
  
  Inferred type:
    i → {i * i}
  
  Elaborated term:
    λ (x : i). (x, x)



  $ minihell annot/should-fail/bad-funannot.test
  Input term:
    (λ x. x : i → {i * i})
  
  Generated constraint:
    ∃?final_type.
      (∃ (?i = i) (?i₁ = i) (?i₂ = i) (?prod = {?i₁ * ?i₂})
        (?arr = ?i → ?prod)
      .
        (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
          ?x = ?wpat ∧ decode ?x ∧ ?warr = ?arr ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt))
        ∧ ?arr = ?final_type
        ∧ decode ?arr)
      ∧ decode ?final_type
  
  Error:
      (1)  i
    incompatible with
      (2)  {i * i}
    help:
       1| ((lambda x. x) : i -> {i * i})
                           ^found to impose structure (1)
       1| ((lambda x. x) : i -> {i * i})
                                ^^^^^^^incompatible with structure (2)
    hint: rigid type variable `i` is only compatible with itself
  [101]



More control over annotations:

  $ minihell --annot-level=0 annot/demo.test
  Input term:
    λ ((x, (y, w)) : {int * _}).
      let (f : _ → _) = λ z. z in (f x, (y : _ → _), w)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _ _ _.
        (∃(?int = int) ?_ (?prod = {?int * ?_}).
          (∃_ _ (?prod₁ = {?p * ?p₁}).
            ?prod₁ = ?prod
            ∧ (∃_ _ (?prod₂ = {?p₂ * ?p₃}).
              ?prod₂ = ?p₁ ∧ ?w = ?p₃ ∧ decode ?w ∧ ?y = ?p₂ ∧ decode ?y)
            ∧ ?x = ?p
            ∧ decode ?x)
          ∧ ?prod = ?wpat
          ∧ decode ?prod)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _.
          (∃?_₁ ?_₂ (?arr = ?_₁ → ?_₂).
            ?f = ?arr ∧ decode ?f ∧ ?arr = ?wpat₁ ∧ decode ?arr)
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ ?warr₁ = ?wt₁
            ∧ (∃_. ?z₁ = ?z ∧ ?z = ?wt₂))
          ∧ (∃?s ?s₁ ?s₂ (?prod₃ = {?s * ?s₁ * ?s₂}).
            (∃_. ?w₁ = ?w ∧ ?w = ?s₂)
            ∧ (∃?_₃ ?_₄ (?arr₁ = ?_₃ → ?_₄).
              (∃_. ?y₁ = ?y ∧ ?y = ?arr₁) ∧ ?arr₁ = ?s₁ ∧ decode ?arr₁)
            ∧ (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
              (∃_. ?f₁ = ?f ∧ ?f = ?wt₃)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
              ∧ ?wr = ?s)
            ∧ ?prod₃ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {int * {α → β * γ}} → {int * α → β * γ}
  
  Elaborated term:
    λ ((x : int), ((y : α → β), (w : γ))).
      let (f : int → int) = λ (z : int). z in (f x, y, w)
  $ minihell --annot-level=1 annot/demo.test
  Input term:
    λ ((x, (y, w)) : {int * _}).
      let (f : _ → _) = λ z. z in (f x, (y : _ → _), w)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _ _ _.
        (∃(?int = int) ?_ (?prod = {?int * ?_}).
          (∃_ _ (?prod₁ = {?p * ?p₁}).
            ?prod₁ = ?prod
            ∧ (∃_ _ (?prod₂ = {?p₂ * ?p₃}).
              ?prod₂ = ?p₁ ∧ ?w = ?p₃ ∧ decode ?w ∧ ?y = ?p₂ ∧ decode ?y)
            ∧ ?x = ?p
            ∧ decode ?x)
          ∧ ?prod = ?wpat
          ∧ decode ?prod)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _.
          (∃?_₁ ?_₂ (?arr = ?_₁ → ?_₂).
            ?f = ?arr ∧ decode ?f ∧ ?arr = ?wpat₁ ∧ decode ?arr)
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ ?warr₁ = ?wt₁
            ∧ (∃_. ?z₁ = ?z ∧ ?z = ?wt₂))
          ∧ (∃?s ?s₁ ?s₂ (?prod₃ = {?s * ?s₁ * ?s₂}).
            (∃_. ?w₁ = ?w ∧ ?w = ?s₂)
            ∧ (∃?_₃ ?_₄ (?arr₁ = ?_₃ → ?_₄).
              (∃_. ?y₁ = ?y ∧ ?y = ?arr₁) ∧ ?arr₁ = ?s₁ ∧ decode ?arr₁)
            ∧ (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
              (∃_. ?f₁ = ?f ∧ ?f = ?wt₃)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
              ∧ ?wr = ?s)
            ∧ ?prod₃ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {int * {α → β * γ}} → {int * α → β * γ}
  
  Elaborated term:
    λ (((x : int), ((y : α → β), (w : γ))) : {int * {α → β * γ}}).
      let ((f : int → int) : int → int) = λ (z : int). z
      in (f x, (y : α → β), w)
  $ minihell --annot-level=2 annot/demo.test
  Input term:
    λ ((x, (y, w)) : {int * _}).
      let (f : _ → _) = λ z. z in (f x, (y : _ → _), w)
  
  Generated constraint:
    ∃?final_type.
      (∃?annot₁₁.
        (∃?wpat ?wt (?warr = ?wpat → ?wt) _ _ _.
          (∃(?int = int) ?_ (?prod = {?int * ?_}).
            (∃?annot₁.
              (∃_ _ (?prod₁ = {?p * ?p₁}).
                ?prod₁ = ?prod
                ∧ (∃?annot.
                  (∃_ _ (?prod₂ = {?p₂ * ?p₃}).
                    ?prod₂ = ?p₁
                    ∧ ?w = ?p₃
                    ∧ decode ?w
                    ∧ ?y = ?p₂
                    ∧ decode ?y)
                  ∧ ?p₁ = ?annot
                  ∧ decode ?annot)
                ∧ ?x = ?p
                ∧ decode ?x)
              ∧ ?prod = ?annot₁
              ∧ decode ?annot₁)
            ∧ ?prod = ?wpat
            ∧ decode ?prod)
          ∧ ?warr = ?final_type
          ∧ (∃?annot₁₀.
            (∃?wpat₁ ?wt₁ ?wu _.
              (∃?_₁ ?_₂ (?arr = ?_₁ → ?_₂).
                ?f = ?arr ∧ decode ?f ∧ ?arr = ?wpat₁ ∧ decode ?arr)
              ∧ (∃?annot₃.
                (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
                  ?z = ?wpat₂
                  ∧ decode ?z
                  ∧ ?warr₁ = ?wt₁
                  ∧ (∃?annot₂.
                    (∃_. ?z₁ = ?z ∧ ?z = ?wt₂)
                    ∧ ?wt₂ = ?annot₂
                    ∧ decode ?annot₂))
                ∧ ?wt₁ = ?annot₃
                ∧ decode ?annot₃)
              ∧ (∃?annot₉.
                (∃?s ?s₁ ?s₂ (?prod₃ = {?s * ?s₁ * ?s₂}).
                  (∃?annot₄.
                    (∃_. ?w₁ = ?w ∧ ?w = ?s₂)
                    ∧ ?s₂ = ?annot₄
                    ∧ decode ?annot₄)
                  ∧ (∃?_₃ ?_₄ (?arr₁ = ?_₃ → ?_₄).
                    (∃?annot₅.
                      (∃_. ?y₁ = ?y ∧ ?y = ?arr₁)
                      ∧ ?arr₁ = ?annot₅
                      ∧ decode ?annot₅)
                    ∧ ?arr₁ = ?s₁
                    ∧ decode ?arr₁)
                  ∧ (∃?annot₈.
                    (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
                      (∃?annot₆.
                        (∃_. ?f₁ = ?f ∧ ?f = ?wt₃)
                        ∧ ?wt₃ = ?annot₆
                        ∧ decode ?annot₆)
                      ∧ (∃?annot₇.
                        (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
                        ∧ ?wu₁ = ?annot₇
                        ∧ decode ?annot₇)
                      ∧ ?wr = ?s)
                    ∧ ?s = ?annot₈
                    ∧ decode ?annot₈)
                  ∧ ?prod₃ = ?wu)
                ∧ ?wu = ?annot₉
                ∧ decode ?annot₉)
              ∧ ?wu = ?wt
              ∧ ?wpat₁ = ?wt₁)
            ∧ ?wt = ?annot₁₀
            ∧ decode ?annot₁₀))
        ∧ ?final_type = ?annot₁₁
        ∧ decode ?annot₁₁)
      ∧ decode ?final_type
  
  Inferred type:
    {int * {α → β * γ}} → {int * α → β * γ}
  
  Elaborated term:
    (
      λ (
        ((x : int), (((y : α → β), (w : γ)) : {α → β * γ})) :
          {int * {α → β * γ}}
      ).
        (
          let (f : int → int) = (λ (z : int). (z : int) : int → int)
          in
          (
            (((f : int → int) (x : int) : int), (y : α → β), (w : γ)) :
              {int * α → β * γ}
          ) : {int * α → β * γ}
        ) : {int * {α → β * γ}} → {int * α → β * γ}
    )


## Cyclic types

Unification can sometimes create cyclic types. We decide to reject
these situations with an error. (We could also accept those as they
preserve type-safety, but they have the issue, just like the
OCaml -rectypes option, that they allow to write somewhat-nonsensical
program, and our random term generator will be very good at finding
a lot of those.)

Side note: we will later introduce inductive types to the language
which are basically the same thing but in a much more controlled manner.
Making errors while we try to write recursive functions (e.g. forgetting
to write a constructor) can easily produce cyclic types, so I have taken care
to improve the error message for cyclic types so that debugging those tests
is less painful.

  $ minihell --log-solver lambda/should-fail/selfapp.test
  Input term:
    λ foo. foo foo
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?foo = ?wpat
        ∧ decode ?foo
        ∧ ?warr = ?final_type
        ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          (∃_. ?foo₁ = ?foo ∧ ?foo = ?wt₁)
          ∧ (∃_. ?foo₂ = ?foo ∧ ?foo = ?wu)
          ∧ ?wr = ?wt))
      ∧ decode ?final_type
  
  Constraint solving log:
    ∃?final_type.
      decode ?final_type
      ∧ (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          ?wr = ?wt
          ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
          ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
        ∧ ?warr = ?final_type
        ∧ decode ?foo
        ∧ ?foo = ?wpat)
    ∃?final_type.
      decode ?final_type
      ∧ (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          ?wr = ?wt
          ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
          ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
        ∧ ?warr = ?final_type
        ∧ decode ?foo
        ∧ ?foo = ?wpat)
    ∃?wpat ?final_type.
      decode ?final_type
      ∧ (∃?wt (?warr = ?wpat → ?wt) _.
        (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          ?wr = ?wt
          ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
          ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
        ∧ ?warr = ?final_type
        ∧ decode ?foo
        ∧ ?foo = ?wpat)
    ∃?wt ?wpat ?final_type.
      decode ?final_type
      ∧ (∃(?warr = ?wpat → ?wt) _.
        (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          ?wr = ?wt
          ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
          ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
        ∧ ?warr = ?final_type
        ∧ decode ?foo
        ∧ ?foo = ?wpat)
    ∃?wt ?wpat (?warr = ?wpat → ?wt) ?final_type.
      decode ?final_type
      ∧ (∃_.
        (∃?wu ?wr (?wt₁ = ?wu → ?wr).
          ?wr = ?wt
          ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
          ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
        ∧ ?warr = ?final_type
        ∧ decode ?foo
        ∧ ?foo = ?wpat)
    ∃?wt ?wpat (?warr = ?wpat → ?wt) _ ?final_type.
      decode ?final_type
      ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
        ?wr = ?wt
        ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
        ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
      ∧ ?warr = ?final_type
      ∧ decode ?foo
      ∧ ?foo = ?wpat
    ∃?wt _ (?warr = ?foo → ?wt) _ ?final_type.
      decode ?final_type
      ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
        ?wr = ?wt
        ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
        ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
      ∧ ?warr = ?final_type
      ∧ decode ?foo
    ∃?wt _ (?warr = ?foo → ?wt) _.
      decode ?warr
      ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
        ?wr = ?wt
        ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
        ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
      ∧ decode ?foo
    ∃?wu ?wt _ (?warr = ?foo → ?wt) _.
      decode ?warr
      ∧ (∃?wr (?wt₁ = ?wu → ?wr).
        ?wr = ?wt
        ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
        ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
      ∧ decode ?foo
    ∃?wu ?wt ?wr _ (?warr = ?foo → ?wt) _.
      decode ?warr
      ∧ (∃(?wt₁ = ?wu → ?wr).
        ?wr = ?wt
        ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
        ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo))
      ∧ decode ?foo
    ∃?wu ?wr (?wt₁ = ?wu → ?wr) ?wt ?wr _ (?warr = ?foo → ?wt) _.
      decode ?warr
      ∧ ?wr = ?wt
      ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
      ∧ (∃_. ?foo = ?wt₁ ∧ ?foo₁ = ?foo)
      ∧ decode ?foo
    ∃?wu ?wr (?wt₁ = ?wu → ?wr) ?wt ?wr _ (?warr = ?foo → ?wt) _ _.
      decode ?warr
      ∧ ?wr = ?wt
      ∧ (∃_. ?foo = ?wu ∧ ?foo₂ = ?foo)
      ∧ ?foo = ?wt₁
      ∧ ?foo₁ = ?foo
      ∧ decode ?foo
    ∃?wu ?wr (?wt₁ = ?wu → ?wr) ?wt ?wr _ (?warr = ?foo₁ → ?wt) _.
      decode ?warr
      ∧ ?wr = ?wt
      ∧ (∃_. ?foo₁ = ?wu ∧ ?foo₂ = ?foo₁)
      ∧ ?foo₁ = ?wt₁
      ∧ decode ?foo₁
    ∃ ?wu ?wt ?wr (?foo₁ = ?wu → ?wr) (?warr = ?foo₁ → ?wt)
      (?foo₁ = ?wu → ?wr)
    .
      decode ?warr ∧ ?wr = ?wt ∧ (∃_. ⊥ ∧ ?foo₂ = ?foo₁) ∧ decode ?foo₁
    ∃ ?wu ?wt ?wr (?foo₁ = ?wu → ?wr) (?warr = ?foo₁ → ?wt) _
      (?foo₁ = ?wu → ?wr)
    . decode ?warr ∧ ?wr = ?wt ∧ ⊥ ∧ ?foo₂ = ?foo₁ ∧ decode ?foo₁
    ∃ ?wu ?wt ?wr (?foo₂ = ?wu → ?wr) (?warr = ?foo₂ → ?wt)
      (?foo₂ = ?wu → ?wr)
    . decode ?warr ∧ ?wr = ?wt ∧ ⊥ ∧ decode ?foo₂
    ∃ ?wu ?wr (?foo₂ = ?wu → ?wr) (?warr = ?foo₂ → ?wr)
      (?foo₂ = ?wu → ?wr)
    . decode ?warr ∧ ⊥ ∧ decode ?foo₂
  
  Error:
    Cycle detected: ω occurs in
    ω = ω → _
    without any inductive constructor in-between.
    The cycle goes through the following variables:
    foo (ll. 2), foo (ll. 2), foo (ll. 1)
    help:
       1| lambda foo.
                 ^^^these variables are involved in the cycle
       2|   foo foo
            ^^^ ^^^
  [101]


As you can see above, the error message now includes the partial structure
and a list of variables from the source program that are involved in the cycle.


## Extensions

This concludes the tests that cover the mandatory part of the project.
We now move on to extensions.


## Annotation wildcards

We do not want to have to specify the whole type in the annotation, for several
reasons:
- it may be more precise than we want to specify (e.g. we just want to assert
that the type is an arrow, not fix its entire structure
- it disables polymorphism (as shown in a previous test, annotating the polymorphic
identity makes it no longer polymorphic)

My first (very easy) extension was to add the possibility to annotate types
only partially, by interpreting `_` as a hole in the annotation.
As shown here partial annotations can be merged together.

  $ minihell annot/wildcard.test
  Input term:
    λ x. let y = (x : _ → int) in let z = (x : {_ * _} → _) in x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ (∃?_ (?int = int) (?arr = ?_ → ?int).
            (∃_. ?x₁ = ?x ∧ ?x = ?arr) ∧ ?arr = ?wt₁ ∧ decode ?arr)
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ (∃ ?_₁ ?_₂ (?prod = {?_₁ * ?_₂}) ?_₃
              (?arr₁ = ?prod → ?_₃)
            . (∃_. ?x₂ = ?x ∧ ?x = ?arr₁) ∧ ?arr₁ = ?wt₂ ∧ decode ?arr₁)
            ∧ (∃_. ?x₃ = ?x ∧ ?x = ?wu₁)
            ∧ ?wu₁ = ?wu
            ∧ ?wpat₂ = ?wt₂)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    ({α * β} → int) → {α * β} → int
  
  Elaborated term:
    λ (x : {α * β} → int).
      let (y : {α * β} → int) = x in let (z : {α * β} → int) = x in x



## Type variable naming heuristics

This minor cosmetic change is to make the part of the algorithm in charge of
choosing names for type variables prefer `_` for values that are found to be
ignored in the program and `ø` for those that are found to be unreachable.
This is NOT
- a change to any actual part of the inference algorithm (only occurs at pretty-printing time)
- ambiguous (variables named `_` and `ø` still get their own unique identifier)
- always accurate (it is a heuristic, we show false negatives and positives below)


A function argument that is never used is given a type represented by the
type variable `_`.

  $ minihell naming/ignored-input.test
  Input term:
    λ x. λ y. y
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wt₁)))
      ∧ decode ?final_type
  
  Inferred type:
    _ → α → α
  
  Elaborated term:
    λ (x : _). λ (y : α). y


The same holds for a tuple field that is never used.

  $ minihell naming/ignored-field.test
  Input term:
    λ x. let (a, c) = x in a
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _.
          (∃_ _ (?prod = {?p * ?p₁}).
            ?prod = ?wpat₁ ∧ ?c = ?p₁ ∧ decode ?c ∧ ?a = ?p ∧ decode ?a)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {α * _} → α
  
  Elaborated term:
    λ (x : {α * _}). let ((a : α), (c : _)) = x in a


A sum variant that is never instanciated is given a type represented by
the type variable `ø`. One that is never read is once more given `_`.

  $ minihell naming/sparse-sum.test
  Input term:
    λ x. case x of | a ⇒ '3 a | b ⇒ '7 b esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = [?wp + ?wp₁]).
            ?ws = ?it
            ∧ (∃?b₃ _.
              (∃?b _.
                ?b₁ = ?wp₁
                ∧ decode ?b₁
                ∧ (∃ ø ø ø ø ø ø ø ?nth
                  (
                    ?ws₁
                    =
                    [?_
                    + ?_₁
                    + ?_₂
                    + ?_₃
                    + ?_₄
                    + ?_₅
                    + ?_₆
                    + ?nth
                    + …]
                  )
                . ?ws₁ = ?b ∧ (∃_. ?b₂ = ?b₁ ∧ ?b₁ = ?nth))
                ∧ ?b = ?wt)
              ∧ ?a = ?wp
              ∧ decode ?a
              ∧ (∃ ø ø ø ?nth₁
                (?ws₂ = [?_₇ + ?_₈ + ?_₉ + ?nth₁ + …])
              . ?ws₂ = ?b₃ ∧ (∃_. ?a₁ = ?a ∧ ?a = ?nth₁))
              ∧ ?b₃ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α + β]
      → [ø + ø₁ + ø₂ + α + ø₃ + ø₄ + ø₅ + β + …]
  
  Elaborated term:
    λ (x : [α + β]).
      case x of | (a : α) ⇒ '3 a | (b : β) ⇒ '7 b esac


  $ minihell naming/ignored-variant.test
  Input term:
    λ x. λ y. case y of | a ⇒ x | b ⇒ b esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?it.
            (∃_ _ (?ws = [?wp + ?wp₁]).
              ?ws = ?it
              ∧ (∃?b₃ _.
                (∃?b _.
                  ?b₁ = ?wp₁
                  ∧ decode ?b₁
                  ∧ (∃_. ?b₂ = ?b₁ ∧ ?b₁ = ?b)
                  ∧ ?b = ?wt₁)
                ∧ ?a = ?wp
                ∧ decode ?a
                ∧ (∃_. ?x₁ = ?x ∧ ?x = ?b₃)
                ∧ ?b₃ = ?wt₁))
            ∧ (∃_. ?y₁ = ?y ∧ ?y = ?it))))
      ∧ decode ?final_type
  
  Inferred type:
    α → [_ + α] → α
  
  Elaborated term:
    λ (x : α).
      λ (y : [_ + α]). case y of | (a : _) ⇒ x | (b : α) ⇒ b esac


As stated above this heuristic is not perfect.
An inference variable is considered "unused" mostly if it is not involved
in any equality constraint, so it is bad at detecting values that are transitively
unused:

  $ minihell known-bugs/ignored-twice.test
  Input term:
    λ x. λ y. let a = x in y
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ ?wu _.
            ?a = ?wpat₂
            ∧ decode ?a
            ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₂)
            ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wu)
            ∧ ?wu = ?wt₁
            ∧ ?wpat₂ = ?wt₂)))
      ∧ decode ?final_type
  
  Inferred type:
    α → β → β
  
  Elaborated term:
    λ (x : α). λ (y : β). let (a : α) = x in y

Could by typed `_ → β → β` but the equality constraint between `x` and `a`
disables this.


Deep partial annotations also produce values considered unused.

  $ minihell known-bugs/nested-wildcard-pat.test
  Input term:
    λ ((_, _) as y). y
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?y = ?wpat
        ∧ decode ?y
        ∧ (∃_ _ (?prod = {?p * ?p₁}). ?prod = ?y)
        ∧ ?warr = ?final_type
        ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    {_ * _₁} → {_ * _₁}
  
  Elaborated term:
    λ ((_, _) as (y : {_ * _₁})). y

Could be typed `{α * β} → {α * β}` but nowhere is any variable of type `α` or
`β` actually bound.




## Sum types

These have been hinted at in previous tests, but now we finally check their
implementation more thoroughly.
The solver has not changed, so we will not do any `--log-solver`.
However sum types involve some tricky syntax so it is important to check the
parser.

There is a deceptively large amount of things being tested in the following.
We need to check that
- `'2' is correctly parsed as a variant, for which the lexer and parser had to be modified
- the inference correctly places the inner term as the 3rd component of the sum
- the inferred type is unbounded (ends with `+ ...`) because we don't know how many

  $ minihell sum/inj.test
  Input term:
    λ x. '2 x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃ø ø ?nth (?ws = [?_ + ?_₁ + ?nth + …]).
          ?ws = ?wt ∧ (∃_. ?x₁ = ?x ∧ ?x = ?nth)))
      ∧ decode ?final_type
  
  Inferred type:
    α → [ø + ø₁ + α + …]
  
  Elaborated term:
    λ (x : α). '2 x


Then there's proper parsing of type annotations for sums, and simultaneously
checking the updated implementation of `( let-: )` to include Sum annotations.

  $ minihell annot/sum.test
  Input term:
    λ x. ('1 x : [a + b])
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃(?a = a) (?b = b) (?sum = [?a + ?b]).
          (∃ø ?nth (?ws = [?_ + ?nth + …]).
            ?ws = ?sum ∧ (∃_. ?x₁ = ?x ∧ ?x = ?nth))
          ∧ ?sum = ?wt
          ∧ decode ?sum))
      ∧ decode ?final_type
  
  Inferred type:
    b → [a + b]
  
  Elaborated term:
    λ (x : b). '1 x

Check parsing of `case _ of ... esac` as well, the parser should handle an
arbitrary (incl. zero) number of arms but in a first step we assume
at the `Infer` level that sum types are binary. A later extension will remove
that restriction.

  $ minihell sum/case.test
  Input term:
    λ x. case x of | x₁ ⇒ x₁ | y ⇒ y esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = [?wp + ?wp₁]).
            ?ws = ?it
            ∧ (∃?b₁ _.
              (∃?b _.
                ?y = ?wp₁ ∧ decode ?y ∧ (∃_. ?y₁ = ?y ∧ ?y = ?b) ∧ ?b = ?wt)
              ∧ ?x₁ = ?wp
              ∧ decode ?x₁
              ∧ (∃_. ?x₂ = ?x₁ ∧ ?x₁ = ?b₁)
              ∧ ?b₁ = ?wt))
          ∧ (∃_. ?x₃ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α + α] → α
  
  Elaborated term:
    λ (x : [α + α]).
      case x of | (x₁ : α) ⇒ x₁ | (y : α) ⇒ y esac


Now that the parser is good, we can check some more interesting functions.
Exchanging the position of the two types (a.k.a. commutativity of disjunction).

  $ minihell sum/swap.test
  Input term:
    λ x. case x of | a ⇒ '1 a | b ⇒ '0 b esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = [?wp + ?wp₁]).
            ?ws = ?it
            ∧ (∃?b₃ _.
              (∃?b _.
                ?b₁ = ?wp₁
                ∧ decode ?b₁
                ∧ (∃?nth (?ws₁ = [?nth + …]).
                  ?ws₁ = ?b ∧ (∃_. ?b₂ = ?b₁ ∧ ?b₁ = ?nth))
                ∧ ?b = ?wt)
              ∧ ?a = ?wp
              ∧ decode ?a
              ∧ (∃ø ?nth₁ (?ws₂ = [?_ + ?nth₁ + …]).
                ?ws₂ = ?b₃ ∧ (∃_. ?a₁ = ?a ∧ ?a = ?nth₁))
              ∧ ?b₃ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α + β] → [β + α + …]
  
  Elaborated term:
    λ (x : [α + β]).
      case x of | (a : α) ⇒ '1 a | (b : β) ⇒ '0 b esac

The general eliminator for disjunction:

  $ minihell sum/orelim.test
  Input term:
    λ x. λ f. λ g. case x of | l ⇒ f l | r ⇒ g r esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?f = ?wpat₁
          ∧ decode ?f
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?g = ?wpat₂
            ∧ decode ?g
            ∧ ?warr₂ = ?wt₁
            ∧ (∃?it.
              (∃_ _ (?ws = [?wp + ?wp₁]).
                ?ws = ?it
                ∧ (∃?b₁ _.
                  (∃?b _.
                    ?r = ?wp₁
                    ∧ decode ?r
                    ∧ (∃?wu ?wr (?wt₃ = ?wu → ?wr).
                      (∃_. ?g₁ = ?g ∧ ?g = ?wt₃)
                      ∧ (∃_. ?r₁ = ?r ∧ ?r = ?wu)
                      ∧ ?wr = ?b)
                    ∧ ?b = ?wt₂)
                  ∧ ?l = ?wp
                  ∧ decode ?l
                  ∧ (∃?wu₁ ?wr₁ (?wt₄ = ?wu₁ → ?wr₁).
                    (∃_. ?f₁ = ?f ∧ ?f = ?wt₄)
                    ∧ (∃_. ?l₁ = ?l ∧ ?l = ?wu₁)
                    ∧ ?wr₁ = ?b₁)
                  ∧ ?b₁ = ?wt₂))
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))))
      ∧ decode ?final_type
  
  Inferred type:
    [α + β] → (α → γ) → (β → γ) → γ
  
  Elaborated term:
    λ (x : [α + β]).
      λ (f : α → γ).
        λ (g : β → γ). case x of | (l : α) ⇒ f l | (r : β) ⇒ g r esac



The standard proof of distributivity of conjunction over disjunction, in other words
moving the first element of the tuple in and out of the sum.

We use a trick for mostly aesthetic reasons: `identity` is private and defined
just so that the solver is forced to unify the two input/output pairs together,
which enforces two important things:
- that the output types are exhaustive (`[{α * γ} + {α * β}]` rather than `[{α * γ} + {α * β} + ...]`)
- that the two functions are over the same types `α, γ, β`, and not some more general signature

  $ minihell sum/distrib.test
  Input term:
    let forward =
      λ abc.
          let (a₂, bc) = abc
          in case bc of | b₁ ⇒ '0 (a₂, b₁) | c₁ ⇒ '1 (a₂, c₁) esac
    in
    let reverse =
      λ abac.
          case abac of
          | ab ⇒ let (a, b) = ab in (a, '0 b)
          | ac ⇒ let (a₁, c) = ac in (a₁, '1 c)
          esac
    in
    let identity = λ x₁. reverse (forward x₁)
    in let identity₁ = λ x. forward (reverse x) in (forward, reverse)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?forward = ?wpat
        ∧ decode ?forward
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?abc = ?wpat₁
          ∧ decode ?abc
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _ _.
            (∃_ _ (?prod = {?p * ?p₁}).
              ?prod = ?wpat₂ ∧ ?bc = ?p₁ ∧ decode ?bc ∧ ?a = ?p ∧ decode ?a)
            ∧ (∃_. ?abc₁ = ?abc ∧ ?abc = ?wt₂)
            ∧ (∃?it.
              (∃_ _ (?ws = [?wp + ?wp₁]).
                ?ws = ?it
                ∧ (∃?b₁ _.
                  (∃?b _.
                    ?c = ?wp₁
                    ∧ decode ?c
                    ∧ (∃ø ?nth (?ws₁ = [?_ + ?nth + …]).
                      ?ws₁ = ?b
                      ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
                        (∃_. ?c₁ = ?c ∧ ?c = ?s₁)
                        ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
                        ∧ ?prod₁ = ?nth))
                    ∧ ?b = ?wu₁)
                  ∧ ?b₂ = ?wp
                  ∧ decode ?b₂
                  ∧ (∃?nth₁ (?ws₂ = [?nth₁ + …]).
                    ?ws₂ = ?b₁
                    ∧ (∃?s₂ ?s₃ (?prod₂ = {?s₂ * ?s₃}).
                      (∃_. ?b₃ = ?b₂ ∧ ?b₂ = ?s₃)
                      ∧ (∃_. ?a₂ = ?a ∧ ?a = ?s₂)
                      ∧ ?prod₂ = ?nth₁))
                  ∧ ?b₁ = ?wu₁))
              ∧ (∃_. ?bc₁ = ?bc ∧ ?bc = ?it))
            ∧ ?wu₁ = ?wt₁
            ∧ ?wpat₂ = ?wt₂))
        ∧ (∃?wpat₃ ?wt₃ ?wu₂ _.
          ?reverse = ?wpat₃
          ∧ decode ?reverse
          ∧ (∃?wpat₄ ?wt₄ (?warr₁ = ?wpat₄ → ?wt₄) _.
            ?abac = ?wpat₄
            ∧ decode ?abac
            ∧ ?warr₁ = ?wt₃
            ∧ (∃?it₁.
              (∃_ _ (?ws₃ = [?wp₂ + ?wp₃]).
                ?ws₃ = ?it₁
                ∧ (∃?b₅ _.
                  (∃?b₄ _.
                    ?ac = ?wp₃
                    ∧ decode ?ac
                    ∧ (∃?wpat₅ ?wt₅ ?wu₃ _ _.
                      (∃_ _ (?prod₃ = {?p₂ * ?p₃}).
                        ?prod₃ = ?wpat₅
                        ∧ ?c₂ = ?p₃
                        ∧ decode ?c₂
                        ∧ ?a₃ = ?p₂
                        ∧ decode ?a₃)
                      ∧ (∃_. ?ac₁ = ?ac ∧ ?ac = ?wt₅)
                      ∧ (∃?s₄ ?s₅ (?prod₄ = {?s₄ * ?s₅}).
                        (∃ø ?nth₂ (?ws₄ = [?_₁ + ?nth₂ + …]).
                          ?ws₄ = ?s₅ ∧ (∃_. ?c₃ = ?c₂ ∧ ?c₂ = ?nth₂))
                        ∧ (∃_. ?a₄ = ?a₃ ∧ ?a₃ = ?s₄)
                        ∧ ?prod₄ = ?wu₃)
                      ∧ ?wu₃ = ?b₄
                      ∧ ?wpat₅ = ?wt₅)
                    ∧ ?b₄ = ?wt₄)
                  ∧ ?ab = ?wp₂
                  ∧ decode ?ab
                  ∧ (∃?wpat₆ ?wt₆ ?wu₄ _ _.
                    (∃_ _ (?prod₅ = {?p₄ * ?p₅}).
                      ?prod₅ = ?wpat₆
                      ∧ ?b₆ = ?p₅
                      ∧ decode ?b₆
                      ∧ ?a₅ = ?p₄
                      ∧ decode ?a₅)
                    ∧ (∃_. ?ab₁ = ?ab ∧ ?ab = ?wt₆)
                    ∧ (∃?s₆ ?s₇ (?prod₆ = {?s₆ * ?s₇}).
                      (∃?nth₃ (?ws₅ = [?nth₃ + …]).
                        ?ws₅ = ?s₇ ∧ (∃_. ?b₇ = ?b₆ ∧ ?b₆ = ?nth₃))
                      ∧ (∃_. ?a₆ = ?a₅ ∧ ?a₅ = ?s₆)
                      ∧ ?prod₆ = ?wu₄)
                    ∧ ?wu₄ = ?b₅
                    ∧ ?wpat₆ = ?wt₆)
                  ∧ ?b₅ = ?wt₄))
              ∧ (∃_. ?abac₁ = ?abac ∧ ?abac = ?it₁)))
          ∧ (∃?wpat₇ ?wt₇ ?wu₅ _.
            ?identity = ?wpat₇
            ∧ decode ?identity
            ∧ (∃?wpat₈ ?wt₈ (?warr₂ = ?wpat₈ → ?wt₈) _.
              ?x = ?wpat₈
              ∧ decode ?x
              ∧ ?warr₂ = ?wt₇
              ∧ (∃?wu₆ ?wr (?wt₉ = ?wu₆ → ?wr).
                (∃_. ?reverse₁ = ?reverse ∧ ?reverse = ?wt₉)
                ∧ (∃?wu₇ ?wr₁ (?wt₁₀ = ?wu₇ → ?wr₁).
                  (∃_. ?forward₁ = ?forward ∧ ?forward = ?wt₁₀)
                  ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₇)
                  ∧ ?wr₁ = ?wu₆)
                ∧ ?wr = ?wt₈))
            ∧ (∃?wpat₉ ?wt₁₁ ?wu₈ _.
              ?identity₁ = ?wpat₉
              ∧ decode ?identity₁
              ∧ (∃?wpat₁₀ ?wt₁₂ (?warr₃ = ?wpat₁₀ → ?wt₁₂) _.
                ?x₂ = ?wpat₁₀
                ∧ decode ?x₂
                ∧ ?warr₃ = ?wt₁₁
                ∧ (∃?wu₉ ?wr₂ (?wt₁₃ = ?wu₉ → ?wr₂).
                  (∃_. ?forward₂ = ?forward ∧ ?forward = ?wt₁₃)
                  ∧ (∃?wu₁₀ ?wr₃ (?wt₁₄ = ?wu₁₀ → ?wr₃).
                    (∃_. ?reverse₂ = ?reverse ∧ ?reverse = ?wt₁₄)
                    ∧ (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?wu₁₀)
                    ∧ ?wr₃ = ?wu₉)
                  ∧ ?wr₂ = ?wt₁₂))
              ∧ (∃?s₈ ?s₉ (?prod₇ = {?s₈ * ?s₉}).
                (∃_. ?reverse₃ = ?reverse ∧ ?reverse = ?s₉)
                ∧ (∃_. ?forward₃ = ?forward ∧ ?forward = ?s₈)
                ∧ ?prod₇ = ?wu₈)
              ∧ ?wu₈ = ?wu₅
              ∧ ?wpat₉ = ?wt₁₁)
            ∧ ?wu₅ = ?wu₂
            ∧ ?wpat₇ = ?wt₇)
          ∧ ?wu₂ = ?wu
          ∧ ?wpat₃ = ?wt₃)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    {{α * [β + γ]} → [{α * β} + {α * γ}]
    * [{α * β} + {α * γ}] → {α * [β + γ]}}
  
  Elaborated term:
    let (forward : {α * [β + γ]} → [{α * β} + {α * γ}]) =
      λ (abc : {α * [β + γ]}).
          let ((a₂ : α), (bc : [β + γ])) = abc
          in
          case bc of
          | (b₁ : β) ⇒ '0 (a₂, b₁)
          | (c₁ : γ) ⇒ '1 (a₂, c₁)
          esac
    in
    let (reverse : [{α * β} + {α * γ}] → {α * [β + γ]}) =
      λ (abac : [{α * β} + {α * γ}]).
          case abac of
          | (ab : {α * β}) ⇒ let ((a : α), (b : β)) = ab in (a, '0 b)
          | (ac : {α * γ}) ⇒ let ((a₁ : α), (c : γ)) = ac in (a₁, '1 c)
          esac
    in
    let (identity : {α * [β + γ]} → {α * [β + γ]}) =
      λ (x₁ : {α * [β + γ]}). reverse (forward x₁)
    in
    let (identity₁ : [{α * β} + {α * γ}] → [{α * β} + {α * γ}]) =
      λ (x : [{α * β} + {α * γ}]). forward (reverse x)
    in (forward, reverse)



After sums of arbitrary arity were added, we can do a lot more.

  $ minihell sum/nary-match.test
  Input term:
    λ x.
      case x of
      | a ⇒ a
      | a₁ ⇒ a₁
      | a₂ ⇒ a₂
      | a₃ ⇒ a₃
      | a₄ ⇒ a₄
      | a₅ ⇒ a₅
      | a₆ ⇒ a₆
      | a₇ ⇒ a₇
      | a₈ ⇒ a₈
      | a₉ ⇒ a₉
      | a₁₀ ⇒ a₁₀
      | a₁₁ ⇒ a₁₁
      esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃ _ _ _ _ _ _ _ _ _ _ _ _
            (
              ?ws
              =
              [?wp
              + ?wp₁
              + ?wp₂
              + ?wp₃
              + ?wp₄
              + ?wp₅
              + ?wp₆
              + ?wp₇
              + ?wp₈
              + ?wp₉
              + ?wp₁₀
              + ?wp₁₁]
            )
          .
            ?ws = ?it
            ∧ (∃?b₁₁ _.
              (∃?b₁₀ _.
                (∃?b₉ _.
                  (∃?b₈ _.
                    (∃?b₇ _.
                      (∃?b₆ _.
                        (∃?b₅ _.
                          (∃?b₄ _.
                            (∃?b₃ _.
                              (∃?b₂ _.
                                (∃?b₁ _.
                                  (∃?b _.
                                    ?a = ?wp₁₁
                                    ∧ decode ?a
                                    ∧ (∃_. ?a₁ = ?a ∧ ?a = ?b)
                                    ∧ ?b = ?wt)
                                  ∧ ?a₂ = ?wp₁₀
                                  ∧ decode ?a₂
                                  ∧ (∃_. ?a₃ = ?a₂ ∧ ?a₂ = ?b₁)
                                  ∧ ?b₁ = ?wt)
                                ∧ ?a₄ = ?wp₉
                                ∧ decode ?a₄
                                ∧ (∃_. ?a₅ = ?a₄ ∧ ?a₄ = ?b₂)
                                ∧ ?b₂ = ?wt)
                              ∧ ?a₆ = ?wp₈
                              ∧ decode ?a₆
                              ∧ (∃_. ?a₇ = ?a₆ ∧ ?a₆ = ?b₃)
                              ∧ ?b₃ = ?wt)
                            ∧ ?a₈ = ?wp₇
                            ∧ decode ?a₈
                            ∧ (∃_. ?a₉ = ?a₈ ∧ ?a₈ = ?b₄)
                            ∧ ?b₄ = ?wt)
                          ∧ ?a₁₀ = ?wp₆
                          ∧ decode ?a₁₀
                          ∧ (∃_. ?a₁₁ = ?a₁₀ ∧ ?a₁₀ = ?b₅)
                          ∧ ?b₅ = ?wt)
                        ∧ ?a₁₂ = ?wp₅
                        ∧ decode ?a₁₂
                        ∧ (∃_. ?a₁₃ = ?a₁₂ ∧ ?a₁₂ = ?b₆)
                        ∧ ?b₆ = ?wt)
                      ∧ ?a₁₄ = ?wp₄
                      ∧ decode ?a₁₄
                      ∧ (∃_. ?a₁₅ = ?a₁₄ ∧ ?a₁₄ = ?b₇)
                      ∧ ?b₇ = ?wt)
                    ∧ ?a₁₆ = ?wp₃
                    ∧ decode ?a₁₆
                    ∧ (∃_. ?a₁₇ = ?a₁₆ ∧ ?a₁₆ = ?b₈)
                    ∧ ?b₈ = ?wt)
                  ∧ ?a₁₈ = ?wp₂
                  ∧ decode ?a₁₈
                  ∧ (∃_. ?a₁₉ = ?a₁₈ ∧ ?a₁₈ = ?b₉)
                  ∧ ?b₉ = ?wt)
                ∧ ?a₂₀ = ?wp₁
                ∧ decode ?a₂₀
                ∧ (∃_. ?a₂₁ = ?a₂₀ ∧ ?a₂₀ = ?b₁₀)
                ∧ ?b₁₀ = ?wt)
              ∧ ?a₂₂ = ?wp
              ∧ decode ?a₂₂
              ∧ (∃_. ?a₂₃ = ?a₂₂ ∧ ?a₂₂ = ?b₁₁)
              ∧ ?b₁₁ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α + α + α + α + α + α + α + α + α + α + α + α] → α
  
  Elaborated term:
    λ (x : [α + α + α + α + α + α + α + α + α + α + α + α]).
      case x of
      | (a : α) ⇒ a
      | (a₁ : α) ⇒ a₁
      | (a₂ : α) ⇒ a₂
      | (a₃ : α) ⇒ a₃
      | (a₄ : α) ⇒ a₄
      | (a₅ : α) ⇒ a₅
      | (a₆ : α) ⇒ a₆
      | (a₇ : α) ⇒ a₇
      | (a₈ : α) ⇒ a₈
      | (a₉ : α) ⇒ a₉
      | (a₁₀ : α) ⇒ a₁₀
      | (a₁₁ : α) ⇒ a₁₁
      esac


It is also interesting to check that we can do the standard things
with low-arity sum types:

* projection to the only variant

  $ minihell sum/unary-sum-projection.test
  Input term:
    λ x. case x of | y ⇒ y esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ (?ws = [?wp]).
            ?ws = ?it
            ∧ (∃?b _. ?y = ?wp ∧ decode ?y ∧ (∃_. ?y₁ = ?y ∧ ?y = ?b) ∧ ?b = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α] → α
  
  Elaborated term:
    λ (x : [α]). case x of | (y : α) ⇒ y esac

* explosion principle (`[]` the empty sum is False)

  $ minihell sum/explosion.test
  Input term:
    λ x. case x of  esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it. (∃(?ws = []). ?ws = ?it) ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [] → α
  
  Elaborated term:
    λ (x : []). case x of  esac


* double negation

  $ minihell sum/double-negation.test
  Input term:
    λ x. λ nx. (nx x : [])
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?nx = ?wpat₁
          ∧ decode ?nx
          ∧ ?warr₁ = ?wt
          ∧ (∃(?sum = []).
            (∃?wu ?wr (?wt₂ = ?wu → ?wr).
              (∃_. ?nx₁ = ?nx ∧ ?nx = ?wt₂)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu)
              ∧ ?wr = ?sum)
            ∧ ?sum = ?wt₁
            ∧ decode ?sum)))
      ∧ decode ?final_type
  
  Inferred type:
    α → (α → []) → []
  
  Elaborated term:
    λ (x : α). λ (nx : α → []). nx x

* double negation elimination.

  $ minihell sum/dne.test
  Input term:
    let (double_neg : _ → (_ → []) → []) = λ x₁. λ nx. nx x₁
    in λ (nnnx : ((_ → []) → []) → []). λ x. nnnx (double_neg x)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        (∃ ?_ ?_₁ (?sum = []) (?arr = ?_₁ → ?sum) (?sum₁ = [])
          (?arr₁ = ?arr → ?sum₁) (?arr₂ = ?_ → ?arr₁)
        .
          ?double_neg = ?arr₂
          ∧ decode ?double_neg
          ∧ ?arr₂ = ?wpat
          ∧ decode ?arr₂)
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?x = ?wpat₁
          ∧ decode ?x
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            ?nx = ?wpat₂
            ∧ decode ?nx
            ∧ ?warr₁ = ?wt₁
            ∧ (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
              (∃_. ?nx₁ = ?nx ∧ ?nx = ?wt₃)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
              ∧ ?wr = ?wt₂)))
        ∧ (∃?wpat₃ ?wt₄ (?warr₂ = ?wpat₃ → ?wt₄) _.
          (∃ ?_₂ (?sum₂ = []) (?arr₃ = ?_₂ → ?sum₂) (?sum₃ = [])
            (?arr₄ = ?arr₃ → ?sum₃) (?sum₄ = [])
            (?arr₅ = ?arr₄ → ?sum₄)
          . ?nnnx = ?arr₅ ∧ decode ?nnnx ∧ ?arr₅ = ?wpat₃ ∧ decode ?arr₅)
          ∧ ?warr₂ = ?wu
          ∧ (∃?wpat₄ ?wt₅ (?warr₃ = ?wpat₄ → ?wt₅) _.
            ?x₂ = ?wpat₄
            ∧ decode ?x₂
            ∧ ?warr₃ = ?wt₄
            ∧ (∃?wu₂ ?wr₁ (?wt₆ = ?wu₂ → ?wr₁).
              (∃_. ?nnnx₁ = ?nnnx ∧ ?nnnx = ?wt₆)
              ∧ (∃?wu₃ ?wr₂ (?wt₇ = ?wu₃ → ?wr₂).
                (∃_. ?double_neg₁ = ?double_neg ∧ ?double_neg = ?wt₇)
                ∧ (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?wu₃)
                ∧ ?wr₂ = ?wu₂)
              ∧ ?wr₁ = ?wt₅)))
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    (((α → []) → []) → []) → α → []
  
  Elaborated term:
    let (double_neg : α → (α → []) → []) =
      λ (x₁ : α). λ (nx : α → []). nx x₁
    in
    λ (nnnx : ((α → []) → []) → []).
      λ (x : α). nnnx (double_neg x)



* contrapositive

  $ minihell sum/contrapositive.test
  Input term:
    λ ab. λ bn. λ a. (bn (ab a) : [])
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?ab = ?wpat
        ∧ decode ?ab
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?bn = ?wpat₁
          ∧ decode ?bn
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?a = ?wpat₂
            ∧ decode ?a
            ∧ ?warr₂ = ?wt₁
            ∧ (∃(?sum = []).
              (∃?wu ?wr (?wt₃ = ?wu → ?wr).
                (∃_. ?bn₁ = ?bn ∧ ?bn = ?wt₃)
                ∧ (∃?wu₁ ?wr₁ (?wt₄ = ?wu₁ → ?wr₁).
                  (∃_. ?ab₁ = ?ab ∧ ?ab = ?wt₄)
                  ∧ (∃_. ?a₁ = ?a ∧ ?a = ?wu₁)
                  ∧ ?wr₁ = ?wu)
                ∧ ?wr = ?sum)
              ∧ ?sum = ?wt₂
              ∧ decode ?sum))))
      ∧ decode ?final_type
  
  Inferred type:
    (α → β) → (β → []) → α → []
  
  Elaborated term:
    λ (ab : α → β). λ (bn : β → []). λ (a : α). bn (ab a)

See also some improved versions of these same functions in
`library.t/logic.test`.


A `case` must be exhaustive, and we must raise an error if not all cases are handled.
Here we try to give an input with at least 3 variants to a `case` that expects at most 2.

  $ minihell sum/should-fail/non-exhaustive.test
  Input term:
    case '2 (λ x₂. x₂) of | x ⇒ x | x₁ ⇒ x₁ esac
  
  Generated constraint:
    ∃?final_type.
      (∃?it.
        (∃_ _ (?ws = [?wp + ?wp₁]).
          ?ws = ?it
          ∧ (∃?b₁ _.
            (∃?b _.
              ?x = ?wp₁
              ∧ decode ?x
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?b)
              ∧ ?b = ?final_type)
            ∧ ?x₂ = ?wp
            ∧ decode ?x₂
            ∧ (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?b₁)
            ∧ ?b₁ = ?final_type))
        ∧ (∃ø ø ?nth (?ws₁ = [?_ + ?_₁ + ?nth + …]).
          ?ws₁ = ?it
          ∧ (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
            ?x₄ = ?wpat
            ∧ decode ?x₄
            ∧ ?warr = ?nth
            ∧ (∃_. ?x₅ = ?x₄ ∧ ?x₄ = ?wt))))
      ∧ decode ?final_type
  
  Error:
      (1)  [ø + ø₁ + β + …]
    incompatible with
      (2)  [α + α]
    help:
       1| case '2 (lambda x. x) of
               ^^^^^^^^^^^^^^^^found to impose structure (1)
       1| case '2 (lambda x. x) of
          ^^^^^^^^^^^^^^^^^^^^^^^^...
            ...
       4| esac
          ^^^^incompatible with structure (2)
    hint: missing index 2
  [101]


## Tuples of higher arity

Once facilities for nary sums are in place, nary tuples are not much different.
The parsing for those is already implemented too.

  $ minihell tuple/unit.test
  Input term:
    (λ y. λ x. x) ()
  
  Generated constraint:
    ∃?final_type.
      (∃?wu ?wr (?wt = ?wu → ?wr).
        (∃?wpat ?wt₁ (?warr = ?wpat → ?wt₁) _.
          ?y = ?wpat
          ∧ decode ?y
          ∧ ?warr = ?wt
          ∧ (∃?wpat₁ ?wt₂ (?warr₁ = ?wpat₁ → ?wt₂) _.
            ?x = ?wpat₁
            ∧ decode ?x
            ∧ ?warr₁ = ?wt₁
            ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₂)))
        ∧ (∃(?prod = {}). ?prod = ?wu)
        ∧ ?wr = ?final_type)
      ∧ decode ?final_type
  
  Inferred type:
    α → α
  
  Elaborated term:
    (λ (y : {}). λ (x : α). x) ()


  $ minihell tuple/bigprod.test
  Input term:
    λ p. let (a, b, c, d, e, f) = p in (f, e, d, c, b, a)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?p = ?wpat
        ∧ decode ?p
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _ _ _ _ _.
          (∃_ _ _ _ _ _ (?prod = {?p₁ * ?p₂ * ?p₃ * ?p₄ * ?p₅ * ?p₆}).
            ?prod = ?wpat₁
            ∧ ?f = ?p₆
            ∧ decode ?f
            ∧ ?e = ?p₅
            ∧ decode ?e
            ∧ ?d = ?p₄
            ∧ decode ?d
            ∧ ?c = ?p₃
            ∧ decode ?c
            ∧ ?b = ?p₂
            ∧ decode ?b
            ∧ ?a = ?p₁
            ∧ decode ?a)
          ∧ (∃_. ?p₇ = ?p ∧ ?p = ?wt₁)
          ∧ (∃ ?s ?s₁ ?s₂ ?s₃ ?s₄ ?s₅
            (?prod₁ = {?s * ?s₁ * ?s₂ * ?s₃ * ?s₄ * ?s₅})
          .
            (∃_. ?a₁ = ?a ∧ ?a = ?s₅)
            ∧ (∃_. ?b₁ = ?b ∧ ?b = ?s₄)
            ∧ (∃_. ?c₁ = ?c ∧ ?c = ?s₃)
            ∧ (∃_. ?d₁ = ?d ∧ ?d = ?s₂)
            ∧ (∃_. ?e₁ = ?e ∧ ?e = ?s₁)
            ∧ (∃_. ?f₁ = ?f ∧ ?f = ?s)
            ∧ ?prod₁ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {α * β * γ * δ * α₁ * β₁}
      → {β₁ * α₁ * δ * γ * β * α}
  
  Elaborated term:
    λ (p : {α * β * γ * δ * α₁ * β₁}).
      let ((a : α), (b : β), (c : γ), (d : δ), (e : α₁), (f : β₁)) =
        p
      in (f, e, d, c, b, a)

And we get the same length compatibility guarantees.

  $ minihell tuple/should-fail/length-mismatch.test
  Input term:
    λ x. let (a, b, c) = x in let (a₁, b₁) = x in ()
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _ _.
          (∃_ _ _ (?prod = {?p * ?p₁ * ?p₂}).
            ?prod = ?wpat₁
            ∧ ?c = ?p₂
            ∧ decode ?c
            ∧ ?b = ?p₁
            ∧ decode ?b
            ∧ ?a = ?p
            ∧ decode ?a)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _ _.
            (∃_ _ (?prod₁ = {?p₃ * ?p₄}).
              ?prod₁ = ?wpat₂
              ∧ ?b₁ = ?p₄
              ∧ decode ?b₁
              ∧ ?a₁ = ?p₃
              ∧ decode ?a₁)
            ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wt₂)
            ∧ (∃(?prod₂ = {}). ?prod₂ = ?wu₁)
            ∧ ?wu₁ = ?wu
            ∧ ?wpat₂ = ?wt₂)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Error:
      (1)  {_₂ * _₃ * _₄}
    incompatible with
      (2)  {_ * _₁}
    help:
       2|   let (a, b, c) = x in
                ^^^^^^^^^found to impose structure (1)
       3|   let (a, b) = x in
                ^^^^^^incompatible with structure (2)
    hint: missing 2
  [101]


## Patterns

As was suggested in the README, patterns did indeed seem like a natural
step after nary tuples. Only irrefutable patterns are implemented for `let`,
so no sum types in patterns.
Patterns are allowed in the following positions:
- `let <pattern> = <term> in <term>`
- `lambda <pattern> . <term>`
- `case <term> of | <pattern> => <term> esac`
and we briefly check parsing for all of these syntaxes.
Interesting edge cases are when the pattern is nested or is `()`.

  $ minihell pattern/let-pat-unit.test
  Input term:
    λ x. let () = x in x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu.
          (∃(?prod = {}). ?prod = ?wpat₁)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {} → {}
  
  Elaborated term:
    λ (x : {}). let () = x in x


  $ minihell pattern/let-pat-nested.test
  Input term:
    λ a. let (x, y, (_, z, (w,))) = a in (x, y, z, w)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?a = ?wpat
        ∧ decode ?a
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _ _ _.
          (∃_ _ _ (?prod = {?p * ?p₁ * ?p₂}).
            ?prod = ?wpat₁
            ∧ (∃_ _ _ (?prod₁ = {?p₃ * ?p₄ * ?p₅}).
              ?prod₁ = ?p₂
              ∧ (∃_ (?prod₂ = {?p₆}).
                ?prod₂ = ?p₅ ∧ ?w = ?p₆ ∧ decode ?w)
              ∧ ?z = ?p₄
              ∧ decode ?z)
            ∧ ?y = ?p₁
            ∧ decode ?y
            ∧ ?x = ?p
            ∧ decode ?x)
          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?wt₁)
          ∧ (∃?s ?s₁ ?s₂ ?s₃ (?prod₃ = {?s * ?s₁ * ?s₂ * ?s₃}).
            (∃_. ?w₁ = ?w ∧ ?w = ?s₃)
            ∧ (∃_. ?z₁ = ?z ∧ ?z = ?s₂)
            ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
            ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
            ∧ ?prod₃ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {α * β * {_ * γ * {δ}}} → {α * β * γ * δ}
  
  Elaborated term:
    λ (a : {α * β * {_ * γ * {δ}}}).
      let ((x : α), (y : β), (_, (z : γ), ((w : δ),))) = a in (x, y, z, w)

Parsing is nontrivial, so we check that we correctly distinguish between
atomic and nonatomic patterns.

  $ minihell pattern/atomic-pats.test
  Input term:
    λ ((x : a), ((u, v) as y)). (x, y, u, v)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _ _ _ _.
        (∃_ _ (?prod = {?p * ?p₁}).
          ?prod = ?wpat
          ∧ ?y = ?p₁
          ∧ decode ?y
          ∧ (∃_ _ (?prod₁ = {?p₂ * ?p₃}).
            ?prod₁ = ?y ∧ ?v = ?p₃ ∧ decode ?v ∧ ?u = ?p₂ ∧ decode ?u)
          ∧ (∃(?a = a). ?x = ?a ∧ decode ?x ∧ ?a = ?p ∧ decode ?a))
        ∧ ?warr = ?final_type
        ∧ (∃?s ?s₁ ?s₂ ?s₃ (?prod₂ = {?s * ?s₁ * ?s₂ * ?s₃}).
          (∃_. ?v₁ = ?v ∧ ?v = ?s₃)
          ∧ (∃_. ?u₁ = ?u ∧ ?u = ?s₂)
          ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
          ∧ ?prod₂ = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    {a * {α * β}} → {a * {α * β} * α * β}
  
  Elaborated term:
    λ ((x : a), (((u : α), (v : β)) as (y : {α * β}))). (x, y, u, v)


  $ minihell pattern/lambda-pat-unit.test
  Input term:
    λ (). λ (). ()
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt).
        (∃(?prod = {}). ?prod = ?wpat)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁).
          (∃(?prod₁ = {}). ?prod₁ = ?wpat₁)
          ∧ ?warr₁ = ?wt
          ∧ (∃(?prod₂ = {}). ?prod₂ = ?wt₁)))
      ∧ decode ?final_type
  
  Inferred type:
    {} → {} → {}
  
  Elaborated term:
    λ (). λ (). ()


  $ minihell pattern/lambda-pat-nested.test
  Input term:
    λ (x, y). λ (a, (b, (c, (d,)))). (x, y, a, b, c, d)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _ _.
        (∃_ _ (?prod = {?p * ?p₁}).
          ?prod = ?wpat ∧ ?y = ?p₁ ∧ decode ?y ∧ ?x = ?p ∧ decode ?x)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _ _ _ _.
          (∃_ _ (?prod₁ = {?p₂ * ?p₃}).
            ?prod₁ = ?wpat₁
            ∧ (∃_ _ (?prod₂ = {?p₄ * ?p₅}).
              ?prod₂ = ?p₃
              ∧ (∃_ _ (?prod₃ = {?p₆ * ?p₇}).
                ?prod₃ = ?p₅
                ∧ (∃_ (?prod₄ = {?p₈}).
                  ?prod₄ = ?p₇ ∧ ?d = ?p₈ ∧ decode ?d)
                ∧ ?c = ?p₆
                ∧ decode ?c)
              ∧ ?b = ?p₄
              ∧ decode ?b)
            ∧ ?a = ?p₂
            ∧ decode ?a)
          ∧ ?warr₁ = ?wt
          ∧ (∃ ?s ?s₁ ?s₂ ?s₃ ?s₄ ?s₅
            (?prod₅ = {?s * ?s₁ * ?s₂ * ?s₃ * ?s₄ * ?s₅})
          .
            (∃_. ?d₁ = ?d ∧ ?d = ?s₅)
            ∧ (∃_. ?c₁ = ?c ∧ ?c = ?s₄)
            ∧ (∃_. ?b₁ = ?b ∧ ?b = ?s₃)
            ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s₂)
            ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
            ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
            ∧ ?prod₅ = ?wt₁)))
      ∧ decode ?final_type
  
  Inferred type:
    {α * β}
      → {γ * {δ * {α₁ * {β₁}}}}
        → {α * β * γ * δ * α₁ * β₁}
  
  Elaborated term:
    λ ((x : α), (y : β)).
      λ ((a : γ), ((b : δ), ((c : α₁), ((d : β₁),)))).
        (x, y, a, b, c, d)


  $ minihell pattern/case-pat-unit.test
  Input term:
    λ x. case x of | () ⇒ () | () ⇒ () | () ⇒ () esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ _ (?ws = [?wp + ?wp₁ + ?wp₂]).
            ?ws = ?it
            ∧ (∃?b₂.
              (∃?b₁.
                (∃?b.
                  (∃(?prod = {}). ?prod = ?wp₂)
                  ∧ (∃(?prod₁ = {}). ?prod₁ = ?b)
                  ∧ ?b = ?wt)
                ∧ (∃(?prod₂ = {}). ?prod₂ = ?wp₁)
                ∧ (∃(?prod₃ = {}). ?prod₃ = ?b₁)
                ∧ ?b₁ = ?wt)
              ∧ (∃(?prod₄ = {}). ?prod₄ = ?wp)
              ∧ (∃(?prod₅ = {}). ?prod₅ = ?b₂)
              ∧ ?b₂ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [{} + {} + {}] → {}
  
  Elaborated term:
    λ (x : [{} + {} + {}]).
      case x of | () ⇒ () | () ⇒ () | () ⇒ () esac


  $ minihell pattern/case-pat-nested.test
  Input term:
    λ x.
      case x of
      | (a,) ⇒ a
      | (a₁, b) ⇒ (a₁, b)
      | (a₂, b₁, c) ⇒ ((a₂, b₁), c)
      | (a₃, b₂, c₁, d) ⇒ (((a₃, b₂), c₁), d)
      esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ _ _ (?ws = [?wp + ?wp₁ + ?wp₂ + ?wp₃]).
            ?ws = ?it
            ∧ (∃?b₉ _.
              (∃?b₆ _ _.
                (∃?b₃ _ _ _.
                  (∃?b _ _ _ _.
                    (∃_ _ _ _ (?prod = {?p * ?p₁ * ?p₂ * ?p₃}).
                      ?prod = ?wp₃
                      ∧ ?d = ?p₃
                      ∧ decode ?d
                      ∧ ?c = ?p₂
                      ∧ decode ?c
                      ∧ ?b₁ = ?p₁
                      ∧ decode ?b₁
                      ∧ ?a = ?p
                      ∧ decode ?a)
                    ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
                      (∃_. ?d₁ = ?d ∧ ?d = ?s₁)
                      ∧ (∃?s₂ ?s₃ (?prod₂ = {?s₂ * ?s₃}).
                        (∃_. ?c₁ = ?c ∧ ?c = ?s₃)
                        ∧ (∃?s₄ ?s₅ (?prod₃ = {?s₄ * ?s₅}).
                          (∃_. ?b₂ = ?b₁ ∧ ?b₁ = ?s₅)
                          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s₄)
                          ∧ ?prod₃ = ?s₂)
                        ∧ ?prod₂ = ?s)
                      ∧ ?prod₁ = ?b)
                    ∧ ?b = ?wt)
                  ∧ (∃_ _ _ (?prod₄ = {?p₄ * ?p₅ * ?p₆}).
                    ?prod₄ = ?wp₂
                    ∧ ?c₂ = ?p₆
                    ∧ decode ?c₂
                    ∧ ?b₄ = ?p₅
                    ∧ decode ?b₄
                    ∧ ?a₂ = ?p₄
                    ∧ decode ?a₂)
                  ∧ (∃?s₆ ?s₇ (?prod₅ = {?s₆ * ?s₇}).
                    (∃_. ?c₃ = ?c₂ ∧ ?c₂ = ?s₇)
                    ∧ (∃?s₈ ?s₉ (?prod₆ = {?s₈ * ?s₉}).
                      (∃_. ?b₅ = ?b₄ ∧ ?b₄ = ?s₉)
                      ∧ (∃_. ?a₃ = ?a₂ ∧ ?a₂ = ?s₈)
                      ∧ ?prod₆ = ?s₆)
                    ∧ ?prod₅ = ?b₃)
                  ∧ ?b₃ = ?wt)
                ∧ (∃_ _ (?prod₇ = {?p₇ * ?p₈}).
                  ?prod₇ = ?wp₁
                  ∧ ?b₇ = ?p₈
                  ∧ decode ?b₇
                  ∧ ?a₄ = ?p₇
                  ∧ decode ?a₄)
                ∧ (∃?s₁₀ ?s₁₁ (?prod₈ = {?s₁₀ * ?s₁₁}).
                  (∃_. ?b₈ = ?b₇ ∧ ?b₇ = ?s₁₁)
                  ∧ (∃_. ?a₅ = ?a₄ ∧ ?a₄ = ?s₁₀)
                  ∧ ?prod₈ = ?b₆)
                ∧ ?b₆ = ?wt)
              ∧ (∃_ (?prod₉ = {?p₉}).
                ?prod₉ = ?wp ∧ ?a₆ = ?p₉ ∧ decode ?a₆)
              ∧ (∃_. ?a₇ = ?a₆ ∧ ?a₆ = ?b₉)
              ∧ ?b₉ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [{{{{α * β} * γ} * δ}}
    + {{{α * β} * γ} * δ}
    + {{α * β} * γ * δ}
    + {α * β * γ * δ}]
      → {{{α * β} * γ} * δ}
  
  Elaborated term:
    λ (
      x :
        [{{{{α * β} * γ} * δ}}
        + {{{α * β} * γ} * δ}
        + {{α * β} * γ * δ}
        + {α * β * γ * δ}]
    ).
      case x of
      | ((a : {{{α * β} * γ} * δ}),) ⇒ a
      | ((a₁ : {{α * β} * γ}), (b : δ)) ⇒ (a₁, b)
      | ((a₂ : {α * β}), (b₁ : γ), (c : δ)) ⇒ ((a₂, b₁), c)
      | ((a₃ : α), (b₂ : β), (c₁ : γ), (d : δ)) ⇒
        (((a₃, b₂), c₁), d)
      esac

  $ minihell pattern/pat-annot.test
  Input term:
    λ x. case x of | ((a : a → _), b) ⇒ a b | (_ : b) ⇒ () esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = [?wp + ?wp₁]).
            ?ws = ?it
            ∧ (∃?b₂ _ _.
              (∃?b.
                (∃(?b₁ = b). ?b₁ = ?wp₁ ∧ decode ?b₁)
                ∧ (∃(?prod = {}). ?prod = ?b)
                ∧ ?b = ?wt)
              ∧ (∃_ _ (?prod₁ = {?p * ?p₁}).
                ?prod₁ = ?wp
                ∧ ?b₃ = ?p₁
                ∧ decode ?b₃
                ∧ (∃(?a₁ = a) ?_ (?arr = ?a₁ → ?_).
                  ?a = ?arr ∧ decode ?a ∧ ?arr = ?p ∧ decode ?arr))
              ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
                (∃_. ?a₂ = ?a ∧ ?a = ?wt₁)
                ∧ (∃_. ?b₄ = ?b₃ ∧ ?b₃ = ?wu)
                ∧ ?wr = ?b₂)
              ∧ ?b₂ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [{a → {} * a} + b] → {}
  
  Elaborated term:
    λ (x : [{a → {} * a} + b]).
      case x of | ((a : a → {}), (b : a)) ⇒ a b | _ ⇒ () esac

  $ minihell pattern/as.test
  Input term:
    λ x. let ((a, _) as b) = x in (a, b)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _.
          ?b = ?wpat₁
          ∧ decode ?b
          ∧ (∃_ _ (?prod = {?p * ?p₁}). ?prod = ?b ∧ ?a = ?p ∧ decode ?a)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
            (∃_. ?b₁ = ?b ∧ ?b = ?s₁)
            ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
            ∧ ?prod₁ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {α * _} → {α * {α * _}}
  
  Elaborated term:
    λ (x : {α * _}). let (((a : α), _) as (b : {α * _})) = x in (a, b)


## More destructors

We now have the full expressivity of nary tuples and sums, but none of the convenience.
Here are some extra operators to help with manipulating those terms.

Field accesses on tuples

  $ minihell tuple/field-access.test
  Input term:
    λ x. (x.2, x.4)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
          (∃_ _ _ _ ?nth (?wp = {?_ * ?_₁ * ?_₂ * ?_₃ * ?nth * …}).
            ?nth = ?s₁ ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wp))
          ∧ (∃_ _ ?nth₁ (?wp₁ = {?_₄ * ?_₅ * ?nth₁ * …}).
            ?nth₁ = ?s ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wp₁))
          ∧ ?prod = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    {_ * _₁ * α * _₂ * β * …} → {α * β}
  
  Elaborated term:
    λ (x : {_ * _₁ * α * _₂ * β * …}). (x.2, x.4)


There was a bug in the parser with the precedence of field accesses.

  $ minihell parsing/field-precedence.test
  Input term:
    λ x. λ y. x y.1
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?wu ?wr (?wt₂ = ?wu → ?wr).
            (∃_. ?x₁ = ?x ∧ ?x = ?wt₂)
            ∧ (∃_ ?nth (?wp = {?_ * ?nth * …}).
              ?nth = ?wu ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wp))
            ∧ ?wr = ?wt₁)))
      ∧ decode ?final_type
  
  Inferred type:
    (α → β) → {_ * α * …} → β
  
  Elaborated term:
    λ (x : α → β). λ (y : {_ * α * …}). x y.1


Annotations can use the `...` syntax too.

  $ minihell annot/should-fail/approx-annot.test
  Input term:
    λ (x : {_ * _ → _ * _ * …}). let (y, z) = x in (y, z)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃ ?_ ?_₁ ?_₂ (?arr = ?_₁ → ?_₂) ?_₃
          (?prod = {?_ * ?arr * ?_₃ * …})
        . ?x = ?prod ∧ decode ?x ∧ ?prod = ?wpat ∧ decode ?prod)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _.
          (∃_ _ (?prod₁ = {?p * ?p₁}).
            ?prod₁ = ?wpat₁ ∧ ?z = ?p₁ ∧ decode ?z ∧ ?y = ?p ∧ decode ?y)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?s ?s₁ (?prod₂ = {?s * ?s₁}).
            (∃_. ?z₁ = ?z ∧ ?z = ?s₁)
            ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s)
            ∧ ?prod₂ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Error:
      (1)  {α₁ * β₁}
    incompatible with
      (2)  {α * β → γ * δ * …}
    help:
       2|   let (y, z) = x in (y, z)
                ^^^^^^found to impose structure (1)
       1| λ (x : {_ * _ → _ * _ * …}).
                  ^^^^^^^^^^^^^^^^^^^^^^^incompatible with structure (2)
    hint: extraneous 2
  [101]


## Labeled tuples and sums

So far our tuples and sums were indexed. The constuctor of products and
the destructor of sums were positional only.
We now introduce a variant of the same types but with named fields/variants.
Because the inference algorithm is fully generic with respect to labels,
we basically need only check that they are correctly parsed, because once they
are in the AST the rest of the algorithm is mostly oblivious to the difference
between the two.

  $ minihell label/named-field.test
  Input term:
    λ x. let y = x.foo in let z = x.bar in (y, z)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ (∃?nth (?wp = {foo:?nth * …}).
            ?nth = ?wt₁ ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wp))
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ (∃?nth₁ (?wp₁ = {bar:?nth₁ * …}).
              ?nth₁ = ?wt₂ ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wp₁))
            ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
              (∃_. ?z₁ = ?z ∧ ?z = ?s₁)
              ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s)
              ∧ ?prod = ?wu₁)
            ∧ ?wu₁ = ?wu
            ∧ ?wpat₂ = ?wt₂)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {bar:α * foo:β * …} → {β * α}
  
  Elaborated term:
    λ (x : {bar:α * foo:β * …}).
      let (y : β) = x.foo in let (z : α) = x.bar in (y, z)

  $ minihell label/named-tup.test
  Input term:
    λ x. λ y. λ z. let tup = (x=x, y=y, z=z) in tup
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ ?warr₂ = ?wt₁
            ∧ (∃?wpat₃ ?wt₃ ?wu _.
              ?tup = ?wpat₃
              ∧ decode ?tup
              ∧ (∃?s ?s₁ ?s₂ (?prod = {x:?s * y:?s₁ * z:?s₂}).
                (∃_. ?z₁ = ?z ∧ ?z = ?s₂)
                ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
                ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
                ∧ ?prod = ?wt₃)
              ∧ (∃_. ?tup₁ = ?tup ∧ ?tup = ?wu)
              ∧ ?wu = ?wt₂
              ∧ ?wpat₃ = ?wt₃))))
      ∧ decode ?final_type
  
  Inferred type:
    α → β → γ → {x:α * y:β * z:γ}
  
  Elaborated term:
    λ (x : α).
      λ (y : β).
        λ (z : γ). let (tup : {x:α * y:β * z:γ}) = (x=x, y=y, z=z) in tup

  $ minihell label/named-lettup.test
  Input term:
    λ t. let (x=a, y=b, z=c) = t in (a, b, c)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?t = ?wpat
        ∧ decode ?t
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _ _.
          (∃_ _ _ (?prod = {x:?p * y:?p₁ * z:?p₂}).
            ?prod = ?wpat₁
            ∧ ?c = ?p₂
            ∧ decode ?c
            ∧ ?b = ?p₁
            ∧ decode ?b
            ∧ ?a = ?p
            ∧ decode ?a)
          ∧ (∃_. ?t₁ = ?t ∧ ?t = ?wt₁)
          ∧ (∃?s ?s₁ ?s₂ (?prod₁ = {?s * ?s₁ * ?s₂}).
            (∃_. ?c₁ = ?c ∧ ?c = ?s₂)
            ∧ (∃_. ?b₁ = ?b ∧ ?b = ?s₁)
            ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
            ∧ ?prod₁ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {x:α * y:β * z:γ} → {α * β * γ}
  
  Elaborated term:
    λ (t : {x:α * y:β * z:γ}).
      let (x=(a : α), y=(b : β), z=(c : γ)) = t in (a, b, c)

  $ minihell label/named-variant.test
  Input term:
    λ x. case x of | a ⇒ 'Left a | b ⇒ 'Right b esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = [?wp + ?wp₁]).
            ?ws = ?it
            ∧ (∃?b₃ _.
              (∃?b _.
                ?b₁ = ?wp₁
                ∧ decode ?b₁
                ∧ (∃?nth (?ws₁ = ['Right:?nth + …]).
                  ?ws₁ = ?b ∧ (∃_. ?b₂ = ?b₁ ∧ ?b₁ = ?nth))
                ∧ ?b = ?wt)
              ∧ ?a = ?wp
              ∧ decode ?a
              ∧ (∃?nth₁ (?ws₂ = ['Left:?nth₁ + …]).
                ?ws₂ = ?b₃ ∧ (∃_. ?a₁ = ?a ∧ ?a = ?nth₁))
              ∧ ?b₃ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    [α + β] → ['Left:α + 'Right:β + …]
  
  Elaborated term:
    λ (x : [α + β]).
      case x of | (a : α) ⇒ 'Left a | (b : β) ⇒ 'Right b esac

  $ minihell label/named-match.test
  Input term:
    λ x. case x of | 'C a ⇒ 'A a | 'B b ⇒ 'B b | 'A c ⇒ 'C c esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ _ (?ws = ['A:?wp + 'B:?wp₁ + 'C:?wp₂]).
            ?ws = ?it
            ∧ (∃?b₄ _.
              (∃?b₁ _.
                (∃?b _.
                  ?a = ?wp₂
                  ∧ decode ?a
                  ∧ (∃?nth (?ws₁ = ['A:?nth + …]).
                    ?ws₁ = ?b ∧ (∃_. ?a₁ = ?a ∧ ?a = ?nth))
                  ∧ ?b = ?wt)
                ∧ ?b₂ = ?wp₁
                ∧ decode ?b₂
                ∧ (∃?nth₁ (?ws₂ = ['B:?nth₁ + …]).
                  ?ws₂ = ?b₁ ∧ (∃_. ?b₃ = ?b₂ ∧ ?b₂ = ?nth₁))
                ∧ ?b₁ = ?wt)
              ∧ ?c = ?wp
              ∧ decode ?c
              ∧ (∃?nth₂ (?ws₃ = ['C:?nth₂ + …]).
                ?ws₃ = ?b₄ ∧ (∃_. ?c₁ = ?c ∧ ?c = ?nth₂))
              ∧ ?b₄ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    ['A:α + 'B:β + 'C:γ] → ['A:γ + 'B:β + 'C:α + …]
  
  Elaborated term:
    λ (x : ['A:α + 'B:β + 'C:γ]).
      case x of
      | 'A (c : α) ⇒ 'C c
      | 'B (b : β) ⇒ 'B b
      | 'C (a : γ) ⇒ 'A a
      esac

  $ minihell sum/should-fail/partial-named-match.test
  Input term:
    case 'Foo () of | 'Bar () ⇒ () | 'Quux _ ⇒ () esac
  
  Generated constraint:
    ∃?final_type.
      (∃?it.
        (∃_ _ (?ws = ['Bar:?wp + 'Quux:?wp₁]).
          ?ws = ?it
          ∧ (∃?b₁.
            (∃?b. (∃(?prod = {}). ?prod = ?b) ∧ ?b = ?final_type)
            ∧ (∃(?prod₁ = {}). ?prod₁ = ?wp)
            ∧ (∃(?prod₂ = {}). ?prod₂ = ?b₁)
            ∧ ?b₁ = ?final_type))
        ∧ (∃?nth (?ws₁ = ['Foo:?nth + …]).
          ?ws₁ = ?it ∧ (∃(?prod₃ = {}). ?prod₃ = ?nth)))
      ∧ decode ?final_type
  
  Error:
      (1)  ['Foo:α + …]
    incompatible with
      (2)  ['Bar:{} + 'Quux:_]
    help:
       1| case 'Foo () of
               ^^^^^^^found to impose structure (1)
       1| case 'Foo () of
          ^^^^^^^^^^^^^^^...
            ...
       4| esac
          ^^^^incompatible with structure (2)
    hint: missing label Foo
  [101]


  $ minihell pattern/should-fail/partial-named-let.test
  Input term:
    λ x. let (a=a, b=b, c=c) = x in let (a=a₁, b=b₁) = x in x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _ _.
          (∃_ _ _ (?prod = {a:?p * b:?p₁ * c:?p₂}).
            ?prod = ?wpat₁
            ∧ ?c = ?p₂
            ∧ decode ?c
            ∧ ?b = ?p₁
            ∧ decode ?b
            ∧ ?a = ?p
            ∧ decode ?a)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _ _.
            (∃_ _ (?prod₁ = {a:?p₃ * b:?p₄}).
              ?prod₁ = ?wpat₂
              ∧ ?b₁ = ?p₄
              ∧ decode ?b₁
              ∧ ?a₁ = ?p₃
              ∧ decode ?a₁)
            ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wt₂)
            ∧ (∃_. ?x₃ = ?x ∧ ?x = ?wu₁)
            ∧ ?wu₁ = ?wu
            ∧ ?wpat₂ = ?wt₂)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Error:
      (1)  {a:_₂ * b:_₃ * c:_₄}
    incompatible with
      (2)  {a:_ * b:_₁}
    help:
       2|   let (a=a, b=b, c=c) = x in
                ^^^^^^^^^^^^^^^found to impose structure (1)
       3|   let (a=a, b=b) = x in
                ^^^^^^^^^^incompatible with structure (2)
    hint: missing label c
  [101]

  $ minihell annot/should-fail/named-annotation.test
  Input term:
    λ (x : ['A:a + 'B:b]). case x of | 'A a ⇒ a esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?a = a) (?b = b) (?sum = ['A:?a + 'B:?b]).
          ?x = ?sum ∧ decode ?x ∧ ?sum = ?wpat ∧ decode ?sum)
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ (?ws = ['A:?wp]).
            ?ws = ?it
            ∧ (∃?b₁ _.
              ?a₁ = ?wp
              ∧ decode ?a₁
              ∧ (∃_. ?a₂ = ?a₁ ∧ ?a₁ = ?b₁)
              ∧ ?b₁ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Error:
      (1)  ['A:a + 'B:b]
    incompatible with
      (2)  ['A:α]
    help:
       1| lambda (x : ['A:a + 'B:b]).
                      ^^^^^^^^^^^^^found to impose structure (1)
       2|   case x of
            ^^^^^^^^^...
            ...
       4|   esac
          ^^^^^^incompatible with structure (2)
    hint: missing label B
  [101]

Here's a subtlety: for the grammar to be unambiguous we need to parse
`()`, `{}` and `[]` as the *indexed* unit, product, and sum respectively.
But if it turns out later to be part of a *labeled* type then a coercion
to the other needs to be done.
E.g. `{…}` parsed as the empty underapproximation of an indexed product can coerce to
any (but not all simultaneously) of the following:
- `{}` the exact empty indexed sum
- `{}` the exact empty labeled product ¹
- `{…}` the empty underapproximation of an indexed product ¹
- `{a * …}` and everything it coerces to (`{a}`, `{a * b}`, `{a * b * …}`, `{a * b * c}`, ...)
- `{a:a * …}` and everything it coerces to (`{a=a}`, `{a=a * b=b}`, `{a=a * b=b * …}`, ...)

¹. not expressible in the grammar but exists as an internal representation in
the inference algorithm.


  $ minihell parsing/labeled-approx-empty.test
  Input term:
    λ (x : […]). case x of | 'A _ ⇒ () esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?sum = […]). ?x = ?sum ∧ decode ?x ∧ ?sum = ?wpat ∧ decode ?sum)
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ (?ws = ['A:?wp]).
            ?ws = ?it ∧ (∃?b. (∃(?prod = {}). ?prod = ?b) ∧ ?b = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
      ∧ decode ?final_type
  
  Inferred type:
    ['A:_] → {}
  
  Elaborated term:
    λ (x : ['A:_]). case x of | 'A _ ⇒ () esac


  $ minihell parsing/labeled-approx-unit.test
  Input term:
    λ (x : {…}). x.foo
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?prod = {…}). ?x = ?prod ∧ decode ?x ∧ ?prod = ?wpat ∧ decode ?prod)
        ∧ ?warr = ?final_type
        ∧ (∃?nth (?wp = {foo:?nth * …}).
          ?nth = ?wt ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wp)))
      ∧ decode ?final_type
  
  Inferred type:
    {foo:α * …} → α
  
  Elaborated term:
    λ (x : {foo:α * …}). x.foo

  $ minihell parsing/drop-all-fields.test
  Input term:
    λ x. let (…) = (a=x, b=x, c=x) in ()
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu.
          (∃(?prod = {…}). ?prod = ?wpat₁)
          ∧ (∃?s ?s₁ ?s₂ (?prod₁ = {a:?s * b:?s₁ * c:?s₂}).
            (∃_. ?x₁ = ?x ∧ ?x = ?s₂)
            ∧ (∃_. ?x₂ = ?x ∧ ?x = ?s₁)
            ∧ (∃_. ?x₃ = ?x ∧ ?x = ?s)
            ∧ ?prod₁ = ?wt₁)
          ∧ (∃(?prod₂ = {}). ?prod₂ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    α → {}
  
  Elaborated term:
    λ (x : α). let (…) = (a=x, b=x, c=x) in ()


Patterns are also improved with labeled tuples.

  $ minihell pattern/drop-fields.test
  Input term:
    λ x. let (a=a, b=b, …) = x in (a, b)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _.
          (∃_ _ (?prod = {a:?p * b:?p₁ * …}).
            ?prod = ?wpat₁ ∧ ?b = ?p₁ ∧ decode ?b ∧ ?a = ?p ∧ decode ?a)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
            (∃_. ?b₁ = ?b ∧ ?b = ?s₁)
            ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
            ∧ ?prod₁ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    {a:α * b:β * …} → {α * β}
  
  Elaborated term:
    λ (x : {a:α * b:β * …}).
      let (a=(a : α), b=(b : β), …) = x in (a, b)

  $ minihell sum/default-case.test
  Input term:
    λ x.
      case x of | 'Yes y ⇒ 'Yes y | 'No n ⇒ 'No n | … ⇒ 'Other () esac
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?it.
          (∃_ _ (?ws = ['No:?wp + 'Yes:?wp₁ + …]).
            ?ws = ?it
            ∧ (∃?b₁ _.
              (∃?b _.
                ?y = ?wp₁
                ∧ decode ?y
                ∧ (∃?nth (?ws₁ = ['Yes:?nth + …]).
                  ?ws₁ = ?b ∧ (∃_. ?y₁ = ?y ∧ ?y = ?nth))
                ∧ ?b = ?wt)
              ∧ ?n = ?wp
              ∧ decode ?n
              ∧ (∃?nth₁ (?ws₂ = ['No:?nth₁ + …]).
                ?ws₂ = ?b₁ ∧ (∃_. ?n₁ = ?n ∧ ?n = ?nth₁))
              ∧ ?b₁ = ?wt))
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)
          ∧ (∃?nth₂ (?ws₃ = ['Other:?nth₂ + …]).
            ?ws₃ = ?wt ∧ (∃(?prod = {}). ?prod = ?nth₂))))
      ∧ decode ?final_type
  
  Inferred type:
    ['No:α + 'Yes:β + …] → ['No:α + 'Other:{} + 'Yes:β + …]
  
  Elaborated term:
    λ (x : ['No:α + 'Yes:β + …]).
      case x of
      | 'No (n : α) ⇒ 'No n
      | 'Yes (y : β) ⇒ 'Yes y
      | … ⇒ 'Other ()
      esac


And so are sums (we also introduce the `if let` destructor that
provides something like a shallow refutable pattern).

  $ minihell sum/iflet.test
  Input term:
    λ x. if let 'Foo (a, _, c) = x then a c else () fi
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃_ _ _ (?sum = ['Foo:?nth + …]).
          (∃_. ?x₁ = ?x ∧ ?x = ?sum)
          ∧ (∃_ _ _ (?prod = {?p * ?p₁ * ?p₂}).
            ?prod = ?nth ∧ ?c = ?p₂ ∧ decode ?c ∧ ?a = ?p ∧ decode ?a)
          ∧ (∃?wu ?wr (?wt₁ = ?wu → ?wr).
            (∃_. ?a₁ = ?a ∧ ?a = ?wt₁)
            ∧ (∃_. ?c₁ = ?c ∧ ?c = ?wu)
            ∧ ?wr = ?wt)
          ∧ (∃(?prod₁ = {}). ?prod₁ = ?wt)))
      ∧ decode ?final_type
  
  Inferred type:
    ['Foo:{α → {} * _ * α} + …] → {}
  
  Elaborated term:
    λ (x : ['Foo:{α → {} * _ * α} + …]).
      if let 'Foo ((a : α → {}), _, (c : α)) = x then a c else () fi

  $ minihell sum/should-fail/dup-case.test
  Input term:
    λ x. case x of | 'Foo y ⇒ y | 'Foo y₁ ⇒ y₁ esac
  
  Fatal error:
    There is a duplicate label
       2|   case x of
            ^^^^^^^^^...
            ...
       5|   esac
          ^^^^^^label Foo appears twice
  [101]



We also introduce a new operator `with`.
As explained in `REPORT.md`, I'm kind of disappointed that I failed to make
it more general, but this is still fine.

  $ minihell tuple/with.test
  Input term:
    λ x.
      λ y. λ z. λ w. ((x with a=w), (y with 1=w), (z with n=w, m=w, k=w))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ ?warr₂ = ?wt₁
            ∧ (∃?wpat₃ ?wt₃ (?warr₃ = ?wpat₃ → ?wt₃) _.
              ?w = ?wpat₃
              ∧ decode ?w
              ∧ ?warr₃ = ?wt₂
              ∧ (∃?s ?s₁ ?s₂ (?prod = {?s * ?s₁ * ?s₂}).
                (∃_. ?z₁ = ?z ∧ ?z = ?s₂)
                ∧ (∃ ?s₃ ?s₄ ?s₅
                  (?prod₁ = {k:?s₃ * m:?s₄ * n:?s₅ * …})
                .
                  (∃_. ?w₁ = ?w ∧ ?w = ?s₅)
                  ∧ (∃_. ?w₂ = ?w ∧ ?w = ?s₄)
                  ∧ (∃_. ?w₃ = ?w ∧ ?w = ?s₃)
                  ∧ ?prod₁ = ?s₂)
                ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
                ∧ (∃?s₆ (?prod₂ = {1:?s₆ * …}).
                  (∃_. ?w₄ = ?w ∧ ?w = ?s₆) ∧ ?prod₂ = ?s₁)
                ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
                ∧ (∃?s₇ (?prod₃ = {a:?s₇ * …}).
                  (∃_. ?w₅ = ?w ∧ ?w = ?s₇) ∧ ?prod₃ = ?s)
                ∧ ?prod = ?wt₃)))))
      ∧ decode ?final_type
  
  Inferred type:
    {a:α * …}
      → {1:α * …}
        → {k:α * m:α * n:α * …}
          → α → {{a:α * …} * {1:α * …} * {k:α * m:α * n:α * …}}
  
  Elaborated term:
    λ (x : {a:α * …}).
      λ (y : {1:α * …}).
        λ (z : {k:α * m:α * n:α * …}).
          λ (w : α). ((x with a=w), (y with 1=w), (z with k=w, m=w, n=w))



## Induction

The rest of the tests are devoted to the main feature of my project: inductive
types. This combines previously introduced sums and products with new syntaxes for
- defining a new inductive type
- folding and unfolding its definition

Several simplifying design choices are made
- all foldings and unfoldings are made explicit
- all inductive types have exactly one constructor
- this constructor has the same name as the type
- unfolding is lazy (no need for special syntax for mutually recursive types)

Interestingly, the hard part of the above is not the induction but the unfolding.
Once we have a way to fold and unfold type definitions, induction comes for free.
My experience is that although it required quite a lot of modifications in `Infer.ml`,
it was quite easy to get right. This is of course dependent on already having
efficient ways of handling higher arities and structure, so this was only easy
thanks to prior improvements.



We can check that type definitions are correctly parsed, and that the same
type can be instanciated with several different parameters.

  $ minihell rec/simple.test
  Input term:
    type <a, b>Foo of {a * b} in λ x. λ y. ((Foo (x, y)), (Foo (y, x)))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
            (∃?v ?v₁ (?prod₁ = {?v * ?v₁}) (?opaque = <?v, ?v₁>Foo).
              (∃?s₂ ?s₃ (?prod₂ = {?s₂ * ?s₃}).
                (∃_. ?x₁ = ?x ∧ ?x = ?s₃)
                ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₂)
                ∧ ?prod₂ = ?prod₁)
              ∧ ?s₁ = ?opaque)
            ∧ (∃ ?v₂ ?v₃ (?prod₃ = {?v₂ * ?v₃})
              (?opaque₁ = <?v₂, ?v₃>Foo)
            .
              (∃?s₄ ?s₅ (?prod₄ = {?s₄ * ?s₅}).
                (∃_. ?y₂ = ?y ∧ ?y = ?s₅)
                ∧ (∃_. ?x₂ = ?x ∧ ?x = ?s₄)
                ∧ ?prod₄ = ?prod₃)
              ∧ ?s = ?opaque₁)
            ∧ ?prod = ?wt₁)))
      ∧ decode ?final_type
  
  Inferred type:
    α → β → {<α, β>Foo * <β, α>Foo}
  
  Elaborated term:
    type <a, b>Foo of {a * b} in
    λ (x : α). λ (y : β). ((Foo (x, y)), (Foo (y, x)))


By virtue of unfolding the definition as a structure, we can have the type
definion enforce some custom equality constraints.

  $ minihell rec/should-fail/unequal.test
  Input term:
    type <a>Pair of {a * a} in λ (x : xt). λ (y : yt). (Pair (x, y))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?xt = xt). ?x = ?xt ∧ decode ?x ∧ ?xt = ?wpat ∧ decode ?xt)
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          (∃(?yt = yt). ?y = ?yt ∧ decode ?y ∧ ?yt = ?wpat₁ ∧ decode ?yt)
          ∧ ?warr₁ = ?wt
          ∧ (∃?v (?prod = {?v * ?v}) (?opaque = <?v>Pair).
            (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
              (∃_. ?y₁ = ?y ∧ ?y = ?s₁)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
              ∧ ?prod₁ = ?prod)
            ∧ ?wt₁ = ?opaque)))
      ∧ decode ?final_type
  
  Error:
      (1)  yt
    incompatible with
      (2)  xt
    help:
       2| lambda (x : xt) (y : yt).
                               ^^found to impose structure (1)
       2| lambda (x : xt) (y : yt).
                      ^^incompatible with structure (2)
    hint: rigid type variables `yt` and `xt` are unequal
  [101]


I have observed that once we have type definitions as above and the ability
to fold them to produce a `Structure` that is self-contained, we gain induction
for free. I had to implement literally nothing to go from type definitions
to inductive type definitions.

  $ minihell rec/list.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    λ x.
      λ y.
        λ z.
          (List 'Cons (x, (List 'Cons (y, (List 'Cons (z, (List 'Nil ())))))))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?y = ?wpat₁
          ∧ decode ?y
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?z = ?wpat₂
            ∧ decode ?z
            ∧ ?warr₂ = ?wt₁
            ∧ (∃ ?v (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
              (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
            .
              (∃?nth (?ws = ['Cons:?nth + …]).
                ?ws = ?sum
                ∧ (∃?s ?s₁ (?prod₂ = {?s * ?s₁}).
                  (∃ ?v₁ (?alias₁ = <?v₁>List)
                    (?prod₃ = {?v₁ * ?alias₁}) (?prod₄ = {})
                    (?sum₁ = ['Cons:?prod₃ + 'Nil:?prod₄])
                    (?opaque₁ = <?v₁>List)
                  .
                    (∃?nth₁ (?ws₁ = ['Cons:?nth₁ + …]).
                      ?ws₁ = ?sum₁
                      ∧ (∃?s₂ ?s₃ (?prod₅ = {?s₂ * ?s₃}).
                        (∃ ?v₂ (?alias₂ = <?v₂>List)
                          (?prod₆ = {?v₂ * ?alias₂}) (?prod₇ = {})
                          (?sum₂ = ['Cons:?prod₆ + 'Nil:?prod₇])
                          (?opaque₂ = <?v₂>List)
                        .
                          (∃?nth₂ (?ws₂ = ['Cons:?nth₂ + …]).
                            ?ws₂ = ?sum₂
                            ∧ (∃?s₄ ?s₅ (?prod₈ = {?s₄ * ?s₅}).
                              (∃ ?v₃ (?alias₃ = <?v₃>List)
                                (?prod₉ = {?v₃ * ?alias₃})
                                (?prod₁₀ = {})
                                (?sum₃ = ['Cons:?prod₉ + 'Nil:?prod₁₀])
                                (?opaque₃ = <?v₃>List)
                              .
                                (∃?nth₃ (?ws₃ = ['Nil:?nth₃ + …]).
                                  ?ws₃ = ?sum₃
                                  ∧ (∃(?prod₁₁ = {}). ?prod₁₁ = ?nth₃))
                                ∧ ?s₅ = ?opaque₃)
                              ∧ (∃_. ?z₁ = ?z ∧ ?z = ?s₄)
                              ∧ ?prod₈ = ?nth₂))
                          ∧ ?s₃ = ?opaque₂)
                        ∧ (∃_. ?y₁ = ?y ∧ ?y = ?s₂)
                        ∧ ?prod₅ = ?nth₁))
                    ∧ ?s₁ = ?opaque₁)
                  ∧ (∃_. ?x₁ = ?x ∧ ?x = ?s)
                  ∧ ?prod₂ = ?nth))
              ∧ ?wt₂ = ?opaque))))
      ∧ decode ?final_type
  
  Inferred type:
    α → α → α → <α>List
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    λ (x : α).
      λ (y : α).
        λ (z : α).
          (List 'Cons (x, (List 'Cons (y, (List 'Cons (z, (List 'Nil ())))))))

  $ minihell rec/emptylist.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in (List 'Nil ())
  
  Generated constraint:
    ∃?final_type.
      (∃ ?v (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
        (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
      .
        (∃?nth (?ws = ['Nil:?nth + …]).
          ?ws = ?sum ∧ (∃(?prod₂ = {}). ?prod₂ = ?nth))
        ∧ ?final_type = ?opaque)
      ∧ decode ?final_type
  
  Inferred type:
    <α>List
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in (List 'Nil ())

Type constructors have a weird place in the atomic/nonatomic term syntax.
We check that the following term is correctly parsed.

  $ minihell parsing/alias-precedence.test
  Input term:
    type <a>Pair of {a * a} in λ f. λ x. λ y. (Pair f x y)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?f = ?wpat
        ∧ decode ?f
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
          ?x = ?wpat₁
          ∧ decode ?x
          ∧ ?warr₁ = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₂ = ?wpat₂ → ?wt₂) _.
            ?y = ?wpat₂
            ∧ decode ?y
            ∧ ?warr₂ = ?wt₁
            ∧ (∃?v (?prod = {?v * ?v}) (?opaque = <?v>Pair).
              (∃?wu ?wr (?wt₃ = ?wu → ?wr).
                (∃?wu₁ ?wr₁ (?wt₄ = ?wu₁ → ?wr₁).
                  (∃_. ?f₁ = ?f ∧ ?f = ?wt₄)
                  ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
                  ∧ ?wr₁ = ?wt₃)
                ∧ (∃_. ?y₁ = ?y ∧ ?y = ?wu)
                ∧ ?wr = ?prod)
              ∧ ?wt₂ = ?opaque))))
      ∧ decode ?final_type
  
  Inferred type:
    (α → β → {γ * γ}) → α → β → <γ>Pair
  
  Elaborated term:
    type <a>Pair of {a * a} in
    λ (f : α → β → {γ * γ}). λ (x : α). λ (y : β). (Pair f x y)

  $ minihell rec/func.test
  Input term:
    type <>B of ['False:{} + 'True:{}] in
    type <a>Cmp of a → a → <>B in (Cmp λ x. λ y. (B 'True ()))
  
  Generated constraint:
    ∃?final_type.
      (∃ ?v (?alias = <>B) (?arr = ?v → ?alias) (?arr₁ = ?v → ?arr)
        (?opaque = <?v>Cmp)
      .
        (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
          ?x = ?wpat
          ∧ decode ?x
          ∧ ?warr = ?arr₁
          ∧ (∃?wpat₁ ?wt₁ (?warr₁ = ?wpat₁ → ?wt₁) _.
            ?y = ?wpat₁
            ∧ decode ?y
            ∧ ?warr₁ = ?wt
            ∧ (∃ (?prod = {}) (?prod₁ = {})
              (?sum = ['False:?prod + 'True:?prod₁]) (?opaque₁ = <>B)
            .
              (∃?nth (?ws = ['True:?nth + …]).
                ?ws = ?sum ∧ (∃(?prod₂ = {}). ?prod₂ = ?nth))
              ∧ ?wt₁ = ?opaque₁)))
        ∧ ?final_type = ?opaque)
      ∧ decode ?final_type
  
  Inferred type:
    <α>Cmp
  
  Elaborated term:
    type <>B of ['False:{} + 'True:{}] in
    type <a>Cmp of a → a → <>B in
    (Cmp λ (x : α). λ (y : α). (B 'True ()))


  $ minihell rec/let.test
  Input term:
    type <a>Pair of {a * a} in λ x. let (Pair (a, b)) = x in (a, b)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?wpat₁ ?wt₁ ?wu _ _.
          (∃_ (?prod = {?v * ?v}) (?opaque = <?v>Pair).
            (∃_ _ (?prod₁ = {?p * ?p₁}).
              ?prod₁ = ?prod ∧ ?b = ?p₁ ∧ decode ?b ∧ ?a = ?p ∧ decode ?a)
            ∧ ?wpat₁ = ?opaque)
          ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt₁)
          ∧ (∃?s ?s₁ (?prod₂ = {?s * ?s₁}).
            (∃_. ?b₁ = ?b ∧ ?b = ?s₁)
            ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
            ∧ ?prod₂ = ?wu)
          ∧ ?wu = ?wt
          ∧ ?wpat₁ = ?wt₁))
      ∧ decode ?final_type
  
  Inferred type:
    <α>Pair → {α * α}
  
  Elaborated term:
    type <a>Pair of {a * a} in
    λ (x : <α>Pair). let (Pair ((a : α), (b : α))) = x in (a, b)

  $ minihell rec/is-empty.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <>Bool of ['False:{} + 'True:{}] in
    let is_empty =
      λ (List l).
          case l of
          | 'Nil _ ⇒ (Bool 'True ())
          | 'Cons _ ⇒ (Bool 'False ())
          esac
    in is_empty
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?is_empty = ?wpat
        ∧ decode ?is_empty
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          (∃ _ (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
            (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
          . ?l = ?sum ∧ decode ?l ∧ ?wpat₁ = ?opaque)
          ∧ ?warr = ?wt
          ∧ (∃?it.
            (∃_ _ (?ws = ['Cons:?wp + 'Nil:?wp₁]).
              ?ws = ?it
              ∧ (∃?b₁.
                (∃?b.
                  (∃ (?prod₂ = {}) (?prod₃ = {})
                    (?sum₁ = ['False:?prod₂ + 'True:?prod₃])
                    (?opaque₁ = <>Bool)
                  .
                    (∃?nth (?ws₁ = ['True:?nth + …]).
                      ?ws₁ = ?sum₁ ∧ (∃(?prod₄ = {}). ?prod₄ = ?nth))
                    ∧ ?b = ?opaque₁)
                  ∧ ?b = ?wt₁)
                ∧ (∃ (?prod₅ = {}) (?prod₆ = {})
                  (?sum₂ = ['False:?prod₅ + 'True:?prod₆])
                  (?opaque₂ = <>Bool)
                .
                  (∃?nth₁ (?ws₂ = ['False:?nth₁ + …]).
                    ?ws₂ = ?sum₂ ∧ (∃(?prod₇ = {}). ?prod₇ = ?nth₁))
                  ∧ ?b₁ = ?opaque₂)
                ∧ ?b₁ = ?wt₁))
            ∧ (∃_. ?l₁ = ?l ∧ ?l = ?it)))
        ∧ (∃_. ?is_empty₁ = ?is_empty ∧ ?is_empty = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    <_>List → <>Bool
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <>Bool of ['False:{} + 'True:{}] in
    let (is_empty : <_>List → <>Bool) =
      λ (List (l : ['Cons:{_ * <_>List} + 'Nil:{}])).
          case l of
          | 'Cons _ ⇒ (Bool 'False ())
          | 'Nil _ ⇒ (Bool 'True ())
          esac
    in is_empty

Finally our first recursive function. It can easily manipulate inductive types.

  $ minihell rec/tree.test
  Input term:
    type <n, l>Tree of ['Leaf:l + 'Node:{<n, l>Tree * n * <n, l>Tree}] in
    let rec map =
      λ f.
          λ g.
            λ (Tree t).
              case t of
              | 'Leaf l ⇒ (Tree 'Leaf (f l))
              | 'Node (left, data, right) ⇒
                (Tree 'Node (map f g left, g data, map f g right))
              esac
    in map
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?map = ?wpat
        ∧ decode ?map
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?f = ?wpat₁
          ∧ decode ?f
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            ?g = ?wpat₂
            ∧ decode ?g
            ∧ ?warr₁ = ?wt₁
            ∧ (∃?wpat₃ ?wt₃ (?warr₂ = ?wpat₃ → ?wt₃) _.
              (∃ _ _ (?alias = <?v, ?v₁>Tree) (?alias₁ = <?v, ?v₁>Tree)
                (?prod = {?alias * ?v * ?alias₁})
                (?sum = ['Leaf:?v₁ + 'Node:?prod]) (?opaque = <?v, ?v₁>Tree)
              . ?t = ?sum ∧ decode ?t ∧ ?wpat₃ = ?opaque)
              ∧ ?warr₂ = ?wt₂
              ∧ (∃?it.
                (∃_ _ (?ws = ['Leaf:?wp + 'Node:?wp₁]).
                  ?ws = ?it
                  ∧ (∃?b₁ _.
                    (∃?b _ _ _.
                      (∃_ _ _ (?prod₁ = {?p * ?p₁ * ?p₂}).
                        ?prod₁ = ?wp₁
                        ∧ ?right = ?p₂
                        ∧ decode ?right
                        ∧ ?data = ?p₁
                        ∧ decode ?data
                        ∧ ?left = ?p
                        ∧ decode ?left)
                      ∧ (∃ ?v₂ ?v₃ (?alias₂ = <?v₂, ?v₃>Tree)
                        (?alias₃ = <?v₂, ?v₃>Tree)
                        (?prod₂ = {?alias₂ * ?v₂ * ?alias₃})
                        (?sum₁ = ['Leaf:?v₃ + 'Node:?prod₂])
                        (?opaque₁ = <?v₂, ?v₃>Tree)
                      .
                        (∃?nth (?ws₁ = ['Node:?nth + …]).
                          ?ws₁ = ?sum₁
                          ∧ (∃?s ?s₁ ?s₂ (?prod₃ = {?s * ?s₁ * ?s₂}).
                            (∃?wu₁ ?wr (?wt₄ = ?wu₁ → ?wr).
                              (∃?wu₂ ?wr₁ (?wt₅ = ?wu₂ → ?wr₁).
                                (∃?wu₃ ?wr₂ (?wt₆ = ?wu₃ → ?wr₂).
                                  (∃_. ?map₁ = ?map ∧ ?map = ?wt₆)
                                  ∧ (∃_. ?f₁ = ?f ∧ ?f = ?wu₃)
                                  ∧ ?wr₂ = ?wt₅)
                                ∧ (∃_. ?g₁ = ?g ∧ ?g = ?wu₂)
                                ∧ ?wr₁ = ?wt₄)
                              ∧ (∃_. ?right₁ = ?right ∧ ?right = ?wu₁)
                              ∧ ?wr = ?s₂)
                            ∧ (∃?wu₄ ?wr₃ (?wt₇ = ?wu₄ → ?wr₃).
                              (∃_. ?g₂ = ?g ∧ ?g = ?wt₇)
                              ∧ (∃_. ?data₁ = ?data ∧ ?data = ?wu₄)
                              ∧ ?wr₃ = ?s₁)
                            ∧ (∃?wu₅ ?wr₄ (?wt₈ = ?wu₅ → ?wr₄).
                              (∃?wu₆ ?wr₅ (?wt₉ = ?wu₆ → ?wr₅).
                                (∃?wu₇ ?wr₆ (?wt₁₀ = ?wu₇ → ?wr₆).
                                  (∃_. ?map₂ = ?map ∧ ?map = ?wt₁₀)
                                  ∧ (∃_. ?f₂ = ?f ∧ ?f = ?wu₇)
                                  ∧ ?wr₆ = ?wt₉)
                                ∧ (∃_. ?g₃ = ?g ∧ ?g = ?wu₆)
                                ∧ ?wr₅ = ?wt₈)
                              ∧ (∃_. ?left₁ = ?left ∧ ?left = ?wu₅)
                              ∧ ?wr₄ = ?s)
                            ∧ ?prod₃ = ?nth))
                        ∧ ?b = ?opaque₁)
                      ∧ ?b = ?wt₃)
                    ∧ ?l = ?wp
                    ∧ decode ?l
                    ∧ (∃ ?v₄ ?v₅ (?alias₄ = <?v₄, ?v₅>Tree)
                      (?alias₅ = <?v₄, ?v₅>Tree)
                      (?prod₄ = {?alias₄ * ?v₄ * ?alias₅})
                      (?sum₂ = ['Leaf:?v₅ + 'Node:?prod₄])
                      (?opaque₂ = <?v₄, ?v₅>Tree)
                    .
                      (∃?nth₁ (?ws₂ = ['Leaf:?nth₁ + …]).
                        ?ws₂ = ?sum₂
                        ∧ (∃?wu₈ ?wr₇ (?wt₁₁ = ?wu₈ → ?wr₇).
                          (∃_. ?f₃ = ?f ∧ ?f = ?wt₁₁)
                          ∧ (∃_. ?l₁ = ?l ∧ ?l = ?wu₈)
                          ∧ ?wr₇ = ?nth₁))
                      ∧ ?b₁ = ?opaque₂)
                    ∧ ?b₁ = ?wt₃))
                ∧ (∃_. ?t₁ = ?t ∧ ?t = ?it)))))
        ∧ (∃_. ?map₃ = ?map ∧ ?map = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    (α → β) → (γ → δ) → <γ, α>Tree → <δ, β>Tree
  
  Elaborated term:
    type <n, l>Tree of ['Leaf:l + 'Node:{<n, l>Tree * n * <n, l>Tree}] in
    let rec (
      map : (α → β) → (γ → δ) → <γ, α>Tree → <δ, β>Tree
    ) =
      λ (f : α → β).
          λ (g : γ → δ).
            λ (Tree (t : ['Leaf:α + 'Node:{<γ, α>Tree * γ * <γ, α>Tree}])).
              case t of
              | 'Leaf (l : α) ⇒ (Tree 'Leaf (f l))
              | 'Node ((left : <γ, α>Tree), (data : γ), (right : <γ, α>Tree)) ⇒
                (Tree 'Node (map f g left, g data, map f g right))
              esac
    in map

  $ minihell rec/rev.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    let rev =
      λ l.
          let rec aux =
            λ acc.
                λ (List l₁).
                  case l₁ of
                  | 'Nil () ⇒ acc
                  | 'Cons (a, tail) ⇒ aux (List 'Cons (a, acc)) tail
                  esac
          in aux (List 'Nil ()) l
    in rev
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?rev = ?wpat
        ∧ decode ?rev
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?l = ?wpat₁
          ∧ decode ?l
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ ?wu₁ _.
            ?aux = ?wpat₂
            ∧ decode ?aux
            ∧ (∃?wpat₃ ?wt₃ (?warr₁ = ?wpat₃ → ?wt₃) _.
              ?acc = ?wpat₃
              ∧ decode ?acc
              ∧ ?warr₁ = ?wt₂
              ∧ (∃?wpat₄ ?wt₄ (?warr₂ = ?wpat₄ → ?wt₄) _.
                (∃ _ (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
                  (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
                . ?l₁ = ?sum ∧ decode ?l₁ ∧ ?wpat₄ = ?opaque)
                ∧ ?warr₂ = ?wt₃
                ∧ (∃?it.
                  (∃_ _ (?ws = ['Cons:?wp + 'Nil:?wp₁]).
                    ?ws = ?it
                    ∧ (∃?b₁ _ _.
                      (∃?b.
                        (∃(?prod₂ = {}). ?prod₂ = ?wp₁)
                        ∧ (∃_. ?acc₁ = ?acc ∧ ?acc = ?b)
                        ∧ ?b = ?wt₄)
                      ∧ (∃_ _ (?prod₃ = {?p * ?p₁}).
                        ?prod₃ = ?wp
                        ∧ ?tail = ?p₁
                        ∧ decode ?tail
                        ∧ ?a = ?p
                        ∧ decode ?a)
                      ∧ (∃?wu₂ ?wr (?wt₅ = ?wu₂ → ?wr).
                        (∃?wu₃ ?wr₁ (?wt₆ = ?wu₃ → ?wr₁).
                          (∃_. ?aux₁ = ?aux ∧ ?aux = ?wt₆)
                          ∧ (∃ ?v₁ (?alias₁ = <?v₁>List)
                            (?prod₄ = {?v₁ * ?alias₁}) (?prod₅ = {})
                            (?sum₁ = ['Cons:?prod₄ + 'Nil:?prod₅])
                            (?opaque₁ = <?v₁>List)
                          .
                            (∃?nth (?ws₁ = ['Cons:?nth + …]).
                              ?ws₁ = ?sum₁
                              ∧ (∃?s ?s₁ (?prod₆ = {?s * ?s₁}).
                                (∃_. ?acc₂ = ?acc ∧ ?acc = ?s₁)
                                ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
                                ∧ ?prod₆ = ?nth))
                            ∧ ?wu₃ = ?opaque₁)
                          ∧ ?wr₁ = ?wt₅)
                        ∧ (∃_. ?tail₁ = ?tail ∧ ?tail = ?wu₂)
                        ∧ ?wr = ?b₁)
                      ∧ ?b₁ = ?wt₄))
                  ∧ (∃_. ?l₂ = ?l₁ ∧ ?l₁ = ?it))))
            ∧ (∃?wu₄ ?wr₂ (?wt₇ = ?wu₄ → ?wr₂).
              (∃?wu₅ ?wr₃ (?wt₈ = ?wu₅ → ?wr₃).
                (∃_. ?aux₂ = ?aux ∧ ?aux = ?wt₈)
                ∧ (∃ ?v₂ (?alias₂ = <?v₂>List)
                  (?prod₇ = {?v₂ * ?alias₂}) (?prod₈ = {})
                  (?sum₂ = ['Cons:?prod₇ + 'Nil:?prod₈])
                  (?opaque₂ = <?v₂>List)
                .
                  (∃?nth₁ (?ws₂ = ['Nil:?nth₁ + …]).
                    ?ws₂ = ?sum₂ ∧ (∃(?prod₉ = {}). ?prod₉ = ?nth₁))
                  ∧ ?wu₅ = ?opaque₂)
                ∧ ?wr₃ = ?wt₇)
              ∧ (∃_. ?l₃ = ?l ∧ ?l = ?wu₄)
              ∧ ?wr₂ = ?wu₁)
            ∧ ?wu₁ = ?wt₁
            ∧ ?wpat₂ = ?wt₂))
        ∧ (∃_. ?rev₁ = ?rev ∧ ?rev = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    <α>List → <α>List
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    let (rev : <α>List → <α>List) =
      λ (l : <α>List).
          let rec (aux : <α>List → <α>List → <α>List) =
            λ (acc : <α>List).
                λ (List (l₁ : ['Cons:{α * <α>List} + 'Nil:{}])).
                  case l₁ of
                  | 'Cons ((a : α), (tail : <α>List)) ⇒
                    aux (List 'Cons (a, acc)) tail
                  | 'Nil () ⇒ acc
                  esac
          in aux (List 'Nil ()) l
    in rev


  $ minihell rec/list-map.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    let rec map =
      λ f.
          λ (List l).
            (
              List case l of
              | 'Nil () ⇒ 'Nil ()
              | 'Cons (a, tail) ⇒ 'Cons (f a, map f tail)
              esac
            )
    in map
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?map = ?wpat
        ∧ decode ?map
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?f = ?wpat₁
          ∧ decode ?f
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            (∃ _ (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
              (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
            . ?l = ?sum ∧ decode ?l ∧ ?wpat₂ = ?opaque)
            ∧ ?warr₁ = ?wt₁
            ∧ (∃ ?v₁ (?alias₁ = <?v₁>List) (?prod₂ = {?v₁ * ?alias₁})
              (?prod₃ = {}) (?sum₁ = ['Cons:?prod₂ + 'Nil:?prod₃])
              (?opaque₁ = <?v₁>List)
            .
              (∃?it.
                (∃_ _ (?ws = ['Cons:?wp + 'Nil:?wp₁]).
                  ?ws = ?it
                  ∧ (∃?b₁ _ _.
                    (∃?b.
                      (∃(?prod₄ = {}). ?prod₄ = ?wp₁)
                      ∧ (∃?nth (?ws₁ = ['Nil:?nth + …]).
                        ?ws₁ = ?b ∧ (∃(?prod₅ = {}). ?prod₅ = ?nth))
                      ∧ ?b = ?sum₁)
                    ∧ (∃_ _ (?prod₆ = {?p * ?p₁}).
                      ?prod₆ = ?wp
                      ∧ ?tail = ?p₁
                      ∧ decode ?tail
                      ∧ ?a = ?p
                      ∧ decode ?a)
                    ∧ (∃?nth₁ (?ws₂ = ['Cons:?nth₁ + …]).
                      ?ws₂ = ?b₁
                      ∧ (∃?s ?s₁ (?prod₇ = {?s * ?s₁}).
                        (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
                          (∃?wu₂ ?wr₁ (?wt₄ = ?wu₂ → ?wr₁).
                            (∃_. ?map₁ = ?map ∧ ?map = ?wt₄)
                            ∧ (∃_. ?f₁ = ?f ∧ ?f = ?wu₂)
                            ∧ ?wr₁ = ?wt₃)
                          ∧ (∃_. ?tail₁ = ?tail ∧ ?tail = ?wu₁)
                          ∧ ?wr = ?s₁)
                        ∧ (∃?wu₃ ?wr₂ (?wt₅ = ?wu₃ → ?wr₂).
                          (∃_. ?f₂ = ?f ∧ ?f = ?wt₅)
                          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?wu₃)
                          ∧ ?wr₂ = ?s)
                        ∧ ?prod₇ = ?nth₁))
                    ∧ ?b₁ = ?sum₁))
                ∧ (∃_. ?l₁ = ?l ∧ ?l = ?it))
              ∧ ?wt₂ = ?opaque₁)))
        ∧ (∃_. ?map₂ = ?map ∧ ?map = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    (α → β) → <α>List → <β>List
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    let rec (map : (α → β) → <α>List → <β>List) =
      λ (f : α → β).
          λ (List (l : ['Cons:{α * <α>List} + 'Nil:{}])).
            (
              List case l of
              | 'Cons ((a : α), (tail : <α>List)) ⇒ 'Cons (f a, map f tail)
              | 'Nil () ⇒ 'Nil ()
              esac
            )
    in map


We have an explicit unfolding operation.
`T/x` is the reciprocal of `T v` and can be seen as a notation for
`let T v = x in v`

  $ minihell rec/unfold.test
  Input term:
    type <>Bool of ['F:{} + 'T:{}] in
    let not =
      λ b. (Bool case (Bool/ b) of | 'T () ⇒ 'F () | 'F () ⇒ 'T () esac)
    in not
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?not = ?wpat
        ∧ decode ?not
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?b = ?wpat₁
          ∧ decode ?b
          ∧ ?warr = ?wt
          ∧ (∃ (?prod = {}) (?prod₁ = {}) (?sum = ['F:?prod + 'T:?prod₁])
            (?opaque = <>Bool)
          .
            (∃?it.
              (∃_ _ (?ws = ['F:?wp + 'T:?wp₁]).
                ?ws = ?it
                ∧ (∃?b₂.
                  (∃?b₁.
                    (∃(?prod₂ = {}). ?prod₂ = ?wp₁)
                    ∧ (∃?nth (?ws₁ = ['F:?nth + …]).
                      ?ws₁ = ?b₁ ∧ (∃(?prod₃ = {}). ?prod₃ = ?nth))
                    ∧ ?b₁ = ?sum)
                  ∧ (∃(?prod₄ = {}). ?prod₄ = ?wp)
                  ∧ (∃?nth₁ (?ws₂ = ['T:?nth₁ + …]).
                    ?ws₂ = ?b₂ ∧ (∃(?prod₅ = {}). ?prod₅ = ?nth₁))
                  ∧ ?b₂ = ?sum))
              ∧ (∃ (?prod₆ = {}) (?prod₇ = {})
                (?sum₁ = ['F:?prod₆ + 'T:?prod₇]) (?opaque₁ = <>Bool)
              . (∃_. ?b₃ = ?b ∧ ?b = ?opaque₁) ∧ ?it = ?sum₁))
            ∧ ?wt₁ = ?opaque))
        ∧ (∃_. ?not₁ = ?not ∧ ?not = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    <>Bool → <>Bool
  
  Elaborated term:
    type <>Bool of ['F:{} + 'T:{}] in
    let (not : <>Bool → <>Bool) =
      λ (b : <>Bool).
          (Bool case (Bool/ b) of | 'F () ⇒ 'T () | 'T () ⇒ 'F () esac)
    in not

  $ minihell rec/annot.test
  Input term:
    type <a>T of a in λ (x : <int>T). x
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        (∃(?int = int) (?alias = <?int>T).
          ?x = ?alias ∧ decode ?x ∧ ?alias = ?wpat ∧ decode ?alias)
        ∧ ?warr = ?final_type
        ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    <int>T → <int>T
  
  Elaborated term:
    type <a>T of a in λ (x : <int>T). x


  $ minihell rec/should-fail/not-found.test
  Input term:
    λ x. (Foo x)
  
  Fatal error:
    Find failed: Foo not found.
       1| lambda x. Foo x
                    ^^^this constructor escaped its scope
  [101]


Mutually recursive functions should be defined as a tuple because we have no `and` operator.

  $ minihell rec/mutual.test
  Input term:
    type <>Int of ['S:<>Int + 'Z:{}] in
    type <>Bool of ['F:{} + 'T:{}] in
    let rec (even, odd) =
      (
          λ (Int n).
            case n of | 'Z () ⇒ (Bool 'T ()) | 'S n₁ ⇒ odd n₁ esac,
          λ (Int n₂).
            case n₂ of | 'Z () ⇒ (Bool 'F ()) | 'S n₃ ⇒ even n₃ esac
        )
    in (even=even, odd=odd)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _ _.
        (∃_ _ (?prod = {?p * ?p₁}).
          ?prod = ?wpat ∧ ?odd = ?p₁ ∧ decode ?odd ∧ ?even = ?p ∧ decode ?even)
        ∧ (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
          (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
            (∃ (?alias = <>Int) (?prod₂ = {}) (?sum = ['S:?alias + 'Z:?prod₂])
              (?opaque = <>Int)
            . ?n = ?sum ∧ decode ?n ∧ ?wpat₁ = ?opaque)
            ∧ ?warr = ?s₁
            ∧ (∃?it.
              (∃_ _ (?ws = ['S:?wp + 'Z:?wp₁]).
                ?ws = ?it
                ∧ (∃?b₁ _.
                  (∃?b.
                    (∃(?prod₃ = {}). ?prod₃ = ?wp₁)
                    ∧ (∃ (?prod₄ = {}) (?prod₅ = {})
                      (?sum₁ = ['F:?prod₄ + 'T:?prod₅])
                      (?opaque₁ = <>Bool)
                    .
                      (∃?nth (?ws₁ = ['F:?nth + …]).
                        ?ws₁ = ?sum₁ ∧ (∃(?prod₆ = {}). ?prod₆ = ?nth))
                      ∧ ?b = ?opaque₁)
                    ∧ ?b = ?wt₁)
                  ∧ ?n₁ = ?wp
                  ∧ decode ?n₁
                  ∧ (∃?wu₁ ?wr (?wt₂ = ?wu₁ → ?wr).
                    (∃_. ?even₁ = ?even ∧ ?even = ?wt₂)
                    ∧ (∃_. ?n₂ = ?n₁ ∧ ?n₁ = ?wu₁)
                    ∧ ?wr = ?b₁)
                  ∧ ?b₁ = ?wt₁))
              ∧ (∃_. ?n₃ = ?n ∧ ?n = ?it)))
          ∧ (∃?wpat₂ ?wt₃ (?warr₁ = ?wpat₂ → ?wt₃) _.
            (∃ (?alias₁ = <>Int) (?prod₇ = {})
              (?sum₂ = ['S:?alias₁ + 'Z:?prod₇]) (?opaque₂ = <>Int)
            . ?n₄ = ?sum₂ ∧ decode ?n₄ ∧ ?wpat₂ = ?opaque₂)
            ∧ ?warr₁ = ?s
            ∧ (∃?it₁.
              (∃_ _ (?ws₂ = ['S:?wp₂ + 'Z:?wp₃]).
                ?ws₂ = ?it₁
                ∧ (∃?b₃ _.
                  (∃?b₂.
                    (∃(?prod₈ = {}). ?prod₈ = ?wp₃)
                    ∧ (∃ (?prod₉ = {}) (?prod₁₀ = {})
                      (?sum₃ = ['F:?prod₉ + 'T:?prod₁₀])
                      (?opaque₃ = <>Bool)
                    .
                      (∃?nth₁ (?ws₃ = ['T:?nth₁ + …]).
                        ?ws₃ = ?sum₃
                        ∧ (∃(?prod₁₁ = {}). ?prod₁₁ = ?nth₁))
                      ∧ ?b₂ = ?opaque₃)
                    ∧ ?b₂ = ?wt₃)
                  ∧ ?n₅ = ?wp₂
                  ∧ decode ?n₅
                  ∧ (∃?wu₂ ?wr₁ (?wt₄ = ?wu₂ → ?wr₁).
                    (∃_. ?odd₁ = ?odd ∧ ?odd = ?wt₄)
                    ∧ (∃_. ?n₆ = ?n₅ ∧ ?n₅ = ?wu₂)
                    ∧ ?wr₁ = ?b₃)
                  ∧ ?b₃ = ?wt₃))
              ∧ (∃_. ?n₇ = ?n₄ ∧ ?n₄ = ?it₁)))
          ∧ ?prod₁ = ?wt)
        ∧ (∃?s₂ ?s₃ (?prod₁₂ = {even:?s₂ * odd:?s₃}).
          (∃_. ?odd₂ = ?odd ∧ ?odd = ?s₃)
          ∧ (∃_. ?even₂ = ?even ∧ ?even = ?s₂)
          ∧ ?prod₁₂ = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    {even:<>Int → <>Bool * odd:<>Int → <>Bool}
  
  Elaborated term:
    type <>Int of ['S:<>Int + 'Z:{}] in
    type <>Bool of ['F:{} + 'T:{}] in
    let rec ((even : <>Int → <>Bool), (odd : <>Int → <>Bool)) =
      (
          λ (Int (n : ['S:<>Int + 'Z:{}])).
            case n of
            | 'S (n₁ : <>Int) ⇒ odd n₁
            | 'Z () ⇒ (Bool 'T ())
            esac,
          λ (Int (n₂ : ['S:<>Int + 'Z:{}])).
            case n₂ of
            | 'S (n₃ : <>Int) ⇒ even n₃
            | 'Z () ⇒ (Bool 'F ())
            esac
        )
    in (even=even, odd=odd)

A slightly more complex definition with nested inductive types.
We get mutually inductive types for free too because unfolding definitions
is on-demand.

  $ minihell rec/nary-tree.test
  Input term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <a>Tree of ['Leaf:{} + 'Node:{a * <<a>Tree>List}] in
    let rec lmap =
      λ f₁.
          λ (List l).
            (
              List case l of
              | 'Nil () ⇒ 'Nil ()
              | 'Cons (a, tail) ⇒ 'Cons (f₁ a, lmap f₁ tail)
              esac
            )
    in
    let rec tmap =
      λ f.
          λ (Tree t).
            (
              Tree case t of
              | 'Leaf () ⇒ 'Leaf ()
              | 'Node (v, ts) ⇒ 'Node (f v, lmap (tmap f) ts)
              esac
            )
    in tmap
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?lmap = ?wpat
        ∧ decode ?lmap
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?f = ?wpat₁
          ∧ decode ?f
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            (∃ _ (?alias = <?v>List) (?prod = {?v * ?alias}) (?prod₁ = {})
              (?sum = ['Cons:?prod + 'Nil:?prod₁]) (?opaque = <?v>List)
            . ?l = ?sum ∧ decode ?l ∧ ?wpat₂ = ?opaque)
            ∧ ?warr₁ = ?wt₁
            ∧ (∃ ?v₁ (?alias₁ = <?v₁>List) (?prod₂ = {?v₁ * ?alias₁})
              (?prod₃ = {}) (?sum₁ = ['Cons:?prod₂ + 'Nil:?prod₃])
              (?opaque₁ = <?v₁>List)
            .
              (∃?it.
                (∃_ _ (?ws = ['Cons:?wp + 'Nil:?wp₁]).
                  ?ws = ?it
                  ∧ (∃?b₁ _ _.
                    (∃?b.
                      (∃(?prod₄ = {}). ?prod₄ = ?wp₁)
                      ∧ (∃?nth (?ws₁ = ['Nil:?nth + …]).
                        ?ws₁ = ?b ∧ (∃(?prod₅ = {}). ?prod₅ = ?nth))
                      ∧ ?b = ?sum₁)
                    ∧ (∃_ _ (?prod₆ = {?p * ?p₁}).
                      ?prod₆ = ?wp
                      ∧ ?tail = ?p₁
                      ∧ decode ?tail
                      ∧ ?a = ?p
                      ∧ decode ?a)
                    ∧ (∃?nth₁ (?ws₂ = ['Cons:?nth₁ + …]).
                      ?ws₂ = ?b₁
                      ∧ (∃?s ?s₁ (?prod₇ = {?s * ?s₁}).
                        (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
                          (∃?wu₂ ?wr₁ (?wt₄ = ?wu₂ → ?wr₁).
                            (∃_. ?lmap₁ = ?lmap ∧ ?lmap = ?wt₄)
                            ∧ (∃_. ?f₁ = ?f ∧ ?f = ?wu₂)
                            ∧ ?wr₁ = ?wt₃)
                          ∧ (∃_. ?tail₁ = ?tail ∧ ?tail = ?wu₁)
                          ∧ ?wr = ?s₁)
                        ∧ (∃?wu₃ ?wr₂ (?wt₅ = ?wu₃ → ?wr₂).
                          (∃_. ?f₂ = ?f ∧ ?f = ?wt₅)
                          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?wu₃)
                          ∧ ?wr₂ = ?s)
                        ∧ ?prod₇ = ?nth₁))
                    ∧ ?b₁ = ?sum₁))
                ∧ (∃_. ?l₁ = ?l ∧ ?l = ?it))
              ∧ ?wt₂ = ?opaque₁)))
        ∧ (∃?wpat₃ ?wt₆ ?wu₄ _.
          ?tmap = ?wpat₃
          ∧ decode ?tmap
          ∧ (∃?wpat₄ ?wt₇ (?warr₂ = ?wpat₄ → ?wt₇) _.
            ?f₃ = ?wpat₄
            ∧ decode ?f₃
            ∧ ?warr₂ = ?wt₆
            ∧ (∃?wpat₅ ?wt₈ (?warr₃ = ?wpat₅ → ?wt₈) _.
              (∃ _ (?prod₈ = {}) (?alias₂ = <?v₂>Tree)
                (?alias₃ = <?alias₂>List) (?prod₉ = {?v₂ * ?alias₃})
                (?sum₂ = ['Leaf:?prod₈ + 'Node:?prod₉])
                (?opaque₂ = <?v₂>Tree)
              . ?t = ?sum₂ ∧ decode ?t ∧ ?wpat₅ = ?opaque₂)
              ∧ ?warr₃ = ?wt₇
              ∧ (∃ ?v₃ (?prod₁₀ = {}) (?alias₄ = <?v₃>Tree)
                (?alias₅ = <?alias₄>List) (?prod₁₁ = {?v₃ * ?alias₅})
                (?sum₃ = ['Leaf:?prod₁₀ + 'Node:?prod₁₁])
                (?opaque₃ = <?v₃>Tree)
              .
                (∃?it₁.
                  (∃_ _ (?ws₃ = ['Leaf:?wp₂ + 'Node:?wp₃]).
                    ?ws₃ = ?it₁
                    ∧ (∃?b₃.
                      (∃?b₂ _ _.
                        (∃_ _ (?prod₁₂ = {?p₂ * ?p₃}).
                          ?prod₁₂ = ?wp₃
                          ∧ ?ts = ?p₃
                          ∧ decode ?ts
                          ∧ ?v₄ = ?p₂
                          ∧ decode ?v₄)
                        ∧ (∃?nth₂ (?ws₄ = ['Node:?nth₂ + …]).
                          ?ws₄ = ?b₂
                          ∧ (∃?s₂ ?s₃ (?prod₁₃ = {?s₂ * ?s₃}).
                            (∃?wu₅ ?wr₃ (?wt₉ = ?wu₅ → ?wr₃).
                              (∃?wu₆ ?wr₄ (?wt₁₀ = ?wu₆ → ?wr₄).
                                (∃_. ?lmap₂ = ?lmap ∧ ?lmap = ?wt₁₀)
                                ∧ (∃?wu₇ ?wr₅ (?wt₁₁ = ?wu₇ → ?wr₅).
                                  (∃_. ?tmap₁ = ?tmap ∧ ?tmap = ?wt₁₁)
                                  ∧ (∃_. ?f₄ = ?f₃ ∧ ?f₃ = ?wu₇)
                                  ∧ ?wr₅ = ?wu₆)
                                ∧ ?wr₄ = ?wt₉)
                              ∧ (∃_. ?ts₁ = ?ts ∧ ?ts = ?wu₅)
                              ∧ ?wr₃ = ?s₃)
                            ∧ (∃?wu₈ ?wr₆ (?wt₁₂ = ?wu₈ → ?wr₆).
                              (∃_. ?f₅ = ?f₃ ∧ ?f₃ = ?wt₁₂)
                              ∧ (∃_. ?v₅ = ?v₄ ∧ ?v₄ = ?wu₈)
                              ∧ ?wr₆ = ?s₂)
                            ∧ ?prod₁₃ = ?nth₂))
                        ∧ ?b₂ = ?sum₃)
                      ∧ (∃(?prod₁₄ = {}). ?prod₁₄ = ?wp₂)
                      ∧ (∃?nth₃ (?ws₅ = ['Leaf:?nth₃ + …]).
                        ?ws₅ = ?b₃
                        ∧ (∃(?prod₁₅ = {}). ?prod₁₅ = ?nth₃))
                      ∧ ?b₃ = ?sum₃))
                  ∧ (∃_. ?t₁ = ?t ∧ ?t = ?it₁))
                ∧ ?wt₈ = ?opaque₃)))
          ∧ (∃_. ?tmap₂ = ?tmap ∧ ?tmap = ?wu₄)
          ∧ ?wu₄ = ?wu
          ∧ ?wpat₃ = ?wt₆)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    (α → β) → <α>Tree → <β>Tree
  
  Elaborated term:
    type <a>List of ['Cons:{a * <a>List} + 'Nil:{}] in
    type <a>Tree of ['Leaf:{} + 'Node:{a * <<a>Tree>List}] in
    let rec (
      lmap : (<α>Tree → <β>Tree) → <<α>Tree>List → <<β>Tree>List
    ) =
      λ (f₁ : <α>Tree → <β>Tree).
          λ (List (l : ['Cons:{<α>Tree * <<α>Tree>List} + 'Nil:{}])).
            (
              List case l of
              | 'Cons ((a : <α>Tree), (tail : <<α>Tree>List)) ⇒
                'Cons (f₁ a, lmap f₁ tail)
              | 'Nil () ⇒ 'Nil ()
              esac
            )
    in
    let rec (tmap : (α → β) → <α>Tree → <β>Tree) =
      λ (f : α → β).
          λ (Tree (t : ['Leaf:{} + 'Node:{α * <<α>Tree>List}])).
            (
              Tree case t of
              | 'Leaf () ⇒ 'Leaf ()
              | 'Node ((v : α), (ts : <<α>Tree>List)) ⇒
                'Node (f v, lmap (tmap f) ts)
              esac
            )
    in tmap

Some improved versions of previous tests.

  $ minihell rec/dne.test
  Input term:
    type <a>Neg of a → [] in
    let double_neg = λ x₁. λ (Neg nx). nx x₁
    in λ (Neg nnnx). (Neg λ x. nnnx (Neg double_neg x))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?double_neg = ?wpat
        ∧ decode ?double_neg
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?x = ?wpat₁
          ∧ decode ?x
          ∧ ?warr = ?wt
          ∧ (∃?wpat₂ ?wt₂ (?warr₁ = ?wpat₂ → ?wt₂) _.
            (∃_ (?sum = []) (?arr = ?v → ?sum) (?opaque = <?v>Neg).
              ?nx = ?arr ∧ decode ?nx ∧ ?wpat₂ = ?opaque)
            ∧ ?warr₁ = ?wt₁
            ∧ (∃?wu₁ ?wr (?wt₃ = ?wu₁ → ?wr).
              (∃_. ?nx₁ = ?nx ∧ ?nx = ?wt₃)
              ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₁)
              ∧ ?wr = ?wt₂)))
        ∧ (∃?wpat₃ ?wt₄ (?warr₂ = ?wpat₃ → ?wt₄) _.
          (∃ _ (?sum₁ = []) (?arr₁ = ?v₁ → ?sum₁)
            (?opaque₁ = <?v₁>Neg)
          . ?nnnx = ?arr₁ ∧ decode ?nnnx ∧ ?wpat₃ = ?opaque₁)
          ∧ ?warr₂ = ?wu
          ∧ (∃ ?v₂ (?sum₂ = []) (?arr₂ = ?v₂ → ?sum₂)
            (?opaque₂ = <?v₂>Neg)
          .
            (∃?wpat₄ ?wt₅ (?warr₃ = ?wpat₄ → ?wt₅) _.
              ?x₂ = ?wpat₄
              ∧ decode ?x₂
              ∧ ?warr₃ = ?arr₂
              ∧ (∃?wu₂ ?wr₁ (?wt₆ = ?wu₂ → ?wr₁).
                (∃_. ?nnnx₁ = ?nnnx ∧ ?nnnx = ?wt₆)
                ∧ (∃ ?v₃ (?sum₃ = []) (?arr₃ = ?v₃ → ?sum₃)
                  (?opaque₃ = <?v₃>Neg)
                .
                  (∃?wu₃ ?wr₂ (?wt₇ = ?wu₃ → ?wr₂).
                    (∃_. ?double_neg₁ = ?double_neg ∧ ?double_neg = ?wt₇)
                    ∧ (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?wu₃)
                    ∧ ?wr₂ = ?arr₃)
                  ∧ ?wu₂ = ?opaque₃)
                ∧ ?wr₁ = ?wt₅))
            ∧ ?wt₄ = ?opaque₂))
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    <<<α>Neg>Neg>Neg → <α>Neg
  
  Elaborated term:
    type <a>Neg of a → [] in
    let (double_neg : α → <α>Neg → []) =
      λ (x₁ : α). λ (Neg (nx : α → [])). nx x₁
    in
    λ (Neg (nnnx : <<α>Neg>Neg → [])).
      (Neg λ (x : α). nnnx (Neg double_neg x))

  $ minihell rec/distrib.test
  Input term:
    type <a, b>And of {a * b} in
    type <a, b>Or of [a + b] in
    let forward =
      λ (And (a₂, (Or bc))).
          case bc of
          | b₁ ⇒ (Or '0 (And (a₂, b₁)))
          | c₁ ⇒ (Or '1 (And (a₂, c₁)))
          esac
    in
    let reverse =
      λ (Or abac).
          case abac of
          | (And (a, b)) ⇒ (And (a, (Or '0 b)))
          | (And (a₁, c)) ⇒ (And (a₁, (Or '1 c)))
          esac
    in
    let id1 = λ x₁. reverse (forward x₁)
    in let id2 = λ x. forward (reverse x) in (forward, reverse)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?forward = ?wpat
        ∧ decode ?forward
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _ _.
          (∃_ _ (?prod = {?v * ?v₁}) (?opaque = <?v, ?v₁>And).
            (∃_ _ (?prod₁ = {?p * ?p₁}).
              ?prod₁ = ?prod
              ∧ (∃_ _ (?sum = [?v₂ + ?v₃]) (?opaque₁ = <?v₂, ?v₃>Or).
                ?bc = ?sum ∧ decode ?bc ∧ ?p₁ = ?opaque₁)
              ∧ ?a = ?p
              ∧ decode ?a)
            ∧ ?wpat₁ = ?opaque)
          ∧ ?warr = ?wt
          ∧ (∃?it.
            (∃_ _ (?ws = [?wp + ?wp₁]).
              ?ws = ?it
              ∧ (∃?b₁ _.
                (∃?b _.
                  ?c = ?wp₁
                  ∧ decode ?c
                  ∧ (∃ ?v₄ ?v₅ (?sum₁ = [?v₄ + ?v₅])
                    (?opaque₂ = <?v₄, ?v₅>Or)
                  .
                    (∃ø ?nth (?ws₁ = [?_ + ?nth + …]).
                      ?ws₁ = ?sum₁
                      ∧ (∃ ?v₆ ?v₇ (?prod₂ = {?v₆ * ?v₇})
                        (?opaque₃ = <?v₆, ?v₇>And)
                      .
                        (∃?s ?s₁ (?prod₃ = {?s * ?s₁}).
                          (∃_. ?c₁ = ?c ∧ ?c = ?s₁)
                          ∧ (∃_. ?a₁ = ?a ∧ ?a = ?s)
                          ∧ ?prod₃ = ?prod₂)
                        ∧ ?nth = ?opaque₃))
                    ∧ ?b = ?opaque₂)
                  ∧ ?b = ?wt₁)
                ∧ ?b₂ = ?wp
                ∧ decode ?b₂
                ∧ (∃ ?v₈ ?v₉ (?sum₂ = [?v₈ + ?v₉])
                  (?opaque₄ = <?v₈, ?v₉>Or)
                .
                  (∃?nth₁ (?ws₂ = [?nth₁ + …]).
                    ?ws₂ = ?sum₂
                    ∧ (∃ ?v₁₀ ?v₁₁ (?prod₄ = {?v₁₀ * ?v₁₁})
                      (?opaque₅ = <?v₁₀, ?v₁₁>And)
                    .
                      (∃?s₂ ?s₃ (?prod₅ = {?s₂ * ?s₃}).
                        (∃_. ?b₃ = ?b₂ ∧ ?b₂ = ?s₃)
                        ∧ (∃_. ?a₂ = ?a ∧ ?a = ?s₂)
                        ∧ ?prod₅ = ?prod₄)
                      ∧ ?nth₁ = ?opaque₅))
                  ∧ ?b₁ = ?opaque₄)
                ∧ ?b₁ = ?wt₁))
            ∧ (∃_. ?bc₁ = ?bc ∧ ?bc = ?it)))
        ∧ (∃?wpat₂ ?wt₂ ?wu₁ _.
          ?reverse = ?wpat₂
          ∧ decode ?reverse
          ∧ (∃?wpat₃ ?wt₃ (?warr₁ = ?wpat₃ → ?wt₃) _.
            (∃ _ _ (?sum₃ = [?v₁₂ + ?v₁₃])
              (?opaque₆ = <?v₁₂, ?v₁₃>Or)
            . ?abac = ?sum₃ ∧ decode ?abac ∧ ?wpat₃ = ?opaque₆)
            ∧ ?warr₁ = ?wt₂
            ∧ (∃?it₁.
              (∃_ _ (?ws₃ = [?wp₂ + ?wp₃]).
                ?ws₃ = ?it₁
                ∧ (∃?b₅ _ _.
                  (∃?b₄ _ _.
                    (∃ _ _ (?prod₆ = {?v₁₄ * ?v₁₅})
                      (?opaque₇ = <?v₁₄, ?v₁₅>And)
                    .
                      (∃_ _ (?prod₇ = {?p₂ * ?p₃}).
                        ?prod₇ = ?prod₆
                        ∧ ?c₂ = ?p₃
                        ∧ decode ?c₂
                        ∧ ?a₃ = ?p₂
                        ∧ decode ?a₃)
                      ∧ ?wp₃ = ?opaque₇)
                    ∧ (∃ ?v₁₆ ?v₁₇ (?prod₈ = {?v₁₆ * ?v₁₇})
                      (?opaque₈ = <?v₁₆, ?v₁₇>And)
                    .
                      (∃?s₄ ?s₅ (?prod₉ = {?s₄ * ?s₅}).
                        (∃ ?v₁₈ ?v₁₉ (?sum₄ = [?v₁₈ + ?v₁₉])
                          (?opaque₉ = <?v₁₈, ?v₁₉>Or)
                        .
                          (∃ø ?nth₂ (?ws₄ = [?_₁ + ?nth₂ + …]).
                            ?ws₄ = ?sum₄
                            ∧ (∃_. ?c₃ = ?c₂ ∧ ?c₂ = ?nth₂))
                          ∧ ?s₅ = ?opaque₉)
                        ∧ (∃_. ?a₄ = ?a₃ ∧ ?a₃ = ?s₄)
                        ∧ ?prod₉ = ?prod₈)
                      ∧ ?b₄ = ?opaque₈)
                    ∧ ?b₄ = ?wt₃)
                  ∧ (∃ _ _ (?prod₁₀ = {?v₂₀ * ?v₂₁})
                    (?opaque₁₀ = <?v₂₀, ?v₂₁>And)
                  .
                    (∃_ _ (?prod₁₁ = {?p₄ * ?p₅}).
                      ?prod₁₁ = ?prod₁₀
                      ∧ ?b₆ = ?p₅
                      ∧ decode ?b₆
                      ∧ ?a₅ = ?p₄
                      ∧ decode ?a₅)
                    ∧ ?wp₂ = ?opaque₁₀)
                  ∧ (∃ ?v₂₂ ?v₂₃ (?prod₁₂ = {?v₂₂ * ?v₂₃})
                    (?opaque₁₁ = <?v₂₂, ?v₂₃>And)
                  .
                    (∃?s₆ ?s₇ (?prod₁₃ = {?s₆ * ?s₇}).
                      (∃ ?v₂₄ ?v₂₅ (?sum₅ = [?v₂₄ + ?v₂₅])
                        (?opaque₁₂ = <?v₂₄, ?v₂₅>Or)
                      .
                        (∃?nth₃ (?ws₅ = [?nth₃ + …]).
                          ?ws₅ = ?sum₅ ∧ (∃_. ?b₇ = ?b₆ ∧ ?b₆ = ?nth₃))
                        ∧ ?s₇ = ?opaque₁₂)
                      ∧ (∃_. ?a₆ = ?a₅ ∧ ?a₅ = ?s₆)
                      ∧ ?prod₁₃ = ?prod₁₂)
                    ∧ ?b₅ = ?opaque₁₁)
                  ∧ ?b₅ = ?wt₃))
              ∧ (∃_. ?abac₁ = ?abac ∧ ?abac = ?it₁)))
          ∧ (∃?wpat₄ ?wt₄ ?wu₂ _.
            ?id1 = ?wpat₄
            ∧ decode ?id1
            ∧ (∃?wpat₅ ?wt₅ (?warr₂ = ?wpat₅ → ?wt₅) _.
              ?x = ?wpat₅
              ∧ decode ?x
              ∧ ?warr₂ = ?wt₄
              ∧ (∃?wu₃ ?wr (?wt₆ = ?wu₃ → ?wr).
                (∃_. ?reverse₁ = ?reverse ∧ ?reverse = ?wt₆)
                ∧ (∃?wu₄ ?wr₁ (?wt₇ = ?wu₄ → ?wr₁).
                  (∃_. ?forward₁ = ?forward ∧ ?forward = ?wt₇)
                  ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wu₄)
                  ∧ ?wr₁ = ?wu₃)
                ∧ ?wr = ?wt₅))
            ∧ (∃?wpat₆ ?wt₈ ?wu₅ _.
              ?id2 = ?wpat₆
              ∧ decode ?id2
              ∧ (∃?wpat₇ ?wt₉ (?warr₃ = ?wpat₇ → ?wt₉) _.
                ?x₂ = ?wpat₇
                ∧ decode ?x₂
                ∧ ?warr₃ = ?wt₈
                ∧ (∃?wu₆ ?wr₂ (?wt₁₀ = ?wu₆ → ?wr₂).
                  (∃_. ?forward₂ = ?forward ∧ ?forward = ?wt₁₀)
                  ∧ (∃?wu₇ ?wr₃ (?wt₁₁ = ?wu₇ → ?wr₃).
                    (∃_. ?reverse₂ = ?reverse ∧ ?reverse = ?wt₁₁)
                    ∧ (∃_. ?x₃ = ?x₂ ∧ ?x₂ = ?wu₇)
                    ∧ ?wr₃ = ?wu₆)
                  ∧ ?wr₂ = ?wt₉))
              ∧ (∃?s₈ ?s₉ (?prod₁₄ = {?s₈ * ?s₉}).
                (∃_. ?reverse₃ = ?reverse ∧ ?reverse = ?s₉)
                ∧ (∃_. ?forward₃ = ?forward ∧ ?forward = ?s₈)
                ∧ ?prod₁₄ = ?wu₅)
              ∧ ?wu₅ = ?wu₂
              ∧ ?wpat₆ = ?wt₈)
            ∧ ?wu₂ = ?wu₁
            ∧ ?wpat₄ = ?wt₄)
          ∧ ?wu₁ = ?wu
          ∧ ?wpat₂ = ?wt₂)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Inferred type:
    {<α, <β, γ>Or>And → <<α, β>And, <α, γ>And>Or
    * <<α, β>And, <α, γ>And>Or → <α, <β, γ>Or>And}
  
  Elaborated term:
    type <a, b>And of {a * b} in
    type <a, b>Or of [a + b] in
    let (forward : <α, <β, γ>Or>And → <<α, β>And, <α, γ>And>Or) =
      λ (And ((a₂ : α), (Or (bc : [β + γ])))).
          case bc of
          | (b₁ : β) ⇒ (Or '0 (And (a₂, b₁)))
          | (c₁ : γ) ⇒ (Or '1 (And (a₂, c₁)))
          esac
    in
    let (reverse : <<α, β>And, <α, γ>And>Or → <α, <β, γ>Or>And) =
      λ (Or (abac : [<α, β>And + <α, γ>And])).
          case abac of
          | (And ((a : α), (b : β))) ⇒ (And (a, (Or '0 b)))
          | (And ((a₁ : α), (c : γ))) ⇒ (And (a₁, (Or '1 c)))
          esac
    in
    let (id1 : <α, <β, γ>Or>And → <α, <β, γ>Or>And) =
      λ (x₁ : <α, <β, γ>Or>And). reverse (forward x₁)
    in
    let (
      id2 : <<α, β>And, <α, γ>And>Or → <<α, β>And, <α, γ>And>Or
    ) = λ (x : <<α, β>And, <α, γ>And>Or). forward (reverse x)
    in (forward, reverse)


Recursive functions also include easier ways to write nonsensical programs,
so here's a deep cyclic type involving recursion.

  $ minihell rec/should-fail/cycle.test
  Input term:
    let rec f =
      λ x. case x of | _ ⇒ () | (_, (_, z)) ⇒ f (z ()) | _ ⇒ () esac
    in f
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt ?wu _.
        ?f = ?wpat
        ∧ decode ?f
        ∧ (∃?wpat₁ ?wt₁ (?warr = ?wpat₁ → ?wt₁) _.
          ?x = ?wpat₁
          ∧ decode ?x
          ∧ ?warr = ?wt
          ∧ (∃?it.
            (∃_ _ _ (?ws = [?wp + ?wp₁ + ?wp₂]).
              ?ws = ?it
              ∧ (∃?b₂.
                (∃?b₁ _.
                  (∃?b. (∃(?prod = {}). ?prod = ?b) ∧ ?b = ?wt₁)
                  ∧ (∃_ _ (?prod₁ = {?p * ?p₁}).
                    ?prod₁ = ?wp₁
                    ∧ (∃_ _ (?prod₂ = {?p₂ * ?p₃}).
                      ?prod₂ = ?p₁ ∧ ?z = ?p₃ ∧ decode ?z))
                  ∧ (∃?wu₁ ?wr (?wt₂ = ?wu₁ → ?wr).
                    (∃_. ?f₁ = ?f ∧ ?f = ?wt₂)
                    ∧ (∃?wu₂ ?wr₁ (?wt₃ = ?wu₂ → ?wr₁).
                      (∃_. ?z₁ = ?z ∧ ?z = ?wt₃)
                      ∧ (∃(?prod₃ = {}). ?prod₃ = ?wu₂)
                      ∧ ?wr₁ = ?wu₁)
                    ∧ ?wr = ?b₁)
                  ∧ ?b₁ = ?wt₁)
                ∧ (∃(?prod₄ = {}). ?prod₄ = ?b₂)
                ∧ ?b₂ = ?wt₁))
            ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it)))
        ∧ (∃_. ?f₂ = ?f ∧ ?f = ?wu)
        ∧ ?wu = ?final_type
        ∧ ?wpat = ?wt)
      ∧ decode ?final_type
  
  Error:
    Cycle detected: ω occurs in
    ω = [_ + {_ * {_ * {} → ω}} + _]
    without any inductive constructor in-between.
    The cycle goes through the following variables:
    x (ll. 2), x (ll. 1), z (ll. 4), z (ll. 4)
    help:
       1| let rec f = lambda x.
                             ^these variables are involved in the cycle
       2|   case x of
                 ^
       4|   | (_, (_, z)) => f (z ())
                      ^         ^
  [101]

and an inductive type instanciated with a cyclic argument

  $ minihell rec/should-fail/alias-cycle.test
  Input term:
    type <a>Stream of {a * <a>Stream} in λ x. (Stream (x, x))
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃ ?v (?alias = <?v>Stream) (?prod = {?v * ?alias})
          (?opaque = <?v>Stream)
        .
          (∃?s ?s₁ (?prod₁ = {?s * ?s₁}).
            (∃_. ?x₁ = ?x ∧ ?x = ?s₁)
            ∧ (∃_. ?x₂ = ?x ∧ ?x = ?s)
            ∧ ?prod₁ = ?prod)
          ∧ ?wt = ?opaque))
      ∧ decode ?final_type
  
  Error:
    Cycle detected: ω occurs in
    ω = <ω>Stream
    without any inductive constructor in-between.
    The cycle goes through the following variables:
    x (ll. 2), x (ll. 2), x (ll. 2)
    help:
       2| lambda x. Stream (x, x)
                 ^          ^  ^these variables are involved in the cycle
  [101]



Finally we have proper check of the number of parameters of an inductive type

  $ minihell rec/should-fail/arglen-mismatch.test
  Input term:
    type <a, b>Either of [a + b] in ((Either '0 ()) : <_, _, _>Either)
  
  Generated constraint:
    ∃?final_type.
      (∃?_ ?_₁ ?_₂ (?alias = <?_, ?_₁, ?_₂>Either).
        (∃?v ?v₁ (?sum = [?v + ?v₁]) (?opaque = <?v, ?v₁>Either).
          (∃?nth (?ws = [?nth + …]). ?ws = ?sum ∧ (∃(?prod = {}). ?prod = ?nth))
          ∧ ?alias = ?opaque)
        ∧ ?alias = ?final_type
        ∧ decode ?alias)
      ∧ decode ?final_type
  
  Error:
      (1)  <β, γ, δ>Either
    incompatible with
      (2)  <{}, α>Either
    help:
       2| (Either'0 : <_,_,_>Either)
                      ^^^^^^^^^^^^^found to impose structure (1)
       2| (Either'0 : <_,_,_>Either)
           ^^^^^^^^incompatible with structure (2)
    hint: expected 3 generic parameters, found 2
  [101]


  $ minihell ui/should-fail/sum-prod-coercion.test
  Input term:
    λ x. (x.1, case x of  esac)
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃?s ?s₁ (?prod = {?s * ?s₁}).
          (∃?it. (∃(?ws = []). ?ws = ?it) ∧ (∃_. ?x₁ = ?x ∧ ?x = ?it))
          ∧ (∃_ ?nth (?wp = {?_ * ?nth * …}).
            ?nth = ?s ∧ (∃_. ?x₂ = ?x ∧ ?x = ?wp))
          ∧ ?prod = ?wt))
      ∧ decode ?final_type
  
  Error:
      (1)  []
    incompatible with
      (2)  {_ * α * …}
    help:
       2|   (x.1, case x of esac)
                  ^^^^^^^^^^^^^^found to impose structure (1)
       2|   (x.1, case x of esac)
               ^incompatible with structure (2)
    hint: cannot coerce union to tuple
  [101]
  $ minihell ui/should-fail/idx-lab-empty-coerce.test
  Input term:
    λ x. if let '0 _ = x then () else if let 'O _ = x then () else () fi fi
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃_ (?sum = [?nth + …]).
          (∃_. ?x₁ = ?x ∧ ?x = ?sum)
          ∧ (∃(?prod = {}). ?prod = ?wt)
          ∧ (∃_ (?sum₁ = ['O:?nth₁ + …]).
            (∃_. ?x₂ = ?x ∧ ?x = ?sum₁)
            ∧ (∃(?prod₁ = {}). ?prod₁ = ?wt)
            ∧ (∃(?prod₂ = {}). ?prod₂ = ?wt))))
      ∧ decode ?final_type
  
  Error:
      (1)  [_₁ + …]
    incompatible with
      (2)  ['O:_ + …]
    help:
       2|   if let '0 _ = x then ()
                    ^found to impose structure (1)
       3|   else if let 'O _ = x then ()
                         ^incompatible with structure (2)
    hint: nonempty composite types with incomparable labels
  [101]

  $ minihell ui/should-fail/syntax-1.test
  Fatal error:
    Syntax error in 'ui/should-fail/syntax-1.test'
    help:
       1| lambda lambda x. x
                 ^^^^^^expected function argument
  example: `lambda x. x`
                   +
   (error state 148)
  [101]
  $ minihell ui/should-fail/syntax-2.test
  Fatal error:
    Syntax error in 'ui/should-fail/syntax-2.test'
    help:
       2| -%
          ^unfinished token. Did you mean -> or --% or -- ?
  [101]
  $ minihell ui/should-fail/syntax-3.test
  Fatal error:
    Syntax error in 'ui/should-fail/syntax-3.test'
    help:
       1| lambda â. â
                 ^invalid character.
  [101]
  $ minihell ui/should-fail/recover.batch.test
  Fatal error:
    Syntax error in 'ui/should-fail/recover.batch.test'
    help:
       2| --%
          ^^^abstraction is missing a body
  example: `lambda x. x`
                      +
   (error state 149)
  ----------
  
  Fatal error:
    Find failed: y not found.
       5| lambda x. y
                    ^this variable is unbound in the current term
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/recover.batch.test'
    help:
       7| 1
          ^cannot begin a term
  ----------
  
  Input term:
    λ x₁. x₁
  
  Generated constraint:
    ∃?final_type.
      (∃?wpat ?wt (?warr = ?wpat → ?wt) _.
        ?x = ?wpat
        ∧ decode ?x
        ∧ ?warr = ?final_type
        ∧ (∃_. ?x₁ = ?x ∧ ?x = ?wt))
      ∧ decode ?final_type
  
  Inferred type:
    α → α
  
  Elaborated term:
    λ (x₁ : α). x₁
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/recover.batch.test'
    help:
      11| ã
          ^invalid character.
  [104]

  $ minihell ui/should-fail/exhaustive.batch.test
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
       1| type with --%
               ^^^^expected inductive type declaration
  The token 'type' can only be followed by a type declaration.
  example: `type <arg1,arg2>Name of structure`
                 +++++++++++++++
   (error state 1)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
       2| type <with --%
                ^^^^expected inductive type generic parameter
  Close the angle brackets or provide the next argument.
  example: `type <arg1,arg2>Name of structure`
                  ++++
   (error state 2)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
       4| type <arg with --%
                    ^^^^expected sequence of inductive type generic parameters
  Close the angle brackets or provide the next argument.
  example: `type <arg1,arg2>Name of structure`
                      ++++++
   (error state 4)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
       5| type <arg,with --%
                    ^^^^expected inductive type generic parameter
  Provide the next argument or remove the trailing ','.
  example: `type <arg1,arg2>Name of structure`
                       ++++
   (error state 5)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
       8| type <>with --%
                 ^^^^expected inductive type name
  The next token should be a capitalized name.
  example: `type <arg1,arg2>Name of structure`
                            ++++
   (error state 8)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      10| type <>Name with --%
                      ^^^^expected inductive type structure definition
  The next token should be 'of'.
  example: `type <arg1,arg2>Name of structure`
                                 ++++++++++++
   (error state 10)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      11| type <>Name of with --%
                         ^^^^expected inductive type structure definition
  This token cannot begin a type.
  example: `type <arg1,arg2>Name of structure`
                                    +++++++++
   (error state 11)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      13| (x : (with --%
                ^^^^expected type annotation
  ':' suggests that you are about to provide a type.
  This token cannot begin a type.
  example: `(x : ty)`
                 ++
   (error state 13)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      14| (x : [with --%
                ^^^^expected type
  Opening `[` suggests that you are writing a sum type, the
  elements of a sum type should be either types or labeled types
  example: `(x : [ty + ...])`
                  ++
  example: `(x : ['Label:ty + ...])`
                  +++++++++
   (error state 14)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      16| (x : ['with --%
                 ^^^^expected variant label
  Quote `'` indicates that the next token should be a capitalized identifier.
  example: `(x : ['Foo:ty + ...])`
                   +++
   (error state 16)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      19| (x : {with --%
                ^^^^expected type (as an element of a product)
  Opening `{` suggests that you are writing a product type, the
  elements of a sum product should be either types or labeled types
  example: `(x : {ty * ...})`
                  ++
  example: `(x : {label:ty * ...})`
                  ++++++++
   (error state 19)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      21| (x : {t with --%
                  ^^^^elements of a product should be separated by '*'
  example: `(x : {ty * ...})`
                     +
   (error state 21)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      22| (x : <with --%
                ^^^^unexpected while parsing an inductive type instantiation
  Opening '<' suggests this is an instance of an inductive type.
  Close the angle brackets or provide the next argument.
  example: `(x : <arg1,arg2>Type)`
                  ++++++++++
   (error state 22)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      27| (x : (t -> t) with --%
                        ^^^^unexpected after a full type
  At this point you should probably close the parenthesis surrounding
  the type annotation.
  example: `(x : ty)`
                   +
   (error state 27)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      36| (x : x -> with --%
                    ^^^^expected a type
  Right of `->` should be a full type.
  example: `(x : ty -> ty)`
                       ++
   (error state 36)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      39| (x : <arg) --%
                   ^unexpected while parsing an inductive type instance
  Opening '<' suggests that you are writing an instance of an inductive type.
  At this point you should close the `>` or provide the next argument.
  example: `(x : <arg1,arg2>Type)`
                      ++++++
   (error state 39)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      40| (x : <arg, with --%
                     ^^^^unexpected while parsing an inductive type instance
  Opening '<' suggests that you are writing an instance of an inductive type.
  At this point you should provide the next argument or remove the trailing ','.
  example: `(x : <arg1,arg2>Type)`
                       ++++
   (error state 40)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      43| (x : <>with --%
                 ^^^^expected inductive type name
  Generic arguments `<...>` should be followed by the capitalized name of the type.
  example: `(x : <arg1,arg2>Type)`
                            ++++
   (error state 43)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      45| (x : {... with --%
                    ^^^^expected closing '}' immediately after '...'
  example: `(x : {ty * ...})`
                          +
   (error state 45)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      47| (x : {lab:ty * lab with --%
                             ^^^^expected ':' and type
  Earlier labeled type `label:ty` suggests this should be a labeled product.
  All elements should have the correct shape.
  example: `(x : {lab1:ty1 * lab2:ty2 * ...})`
                                 ++++
   (error state 47)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      48| (x : {x:with --%
                  ^^^^this token cannot begin a type
  Previous ':' suggests a labeled product type.
  example: `(x : {lab:ty * ...})`
                      ++
   (error state 48)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      51| (x : {lab:ty) --%
                      ^unclosed '{'
  Parentheses are not properly nested
  example: `(x : {lab:ty})`
                        +
   (error state 51)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      52| (x : {lab:ty * with --%
                         ^^^^expected label and type
  Previous `lab:ty` suggests you are declaring a labeled product.
  example: `(x : {lab1:ty1 * lab2:ty2 * ...})`
                             ++++++++
   (error state 52)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      54| (x : {lab:ty * ... with --%
                             ^^^^expected closing '}'
  Token '...' should be the last element of the product.
  example: `(x : {lab1:ty1 * ...})`
                                +
   (error state 54)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      56| (x : {lab:ty * lab:ty) --%
                               ^unclosed '{'
  Parentheses are not properly nested.
  example: `(x : {lab1:ty1 * lab2:ty2})`
                                     +
   (error state 56)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      60| (x : {_) --%
                 ^unclosed '{'
  Parentheses are not properly nested.
  example: `(x : {_})`
                   +
   (error state 60)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      61| (x : {ty * with --%
                     ^^^^this token cannot begin a type
  Expected a type because you are currently declaring a product type.
  example: `(x : {ty1 * ty2 * ...})`
                        +++
   (error state 61)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      62| (x : {ty * ... with --%
                         ^^^^expected closing '}'
  Token '...' should be the last element of the product.
  example: `(x : {ty1 * ...})`
                           +
   (error state 62)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      64| (x : {ty * ty) --%
                       ^unclosed '{'
  Parentheses are not properly nested.
  example: `(x : {ty1 * ty2})`
                           +
   (error state 64)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      68| (x : [... with --%
                    ^^^^expected closing ']'
  Token '...' should be the last element of the union.
  example: `(x : [...])`
                     +
   (error state 68)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      70| (x : ['Lab with --%
                     ^^^^expected a type or the next label
  example: `(x : ['Lab:ty + ...])`
                      +++ explicit type
  example: `(x : ['Lab + ...])`
                      ++ implicit unit type and next element
   (error state 70)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      71| (x : ['Lab:with --%
                     ^^^^expected a type
  example: `(x : ['Lab:ty + ...])`
                       ++
   (error state 71)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      73| (x : [ty) --%
                  ^unclosed '['
  Parentheses are not properly nested.
  example: `(x : [ty])`
                    +
   (error state 73)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      75| (x : [ty + with --%
                     ^^^^this token cannot begin a type
  You are currently declaring a union, this should be the next element.
  example: `(x : [ty1 + ty2])`
                        +++
   (error state 75)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      76| (x : [ty + ... with --%
                         ^^^^expected closing ']'
  Token '...' should be the last element of the union.
  example: `(x : [ty + ...])`
                          +
   (error state 76)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      78| (x : [ty + ty) --%
                       ^unclosed '['
  Parentheses are not properly nested.
  example: `(x : [ty1 + ty2])`
                           +
   (error state 78)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      81| (x : ['Lab:ty) --%
                       ^Syntax error. (Please report for improvements on the error message. Error state: #81)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      83| (x : ['Lab + with --%
                       ^^^^Syntax error. (Please report for improvements on the error message. Error state: #83)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      84| (x : ['Lab + ... with --%
                           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #84)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      86| (x : ['Lab + 'Lab:ty) --%
                              ^Syntax error. (Please report for improvements on the error message. Error state: #86)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      89| (x : (ty] --%
                  ^Syntax error. (Please report for improvements on the error message. Error state: #89)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      92| 'with --%
           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #92)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      95| '1 with --%
             ^^^^Syntax error. (Please report for improvements on the error message. Error state: #95)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      96| (with --%
           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #96)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      98| (x _ --%
             ^Syntax error. (Please report for improvements on the error message. Error state: #98)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
      99| let with --%
              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #99)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     101| let rec with --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #101)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     103| lambda (with --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #103)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     105| lambda Lab with --%
                     ^^^^Syntax error. (Please report for improvements on the error message. Error state: #105)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     113| lambda (x = x, x with --%
                           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #113)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     114| lambda (x = with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #114)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     115| lambda (_ _ --%
                    ^Syntax error. (Please report for improvements on the error message. Error state: #115)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     118| lambda (x : with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #118)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     120| lambda (x as with --%
                       ^^^^Syntax error. (Please report for improvements on the error message. Error state: #120)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     125| lambda (... with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #125)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     127| lambda (_ ... --%
                    ^^^Syntax error. (Please report for improvements on the error message. Error state: #127)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     131| lambda (x, with --%
                     ^^^^Syntax error. (Please report for improvements on the error message. Error state: #131)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     133| lambda (x, ... with --%
                         ^^^^Syntax error. (Please report for improvements on the error message. Error state: #133)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     135| lambda (x, x = --%
                       ^Syntax error. (Please report for improvements on the error message. Error state: #135)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     136| lambda (x, x, with --%
                        ^^^^Syntax error. (Please report for improvements on the error message. Error state: #136)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     138| lambda (x = x) --%
                       ^Syntax error. (Please report for improvements on the error message. Error state: #138)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     139| lambda (x = x, with --%
                         ^^^^Syntax error. (Please report for improvements on the error message. Error state: #139)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     141| lambda (x = x, ... with --%
                             ^^^^Syntax error. (Please report for improvements on the error message. Error state: #141)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     143| lambda (x = x, x = x = --%
                               ^Syntax error. (Please report for improvements on the error message. Error state: #143)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     144| lambda (x = x, x = x, with --%
                                ^^^^Syntax error. (Please report for improvements on the error message. Error state: #144)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     146| let x _ --%
                ^Syntax error. (Please report for improvements on the error message. Error state: #146)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     147| let x = with --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #147)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     148| lambda with --%
                 ^^^^expected function argument
  example: `lambda x. x`
                   +
   (error state 148)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     149| lamba x type --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #193)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     152| lambda . with --%
                   ^^^^Syntax error. (Please report for improvements on the error message. Error state: #152)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     153| if with --%
             ^^^^Syntax error. (Please report for improvements on the error message. Error state: #153)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     154| if let with --%
                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #154)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     155| if let ' with --%
                   ^^^^Syntax error. (Please report for improvements on the error message. Error state: #155)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     157| if let 'Lab with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #157)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     162| if let '1 with --%
                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #162)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     168| if let 'Lab x ) --%
                        ^Syntax error. (Please report for improvements on the error message. Error state: #168)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     169| if let 'Lab = with --%
                        ^^^^Syntax error. (Please report for improvements on the error message. Error state: #169)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     170| case with --%
               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #170)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     172| Lab with --%
              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #172)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     173| Lab/ with --%
               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #173)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     174| type <>Lab of ty) --%
                          ^Syntax error. (Please report for improvements on the error message. Error state: #174)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     175| type <>Lab of ty in with --%
                              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #175)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     181| x _ --%
            ^Syntax error. (Please report for improvements on the error message. Error state: #181)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     187| x . with --%
              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #187)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     193| x x _ --%
              ^Syntax error. (Please report for improvements on the error message. Error state: #193)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     207| case x then --%
                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #207)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     208| case x of with --%
                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #208)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     209| case x of | with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #209)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     210| case x of | 'with --%
                       ^^^^Syntax error. (Please report for improvements on the error message. Error state: #210)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     211| case x of | 'Lab with --%
                           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #211)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     214| case x of | ... with --%
                          ^^^^Syntax error. (Please report for improvements on the error message. Error state: #214)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     215| case x of | ... => with --%
                             ^^^^Syntax error. (Please report for improvements on the error message. Error state: #215)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     218| case x of | 'Lab x ) --%
                             ^Syntax error. (Please report for improvements on the error message. Error state: #218)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     219| case x of | 'Lab => with --%
                              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #219)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     221| case x of | x ) --%
                        ^Syntax error. (Please report for improvements on the error message. Error state: #221)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     222| case x of | => with --% ---- Is there a bug here ? --%
                      ^^Syntax error. (Please report for improvements on the error message. Error state: #209)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     225| case x of | 'Lab => x | ... => x then --%
                                           ^^^^Syntax error. (Please report for improvements on the error message. Error state: #225)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     227| case x of | 'Lab => x then --%
                                ^^^^Syntax error. (Please report for improvements on the error message. Error state: #227)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     228| case x of | 'Lab => x | with --%
                                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #228)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     229| case x of | 'Lab => x | 'Lab => x then --%
                                            ^^^^Syntax error. (Please report for improvements on the error message. Error state: #229)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     234| case x of | ... => x then --%
                               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #234)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     242| if let 'Lab = x of --%
                          ^^Syntax error. (Please report for improvements on the error message. Error state: #242)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     243| if let 'Lab = x then with --%
                               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #243)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     244| if let 'Lab = x then y then --%
                                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #244)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     245| if let 'Lab = x then x else with --%
                                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #245)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     247| if let '1 x ) --%
                      ^Syntax error. (Please report for improvements on the error message. Error state: #247)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     248| if let '1 = with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #248)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     249| if let '1 = x of --%
                        ^^Syntax error. (Please report for improvements on the error message. Error state: #249)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     250| if let '1 = x then with --%
                             ^^^^Syntax error. (Please report for improvements on the error message. Error state: #250)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     251| if let '1 = x then x then --%
                               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #251)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     252| if let '1 = x then x else with --%
                                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #252)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     256| let x = x then --%
                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #256)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     257| let x = x in with --%
                       ^^^^Syntax error. (Please report for improvements on the error message. Error state: #257)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     259| (x with x with --%
                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #259)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     260| (x = with --%
               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #260)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     262| (x as --%
             ^^Syntax error. (Please report for improvements on the error message. Error state: #262)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     263| (Lab x then --%
                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #263)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     266| (x with with --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #266)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     267| (x with 1 with --%
                    ^^^^Syntax error. (Please report for improvements on the error message. Error state: #267)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     268| (x with 1 = with --%
                      ^^^^Syntax error. (Please report for improvements on the error message. Error state: #268)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     274| (x with x = x then --%
                        ^^^^Syntax error. (Please report for improvements on the error message. Error state: #274)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     275| (x with x = x, with --%
                         ^^^^Syntax error. (Please report for improvements on the error message. Error state: #275)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     277| (x = x, x = x then --%
                        ^^^^Syntax error. (Please report for improvements on the error message. Error state: #277)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     278| (x = x, x = x, with --%
                         ^^^^Syntax error. (Please report for improvements on the error message. Error state: #278)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     280| (x with 1 = x then --%
                        ^^^^Syntax error. (Please report for improvements on the error message. Error state: #280)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     281| (x with 1 = x, with --%
                         ^^^^Syntax error. (Please report for improvements on the error message. Error state: #281)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     283| (x with 1 = x, 1 = x then --%
                               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #283)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     284| (x with 1 = x, 1 = x, with --%
                                ^^^^Syntax error. (Please report for improvements on the error message. Error state: #284)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     287| (x : with --%
               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #287)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     288| (x : x] --%
                ^Syntax error. (Please report for improvements on the error message. Error state: #288)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     291| (x, with --%
              ^^^^Syntax error. (Please report for improvements on the error message. Error state: #291)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     295| (x, x then --%
                ^^^^Syntax error. (Please report for improvements on the error message. Error state: #295)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     296| (x, x, with --%
                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #296)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     298| (x = x then --%
                 ^^^^Syntax error. (Please report for improvements on the error message. Error state: #298)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     299| (x = x, with --%
                  ^^^^Syntax error. (Please report for improvements on the error message. Error state: #299)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     302| 'Lab x _ --%
                 ^Syntax error. (Please report for improvements on the error message. Error state: #302)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     305| 'Lab with --%
               ^^^^Syntax error. (Please report for improvements on the error message. Error state: #305)
  ----------
  
  Fatal error:
    Syntax error in 'ui/should-fail/exhaustive.batch.test'
    help:
     308| x then --%
            ^^^^Syntax error. (Please report for improvements on the error message. Error state: #308)
  ----------
  
  Input term:
    ()
  
  Generated constraint:
    ∃?final_type. (∃(?prod = {}). ?prod = ?final_type) ∧ decode ?final_type
  
  Inferred type:
    {}
  
  Elaborated term:
    ()
  [199]


  $ minihell ui/empty_pat.test
  Fatal error:
    Syntax error in 'ui/empty_pat.test'
    help:
       3|   | => ()
              ^^Syntax error. (Please report for improvements on the error message. Error state: #209)
  ----------
  
  Fatal error:
    Syntax error in 'ui/empty_pat.test'
    help:
       6| lambda x. let = x in ()
                        ^Syntax error. (Please report for improvements on the error message. Error state: #99)
  ----------
  
  Input term:
    ()
  
  Generated constraint:
    ∃?final_type. (∃(?prod = {}). ?prod = ?final_type) ∧ decode ?final_type
  
  Inferred type:
    {}
  
  Elaborated term:
    ()
  [102]
