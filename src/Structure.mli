module TyVar : module type of Utils.Variables ()

type ('v, 'a) t_ =
  | Wildcard
  | Var of 'v
  | Arrow of 'a * 'a
  | IAdt of (int, 'a) adt
  | SAdt of (string, 'a) adt
  | Alias of 'a alias

and ('l, 'a) adt = Prod of ('l, 'a) Unsized.t | Sum of ('l, 'a) Unsized.t
and 'a alias = string * 'a list

type 'a raw = (string, 'a) t_
type 'a t = (TyVar.t, 'a) t_
type cyclic_ty = Loop | Any | Constr of (TyVar.t, cyclic_ty) t_
type cyclic_ty_data = { ty : cyclic_ty; user_vars : Ls.source list }

val iter : ('a -> unit) -> ('v, 'a) t_ -> unit
val map : ('a -> 'b) -> ('v, 'a) t_ -> ('v, 'b) t_

val merge :
  (('a, 'b) Utils.either_or_both -> 'c) -> 'a t -> 'b t -> 'c t Utils.doc_result

val global_tyvar : string -> TyVar.t
val freshen : ('a -> 'b) -> 'a raw -> 'b t

val product_pp :
  seq:('l, 'a) Unsized.t ->
  obj_pp:('l -> int -> 'a -> PPrint.document) ->
  PPrint.document
(** Pretty-print a collection as ["{t1 * t2 * ...}"]
      (each [ti] is an [obj_pp li i vi] for [(li:vi)] in position [i] of [seq]) *)

val sum_pp :
  seq:('l, 'a) Unsized.t ->
  obj_pp:('l -> int -> 'a -> PPrint.document) ->
  PPrint.document
(** Pretty-print a collection as ["[t1 + t2 + ...]"]
    (each [ti] is an [obj_pp li i vi] for [(li:vi)] in position [i] of [seq]) *)

val shallow_pp : ('a -> PPrint.document) -> 'a t -> PPrint.document
(** Lift a printer of objects into a printer for structure of objects.

    WARNING: if you invoke this function in a recursive caller it might not handle
             atomicity properly. Use [deep_pp] for deep structures. *)

val deep_pp :
  lift:(((TyVar.t, 'b) t_ -> PPrint.document) -> 'a -> PPrint.document) ->
  unfold:('b -> 'a) ->
  'a ->
  PPrint.document
(** Wrapper around [shallow_pp] enriched with information to correctly display
    a recursive structure.
    For a type representing a deep structure, defined as generally as possible as
    {[
        module M = sig
            type 'a t
            val destruct : 'a t -> 'a
        end

        val other_pp : other -> PPrint.document

        type 'a thing = Struct of ('a, 'a thing M.t) Structure.t_ | Other of other
    ]}
    the canonical invocation of [deep_pp] provides a correct printer for
    [TyVar.t thing] and should look like
    {[
        let pp : thing -> PPrint.document =
            Structure.deep_pp ~unfold:M.destruct
                ~lift:(fun deep_pp ->
                    function
                    | Struct s -> deep_pp s
                    | Other other -> other_pp other)
    ]}

    Side note: in the present project, [cyclic_ty] and [STLC.ty] are of this form,
    with respectively [M = Id], [Struct = Constr], [other = Loop | Any]
    and [M = L], [Struct = Constr], [other = |]
    *)

val cycle_pp : cyclic_ty -> PPrint.document
