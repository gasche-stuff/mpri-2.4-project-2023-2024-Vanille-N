type 'a unsorted_accum = Singleton of 'a | Union of 'a unsorted_accum list
type source = string * L.aux
type source_hint = source option
type source_hints = source unsorted_accum option

let source_singleton = function
    | None -> None
    | Some src -> Some (Singleton src)

let source_union s s' =
    match (s, s') with
    | None, s' -> s'
    | s, None -> s
    | Some s, Some s' -> Some (Union [ s; s' ])

let source_collect s =
    let rec aux = function
        | Singleton a -> [ a ]
        | Union ls -> List.concat_map aux ls
    in
    match s with Some s -> aux s | None -> []
