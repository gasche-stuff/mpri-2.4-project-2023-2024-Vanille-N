(** Infer contains the logic to generate an inference constraint from
    an untyped term, that will elaborate to an explicitly-typed term
    or fail with a type error. *)

(* You have to implement the [has_type] function below,
   which is the constraint-generation function. *)

module Make (T : Utils.Functor) = struct
  module Constraint = Constraint.Make (T)
  open Constraint
  module Untyped = Untyped.Make (T)

  module Env = Untyped.Var.Map
  (** The "environment" of the constraint generator maps each program
        variable to an inference variable representing its (monomorphic)
        type.

        For example, to infer the type of the term [lambda x. t], we
        will eventually call [has_type env t] with an environment
        mapping [x] to a local inference variable representing its type.
    *)

  type env = variable Env.t

  type err = eq_error =
    | Clash of (unit -> STLC.ty L.t Utils.clash)
    | Cycle of (unit -> Structure.cyclic_ty_data)

  type 'a constraint_ = ('a, err) Constraint.t

  let eq v1 v2 = Eq (v1, v2)
  let decode v = MapErr (Decode v, fun e -> Cycle (fun () -> e))
  let ret x = Ret (fun _ -> x)
  let fresh : string -> Constraint.variable = Constraint.Var.fresh
  let uname : Untyped.Var.t -> string = Untyped.Var.name

  module AliasEnv = Map.Make (struct
    type t = string

    let compare = compare
  end)

  (* We handle a type alias [<a,b,c>T of s] by storing
     [a,b,c] and [s] together, and during the future instanciation of [s] we will
     inject replacements for occurrences of [a,b,c], making instances of
     [a,b,c] in [s] act not like type variables as specified by [Structure]'s
     [Var] constructor, but more like holes in [s] to be filled at flattening
     time.
     Thus [alias] behaves almost like a function
     [variable Structure.t list -> variable Structure.t]. *)
  type alias = Structure.TyVar.t list * Structure.TyVar.t STLC.ty_ L.t

  (* Said mapping is stored is a map with [T] as key so that when we
     encounter a [T] constructor we can obtain its structure with holes. *)
  and aliases = alias AliasEnv.t

  (* Lookup of an inductive type constructor *)
  let alias_find name loc aliases =
      Utils.try_find AliasEnv.find name aliases (fun x ->
          ( x,
            fun file ->
              L.highlight_range file loc
                ~label:(PPrint.string "this constructor escaped its scope") ))

  (** == Binders cheat sheet ==
      The code that follows contains quite a lot of binders because we need
      to constantly update the environments to
      - introduce new existential variables
      - flatten types by adding type variables at each node
      - flatten patterns in the same way

      We use the following notations:
      - [let/?]        introduce an existential variable
      - [let-:]        flatten a type
      - [let-::]       flatten a tuple of types
      - [let-%]        flatten a pattern
      - [let-%%]       flatten a tuple of patterns

      More precisely:
        [let/? x = ("x", Top) in c] creates a new existential variable named
        [x] with structure constraint [Top] and evaluates the constraint [c]
        in the new environment

        [let-:] replaces [bind] in the original code skeleton. It flattens a
        nested type into a single type variable, with the side effect of
        creating intermediate type variables that connect together all
        the layers of the type.
        [let-::] is the folding of [let-:] over [Unsized.t].
        The extra parameter [foreign] (associative list from type variables
        to inference variables) passed to [let-:] makes this flattening
        consider instances bound in the list not as rigid type variables
        but as holes in the structure.

        [let-%` is the equivalent of [let-:] but instead of being from
        nested types to flat types it is from nested patterns to flat patterns.
        While doing so it adds all the leaf variables to the context, so if
        you have the pattern [(a, (b, _, c))] then calling [let-%] on that
        will unnest the pattern with extra type variables and enrich the
        environment with [a], [b], and [c].
        [let-%%] is the folding of [let-%] over [Unsized.t].
    *)

  (** Shorthand to introduce an existential variable to a constraint.
      Usage: [let/? x = ("name", Top) in ...] *)
  let ( let/? )
      ( (ident : string),
        (here : L.aux),
        (source : Ls.source_hint),
        (structure : structure Hint.t) )
      (term_cont : Constraint.variable -> ('a, 'b) t) : ('a, 'b) t =
      let ident = fresh ident in
      Exist (ident, here, source, structure, term_cont ident)

  (* Associative lists that describe generic type variable substitutions *)
  type foreign = (Structure.TyVar.t * variable) list

  (* Type of auxiliary functions that build structures from tuples *)
  type 'label typ_builder = ('label, variable) Unsized.t -> variable Structure.t

  (* Type of auxiliary functions that build structures from abstract datatype constructors *)
  type 'label structure_builder =
    ('label, variable) Structure.adt -> variable Structure.t

  (* Type of auxiliary functions that build terms from abstract datatype constructors *)
  type 'label stlc_builder = 'label STLC.adt -> STLC.term L.t

  (** This is a helper binder to implement constraint generation for
      the [Annot] construct.

      [let-: (ty, foreign) in k] (ignore [foreign] for now) takes a type [ty],
      and a constraint [k] parametrized over a constraint variable.
      It creates a constraint context that binds a new constraint variable [?w]
      that must be equal to [ty], and places [k ?w] within this context.

      For example, if [ty] is the type [?v1 -> (?v2 -> ?v3)] , then
      [let-: (ty, []) in k] could be the constraint
          [∃(?w1 = ?v2 -> ?v3). ∃(?w2 = ?v1 -> ?w1). k ?w2], or equivalently
          [∃?w3 ?w4. ?w3 = ?v1 -> ?w4 ∧ ?w4 = ?v2 -> ?v3 ∧ k ?w3].

      For this function to also be useful for [Typedef], an additional
      parameter [foreign] is passed, whose role is to lazily substitute
      some type variables.
      [foreign] is an associative list from type variables to inference variables
      and allows binding values deep inside the type to type alias parameters. *)
  let rec ( let-: ) ((ty : STLC.ty L.t), (foreign : foreign))
      (k : Constraint.variable -> ('a, 'e) t) : ('a, 'e) t =
      L.transform ty ~fn:(fun here (ty : STLC.ty) ->
          let (Constr ty) = ty in
          match ty with
          (* No structure constraint here, we just create the variable *)
          | Wildcard ->
              let/? iw = ("_", here, None, Top) in
              k iw
          (* [Var] leaf of the pattern. Due to type aliases there is a subtlety here.

             Variables not bound in [foreign] are rigid type variables and naturally
             get a [Var] structural constraint.

             On the other hand, those variables that are bound should be seen
             as holes in the structure and need special handling.
             During the flattening of the inner [int * a] of [type <a>T of int * a],
             we _DON'T_ want to produce the constraint
             {[
                 (* Incorrect *)
                 exists ?i = Var int,
                   exists ?a = Var a,
                     exists ?p = Prod [?i, ?a],
                       k ?p
             ]}
             because this is not generic in [a].
             Instead we want to know what variable [?a] is bound to [a] and generate
             {[
                 (* Fixed if [foreign = [(a:?a)]] *)
                 exists ?i = Var int,
                   exists ?p = Prod [?i, ?a]
                     k ?p
             ]}
             thus injecting a correspondence between the type parameter
             that [a] is instanciated to, and the inner structure. *)
          | Var x -> (
              match List.assoc_opt x foreign with
              | None ->
                  let/? ix =
                      (Structure.TyVar.name x, here, None, Just (Var x))
                  in
                  k ix
              | Some ix -> k ix)
          (* [Arrow] node of the pattern: naturally gets an [Arrow] structural constraint,
             and we also need to recursively flatten both sides. *)
          | Arrow (x, y) ->
              let-: ix = (x, foreign) in
              let-: iy = (y, foreign) in
              let/? iarr = ("arr", here, None, Just (Arrow (ix, iy))) in
              k iarr
          (* Composite constraints are handled by `( let-:: )` which essentially
             just folds `( let-: )` over all elements of the tuple. *)
          | IAdt (Prod tup) ->
              let-:: itup = ("prod", k, here, tup, foreign) in
              IAdt (Prod itup)
          | SAdt (Prod tup) ->
              let-:: itup = ("prod", k, here, tup, foreign) in
              SAdt (Prod itup)
          | IAdt (Sum case) ->
              let-:: icase = ("sum", k, here, case, foreign) in
              IAdt (Sum icase)
          | SAdt (Sum case) ->
              let-:: icase = ("sum", k, here, case, foreign) in
              SAdt (Sum icase)
          | Alias (name, args) -> flatten_alias foreign k here name [] args)

  (** From the type flattener `let-:`, produce a type tuple flattener by
      folding over `Unsized.t`.
      Without the [type label.] annotation OCaml would infer a type not general
      enough as we need this to work for both [int] and [string] labels.

      WARNING: As a [let] operator this function is written in continuation
      style, but beware that the "actual" continuation is not in the usual
      position. Observing the actual call sites above has revealed that
      invocations of [let-::] are much more concise by having the continuation
      {[
      let-:: itup = ("prod", k, tup) in IAdt (Prod itup)
                             ^here,
                             instead of ^^^^^^^^^^^^^^^^here
                             where it would normally be.
      ]}
      The place where the continuation is usually found instead contains
      a constraint tuple generator. *)
  and ( let-:: ) :
      type label.
      string
      * (Constraint.variable -> ('a, 'e) t)
      * L.aux
      * (label, STLC.ty L.t) Unsized.t
      * foreign ->
      label typ_builder ->
      ('a, 'e) t =
     fun ( name,
           (* Continuation *)
           k,
           here,
           (* The type tuple to flatten *)
           tup,
           (* The Typedef substitutions *) foreign )
         (* This is how to build the constraint from the tuple of variables *)
           typ_builder ->
      Unsized.accumulate
        (fun (k : variable -> _ t) (x : STLC.ty L.t) ->
          (* Recursively flatten the inner patterns and continue with
             the fresh variable they are now bound to. *)
          let-: ix = (x, foreign) in
          k ix)
        (fun (tup : (label, variable) Unsized.t) ->
          (* Base case: use [typ_builder] to build the structural constraint
             on the new variable *)
          let/? iprod = (name, here, None, Just (typ_builder tup)) in
          k iprod)
        tup

  (* This is where to flatten [<a,b,c>T] we
     - instanciate new inference variables for [a,b,c]
     - prevent cyclic types during decoding by not recursing into the inner
       definition and simply generating a new structural constraint [Alias]. *)
  and flatten_alias foreign k here name acc = function
      | [] ->
          let/? ialias =
              ("alias", here, None, Just (Alias (name, List.rev acc)))
          in
          k ialias
      | arg :: args ->
          let-: iarg = (arg, foreign) in
          flatten_alias foreign k here name (iarg :: acc) args

  (** Goes in and out of inductive type constructors.
      [fold_unfold name aliases k] expects
      - [name : string] the name of the type, which is also the label of the constructor,
      - [aliases : aliases] the map from inductive type names to structure definitions,
      - [k] a continuation that generates a constraint

      [k] will be given two inference variables that correspond to the
      types inside and outside of the constructor, in that order. *)
  let fold_unfold (name : string) (head_loc : L.aux) (here : L.aux)
      (hint_weak : bool) (aliases : aliases)
      (k : variable -> variable -> ('a, 'e) t) : ('a, 'e) t =
      let params, structure = alias_find name head_loc aliases in
      (* Helper to introduce all the new inference variables *)
      let rec aux acc k = function
          | [] -> k (List.rev acc)
          | v :: rest ->
              let/? iv = ("v", here, None, if hint_weak then Skip else Top) in
              aux ((v, iv) :: acc) k rest
      in
      aux []
        (fun foreign ->
          let iparams = List.map snd foreign in
          (* We are providing a nonempty [foreign] to [let-:] because any
             variables bound by type parameters need to be inserted inside
             the structure. *)
          let-: iinner = (structure, foreign) in
          (* This structure "blocks" decoding: when [k] calls [decode]
             on [iouter] it will instanciate [Alias _], not the inner
             structure, which prevents cyclic types during decoding. *)
          let/? iouter = ("opaque", here, None, Just (Alias (name, iparams))) in
          k iinner iouter)
        params

  (* From an untyped pattern ([_ Untyped.pattern]), produces a constraint
     that computes an annotated pattern ([STLC.pattern]).
     This transformation removes annotations on the nodes but annotates all
     the non-wildcard leaves (i.e. it annotates all the newly bound variables). *)
  let rec decode_pat (aliases : aliases)
      (pat : (Env.key * variable) Untyped.pattern L.t) (w : variable) :
      (STLC.pattern L.t, err) t =
      let inner =
          L.transform pat ~fn:(fun here pat ->
              Pattern.(
                match pat with
                | Wildcard -> ret @@ L.at here @@ Wildcard
                (* [v] is naturally annotated by [decode iv] *)
                | Var ((v, iv), loc) ->
                    let+ () = eq iv w and+ typ = decode iv in
                    L.at here @@ Var (STLC.{ var = v; typ }, loc)
                (* Check that the types above and below agree,
                   annotate the current variable with its personal inference variable [iv],
                   and recurse into the inner pattern. *)
                | Named ((v, iv), loc, p) ->
                    let+ () = eq iv w
                    and+ typ = decode iv
                    and+ p_ann = decode_pat aliases p iv in
                    L.at here @@ Named (STLC.{ var = v; typ }, loc, p_ann)
                (* Check that the types above and below agree, and there's nothing to
                   annotate here because no variable binding. The [Annot] constructor
                   completely disappears from the elaborated term. *)
                | Annot (t, ty) ->
                    let-: ity = (ty, [ (* Not a [Typedef] *) ]) in
                    let+ t_ann = decode_pat aliases t ity
                    and+ () = eq ity w
                    and+ ty_dec = decode ity in
                    if Config.(OnceCell.get annotation_level) = 1 then
                      L.at here @@ Pattern.Annot (t_ann, ty_dec)
                    else t_ann
                (* Recursively fold [decode_pat] over the tuple of patterns,
                   and reconstruct the final structure with the tuple of annotated patterns. *)
                | Tuple tup ->
                    let+ typed_pat =
                        decode_pats aliases tup w here (fun tup ->
                            IAdt (Prod tup))
                    in
                    L.at here @@ Tuple typed_pat
                | STuple tup ->
                    let+ typed_pat =
                        decode_pats aliases tup w here (fun tup ->
                            SAdt (Prod tup))
                    in
                    L.at here @@ STuple typed_pat
                (* We just need to decode the inner pattern and bind the folded value
                   to the free variable, and [fold_unfold] will do the link between the two. *)
                | Opaque (name, loc, inner) ->
                    fold_unfold name loc here true aliases (fun iinner iouter ->
                        let+ inner_ann = decode_pat aliases inner iinner
                        and+ () = eq w iouter in
                        L.at here @@ Opaque (name, loc, inner_ann))))
      in
      L.transform pat ~fn:(fun here pat ->
          Pattern.(
            match pat with
            | Annot _ | Var _ | Named _ ->
                (* They arleady get their annotation anyway *) inner
            | _ ->
                if Config.(OnceCell.get annotation_level) >= 2 then
                  let/? ity = ("annot", here, None, Weak) in
                  let+ inner_ann = inner
                  and+ () = eq w ity
                  and+ ty_dec = decode ity in
                  L.at here @@ Annot (inner_ann, ty_dec)
                else inner))

  (* Fold [decode_pat] over a tuple to recursively annotate the whole
     pattern. *)
  and decode_pats :
      type label.
      aliases ->
      (label, Env.key * variable, STLC.ty) Pattern.tup ->
      variable ->
      L.aux ->
      label typ_builder ->
      ((label, STLC.pattern L.t) Unsized.t, err) t =
     fun (* Inductive type definitions *)
           aliases
         (* We need to fold [decode_pat] over all elements of this tuple *)
           tup
         (* Finally once the tuple is generated, it should be equal to this free variable. *)
           w here
         (* Auxiliary function: this tells us how to compute a constraint of the right structure
            from the tuple of variables we built. *)
           typ_builder ->
      let fold_decode (pats : (label, _ Pattern.t L.t * variable) Unsized.t) =
          (* This fold looks scary but it's not that bad when you look
             past the horrible type annotations: *)
          Unsized.fold_right
            (* The inductive case consists of... *)
              (fun (l : label) ((p : _ Pattern.t L.t), (ip : variable))
                   (acc :
                     ((label, _ Pattern.t L.t) Unsized.raw, _) Constraint.t) ->
              let+ rest_ann = acc (* annotating the tail, *)
              and+ p_ann = decode_pat aliases p ip (* annotating the head, *) in
              (* concatenating the two. *)
              Unsized.(cons l p_ann rest_ann))
            (* And the base case is of course an empty tuple. *)
              (fun sz -> ret Unsized.(nil sz))
            pats
      in
      (* Having the above as a helper function we can generate our
         tuple and its structure. *)
      Unsized.accumulate
        (fun (k : _ Pattern.t L.t * variable -> (_ Unsized.t, _) Constraint.t)
             (p : _ Pattern.t L.t) ->
          (* For each element we generate a fresh variable *)
          L.transform p ~fn:(fun here _ ->
              let/? ip = ("p", here, None, Skip) in
              k (p, ip)))
        (fun (pats : (label, _ Pattern.t L.t * variable) Unsized.t) ->
          (* This gives us the tuple of only the inference variables... *)
          let vars = pats |> Unsized.map (fun (_, ip) -> ip) in
          (* ...that we bind to its structure. *)
          let/? iprod = ("prod", here, None, Just (typ_builder vars)) in
          let+ () = eq iprod w (* Free variable given by the caller *)
          (* Actual decoding *)
          and+ pats_ann = fold_decode pats in
          (* NOTE: here we need an [Unsized.sort] so that the types match
             because the [Unsized] library doesn't trust the output of
             our [accumulate] to be sorted, but because we didn't touch
             the labels in any way this sorting operation will be a noop. *)
          Unsized.sort
            (* Infaillible *) (L.dummy __FILE__)
            Printer.panic pats_ann)
        tup

  (** Pattern binder: this takes an environment, a pattern, and a continuation and it
      - updates the environment with the new bindings introduced by the pattern
      - zips the pattern with an inference variable for each leaf
      I.e. this serves the same purpose for patterns as `let-:` does for types. *)
  let rec ( let-% ) ((env : env), (pat : Env.key Untyped.pattern L.t))
      (k : env * (Env.key * variable) Untyped.pattern L.t -> ('a, _) t) :
      ('a, _) t =
      L.transform pat ~fn:(fun here pat ->
          Pattern.(
            match pat with
            (* No new binding introduced by [_]. *)
            | Wildcard -> k (env, L.at here @@ Wildcard)
            (* Base case with a new variable in the context. *)
            | Var (v, loc) ->
                let/? iv = (uname v, here, Some (uname v, loc), Skip) in
                k (Env.(env |> add v iv), L.at here @@ Var ((v, iv), loc))
            (* Recurse, but with an extra variable in the context.
               Reminder: we're not doing any queries to [env], only adding stuff to it,
               so we don't need to be very careful about the order in which they are inserted. *)
            | Named (v, loc, p) ->
                let/? iv = (uname v, here, Some (uname v, loc), Skip) in
                let env = Env.(env |> add v iv) in
                let-% env, ip = (env, p) in
                k (env, L.at here @@ Named ((v, iv), loc, ip))
            (* Nothing special occurs here, we just recurse. *)
            | Annot (vt, ty) ->
                let-% env, ivt = (env, vt) in
                k (env, L.at here @@ Annot (ivt, ty))
            (* Composite cases easily handled by [( let-%% )] above. *)
            | Tuple tup ->
                let-%% env, btup = (env, tup) in
                k (env, L.at here @@ Tuple btup)
            | STuple tup ->
                let-%% env, btup = (env, tup) in
                k (env, L.at here @@ STuple btup)
            (* Bound variables are all inside the constructor. *)
            | Opaque (name, loc, inner) ->
                let-% env, binner = (env, inner) in
                k (env, L.at here @@ Opaque (name, loc, binner))))

  (** From the pattern flattener [let-%], produce a pattern tuple flattener by
      folding over [Unsized.t] *)
  and ( let-%% ) :
      type label.
      env * (label, Env.key Untyped.pattern L.t) Unsized.t ->
      (* continuation *)
      (env * (label, (Env.key * variable) Untyped.pattern L.t) Unsized.t ->
      ('a, 'e) t) ->
      ('a, 'e) t =
     fun (env, pat) k ->
      Unsized.accumulate_with
        (fun (k : env -> (Env.key * variable) Untyped.pattern L.t -> ('a, 'e) t)
             (env : env) (pat : Env.key Untyped.pattern L.t) ->
          (* Flatten one element of the tuple then continue *)
          let-% env, ipat = (env, pat) in
          k env ipat)
        (fun (env : env) (tup : (label, _ Pattern.t L.t) Unsized.t) ->
          (* Base case is trivial: just call the continuation! *)
          k (env, tup))
        env pat

  type has_type =
    env -> aliases -> Untyped.term L.t -> variable -> (STLC.term L.t, err) t

  (* Map [has_type] to a tuple.

     Although this is parameterized by [typ_builder], making it look like
     this could work for a generic [Unsized.t], the implementation details
     make this function relevant only to product types. You may however
     instanciate it separately for product types indexed over different labels. *)
  let tup_has_type (has_type : has_type) (env : env) (aliases : aliases)
      ((w, here_w) : variable * L.aux) (typ_builder : 'label typ_builder)
      (tup : ('label, Untyped.term L.t) Unsized.t) : ('label STLC.tuple, err) t
      =
      let aux (tup : ('label, Untyped.term L.t * variable) Unsized.t) :
          ('a, 'e) t =
          (* Straightforward folding into a list, while adding a
             [has_type] at each index. *)
          Unsized.fold_right
            (fun (l : 'label) ((t : Untyped.term L.t), (it : variable))
                 (acc : ('label STLC.tuple, 'e) t) ->
              let+ typed_rest = acc and+ t_typed = has_type env aliases t it in
              (l, t_typed) :: typed_rest)
            (fun _sz -> ret [])
            tup
      in
      Unsized.accumulate
        (fun (k : Untyped.term L.t * variable -> ('label STLC.tuple, _) t)
             (t : Untyped.term L.t) ->
          L.transform t ~fn:(fun here _ ->
              let/? it = ("s", here, None, Top) in
              k (t, it)))
        (fun (tup : ('label, Untyped.term L.t * variable) Unsized.t) ->
          let vars = tup |> Unsized.map (fun (_, ivar) -> ivar) in
          let/? iprod = ("prod", here_w, None, Just (typ_builder vars)) in
          let+ typed_tup = aux tup
          (* Set the output variable to equal the product of all inner variables. *)
          and+ () = eq iprod w in
          typed_tup)
        tup

  (* Map [has_type] to the arms of a [case ... esac] construct.

     Although this is parameterized by [typ_builder], making it look like
     this could work for a generic [Unsized.t], the implementation details
     make this function relevant only to sum types. You may however instanciate
     this separately for sum types indexed over different labels. *)
  let arms_have_type (has_type : has_type) (env : env) (aliases : aliases)
      (here : L.aux)
      ~input_ivar:
        (* Set the *input* of the [case] to equal this variable. *)
        (inp : variable)
      (* Set the *output* of all arms to equal this variable. *)
        (w : variable) (typ_builder : 'label typ_builder)
      (arms :
        ('label, Env.key Untyped.pattern L.t * Untyped.term L.t) Unsized.t) :
      (* Note the loss of information in the [STLC] construct of [case] compared
         to the internal representation: when typing the term we forget its length
         hence the switch from [Unsized.t] in the input to [list] in the output. *)
      (('label * STLC.pattern L.t * STLC.term L.t) list, err) t =
      let arm_has_type pats =
          Unsized.fold_right
            (fun (l : 'label) (pat, ipat, body) acc ->
              L.transform body ~fn:(fun here _ ->
                  let/? ibody = ("b", here, None, Top) in
                  let-% env, pat = (env, pat) in
                  (* Recurse into the later arms *)
                  let+ rest = acc
                  and+ pat_ann = decode_pat aliases pat ipat
                  and+ typed_body = has_type env aliases body ibody
                  (* The output of the arm is the final type of the case. *)
                  and+ () = eq ibody w in
                  (l, pat_ann, typed_body) :: rest))
            (fun _sz -> ret [])
            pats
      in
      Unsized.accumulate
        (fun k (pat, body) ->
          L.transform pat ~fn:(fun here _ ->
              let/? ipat = ("wp", here, None, Skip) in
              k (pat, ipat, body)))
        (fun arms ->
          (* Extract the tuple of variables declared by the patterns. *)
          let ivars = arms |> Unsized.map (fun (_, ivar, _) -> ivar) in
          let/? isum = ("ws", here, None, Just (typ_builder ivars)) in
          (* The input type must equal the sum of all patterns. *)
          let+ () = eq isum inp
          (* Type the arms using the auxiliary function.
             This implicitly binds the output type. *)
          and+ typed_arms = arm_has_type arms in
          typed_arms)
        arms

  (* Map [has_type] over an option. *)
  let try_has_type (has_type : has_type) (env : env) (aliases : aliases)
      (ox : Untyped.term L.t option) (w : variable) :
      (STLC.term L.t option, err) t =
      match ox with
      | None -> ret None
      | Some x ->
          let+ x_typed = has_type env aliases x w in
          Some x_typed

  (* [has_type] helper for a [case] construct of any label type. *)
  let case_has_type (has_type : has_type) (env : env) (aliases : aliases)
      (* Set the output of the [case] to equal this variable. *)
        (here : L.aux) (label_printer : 'label -> PPrint.document)
      (w : variable) (typ_builder : 'label typ_builder)
      (case : ('label, _, _) Untyped.case_) : ('label STLC.case, err) t =
      let t, arms, default = case in
      (* A bit of encoding manipulation:
         the parser provides a [(l * p * t) list],
         the typer works on a [(l, (p * t)) Unsized.t],
         and the output should be a [(l * p_annotated * t_typed) list]. *)
      let here_t = L.transform t ~fn:(fun here _ -> here) in
      let encode (lab, var, res) = (lab, (var, res)) in
      let/? it = ("it", here_t, None, Top) in
      let+ typed_arms =
          arms_have_type has_type env aliases here ~input_ivar:it w typ_builder
            Unsized.(
              sort here label_printer
              @@ of_list
                   (* The match is exhaustive iff [default] is [None] *)
                   Unsized.(bound_of_termin default)
                   (List.map encode arms))
      and+ typed_t = has_type env aliases t it
      and+ typed_default = try_has_type has_type env aliases default w in
      (typed_t, typed_arms, typed_default)

  (** Quickly generate (often unused) variables, e.g. for [Untyped.num] *)
  let fresh_var_binder (name : string) (hint : _ Hint.t) :
      (variable, _) Unsized.var_binder =
     fun k ->
      let/? it =
          (name, L.erased (Format.sprintf "%s:%d" __FILE__ __LINE__), None, hint)
      in
      let+ rest = k it in
      rest

  let adt_has_type (type label)
      (projector : (label, variable, (STLC.term L.t, _) t) Unsized.producer)
      (structure_builder : label structure_builder)
      (stlc_builder : label stlc_builder) (has_type : has_type) (env : env)
      (aliases : aliases) (here : L.aux)
      (label_printer : label -> PPrint.document)
      ((w, here_w) : variable * L.aux) :
      (label, _, _) Untyped.adt_ -> (STLC.term L.t, _) t = function
      (* The [Sum] and [Field] cases look very similar, they're dual of each other.
         Notice the key differences:
         - unused variables are given the hints [Bot] in [Sum] but [Skip] in [Field],
           because an unread tuple field is assumed ignored and an uninstanciated variant
           is assumed empty.
         - in [Sum] the final value [w] is equal to the whole sum, whereas in
           the [Field] case the final value [w] is the projection.
           ([eq isum w] vs [eq inth w]) *)
      | Sum (inj, t) ->
          projector
            (* Unused values are assumed empty *)
            (fresh_var_binder "_" Bot)
            (* This is the value we're interested in *)
            (fresh_var_binder "nth" Top)
            (* Once we have gathered all projections... *)
              (fun (it : variable) (full : (label, variable) Unsized.t) ->
              let/? isum =
                  ("ws", here, None, Just (structure_builder (Sum full)))
              in
              (* ...we set the output [w] to be the whole sum *)
              let+ () = eq isum w
              (* ...and we check that the inner value is indeed the correct projection *)
              and+ typed_t = has_type env aliases t it in
              stlc_builder (Sum (L.destruct inj, typed_t)))
            Unsized.Approx (* not exhaustive *)
            (L.destruct inj)
      | Field (t, nth) ->
          projector
            (* Unused values are assumed ignored *)
            (fresh_var_binder "_" Skip)
            (* This is the value we're interested in *)
            (fresh_var_binder "nth" Top)
            (fun (inth : variable) (full : (label, variable) Unsized.t) ->
              (* Once we have gathered all projections... *)
              let/? iprod =
                  ("wp", L.aux nth, None, Just (structure_builder (Prod full)))
              in
              (* ...we set the output to be the nth component *)
              let+ () = eq inth w
              (* ...and the inner term is the whole product *)
              and+ typed_t = has_type env aliases t iprod in
              stlc_builder (Field (typed_t, L.destruct nth)))
            Unsized.Approx (* not exhaustive *)
            (L.destruct nth)
      | IfLet (inj, pat, term, yes, no) ->
          projector
            (* Unused values are assumed ignored *)
            (fresh_var_binder "_" Skip)
            (* This is the value we're interested in *)
            (fresh_var_binder "nth" Skip)
            (* Once we have gathered all projections... *)
              (fun (it : variable) (full : (label, variable) Unsized.t) ->
              let-% env', pat = (env, pat) in
              let/? isum =
                  ("sum", L.aux inj, None, Just (structure_builder (Sum full)))
              in
              (* ... we can type the destructed term in the current context, *)
              let+ typed_term = has_type env aliases term isum
              (* the pattern is a projection to one component, *)
              and+ pat_ann = decode_pat aliases pat it
              (* the left branch in the *enriched* context *)
              and+ typed_yes = has_type env' aliases yes w
              (* and the right branch doesn't have any extra environment *)
              and+ typed_no = has_type env aliases no w in
              stlc_builder
                (IfLet (L.destruct inj, pat_ann, typed_term, typed_yes, typed_no)))
            Unsized.Approx (* not exhaustive *)
            (L.destruct inj)
      (* See helpers [case_has_type] and [tup_has_type] above. *)
      | Case (t, arms, default) ->
          let+ typed_t, typed_arms, typed_default =
              case_has_type has_type env aliases here label_printer w
                (fun ivars -> structure_builder (Sum ivars))
                (t, arms, default)
          in
          stlc_builder (Case (typed_t, typed_arms, typed_default))
      | Tuple tup ->
          let+ typed_tup =
              tup_has_type has_type env aliases (w, here_w)
                (fun tup -> structure_builder (Prod tup))
                Unsized.(sort here label_printer @@ of_list Exact tup)
          in
          stlc_builder (Tuple typed_tup)
      | With (t, repls) ->
          let+ typed_t = has_type env aliases t w
          and+ typed_subst =
              tup_has_type has_type env aliases (w, here_w)
                (fun tup -> structure_builder (Prod tup))
                Unsized.(sort here label_printer @@ of_list Approx repls)
          in
          stlc_builder (With (typed_t, typed_subst))

  (** This function generates a typing constraint from an untyped term:
        [has_type env t w] generates a constraint [C] which contains [w] as
        a free inference variable, such that [C] has a solution if and only
        if [t] is well-typed in [env], and in that case [w] is the type of [t].

        For example, if [t] is the term [lambda x. x], then [has_type env t w]
        generates a constraint equivalent to [∃?v. ?w = (?v -> ?v)].

        Precondition: when calling [has_type env t], [env] must map each
        term variable that is free in [t] to an inference variable. *)
  let rec has_type (env : env) (aliases : aliases) (t : Untyped.term L.t)
      (w : variable) : (STLC.term L.t, err) t =
      let inner =
          L.transform t ~fn:(fun here t ->
              match t with
              | Untyped.Var (x, loc) ->
                  (* Simply [w = ?x], we just have to do a map lookup to know [?x] *)
                  let ix = STLC.TeVar.Map.find x env in
                  let/? ix' = (uname x, here, Some (uname x, loc), Skip) in
                  let+ () = eq ix' ix and+ () = eq ix w in
                  L.at here @@ STLC.Var x
              | App (t, u) ->
                  let here_u = L.aux u in
                  let here_t = L.aux t in
                  let/? iu = ("wu", here_u, None, Top) in
                  let/? iout = ("wr", here, None, Top) in
                  let/? it = ("wt", here_t, None, Just (Arrow (iu, iout))) in
                  let+ t_typed = has_type env aliases t it
                  and+ u_typed = has_type env aliases u iu
                  (* The right hand side of the arrow is the final type *)
                  and+ () = eq iout w in
                  L.at here @@ STLC.(App (t_typed, u_typed))
              | Abs (pat, t) ->
                  let here_pat = L.aux pat in
                  let here_t = L.aux t in
                  let/? ipat = ("wpat", here_pat, None, Weak) in
                  let/? ires = ("wt", here_t, None, Top) in
                  let/? it = ("warr", here, None, Just (Arrow (ipat, ires))) in
                  (* Enrich [env] with the leaf variables of [pat] *)
                  let-% env', pat = (env, pat) in
                  let+ pat_ann = decode_pat aliases pat ipat
                  (* The final type is the arrow *)
                  and+ () = eq it w
                  (* The body is typed in the updated environment *)
                  and+ t_typed = has_type env' aliases t ires in
                  L.at here @@ STLC.(Abs (pat_ann, t_typed))
              | Annot (t, ty) ->
                  (* [?w = (typeof t) = ty *)
                  let-: ity = (ty, [ (* Not a [Typedef] *) ]) in
                  (* The choice of [(typeof t) = ty /\ ty = ?w] rather than the
                     theoretically equivalent [(typeof t) = ?w /\ ?w = ty] should
                     make error messages more understandable because we don't have
                     conflicts that travel across annotations. *)
                  let+ t_typed = has_type env aliases t ity
                  (* Free variable equal to the type annotation *)
                  and+ () = eq ity w
                  and+ ty_dec = decode ity in
                  (* The sample [tests.t/run.t] given initially never emit an [Annot]
                     construct here. We choose to do so depending on a command-line
                     flag. *)
                  if Config.(OnceCell.get annotation_level) = 1 then
                    L.at here @@ STLC.Annot (t_typed, ty_dec)
                  else t_typed
              | LetPat (recur, pat, t, u) ->
                  let/? ipat = ("wpat", L.aux pat, None, Weak) in
                  let/? it = ("wt", L.aux t, None, Top) in
                  let/? iu = ("wu", L.aux u, None, Top) in
                  (* Enrich the environment with the leaf variables of [pat] *)
                  let-% env', pat = (env, pat) in
                  (* Add annotations inside the pattern *)
                  let+ pat_ann = decode_pat aliases pat ipat
                  (* type [t] in the current environment *)
                  and+ t_typed =
                      has_type
                        (match recur with Rec -> env' | Strict -> env)
                        aliases t it
                  (* type [u] in the updated environment *)
                  and+ u_typed = has_type env' aliases u iu
                  (* [u] is the return value so it must have the type of [w] *)
                  and+ () = eq iu w
                  (* [pat] is bound to [t] so they must have the same type *)
                  and+ () = eq ipat it in
                  L.at here @@ STLC.LetPat (recur, pat_ann, t_typed, u_typed)
              | IAdt adt ->
                  adt_has_type Unsized.nth
                    (fun adt -> Structure.IAdt adt)
                    (fun adt -> L.at here @@ STLC.IAdt adt)
                    has_type env aliases here Printer.index (w, here) adt
              | SAdt adt ->
                  adt_has_type
                    (Unsized.projection Printer.label)
                    (fun adt -> Structure.SAdt adt)
                    (fun adt -> L.at here @@ STLC.SAdt adt)
                    has_type env aliases here Printer.label (w, here) adt
              | Typedef ((name, params, inner), term) ->
                  (* A type definition is fully characterized by the parameters
                     and the structure. During instanciation we will make sure
                     to do a sort of variable injection where we give special behavior
                     to the type variables of [inner] that are bound by [params]. *)
                  let spec = (params, inner) in
                  (* The term that follows has access to one more type alias *)
                  let aliases' = AliasEnv.(aliases |> add name spec) in
                  let+ typed_term = has_type env aliases' term w in
                  L.at here @@ STLC.Typedef ((name, params, inner), typed_term)
              (* Inductive type constructors, we use the [fold_unfold] helper from earlier.
                 These two cases are dual of each other, so they have the same instanciation
                 but the inner/outer inference variables are swapped around. *)
              | Opaque (name, loc, inner) ->
                  fold_unfold name loc here false aliases (fun iinner iouter ->
                      let+ inner_typed = has_type env aliases inner iinner
                      and+ () = eq w iouter in
                      L.at here @@ STLC.Opaque (name, inner_typed))
              | Unfold (name, loc, inner) ->
                  fold_unfold name loc here true aliases (fun iinner iouter ->
                      (* There's a trap here: the "inner" of the _term_ is
                         the outer of the _type_ because [Unfold] removes a constructor. *)
                      let+ inner_typed =
                          has_type env aliases inner iouter (* !!! *)
                      and+ () = eq w iinner in
                      L.at here @@ STLC.Unfold (name, inner_typed))
              (* Abstract monad effect. *)
              | Do p ->
                  Do
                    (T.map
                       (fun term -> has_type env aliases (L.at here term) w)
                       p))
      in
      L.transform t ~fn:(fun here t ->
          match t with
          | Untyped.Annot _ | Do _ | Typedef _ ->
              (* These constructors are transparent so we don't want extra annotations.
                 The inner term will get an annotation anyway. *)
              inner
          | _ ->
              if Config.(OnceCell.get annotation_level) >= 2 then
                let/? ity = ("annot", here, None, Weak) in
                let+ inner_ann = inner
                and+ () = eq w ity
                and+ ty_dec = decode ity in
                L.at here @@ STLC.Annot (inner_ann, ty_dec)
              else inner)
end
