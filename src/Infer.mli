module Make (T : Utils.Functor) : sig
  module Untyped := Untyped.Make(T)
  module Constraint := Constraint.Make(T)

  type err =
    | Clash of (unit -> STLC.ty L.t Utils.clash)
    | Cycle of (unit -> Structure.cyclic_ty_data)

  type 'a constraint_ = ('a, err) Constraint.t

  val eq : Constraint.variable -> Constraint.variable -> unit constraint_
  val decode : Constraint.variable -> STLC.ty L.t constraint_

  type env = Constraint.variable Untyped.Var.Map.t

  module AliasEnv : Map.S with type key = string

  type alias
  type aliases = alias AliasEnv.t

  val has_type :
    env ->
    aliases ->
    Untyped.term L.t ->
    Constraint.variable ->
    STLC.term L.t constraint_
end
