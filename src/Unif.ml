(* There is nothing that you have to implement in this file/module,
   and no particular need to read its implementation. On the other hand,
   you want to understand the interface exposed in [Unif.mli], as it
   is important to implement a constraint solver in Solver.ml. *)

module UF = UnionFind.Make (UnionFind.StoreMap)

type var = Constraint.variable

(* The internal representation in terms of union-find nodes. *)
type uvar = unode UF.rref

and unode = {
  var : var;
  data : uvar Structure.t Hint.t;
  loc : L.aux;
  source : Ls.source_hints;
}

(* The user-facing representation hides union-find nodes,
   replaced by the corresponding constraint variables. *)
type repr = {
  var : var;
  structure : var Structure.t Hint.t;
  loc : L.aux;
  source : Ls.source_hints;
}

module Env : sig
  type t = { store : unode UF.store; map : uvar Constraint.Var.Map.t }

  val empty : t
  val mem : var -> t -> bool

  val add :
    var -> Constraint.structure Hint.t -> L.aux -> Ls.source_hint -> t -> t

  val uvar : var -> t -> uvar
  val repr : var -> t -> repr
end = struct
  type t = { store : unode UF.store; map : uvar Constraint.Var.Map.t }

  let empty =
      let store = UF.new_store () in
      let map = Constraint.Var.Map.empty in
      { store; map }

  let uvar var env : uvar = Constraint.Var.Map.find var env.map
  let mem var env = Constraint.Var.Map.mem var env.map

  let add var structure loc source env =
      let data = Hint.map (Structure.map (fun v -> uvar v env)) structure in
      let source = Ls.source_singleton source in
      let uvar = UF.make env.store { var; data; loc; source } in
      { env with map = Constraint.Var.Map.add var uvar env.map }

  let repr var env =
      let { var; data; loc; source } = UF.get env.store (uvar var env) in
      let var_of_uvar uv = (UF.get env.store uv).var in
      let structure = Hint.map (Structure.map var_of_uvar) data in
      { var; structure; loc; source }
end

type clash = unit -> var Utils.clash
type cycle = unit -> Structure.cyclic_ty_data

exception Clash of clash
exception Cycle of cycle
exception CycleExists

type err = Clash of clash | Cycle of cycle

let check_no_cycle env v =
    let open struct
      type status = Visiting | Visited
    end in
    let table = Hashtbl.create 42 in
    (* Quick pass to detect cycles *)
    let rec loop v =
        let n = UF.get env.Env.store v in
        match Hashtbl.find table n.var with
        | Visited -> ()
        | Visiting -> raise CycleExists
        | exception Not_found ->
            Hashtbl.replace table n.var Visiting;
            Hint.iter (Structure.iter loop) n.data;
            Hashtbl.replace table n.var Visited
    in
    (* If we do detect a cycle then we'll call this
       to get a more readable diagnosis.
       The structure of this more precise function is copy-pasted
       from the one above because we want to find exactly the same error.
    *)
    let pseudo_decode v =
        let table = Hashtbl.create 42 in
        (* Now we care about remembering where the cycle is. *)
        let first_cycle_found = ref None in
        let path_to_first_cycle = ref [] in
        let stack = Stack.create () in
        let rec pseudo_decode v : Structure.cyclic_ty =
            let n = UF.get env.Env.store v in
            Stack.push n stack;
            Fun.protect
              ~finally:(fun () -> ignore (Stack.pop stack))
              (fun () : Structure.cyclic_ty ->
                match Hashtbl.find table n.var with
                | Visited -> Any
                | Visiting ->
                    if !first_cycle_found = None then (
                      first_cycle_found := Some n.var;
                      path_to_first_cycle :=
                        Stack.to_seq stack
                        (* Truncate the stack to the cyclic part *)
                        |> Utils.take_until_second (fun (n' : unode) ->
                               n'.var = n.var)
                        |> Seq.concat_map (fun (n' : unode) ->
                               n'.source |> Ls.source_collect |> List.to_seq)
                        |> List.of_seq);
                    Loop
                | exception Not_found -> (
                    Hashtbl.replace table n.var Visiting;
                    match Hint.map (Structure.map pseudo_decode) n.data with
                    | Just st ->
                        if Some n.var = !first_cycle_found then
                          (* Cycle begins here. Some leaf is [Loop]. *)
                          raise
                            (Cycle
                               (fun () ->
                                 {
                                   ty = Constr st;
                                   user_vars = !path_to_first_cycle;
                                 }))
                        else (
                          (* Cycle is higher up *)
                          Hashtbl.replace table n.var Visited;
                          Constr st)
                    | _ ->
                        (* No cycle in this subterm. *)
                        Hashtbl.replace table n.var Visited;
                        Any))
        in
        match pseudo_decode v with
        | _ -> failwith "Unreachable: pseudo_decode must fail"
        | exception Cycle cyc -> cyc
    in
    match loop v with
    | () -> ()
    | exception CycleExists -> raise (Cycle (fun () -> pseudo_decode v ()))

let rec unify orig_env v1 v2 : (Env.t, err) result =
    let env = { orig_env with Env.store = UF.copy orig_env.Env.store } in
    let queue = Queue.create () in
    Queue.add (Env.uvar v1 env, Env.uvar v2 env) queue;
    match unify_uvars env.Env.store queue with
    | exception Clash clash -> Error (Clash clash)
    | () -> (
        match check_no_cycle env (Env.uvar v1 env) with
        | exception Cycle v -> Error (Cycle v)
        | () -> Ok env)

and unify_uvars store (queue : (uvar * uvar) Queue.t) =
    match Queue.take_opt queue with
    | None -> ()
    | Some (u1, u2) ->
        ignore (UF.merge store (merge queue) u1 u2);
        unify_uvars store queue

and merge queue (n1 : unode) (n2 : unode) : unode =
    let clash doc = raise (Clash (fun () -> (n1.var, n2.var, doc))) in
    let data =
        Hint.merge
          (fun st1 st2 ->
            match
              Structure.merge
                (function
                  | Both (v1, v2) ->
                      Queue.add (v1, v2) queue;
                      v1
                  | Left v1 -> v1
                  | Right v2 -> v2)
                st1 st2
            with
            | Error doc -> clash doc
            | Ok d -> d)
          n1.data n2.data
    in
    let source = Ls.source_union n1.source n2.source in
    let tiebreaker = Some (fun () -> Hint.most_informative n1.data n2.data) in
    let loc = Utils.Ord.max_by ?tiebreaker L.most_informative n1.loc n2.loc in
    { n1 with data; source; loc }

let unifiable env v1 v2 =
    match unify env v1 v2 with Ok _ -> true | Error _ -> false
