type 'a t = Just of 'a | Top | Bot | Skip | Weak

let map (f : 'a -> 'b) : 'a t -> 'b t = function
    | Just a -> Just (f a)
    | Bot -> Bot
    | Top -> Top
    | Skip -> Skip
    | Weak -> Weak

let iter (f : 'a -> unit) : 'a t -> unit = function Just a -> f a | _ -> ()

let default_repr ~(suggestion : unit -> 'b) ~(embed : 'a -> 'b)
    ~(fmt : string -> 'b) : 'a t -> 'b = function
    | Just a -> embed a
    | Top | Weak -> suggestion ()
    | Bot ->
        if Config.(OnceCell.get parseable_elaboration) then fmt "emp"
        else fmt "ø"
    | Skip ->
        if Config.(OnceCell.get parseable_elaboration) then fmt "ig"
        else fmt "_"

let merge (f : 'a -> 'a -> 'a) (h1 : 'a t) (h2 : 'a t) : 'a t =
    match (h1, h2) with
    (* [Weak] is the neutral *)
    | d, Weak | Weak, d -> d
    (* [Bot]s are compatible with each other, so we can have sparse sum types
       [[ø + a + ø + ø + b + ...]] *)
    | Bot, Bot -> Bot
    (* [Skip] as well. *)
    | Skip, Skip -> Skip
    (* [Top] is naturally compatible with itself and absorbing. *)
    | Top, Top -> Top
    | Top, (Skip | Bot) | (Bot | Skip), Top -> Top
    (* It might be surprising that [Skip] and [Bot] anihilate each other,
       but if we have both of them then our heuristics aren't really applicable
       and [Top] is the only sensible option remaining. *)
    | Skip, Bot | Bot, Skip -> Top
    (* Finally remember that [Hint.t] is just a fancy [option]. We still have
       the normal cases with [Just _] that take priority over everything and
       where actual computation might take place. *)
    | (Top | Bot | Skip), (Just _ as d) | (Just _ as d), (Top | Bot | Skip) -> d
    | Just st1, Just st2 -> Just (f st1 st2)

let most_informative a b =
    let quality = function
        | Weak -> 0
        | Skip -> 1
        | Bot -> 2
        | Top -> 3
        | Just _ -> 4
    in
    Utils.Ord.cmp_ (quality a) (quality b)
