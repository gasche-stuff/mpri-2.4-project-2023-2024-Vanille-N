(** Structure of patterns for the [LetPat] language construct.

    This only includes irrefutable patterns: variables, wildcards, products.
    Patterns can be nested and annotated.
 *)

(** Nested variable bindings, including wildcards, anotations, and [as]-patterns. *)
type ('v, 'ty) t =
  | Wildcard  (** [_] *)
  | Var of 'v * L.aux  (** [v] *)
  | Annot of ('v, 'ty) t L.t * 'ty L.t  (** [p : t] *)
  | Named of 'v * L.aux * ('v, 'ty) t L.t  (** [p as v] *)
  | Tuple of (int, 'v, 'ty) tup  (** [(p1, p2, ...)] *)
  | STuple of (string, 'v, 'ty) tup  (** [(l1=p1, l2=p2, ...)] *)
  | Opaque of string * L.aux * ('v, 'ty) t L.t  (** [Ty v] *)

and ('a, 'v, 'ty) tup = ('a, ('v, 'ty) t L.t) Unsized.t
(** Nested tuple of (optionally labeled) patterns. *)

val fold_left_map :
  ('acc -> 'v -> 'acc * 'w) -> 'acc -> ('v, 'ty) t -> 'acc * ('w, 'ty) t
(** Pass an [add] operation with an accumulator through a pattern.
    Same signature and specification as [List.fold_left_map], but recurses into
    nested subterms too. *)

val map_ty : ('typ1 -> 'typ2) -> ('v, 'typ1) t -> ('v, 'typ2) t
(** Apply a transformation only to the [Annot] constructors to rewrite the types. *)

val pp :
  ('v -> PPrint.document) ->
  ('ty -> PPrint.document) ->
  ('v, 'ty) t ->
  PPrint.document
(** Pretty-printer *)
