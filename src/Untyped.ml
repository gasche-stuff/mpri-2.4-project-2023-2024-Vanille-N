(** Our syntax of untyped terms.

   As explained in the README.md ("Abstracting over an effect"),
   this module as well as other modules is parametrized over
   an arbitrary effect [T : Functor].
*)

module Make (T : Utils.Functor) = struct
  module Var = STLC.TeVar

  type 't pattern_ = ('tev, 'tyv STLC.ty_) Pattern.t
    constraint 't = < tevar : 'tev ; tyvar : 'tyv >
  (** ['t term_] is parametrized over the representation
      of term variables. Most of the project code will
      work with the non-parametrized instance [term] below. *)

  type 't term_ =
    | Var of 'tev * L.aux
    | App of 't term_ L.t * 't term_ L.t
    | Abs of 't pattern_ L.t * 't term_ L.t
    | LetPat of 't let_
    | IAdt of (int, 't term_ L.t, 't pattern_ L.t) adt_
    | SAdt of (string, 't term_ L.t, 't pattern_ L.t) adt_
    | Annot of 't term_ L.t * 'tyv STLC.ty_ L.t
    | Typedef of 'tyv typedef_ * 't term_ L.t
    | Opaque of string * L.aux * 't term_ L.t
    | Unfold of string * L.aux * 't term_ L.t
    | Do of 't term_ T.t
    constraint 't = < tevar : 'tev ; tyvar : 'tyv >

  and 't let_ = Utils.recursive * 't pattern_ L.t * 't term_ L.t * 't term_ L.t
  and ('l, 't) tuple_ = ('l * 't) list
  and ('l, 'p, 't) arms_ = ('l * 'p * 't) list
  and ('l, 'p, 't) case_ = 't * ('l, 'p, 't) arms_ * 't option
  and ('l, 't) inj_ = 'l L.t * 't
  and ('l, 't) field_ = 't * 'l L.t
  and ('l, 'p, 't) iflet_ = 'l L.t * 'p * 't * 't * 't
  and 't typedef_ = string * 't list * 't STLC.ty_ L.t

  and ('l, 't, 'p) adt_ =
    | Tuple of ('l, 't) tuple_
    | Field of ('l, 't) field_
    | With of 't * ('l, 't) tuple_
    | Sum of ('l, 't) inj_
    | Case of ('l, 'p, 't) case_
    | IfLet of ('l, 'p, 't) iflet_

  type raw_term = < tevar : string ; tyvar : string > term_
  (** [raw_term] are terms with raw [string] for their
      variables. Several binders may use the same
      variable. These terms are produced by the parser. *)

  type term = < tevar : Var.t ; tyvar : Structure.TyVar.t > term_
  (** [term] are terms using [STLC.TeVar.t] variables,
      which include a unique stamp to guarantee uniqueness
      of binders. This is what most of the code manipulates. *)

  type 'v pattern = < tevar : 'v ; tyvar : Structure.TyVar.t > pattern_

  let freshen_labeled_arm freshen bind env (inj, pat, res) =
      let env, pat = L.bind (Pattern.fold_left_map bind) env pat in
      (inj, L.map (Pattern.map_ty STLC.freshen_ty) pat, L.map (freshen env) res)

  let freshen_adt freshen bind env = function
      | Tuple ts ->
          Tuple (List.map (fun (lab, x) -> (lab, L.map (freshen env) x)) ts)
      | Field (t, nth) -> Field (L.map (freshen env) t, nth)
      | Sum (id, x) -> Sum (id, L.map (freshen env) x)
      | Case (t, arms, default) ->
          Case
            ( L.map (freshen env) t,
              arms |> List.map (freshen_labeled_arm freshen bind env),
              Option.map (L.map (freshen env)) default )
      | IfLet (id, p, t, y, n) ->
          let env', p = L.bind (Pattern.fold_left_map bind) env p in
          IfLet
            ( id,
              L.map (Pattern.map_ty STLC.freshen_ty) p,
              L.map (freshen env) t,
              L.map (freshen env') y,
              L.map (freshen env) n )
      | With (tup, replace) ->
          With
            ( L.map (freshen env) tup,
              List.map (fun (lab, x) -> (lab, L.map (freshen env) x)) replace )

  let freshen : raw_term -> term =
      let module Env = Map.Make (String) in
      let bind env x =
          let x_var = Var.fresh x in
          let env = Env.add x x_var env in
          (env, x_var)
      in
      let rec freshen env = function
          | Var (x, loc) ->
              Var
                ( Utils.try_find Env.find x env (fun x ->
                      ( x,
                        fun file ->
                          L.highlight_many file [ loc ]
                            ~label:
                              (PPrint.string
                                 "this variable is unbound in the current term")
                      )),
                  loc )
          | App (t1, t2) -> App (L.map (freshen env) t1, L.map (freshen env) t2)
          | Abs (pat, t) ->
              let env, pat = L.bind (Pattern.fold_left_map bind) env pat in
              Abs
                ( L.map (Pattern.map_ty STLC.freshen_ty) pat,
                  L.map (freshen env) t )
          | IAdt adt -> IAdt (freshen_adt freshen bind env adt)
          | SAdt adt -> SAdt (freshen_adt freshen bind env adt)
          | LetPat (recur, pat, t1, t2) ->
              let env_inner, pat =
                  L.bind (Pattern.fold_left_map bind) env pat
              in
              LetPat
                ( recur,
                  L.map (Pattern.map_ty STLC.freshen_ty) pat,
                  L.map
                    (freshen
                       (match recur with Rec -> env_inner | Strict -> env))
                    t1,
                  L.map (freshen env_inner) t2 )
          | Annot (t, ty) ->
              Annot (L.map (freshen env) t, L.map STLC.freshen_ty ty)
          | Typedef ((name, params, inner), term) ->
              Typedef
                ( ( name,
                    List.map Structure.global_tyvar params,
                    L.map STLC.freshen_ty inner ),
                  L.map (freshen env) term )
          | Opaque (name, loc, t) -> Opaque (name, loc, L.map (freshen env) t)
          | Unfold (name, loc, t) -> Unfold (name, loc, L.map (freshen env) t)
          | Do p -> Do (T.map (freshen env) p)
      in

      fun t -> freshen Env.empty t
end
