module OnceCell : sig
  (** Module for global variables that can be written exactly once
        and read only after they have been written.
        Ideal for global command-line configuration options. *)

  exception TwiceSet
  exception NeverSet

  type 'a t
  (** A value that can be written exactly once in the lifetime of the program. *)

  val make : unit -> 'a t
  (** Create a new uninitialized value. *)

  val set : 'a t -> 'a -> unit
  (** Initialize the value.

        @raises TwiceSet if this function is called more than once for a given value. *)

  val get : 'a t -> 'a
  (** Read the value.

        @raises NeverSet if [set] has never been called to initialize the value. *)
end = struct
  exception TwiceSet
  exception NeverSet

  type 'a t = 'a option ref

  let make () = ref None
  let set r v = match !r with None -> r := Some v | Some _ -> raise TwiceSet
  let get r = match !r with None -> raise NeverSet | Some v -> v
end

type 'a once = 'a OnceCell.t

let enable_wf_check : bool once = OnceCell.make ()
let generator_ext_features : bool once = OnceCell.make ()
let parseable_elaboration : bool once = OnceCell.make ()
let annotation_level : int once = OnceCell.make ()
