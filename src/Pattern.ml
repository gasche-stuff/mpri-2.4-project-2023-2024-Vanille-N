type ('v, 'ty) t =
  | Wildcard
  | Var of 'v * L.aux
  | Annot of ('v, 'ty) t L.t * 'ty L.t
  | Named of 'v * L.aux * ('v, 'ty) t L.t
  | Tuple of (int, 'v, 'ty) tup
  | STuple of (string, 'v, 'ty) tup
  | Opaque of string * L.aux * ('v, 'ty) t L.t

and ('a, 'v, 'ty) tup = ('a, ('v, 'ty) t L.t) Unsized.t

let rec fold_left_map (add : 'acc -> 'v -> 'acc * 'w) (accum : 'acc) :
    ('v, 'ty) t -> 'acc * ('w, 'ty) t = function
    | Wildcard -> (accum, Wildcard)
    | Var (v, loc) ->
        let accum', w = add accum v in
        (accum', Var (w, loc))
    | Annot (vt, ty) ->
        let accum', wt = L.bind (fold_left_map add) accum vt in
        (accum', Annot (wt, ty))
    | Named (v, loc, vt) ->
        let accum', wt = L.bind (fold_left_map add) accum vt in
        let accum'', w = add accum' v in
        (accum'', Named (w, loc, wt))
    | Tuple vtup ->
        let accum', wtup =
            Unsized.fold_left_map (L.bind (fold_left_map add)) accum vtup
        in
        (accum', Tuple wtup)
    | STuple vtup ->
        let accum', wtup =
            Unsized.fold_left_map (L.bind (fold_left_map add)) accum vtup
        in
        (accum', STuple wtup)
    | Opaque (name, loc, vinner) ->
        let accum', winner = L.bind (fold_left_map add) accum vinner in
        (accum', Opaque (name, loc, winner))

let rec map_ty (f : 'typ1 -> 'typ2) : ('v, 'typ1) t -> ('v, 'typ2) t = function
    | Wildcard -> Wildcard
    | Var (v, loc) -> Var (v, loc)
    | Annot (v, t) -> Annot (L.map (map_ty f) v, L.map f t)
    | Named (v, loc, p) -> Named (v, loc, L.map (map_ty f) p)
    | Tuple ts -> Tuple (Unsized.map (L.map (map_ty f)) ts)
    | STuple ts -> STuple (Unsized.map (L.map (map_ty f)) ts)
    | Opaque (name, loc, inner) -> Opaque (name, loc, L.map (map_ty f) inner)

(* FIXME: refactor *)
let rec pp pv pty pat =
    PPrint.group
    @@
    match pat with
    | Wildcard -> Printer.wildcard
    | Var (v, _loc) -> pv v
    | Annot (t, ty) ->
        Printer.annot (pp pv pty (L.destruct t)) (L.map_destruct pty ty)
    | Named (v, _loc, p) ->
        PPrint.(
          surround 2 0 lparen
            (pp pv pty (L.destruct p) ^^ string " as " ^^ pv v)
            rparen)
    | Opaque (name, _loc, inner) ->
        Printer.opaque name (pp pv pty (L.destruct inner))
    | Tuple tup -> (
        Unsized.(
          match destruct tup with
          | Exact, [] -> Printer.(lparen ^^ rparen)
          | Approx, [] -> Printer.(lparen ^^ etc ^^ rparen)
          | Exact, [ (i, x) ] ->
              Printer.(
                lparen
                ^^ tup_position elided_integer i 0 (pp pv pty (L.destruct x))
                ^^ comma ^^ rparen)
          | Approx, [ (i, x) ] ->
              Printer.(
                lparen
                ^^ tup_position elided_integer i 0 (pp pv pty (L.destruct x))
                ^^ comma ^^ etc ^^ rparen)
          | _ ->
              Printer.parens
                (Unsized.fold_right_enumerate
                   (fun idx lab t doc ->
                     Some
                       (match doc with
                       | Some doc ->
                           Printer.(
                             tup_position elided_integer lab idx (L.destruct t)
                             ^^ comma ^/^ doc)
                       | None ->
                           Printer.(
                             tup_position elided_integer lab idx (L.destruct t))))
                   Unsized.(
                     function
                     | Exact -> None | Approx -> Some Printer.(string "…"))
                   Unsized.(map (L.map (pp pv pty)) tup)
                |> Option.value ~default:Printer.(lparen ^^ rparen))))
    | STuple tup -> (
        Unsized.(
          match destruct tup with
          | Exact, [] -> Printer.(lparen ^^ rparen)
          | Approx, [] -> Printer.(lparen ^^ etc ^^ rparen)
          | Exact, [ (l, x) ] ->
              Printer.(
                lparen
                ^^ tup_position elided_string l 0 (pp pv pty (L.destruct x))
                ^^ comma ^^ rparen)
          | Approx, [ (l, x) ] ->
              Printer.(
                lparen
                ^^ tup_position elided_string l 0 (pp pv pty (L.destruct x))
                ^^ comma ^^ etc ^^ rparen)
          | _ ->
              Printer.parens
                (Unsized.fold_right_enumerate
                   (fun idx lab t doc ->
                     Some
                       (match doc with
                       | Some doc ->
                           Printer.(
                             tup_position elided_string lab idx (L.destruct t)
                             ^^ comma ^/^ doc)
                       | None ->
                           Printer.(
                             tup_position elided_string lab idx (L.destruct t))))
                   Unsized.(
                     function
                     | Exact -> None | Approx -> Some Printer.(string "…"))
                   Unsized.(map (L.map (pp pv pty)) tup)
                |> Option.value ~default:Printer.(lparen ^^ rparen))))
