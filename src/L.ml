type aux = Known of pair | Toplevel | Dummy of string | Erased of string
and pair = Lexing.position * Lexing.position

let of_lex ls = Known ls
let toplevel = Toplevel
let dummy s = Dummy s
let erased s = Erased s

type 'a t = 'a * aux

let at l x = (x, l)
let destruct (x, _) = x
let aux (_, l) = l
let map f (x, l) = (f x, l)
let map_destruct f (x, _) = f x

let bind f acc (x, l) =
    let acc, fx = f acc x in
    (acc, (fx, l))

let transform ~fn (x, l) = fn l x

let aux_pp l =
    match l with
    | Known (li, le) ->
        PPrint.string
          (Format.sprintf "ll. %d.%d .. %d.%d" li.pos_lnum
             (li.pos_cnum - li.pos_bol) le.pos_lnum (le.pos_cnum - le.pos_bol))
    | Toplevel -> PPrint.string "toplevel"
    | Erased s ->
        PPrint.(
          string "<implicit location> (forged at " ^^ string s ^^ string ")")
    | Dummy s ->
        PPrint.(string "<dummy location> (forged at " ^^ string s ^^ string ")")

let aux_pp_lineno l =
    match l with
    | Known (li, _) -> PPrint.string (Format.sprintf "ll. %d" li.pos_lnum)
    | l -> aux_pp l

let pp printer (x, l) = PPrint.(printer x ^^ string "   at " ^^ aux_pp l)

let most_informative aux aux' =
    let quality = function
        | Dummy _ -> 0
        | Erased _ -> 1
        | Toplevel -> 2
        | Known _ -> 3
    in
    Utils.Ord.cmp_ (quality aux) (quality aux')

let highlight_range_aux lines (l1, c1, l2, c2) label =
    let srepeat (c : char) n = PPrint.string (String.make n c) in
    let lineno n = PPrint.string (Format.sprintf "%4d| " (n + 1)) in
    let indent = PPrint.string "      " in
    lines
    |> List.mapi (fun i l -> (i, l))
    |> List.concat_map (fun (i, l) ->
           if i = l1 && i = l2 then
             [
               PPrint.(lineno i ^^ string l);
               PPrint.(
                 indent ^^ srepeat ' ' c1 ^^ srepeat '^' (c2 - c1) ^^ label);
             ]
           else if i = l1 then
             [
               PPrint.(lineno i ^^ string l);
               PPrint.(
                 indent ^^ srepeat ' ' c1
                 ^^ srepeat '^' (Utils.string_utf8_length l - c1)
                 ^^ srepeat '.' 3);
             ]
           else if i = l2 then
             [
               PPrint.(lineno i ^^ string l);
               PPrint.(indent ^^ srepeat '^' c2 ^^ label);
             ]
           else if i = l1 + 1 && i < l2 then
             [ PPrint.(indent ^^ string "  ...") ]
           else [])

let highlight_range file aux ~label =
    match aux with
    | Known (i, e) ->
        let ch = open_in file in
        Fun.protect ~finally:(fun () -> close_in ch) @@ fun () ->
        let full = In_channel.input_lines ch in
        let filtered =
            highlight_range_aux full
              ( i.pos_lnum - 1,
                i.pos_cnum - i.pos_bol,
                e.pos_lnum - 1,
                e.pos_cnum - e.pos_bol )
              label
        in
        PPrint.(separate hardline filtered)
    | Toplevel -> PPrint.(string "the full term is " ^^ label)
    | other -> aux_pp other

let zebra ~(on : char) ~(off : char) segments : PPrint.document =
    let srepeat (c : char) (n : int) = PPrint.string (String.make n c) in
    let rec aux offset = function
        | [] -> []
        | (c1, c2) :: rest ->
            srepeat off (c1 - offset) :: srepeat on (c2 - c1) :: aux c2 rest
    in
    PPrint.(separate empty (aux 0 segments))

let highlight_many_aux lines locs label =
    let lineno n = PPrint.string (Format.sprintf "%4d| " (n + 1)) in
    let indent = PPrint.string "      " in
    let _ = (lineno, lines) in
    locs |> List.sort compare
    |> Utils.segment_map
         ~key:(fun (l, _, _) -> l)
         ~value:(fun (_, c1, c2) -> (c1, c2))
    |> List.map (fun (i, ls) -> (i, zebra ls ~off:' ' ~on:'^'))
    |> List.mapi (fun i0 (i, ln) ->
           (i, if i0 = 0 then PPrint.(ln ^^ label) else ln))
    |> Utils.zip_matching (List.mapi (fun i l -> (i, PPrint.string l)) lines)
    |> List.concat_map (fun (i, ln, stripes) ->
           [ PPrint.(lineno i ^^ ln); PPrint.(indent ^^ stripes) ])

let highlight_many file auxs ~label =
    match
      List.filter_map
        (function
          | Known (i, e) when i.pos_lnum = e.pos_lnum ->
              Some
                (i.pos_lnum - 1, i.pos_cnum - i.pos_bol, e.pos_cnum - e.pos_bol)
          | _ -> None)
        auxs
    with
    | [] -> PPrint.string "<unknown source> (no locations provided)"
    | locs ->
        let ch = open_in file in
        Fun.protect ~finally:(fun () -> close_in ch) @@ fun () ->
        let full = In_channel.input_lines ch in
        let filtered = highlight_many_aux full locs label in
        PPrint.(separate hardline filtered)
