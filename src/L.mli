type aux
type 'a t

val of_lex : Lexing.position * Lexing.position -> aux
val toplevel : aux
val dummy : string -> aux
val erased : string -> aux
val aux_pp : aux -> PPrint.document
val aux_pp_lineno : aux -> PPrint.document
val pp : ('a -> PPrint.document) -> 'a t -> PPrint.document
val at : aux -> 'a -> 'a t
val destruct : 'a t -> 'a
val aux : 'a t -> aux
val map : ('a -> 'b) -> 'a t -> 'b t
val map_destruct : ('a -> 'b) -> 'a t -> 'b
val bind : ('acc -> 'a -> 'acc * 'b) -> 'acc -> 'a t -> 'acc * 'b t
val transform : fn:(aux -> 'a -> 'b) -> 'a t -> 'b
val most_informative : aux -> aux -> Utils.Ord.t
val highlight_range : string -> aux -> label:PPrint.document -> PPrint.document

val highlight_many :
  string -> aux list -> label:PPrint.document -> PPrint.document
