(** Type variable naming conventions.

    In the original code the structure constraint given to a variable was
    a [_ Structure.t option]. This is found to be easily made more expressive
    by replacing [option] with a type that carries some additional information.

    This is the role played by [t]: to be isomorphic to [option] but carry
    extra information only relevant to the pretty-printer.

    {1 Motivation}

    We instanciate a lot of variables, and this has two distinct effects that
    both combine towards signatures being more cluttered than they need to be.
    + fresh name generation works with a small number of base names and an
         integer suffix
    + all variables have the same naming scheme.

    This can lead to signatures such as [a₁ -> a₂ -> [a₃ + a₁]] where it is not
    instantly clear which variables actually occur where.
    We propose here some heuristics to instead rename some type variables
    depending on where they occur, which
    + makes their usage more intuitive based on their name
    + frees up unused names for important variables to use without the need for
       long unique identifiers.

    The signature above could thus become [a -> _ -> [ø + a]] which is more
    immediately interpretable.

    Ultimately these are merely {i heuristics} and we know of several false positives
    and negatives.

    Also keep in mind that everything described here and enabled by this module
    is merely a {b naming convention}. It has {b no actual impact on the logic}
    of the type checker.


    {1 Specification}

    If a union variant is never instanciated, we can reflect that by naming
    the corresponding type variable "[ø]".
    If a tuple field or function argument is never accessed we can reflect
    that by naming the corresponding type variable "[_]".

    These two conventions together produce arguably better signatures for
    functions such as:
    - [lambda x. lambda y. x]: [a -> _ -> a]
    - ['1 x]: [[ø + a + …]]
    - [lambda x. x.1]: [{_ * a * …} -> a]
    - [lambda x. case x of | 'a _ => 'a () esac]: [['a:_] -> ['a:{} + …]]


    {1 Usage}

    The way this works is that during constraint generation, when we introduce a
    new existential variable to put in an [Exist] construct, we can specify not
    only the structure (e.g. [Some Structure.(Arrow _ _)]) but also a hint,
    and this will be used to choose the default name that the type should be given.

    For example `App (x, t)` might choose by default to make the variable for [x] [Skip]
    and the variable for [t] [Top], to represent the fact that until further notice
    the argument to the function is ignored but the return type is important.
    If the argument `x` is actually used within the body of the function then
    the hint given to [x] by the [Var] rule will absorb [Skip].


    The [merge] function handles the precedence between hints and how to merge
    incompatible hints. *)

(** {1 Manipulating hints} *)

type 'a t =
  | Just of 'a
      (** Only variant that carries actual data. Plays the role of [Some]
        while all the others are flavors of [None]. *)
  | Top  (** Maximally general and absorbing, e.g. the [a] of [_ -> a]. *)
  | Bot  (** Empty by default, e.g. the [ø] of [[ø + a + ...]]. *)
  | Skip  (** Ignored by default, e.g. the [_] of [_ -> a]. *)
  | Weak  (** Neutral element, behaves like [Top] if it somehow survives. *)

val map : ('a -> 'b) -> 'a t -> 'b t
(** Apply transformation [f] to the inner data of [Just]. Other variants preserved. *)

val iter : ('a -> unit) -> 'a t -> unit
(** Execute function [f] on the inner data of [Just]. Other variants ignored. *)

val merge : ('a -> 'a -> 'a) -> 'a t -> 'a t -> 'a t
(** Join two hints into one.
    This goes broadly as follows:
    - [Just] has absolute priority making this function logically equivalent to
      for all parts of the implementation that do not look into the details
      of hints (all except pretty printing) to its analogue on [option]:
      {[
        let merge f a a' = match a, a' with
            | None, _ -> a'
            | _, None -> a
            | Some a, Some a' -> Some (f a a')
      ]}
    - then the hints without data come in the order [Top > {Bot, Skip} > Weak]
      ([Bot] and [Skip] are not comparable) and [merge] will resolve to the
      least upper bound according to priority.
 *)

val most_informative : 'a t -> 'b t -> Utils.Ord.t
(** Comparison by amount of information *)

(** {1 Choosing the final name} *)

val default_repr :
  suggestion:(unit -> 'b) ->
  embed:('a -> 'b) ->
  fmt:(string -> 'b) ->
  'a t ->
  'b
(** Choose the final name of the type variable based on the hint that survived.
   - [suggestion]: the default fresh name generator, for example [Decode.new_var]
   - [fmt]: a custom fresh name generator to use when the hint has information,
     for example [STLC.TyVar.fresh]
   - [embed]: case that is applied to [Just], can be used to recurse into
     the inner data. *)
