(** Abstract manipulations of underapproximations of lists
    with a focus on using them for binding operations.

    This is tailor-made the intended use-case in this project, i.e. to use
    these tuples for patterns and type annotations where
    - some types are of arbitrary length and we know only a lower bound
    - other approximative destructors for tuples [let (a, b, ...) = x]
    - variable bindings appear frequently and need to be folded together

    {1 Motivation}

    As a concrete example, if we have [let (a, b, c) = x in ...] this gives
    us the information that [x] is exactly 3 elements long. With this knowledge
    we need to be able to
    - add all of [a], [b], [c] to the environment to type the continuation
    - check whether this knowledge is compatible with other sources
      of information such as
      {ul {- [let x = (1, 2) in ...] -> [x] has exactly 2 elements: incompatible}
      {- [let y = x.0 in ...] -> [x] has at least 1 element: compatible}
      {- [let z = x.5 in ...] -> [x] has at least 6 elements: incompatible}
    }

    We need an abstract interface that works for
    - indexed tuples
      (exact constructor [(a, b, c)],
      approximative destructors [x.1] and [let (a, b, ...) = x],
      exact destructor [let (a, b, c) = x],
      approximative constructor [(x with 1=a)])
    - labeled tuples
      (exact constructor [(a=a, b=b, c=c)],
      approximative destructor [x.a],
      exact destructor [let (a=a, b=b, c=c) = x],
      approximative constructor [(x with a=a)])
    - indexed unions
      (approximative constructor ['1 a],
      exact destructor [case x of | a => a esac]
      with an approximative variant [| … => default],
      approximative destructor [if let 'a a = x])
    - labeled unions
      (approximative constructor ['a a],
      exact destructor [case x of | 'a a => a esac]
      with an approximative variant [| … => default],
      approximative destructor [if let '1 a = x])

    Finally, any facilities for the above need to work for both terms and types,
    because those constructs are going to need matching type annotations.

    This tells us that we need an interface with tuples of labeled elements,
    where both the labels and elements are manipulated abstractly, and it should
    efficiently support merge, map, and fold operations. *)

(** {1 Definitions} *)

(** Bound on the size of a tuple. *)
type size =
  | Exact
      (** The size is exact and the tuple this [size] is applied to cannot be
          merged with tuples that declare a strict superset of labels. *)
  | Approx
      (** The size is an underapproximation and some additional labels
          might be introduced. *)

val bound_of_termin : 'a option -> size
(** [Exact] if there is no terminator, [Approx] if it is [Some _]. *)

type ('l, 'a) t
(** Sized tuple or underapproximation thereof. The only way to construct a value
    of this type is to go through [('l, 'a) raw] then [sort] or [enumerate] it. *)

type ('l, 'a) raw
(** Unvalidated data with the intent of eventually becoming an [('l, 'a) t].
    Can be built as a [('l * 'a) list], either through [nil]/[cons] or [of_list]. *)

(** {1 Constructing values}

    Going through [raw] is the only way to construct new values.
    [('l, 'a) raw] is isomorphic to [size * ('l * 'a) list].
    It naturally supports conversions in both ways, and a [nil]/[cons]
    construction scheme. *)

val nil : size -> ('l, 'a) raw
val cons : 'l -> 'a -> ('l, 'a) raw -> ('l, 'a) raw
val of_list : size -> ('l * 'a) list -> ('l, 'a) raw

val destruct : ('l, 'a) t -> size * ('l * 'a) list
(** If your initial list [l] is sorted by label then
    [fun (sz, l) -> destruct (sort (of_list sz l))] is the identity *)

(** {1 Sanitization functions}

    [('l, 'a) raw] has an internal invariant (its contents are sorted),
    that prevents it from exposing implementation details to guarantee correctness.
    You can build a value of type [('l, 'a) raw] as a normal list without worrying
    about the ordering, but it is imperative that you convert it to a [('l, 'a) t]
    before you can do anything interesting with it. *)

val sort : L.aux -> ('l -> PPrint.document) -> ('l, 'a) raw -> ('l, 'a) t
(** The most straightfowrard way to convert a [raw] to a [t] is to just let
    the library sort its contents as required.
    This takes additional parameters in the form of
    - the location to print in case sorting fails
    - the formatter for labels

    @raise Invalid_argument if the labels are not unique. *)

val enumerate : (unit, 'a) raw -> (int, 'a) t
(** If you specifically want integer labels and the list is already sorted,
    one solution is to go through [List.mapi] then [sort] but you can also
    directly invoke [enumerate] which is more efficient. *)

(** {1 Standard transformation} *)

val iter : ('a -> unit) -> ('l, 'a) t -> unit
(** Execute a function on all items. The [size] parameter is ignored. *)

val map : ('a -> 'b) -> ('l, 'a) t -> ('l, 'b) t
(** Apply a transformation to all items. The [size] parameter is preserved. *)

val raw_map : ('a -> 'b) -> ('l, 'a) raw -> ('l, 'b) raw
(** Same as [map] but works on [raw]. *)

(** {1 Merging} *)

val map2 :
  (('a, 'b) Utils.either_or_both -> 'out) ->
  ('l, 'a) t ->
  ('l, 'b) t ->
  ('l, 'out) t Utils.doc_result
(** Merge two [t] and apply a function. This fails if the size bounds are incompatible.

    When you call [map2 f a b] your function [f] will be applied to a
    collection of [Utils.either_or_both] values computed as follows:
    for every label [(l : label)] in the union of labels of [a] and [b],
    - if [l] is in [a] with value [va] but not in [b] call [f (Left va)]
    - if [l] is in [b] with value [vb] but not in [a] call [f (Right vb)]
    - if [l] is in both [a] and [b] with values [va] and [vb] call [f (Both (va, vb))]

    The output thus computed will be in the final result as the value associated with [l].

    If some label [l] is in [b] but not [a] and if [a] is marked [Exact] then
    the final result will be [None], and symetrically.
    Thus if [a] is [Exact] it can only be merged with [_ t]
    that declare a subset of its labels.

    {[
        map2 f:
               [(1, 'a'); (2, 'b')] [(1, 'A'); (3, 'C')]
            ~> [(1, Both ('a', 'A')); (2, Left 'b'); (3, Right 'C')]             (weave)
            ~> [(1, f (Both ('a', 'A'))); (2, f (Left 'b')); (3, f (Right 'C'))] (map)
    ]}
 *)

val merge_empty :
  ('l1, 'a1) t ->
  ('l2, 'a2) t ->
  (('l1, 'a1) t -> 'out) ->
  (('l2, 'a2) t -> 'out) ->
  'out Utils.doc_result
(** Specifically when at least one tuple is empty, and still under
    the condition of compatibility of size bounds, we can merge them even if
    they have labels of different types.
    
    For this operation we need an additional way to embed the one of the two
    inputs that is not empty into the desired value, which is what the two
    projections [(_, _) t -> 'out] define. *)

(** {1 Folding operations} *)

val fold_right :
  ('l -> 'a -> 'acc -> 'acc) -> (size -> 'acc) -> ('l, 'a) t -> 'acc
(** Pass an accumulator through all values from right to left.
    This is the most general folding function, but unlike more specialized
    operators that follow it will require you to call [sort] again if you try
    to use it to build a value of type [t].
    If you want to fold into a [t] specifically consider using [accumulate]
    instead. *)

val fold_right_enumerate :
  (int -> 'l -> 'a -> 'acc -> 'acc) -> (size -> 'acc) -> ('l, 'a) t -> 'acc
(** Identical to [fold_right], you additionally get the index of the current value. *)

val fold_left_map :
  ('env -> 'inp -> 'env * 'outp) -> 'env -> ('l, 'inp) t -> 'env * ('l, 'outp) t
(** Pass an accumulator through all values and simultaneously apply a transformation.
    The intended use-case is e.g. to apply a sanitization procedure or to
    bind the variables of the tuple in an environment.

    It is impossible to have ['env = _ t] because there is no function
    to construct a [t] from parts. It is strongly discouraged to have
    ['env = _ raw] because it will require an extra call to [sort].
    Consider using the more specialized [accumulate_with] instead.
    *)

(** What follows are the more specialized folding operators.
    Because they are less general they can guarantee that the invariants of
    [_ t] are preserved and avoid useless calls to [sort].

    [accumulate] and [accumulate_with] are respectively forms of [map] and [fold_left],
    in continuation style (this is e.g. to allow introducing bindings).
    In addition the base case has access to the full list.
    *)

val accumulate :
  (('tmp -> 'final) -> 'inp -> 'final) ->
  (('l, 'tmp) t -> 'final) ->
  ('l, 'inp) t ->
  'final
(** As an analogy, here is a function on [List] that has the same specification:
    {[
        let accumulate
            (step : ('tmp -> 'final) -> 'inp -> 'final)
            (base : 'tmp list -> 'final)
            (input : 'inp list)
            : 'final =
                let rec aux accum = function
                    | [] -> base (List.rev accum)
                    | x::xs -> step (fun b -> aux (b::accum) xs) x
                in aux [] input;;

        let arr = accumulate
            (fun k x -> let y = x + 1 in k y)
            (fun l -> Array.of_list l)
            [1; 2; 3; 4]
        (* arr : int array = [|2; 3; 4; 5|] *)
    ]}

    This transformation on [_ t] preserves the [size] bound and avoids needless
    calls to [sort] because it is impossible to misuse it to alter the labels
    or the order of elements.
    *)

val accumulate_with :
  (('acc -> 'tmp -> 'final) -> 'acc -> 'inp -> 'final) ->
  ('acc -> ('l, 'tmp) t -> 'final) ->
  'acc ->
  ('l, 'inp) t ->
  'final
(** Same as [accumulate], but with an extra accumulator of arbitrary type. *)

type ('var, 'final) var_binder = ('var -> 'final) -> 'final
(** The signature of continuations that bind a new variable *)

type ('label, 'var, 'final) producer =
  ('var, 'final) var_binder ->
  ('var, 'final) var_binder ->
  ('var -> ('label, 'var) t -> 'final) ->
  size ->
  'label ->
  'final
(** The signature of functions that generate [t] from projections. *)

val nth : (int, 'tmp, 'final) producer
(** [nth skip base k sz idx] produces a [(int, _) t] in continuation style, where
    - [skip] is the constructor for elements that have a label smaller than [len]
    - [base] is the constructor for the element that has label [len]
    - [finish] is the continuation, it has access to the value bound to [len]
      (the output of [base]) and the full tuple
    - [sz] is the size bound on the full tuple
    - [idx] is the desired label of the final element,
      i.e. one less than the length of the tuple

    Note:
    - [base] will be called exactly once
    - [skip] will be called exactly [idx] times

    for a total of [idx+1] invocations, the total length of the tuple.

    The exact order in which these calls occur is not guaranteed.

    Here is a function on ['a list] with the same specification (except [sz]):
    {[
        let nth
            (skip : ('tmp -> 'final) -> 'final)
            (base : ('tmp -> 'final) -> 'final)
            (finish : 'tmp -> 'tmp list -> 'final)
            (len : int)
            : 'final =
                let rec aux accum = function
                    | 0 -> base (fun d -> finish d (List.rev (d::accum)))
                    | n when n > 0 -> skip (fun d -> aux (d::accum) (n-1))
                    | _ -> raise (Invalid_argument "len < 0")
                in aux [] len

        let arr = nth
            (fun k -> k '_')
            (fun k -> k 'a')
            (fun c l -> (c, Array.of_list l))
            5
        (* - : char * char array = ('a', [|'_'; '_'; '_'; '_'; '_'; 'a'|]) *)
    ]}

    @raise Invalid_argument if [len < 0].
 *)

val projection : ('label -> PPrint.document) -> ('label, 'tmp, 'final) producer
(** Same use as [nth] but does not fill in the previous positions and instead
    builds only the element of the given label. The first parameter is
    unused, it only exists so that [projection] and [nth] have identical
    signatures.
    Works even if [label] can't be enumerated, but in exchange there is no
    way to enumerate all the empty locations and the printer must be provided
    explicitly.

    The resulting tuple is always of length exactly one, and has the size
    bound [Approx]. *)
