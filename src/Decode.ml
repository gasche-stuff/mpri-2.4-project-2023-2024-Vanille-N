(* There is nothing that you have to implement in this file/module,
   and no particular need to read its implementation. [Decode.mli]
   explains its purpose. *)

type env = Unif.Env.t
type slot = Ongoing | Done of STLC.ty

let new_var = STLC.TyVar.namegen [| "α"; "β"; "γ"; "δ" |]
let new_ascii_var = STLC.TyVar.namegen [| "t"; "u"; "v"; "w" |]

type decoder = Constraint.variable -> STLC.ty L.t

(* A very subtle bug was found that encourages this slightly suspicious
   signature and name change for `make_decoder`.
   When using a generation monad, decoding can have too much side effects
   because it memoizes decoding with a `Hashtbl`.

   There is still a `Hashtbl` involved but it will be created locally
   for each instanciation of `make_decoder`. The `unit` parameter indicates
   the exact argument after which the `Hashtbl` is instanciated.
   Be very careful when using this function that the following two forms
   are NOT EQUIVALENT:
   - [fun v -> make_decoder env () v]
   is NOT
   - [make_decoder env ()]
*)
let make_decoder (env : env) () : decoder =
    let exception Found_cycle in
    let table = Hashtbl.create 42 in
    let suggestion =
        if Config.(OnceCell.get parseable_elaboration) then fun () ->
          Structure.Var (new_ascii_var ())
        else fun () -> Structure.Var (new_var ())
    in
    let rec decode (v : Constraint.variable) : STLC.ty L.t =
        let repr = Unif.Env.repr v env in
        L.at repr.loc
          (match Hashtbl.find table repr.var with
          | Done ty -> ty
          | Ongoing -> raise Found_cycle
          | exception Not_found ->
              Hashtbl.replace table repr.var Ongoing;
              let ty =
                  STLC.Constr
                    (Hint.default_repr
                     (* If there's no hint associated, pick a fresh variable as usual *)
                       ~suggestion
                         (* If there is a hint, make a TyVar with it *)
                       ~fmt:(fun s -> Structure.Var (STLC.TyVar.fresh s))
                         (* If there's no hint but a structure, recurse *)
                       ~embed:(Structure.map decode) repr.structure)
              in
              Hashtbl.replace table repr.var (Done ty);
              ty)
    in
    (* Because we perform an occur-check on unification, we can assume
       that we never find any cycle during decoding:
       [Found_cycle] should never be raised here. *)
    decode
