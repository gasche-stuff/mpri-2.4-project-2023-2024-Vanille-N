module Ord = Utils.Ord

(* Determines if the size is known exactly or is just an underapproximation. *)
type size = Exact | Approx

let bound_of_termin = function Some _ -> Approx | None -> Exact

module Item = struct
  (* Private implementation details of the data recorded for each
     element of the tuples that are manipulated here. *)
  type ('l, 'a) t = { label : 'l; data : 'a }

  let cmp (type l a b) (a : (l, a) t) (b : (l, b) t) : Ord.t =
      Ord.cmp_ a.label b.label

  (* Builder and destructor *)
  let wrap (label, data) = { label; data }
  let splat it = (it.label, it.data)
  let label it = it.label

  (* Update *)
  let subst data it = { label = it.label; data }
  let label_subst label it = { label; data = it.data }

  (* Iterators *)
  let iter f it = f it.data
  let map f it = { label = it.label; data = f it.data }

  (* Precondition: [a.label = b.label] *)
  let map2 (type l a b c) (f : a -> b -> c) (a : (l, a) t) (b : (l, b) t) :
      (l, c) t =
      { label = a.label; data = f a.data b.data }
end

(* This is our safety net when interacting with the outside world: make sure
    that in the [.mli] the definitions of both [t] and [raw] are private.
    Because any [t] that we get must have been built through one of the trusted
    functions, we know that any [t] received as input of one of our functions
    is well-formed (= sorted). *)
type ('l, 'a) raw = { bound : size; contents : ('l, 'a) Item.t list }

(* List with a length bound (lower bound or exact length)
   Well-formedness invariant: the correctness of [map2] -- the most important
   operation on [_ t] -- relies on [contents] being sorted in increasing order.
   Do not trust user input, and be very careful inside this file where there
   is no type-level guarantee of sortedness. In particular some functions might
   temporarily reverse the order.

   Important: although it *looks* like the presence of all labels is a hidden
   invariant for [(int, a) t] because all positions must be set in a tuple
   indexed by integers, this is in fact not the case.
   The tuple `(a, b, c)` is read as `(0=a, 1=b, 2=c)` by the parser,
   and the pretty-printer has a heuristic for eliding markers that makes the
   integer labels never visible to the user, but they in fact always exist
   and we don't need any assumptions on them in this file other than uniqueness
   and sortedness. *)
type ('l, 'a) t = { raw : ('l, 'a) raw; printer : 'l -> PPrint.document }

let wf_raw (a : ('l, 'a) raw) : ('l, 'a) raw =
    Utils.wf "Unique and sorted labels" (fun () ->
        let sorted = List.sort (Ord.into Item.cmp) a.contents in
        (* Warning: we can't actually compare the values because they contain
           laziness. We resort to just comparing the labels. *)
        List.map Item.label sorted = List.map Item.label a.contents);
    a

let wf (a : ('l, 'a) t) : ('l, 'a) t = { a with raw = wf_raw a.raw }

(* FIXME: should this return an option in case of failure ? *)
let sort (type l a) (loc : L.aux) (printer : l -> PPrint.document)
    (a : (l, a) raw) : (l, a) t =
    let sorted = List.sort (Ord.into Item.cmp) a.contents in
    match Utils.list_uniq Item.label sorted with
    | Ok contents -> wf { raw = { a with contents }; printer }
    | Error dup ->
        raise
          (Utils.Invalid_argument_at
             ( "There is a duplicate label",
               fun file ->
                 L.highlight_range file loc
                   ~label:PPrint.(printer dup ^^ string " appears twice") ))

let destruct (a : ('l, 'a) t) : size * ('l * 'a) list =
    (a.raw.bound, List.map Item.splat a.raw.contents)

(* Be very careful with [cons] in this module. The outside sees it as a
    [_ -> _ raw -> _ raw] but here it can manipulate [_ t] directly.
    Make sure you keep the labels sorted. *)
let cons (type l a) (label : l) (data : a) (xs : (l, a) raw) : (l, a) raw =
    { bound = xs.bound; contents = { label; data } :: xs.contents }

let nil (bound : size) : _ raw = { bound; contents = [] }

(* Apply computation to all elements. Ignore size bound. *)
let iter (type a) (f : a -> unit) (a : (_, a) t) : unit =
    List.iter (Item.iter f) a.raw.contents

let raw_map (type l a b) (f : a -> b) (a : (l, a) raw) : (l, b) raw =
    { a with contents = List.map (Item.map f) a.contents }

(* Apply function to all elements. Preserve size bound. *)
let map (type l a b) (f : a -> b) (a : (l, a) t) : (l, b) t =
    { a with raw = raw_map f a.raw }

(* Produce a bounded list from a normal list *)
let of_list (type l a) (bound : size) (contents : (l * a) list) : (l, a) raw =
    let contents = List.map Item.wrap contents in
    { contents; bound }

let enumerate (type a) (a : (unit, a) raw) : (int, a) t =
    wf
      {
        printer = PPrint.OCaml.int;
        raw =
          { contents = List.mapi Item.label_subst a.contents; bound = a.bound };
      }

(* Generalization of something in between [List.map2] and a form of [zip_longest],
   but with labeled elements.
   We're going to
   - [map2] the function [f] over the common label set
   - prolong the result with projections if the one where the label is absent
     underapproximation (has [bound = Approx]). *)
let map2 (type l a b out) (f : (a, b) Utils.either_or_both -> out)
    (a : (l, a) t) (b : (l, b) t) : (l, out) t Utils.doc_result =
    (* Merges two sorted lists into one, while preserving provenance
       and duplicates by using [either_or_both]. *)
    let rec weave l r =
        match (l, r) with
        | [], [] -> []
        | it :: xs, [] -> Item.map Utils.left it :: weave xs []
        | [], it' :: xs' -> Item.map Utils.right it' :: weave [] xs'
        | it :: xs, it' :: xs' -> (
            match Item.cmp it it' with
            | Equal -> Item.map2 Utils.both it it' :: weave xs xs'
            | Less -> Item.map Utils.left it :: weave xs r
            | Greater -> Item.map Utils.right it' :: weave l xs')
    in
    weave a.raw.contents b.raw.contents
    |> Utils.try_fold_right (fun (it : _ Item.t) ->
           Utils.(
             match (it.data, a.raw.bound, b.raw.bound) with
             | Left _, _, Approx | Right _, Approx, _ | Both _, _, _ ->
                 Ok (Item.map f it)
             | Left _, _, Exact ->
                 Error
                   (fun () -> PPrint.(string "missing " ^^ a.printer it.label))
             | Right _, Exact, _ ->
                 Error
                   (fun () ->
                     PPrint.(string "extraneous " ^^ b.printer it.label))))
    |> Result.map (fun contents ->
           wf
             {
               a with
               raw =
                 {
                   bound =
                     (match (a.raw.bound, b.raw.bound) with
                     | Approx, Approx -> Approx
                     | _, _ -> Exact);
                   contents;
                 };
             })

let merge_empty (type l1 l2 a1 a2 out) (s1 : (l1, a1) t) (s2 : (l2, a2) t)
    (build1 : (l1, a1) t -> out) (build2 : (l2, a2) t -> out) :
    out Utils.doc_result =
    match (s1.raw.contents, s2.raw.contents, s1.raw.bound, s2.raw.bound) with
    (* Both are empty, we could merge them to either type so let's just
       pick the first one by default.
       Merging the size bounds produces an [Exact] iff at least one of
       the two is [Exact].
       By default the result inherits the printer from [s1], which shouldn't
       really matter anyway. *)
    | [], [], Approx, Approx ->
        Ok (build1 (wf { s1 with raw = { bound = Approx; contents = [] } }))
    | [], [], _, _ ->
        Ok (build1 (wf { s1 with raw = { bound = Exact; contents = [] } }))
    (* When one is empty but not the other we resort to the projections
       and we don't have much freedom. *)
    | [], _, Approx, _ -> Ok (build2 s2)
    | _, [], _, Approx -> Ok (build1 s1)
    (* We can't merge the other "empty" cases because [([], Exact)] is
       not compatible with nonempty tuples. *)
    | _ ->
        Error
          (fun () ->
            PPrint.string "nonempty composite types with incomparable labels")

let fold_right (type l a acc) (f : l -> a -> acc -> acc) (base : size -> acc)
    (a : (l, a) t) : acc =
    let rec aux : _ Item.t list -> acc = function
        | [] -> base a.raw.bound
        | it :: xs -> f it.label it.data (aux xs)
    in
    aux a.raw.contents

let fold_right_enumerate (type l a acc) (f : int -> l -> a -> acc -> acc)
    (base : size -> acc) (a : (l, a) t) : acc =
    let rec aux idx : _ Item.t list -> acc = function
        | [] -> base a.raw.bound
        | it :: xs -> f idx it.label it.data (aux (idx + 1) xs)
    in
    aux 0 a.raw.contents

let accumulate (type l inp tmp final) (inc : (tmp -> final) -> inp -> final)
    (base : (l, tmp) t -> final) (a : (l, inp) t) : final =
    let rec aux (revved : (l, tmp) Item.t list) : (l, inp) Item.t list -> final
        = function
        | [] ->
            base
            @@ wf
                 {
                   a with
                   raw = { bound = a.raw.bound; contents = List.rev revved };
                 }
        | it :: xs ->
            inc (fun (b : tmp) -> aux (Item.(subst b it) :: revved) xs) it.data
    in
    aux [] a.raw.contents

let accumulate_with (type l inp acc tmp final)
    (inc : (acc -> tmp -> final) -> acc -> inp -> final)
    (base : acc -> (l, tmp) t -> final) (init : acc) (a : (l, inp) t) : final =
    let rec aux (acc : acc) (revved : (l, tmp) Item.t list) :
        (l, inp) Item.t list -> final = function
        | [] ->
            base acc
            @@ wf
                 {
                   a with
                   raw = { bound = a.raw.bound; contents = List.rev revved };
                 }
        | it :: xs ->
            inc
              (fun (acc : acc) (b : tmp) ->
                aux acc (Item.(subst b it) :: revved) xs)
              acc it.data
    in
    aux init [] a.raw.contents

let fold_left_map (type l inp env outp) (f : env -> inp -> env * outp)
    (acc : env) (a : (l, inp) t) : env * (l, outp) t =
    let rec aux (acc : env) : (l, inp) Item.t list -> env * (l, outp) raw =
      function
        | [] -> (acc, wf_raw { bound = a.raw.bound; contents = [] })
        | it :: xs ->
            let acc, y = f acc it.data in
            let acc, ys = aux acc xs in
            (* We can use [cons] here because we haven't exposed the
               labels to the user and thus we can trust that the list is
               still sorted. *)
            (acc, wf_raw @@ cons it.label y ys)
    in
    let acc, out = aux acc a.raw.contents in
    (acc, { a with raw = out })

(* Type aliases for the signatures of the functions below *)

type ('var, 'final) var_binder = ('var -> 'final) -> 'final

type ('label, 'var, 'final) producer =
  ('var, 'final) var_binder ->
  ('var, 'final) var_binder ->
  ('var -> ('label, 'var) t -> 'final) ->
  size ->
  'label ->
  'final

let projection (type label tmp final) (printer : label -> PPrint.document)
    (* Ignored elements of the tuple.
       This is here just so that [nth] and [projection] have the same signature *)
      (_inc : (tmp, final) var_binder)
    (* The one projection we're interested in *)
      (base : (tmp, final) var_binder)
    (* Build the final value *)
      (finish : tmp -> (label, tmp) t -> final) (bound : size) (label : label) :
    final =
    base (fun data ->
        let full =
            wf { printer; raw = { bound; contents = [ { label; data } ] } }
        in
        finish data full)

let nth (type tmp final)
    (* Ignored elements of the tuple *)
      (inc : (tmp, final) var_binder)
    (* The one projection we're interested in *)
      (base : (tmp, final) var_binder)
    (* Build the final value *)
      (finish : tmp -> (int, tmp) t -> final) (sz : size) (len : int) : final =
    let rec aux (revved : (int, tmp) Item.t list) : int -> final = function
        | 0 ->
            base (fun data ->
                let full =
                    wf
                      {
                        printer = Printer.index;
                        raw =
                          {
                            bound = sz;
                            contents =
                              List.rev (Item.(wrap (len, data)) :: revved);
                          };
                      }
                in
                finish data full)
        | n when n > 0 ->
            inc (fun data ->
                aux (Item.(wrap (len - n, data)) :: revved) (n - 1))
        | _ -> raise (Invalid_argument "Unsized.nth : len < 0")
    in
    aux [] len
