open PPrint

(* Reexports *)
let lparen = lparen
let rparen = rparen
let comma = comma
let string = string
let parens = parens
let group = group
let star = star
let ( ^^ ) = ( ^^ )
let ( ^/^ ) = ( ^/^ )
let elided_string mk (_ : int) = Some (string mk)
let integer mk = string (Format.sprintf "%d" mk)
let elided_integer mk (i : int) = if i = mk then None else Some (integer mk)
let panic _ : document = failwith "This operation should be infaillible"
let label l = string "label " ^^ string l
let index i = string "index " ^^ integer i

(** ?w *)
let inference_variable w = string "?" ^^ w

(** $t -> $u *)
let arrow t u = group @@ t ^//^ group @@ string "→" ^^ space ^^ u

let labeled_elidable marker_pp ?(prefix = "") ?(infix = "") marker idx elem =
    match marker_pp marker idx with
    | Some mk -> string prefix ^^ mk ^^ string infix ^^ elem
    | None -> elem

let named_ty marker_pp = labeled_elidable ?infix:(Some ":") marker_pp

let quote_named_ty marker_pp =
    labeled_elidable ?prefix:(Some "'") ?infix:(Some ":") marker_pp

let tup_position marker_pp = labeled_elidable ?infix:(Some "=") marker_pp

let variant_case marker_pp =
    labeled_elidable ?prefix:(Some "'") ?infix:(Some " ") marker_pp

let etc = string "…"
let if_ b doc = if b then doc else string ""

let alias name args =
    group
    @@ surround 2 0 (string "<") (separate (comma ^^ break 1) args) (string ">")
    ^^ string name

let typedef name params inner term =
    group
    @@ group
         (string "type" ^^ space ^^ alias name params ^^ string " of" ^/^ inner
        ^/^ string "in")
    ^/^ term

let opaque name inner =
    group @@ surround 2 0 lparen (string name ^^ space ^^ inner) rparen

let unfold name inner =
    group
    @@ surround 2 0 lparen (string name ^^ string "/" ^^ space ^^ inner) rparen

(** ($term : $ty) *)
let annot term ty =
    group @@ surround 2 0 lparen (term ^^ space ^^ colon ^//^ ty) rparen

(** lambda $input. $body *)
let lambda ~input ~body =
    group @@ string "λ" ^^ space ^^ input ^^ string "." ^//^ body

let variant i t = group @@ string "'" ^^ i ^^ space ^^ t

let named_inj marker_pp marker data =
    group @@ string "'" ^^ marker_pp marker ^^ space ^^ data

(*let implicit_inj _marker data = group @@ data*)

let arm pat body = group @@ string "| " ^^ pat ^^ string " ⇒" ^//^ body

let iflet ~id ~pat ~term ~yes ~no =
    group @@ string "if let " ^^ variant id pat ^^ string " =" ^//^ term
    ^//^ string "then" ^/^ yes ^//^ string "else" ^/^ no ^//^ string "fi"

let field t nth = group @@ t ^^ string "." ^^ nth

let print_default termin = function
    | None -> termin
    | Some doc -> (group @@ string "| … ⇒" ^//^ doc) ^/^ termin

let case ~term ~arms ~arm_pp ~default =
    group @@ string "case" ^^ space ^^ term ^^ space ^^ string "of"
    ^/^ separate (break 1) (List.mapi arm_pp arms)
    ^/^ print_default (string "esac") default

(** let $var = $def in $body *)
let let_ ~recur ~var ~def ~body =
    group
    @@ (string "let" ^^ recur ^^ space ^^ var ^^ string " =" ^//^ nest 2 def)
    ^/^ group (string "in" ^/^ body)

let wildcard = string "_"
let omega = string "ω"

(** $t $u *)
let app t u = group @@ t ^//^ u

let separate_mapi sep app ts =
    ts
    |> List.mapi (fun i t -> (i, t))
    |> separate_map sep (fun (i, t) -> app i t)

(** (t1, t2... tn) *)
let tuple pos_pp term_pp ts =
    group
    @@
    match ts with
    | [] -> lparen ^^ rparen
    | _ ->
        surround 2 0 lparen
          (match ts with
          (* For arity-1 tuples we print (foo,)
             instead of (foo) which would be ambiguous. *)
          | [ (lab, t) ] -> pos_pp lab 0 (term_pp t) ^^ comma
          | _ ->
              separate_mapi
                (comma ^^ break 1)
                (fun i (lab, t) -> pos_pp lab i (term_pp t))
                ts)
          rparen

let with_ t pos_pp term_pp repls =
    group
    @@ surround 2 0 lparen
         (t ^/^ string "with"
         ^/^ separate
               (comma ^^ break 1)
               (List.mapi (fun i (lab, t) -> pos_pp lab i (term_pp t)) repls))
         rparen

(** ∃$w1 $w2 ($w3 = $s) $w4... $wn. $c *)
let exist bindings body =
    group
    @@
    let print_binding (w, s) =
        Hint.default_repr
          ~suggestion:(fun () -> w)
          ~fmt:(fun s -> string s)
          ~embed:(fun s ->
            group @@ surround 2 0 lparen (w ^/^ string "=" ^/^ s) rparen)
          s
    in
    let bindings = group (flow_map (break 1) print_binding bindings) in
    group
      (utf8string "∃" ^^ ifflat empty space ^^ nest 2 bindings ^^ break 0
     ^^ string ".")
    ^^ prefix 2 1 empty body

let true_ = utf8string "⊤"
let false_ = utf8string "⊥"

(** $c1 ∧ $c2 ∧ .... ∧ $cn *)
let conjunction docs =
    group
    @@
    match docs with
    | [] -> true_
    | docs -> separate (break 1 ^^ utf8string "∧" ^^ space) docs

(** $v1 = $v2 *)
let eq v1 v2 = group @@ v1 ^/^ string "=" ^/^ v2

(** decode $v *)
let decode v = group @@ string "decode" ^^ break 1 ^^ v

let do_ = string "do?"

(**
   $ty1
incompatible with
   $ty2
*)
let incompatible ty1 ty2 =
    blank 2
    ^^ nest 2 (group @@ string "(1)" ^^ blank 2 ^^ nest 2 ty1)
    ^^ hardline ^^ string "incompatible with" ^^ hardline ^^ blank 2
    ^^ nest 2 (group @@ string "(2)" ^^ blank 2 ^^ nest 2 ty2)

let source_var (src : string * L.aux) =
    let name, loc = src in
    string name ^^ string " "
    ^^ surround 2 0 lparen (L.aux_pp_lineno loc) rparen

let cycle cyc user_vars =
    string "Cycle detected: " ^^ omega ^^ string " occurs in" ^/^ omega
    ^^ string " = " ^^ cyc
    ^/^ string "without any inductive constructor in-between."
    ^/^ string "The cycle goes through the following variables:"
    ^/^ separate (comma ^^ space) user_vars

let with_header header doc =
    string header ^^ colon ^^ nest 2 (group (hardline ^^ doc))
