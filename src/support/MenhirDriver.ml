(** This Menhir driver is adapted from the tutorial
    "Declarative parse error reporting with Menhir"

    writeup: https://baturin.org/blog/declarative-parse-error-reporting-with-menhir/
    source code: https://github.com/dmbaturin/bnfgen
    both accessed 2024-03-11

    This version is optimized for batch parsing and supports recovery after
    a parsing error. *)

module I = UntypedParser.MenhirInterpreter

(* Fetch the current state of the parser and deduce the appropriate error message.
   This depends on `SyntaxErrors.messages`. *)
let get_parse_error env =
    match I.stack env with
    | (lazy Nil) -> "cannot begin a term"
    | (lazy (Cons (I.Element (state, _, _, _), _))) ->
        let msg =
            try SyntaxErrors.message (I.number state)
            with Not_found ->
              "invalid syntax (no specific message for this error)"
        in
        (* Replace the dummy error with something more friendly. *)
        if String.starts_with ~prefix:"<YOUR SYNTAX ERROR MESSAGE HERE>" msg
        then
          Format.sprintf
            "Syntax error. (Please report for improvements on the error \
             message. Error state: #%d)"
            (I.number state)
        else Format.sprintf "%s (error state %d)" msg (I.number state)

(* Main parsing loop.
   We assume that
   - [reader] is a parser that will finish on [EOF] or some token [BATCH_SEP]
   - [UntypedLexer.skip_until_batch_sep] can find the next [BATCH_SEP]

   When invoking [parse ... loc0] the strategy is as follows:
   - [<term> EOF]: this is the final term and can be parsed as [[Ok term]]
   - [<term> BATCH_SEP]: this is a correct term that is not final,
     we record [term], get the location [next] of the token [BATCH_SEP]
     and continue parsing starting from [next] to return [Ok term :: parse next].
   - [<...> BATCH_SEP]: if any parsing error [e] occurs while attempting to parse
     a term, then we obtain
     (1) [loc] the location of the error, and
     (2) [next] the location of the next [BATCH_SEP],
     after which we will recover parsing by emitting [Error e :: parse next].

   In this way [parse] can lift [reader : position -> (t, string) result]
   into [parse reader : position -> (t, position * string) result list]. *)
let rec parse (lexbuf : Lexing.lexbuf)
    (reader : Lexing.position -> unit MenhirUtils.attempted_term I.checkpoint)
    (pos : Lexing.position) :
    (MenhirUtils.parsed_term, L.aux * string) result list =
    (* Loop application of the parser until either
       - the term is complete (either [EOF] or [BATCH_SEP])
       - there is an error *)
    let rec aux (checkpoint : unit MenhirUtils.attempted_term I.checkpoint) :
        (Lexing.position MenhirUtils.attempted_term, L.aux * string) result =
        match checkpoint with
        (* Normal reduction strategy *)
        | I.InputNeeded _env -> (
            try
              let token = UntypedLexer.read lexbuf in
              let start, stop = MenhirUtils.whole_span lexbuf in
              let checkpoint = I.offer checkpoint (token, start, stop) in
              aux checkpoint
            with UntypedLexer.Malformed_token msg ->
              let loc = L.of_lex @@ MenhirUtils.whole_span lexbuf in
              Error (loc, msg))
        | I.Shifting _ | I.AboutToReduce _ ->
            let checkpoint = I.resume checkpoint in
            aux checkpoint
        (* Abort due to a syntax error *)
        | I.HandlingError env ->
            let err = get_parse_error env in
            let loc = L.of_lex @@ MenhirUtils.whole_span lexbuf in
            Error (loc, err)
        | I.Rejected ->
            let err = "invalid syntax (parser rejected the input)" in
            let loc = L.of_lex @@ MenhirUtils.whole_span lexbuf in
            Error (loc, err)
        (* Finished parsing a term, we leave the caller to decide if and where
           to continue parsing for the next term. *)
        | I.Accepted Eof -> Ok Eof
        | I.Accepted (Final t) -> Ok (Final t)
        | I.Accepted (Continue (t, ())) ->
            let loc = lexbuf.lex_curr_p in
            Ok (Continue (t, loc))
    in
    (* Then based on the parsed head term, decide what to do. *)
    match aux (reader pos) with
    (* Parsing error -> use [skip_until_batch_sep] to skip ahead to the beginning
       of the next term. *)
    | Error (loc, err) ->
        let continue = UntypedLexer.skip_until_batch_sep lexbuf in
        let pos = lexbuf.lex_curr_p in
        Error (loc, err) :: (if continue then parse lexbuf reader pos else [])
    (* Success and EOF reached -> stop here *)
    | Ok (Final v) -> [ Ok v ]
    | Ok Eof -> [] (* Support for trailing [BATCH_SEP]. *)
    (* Success followed by [BATCH_SEP] -> keep going. *)
    | Ok (Continue (v, pos)) -> Ok v :: parse lexbuf reader pos
