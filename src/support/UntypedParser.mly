%{
    open Untyped.Make(Utils.Empty)

    let unit_pattern () = Pattern.Tuple Unsized.(enumerate @@ of_list Exact [])
    let unit_term = IAdt (Tuple [])
    let unit_ty () = STLC.Constr (IAdt (Prod Unsized.(enumerate @@ of_list Exact [])))

    let enumerate_implicit_2 l =
        l |> List.mapi (fun i ((), a) -> (i, a))

    let enumerate_implicit_3 l =
        l |> List.mapi (fun i ((), a, b) -> (i, a, b))

    let lazy_default dflt cur =
        match cur with
        | Some cur -> cur
        | None -> dflt ()

    let lazy_located_default dflt cur =
        match cur with
        | Ok cur -> cur
        | Error loc -> L.at loc (dflt ())
%}

%token <string> LIDENT
%token <string> CAPIDENT
%token <int> NUM

%token EOF
%token BATCH_SEP

%token LET "let"
%token REC "rec"
%token IN "in"
%token OF "of"
%token LAMBDA "lambda"
%token CASE "case"
%token ESAC "esac"
%token AS "as"
%token IF "if"
%token THEN "then"
%token ELSE "else"
%token WITH "with"
%token TYPE "type"

%token ARROW "->"
%token DOUBLE "=>"
%token LPAR "("
%token RPAR ")"
%token LBRACE "{"
%token RBRACE "}"
%token LBRACK "["
%token RBRACK "]"
%token STAR "*"
%token PLUS "+"
%token COMMA ","
%token QUOTE "'"
%token EQ "="
%token COLON ":"
%token PERIOD "."
%token ETC "..."
%token DISJ "|"
%token WILDCARD "_"
%token LANGLE "<"
%token RANGLE ">"
%token SLASH "/"

%type<unit MenhirUtils.attempted_term> batch_term_elem

%start batch_term_elem

%%

let batch_term_elem :=
  | t = located(term) ; BATCH_SEP ;
    { MenhirUtils.Continue (t, ()) }
  | t = located(term) ; EOF ;
    { MenhirUtils.Final t }
  | EOF ;
    { MenhirUtils.Eof }

(************* PARAMETERS **************)

let binding(X, T) :=
    | x = X ; "=" ; t = T ;
      { (x, t) }

let labeled(X, T) :=
    | x = X ; ":" ; t = T ;
      { (x, t) }

let elidable_labeled(X, T) :=
    | x = X ;
      { (x, Error (L.of_lex ($endpos, $endpos))) }
    | x = X ; ":" ; t = T ;
      { (x, Ok t) }

let implicit_label(T) :=
    | t = T ;
      { ((), t) }

let elided_value(X) :=
    | x = X ;
      { (x, ()) }

let quoted(T) :=
    | QUOTE ; t = T ;
      { t }

let juxtaposed(A, B) :=
    | a = A ; b = B ;
      { (a, b) }

let dotted(A, B) :=
    | a = A ; "." ; b = B ;
      { (a, b) }

let elidable(X) :=
    |
      { None }
    | x = X ;
      { Some x }


(************* LOCATIONS *******)

let with_loc(T) :=
    | t = T ;
      { (t, L.of_lex ($startpos(t), $endpos(t))) }

let located(T) :=
    | t = T ;
      { L.at (L.of_lex ($startpos, $endpos)) t }

(***************** TERMS ***************)

let term :=
  | ~ = term_abs ; <>

let injected(I, P) :=
    | ~ = quoted(juxtaposed(I, P)) ; <>

let annotated(X, T) :=
    | ~ = labeled(X, T) ; <>

let elidable_pat :=
  | p = elidable(pat) ; { p |> lazy_default unit_pattern }

let pat :=
  | p = located(pat_atom) ; "as" ; x = tevar ;
    { Pattern.(Named (x, L.of_lex ($startpos(x), $endpos(x)), p)) }
  | (p, ty) = annotated(located(pat_atom), located(typ)) ;
    { Pattern.(Annot (p, ty)) }
  | ~ = pat_atom ;
    <>

let pat_atom :=
  | "_" ;
    { Pattern.Wildcard }
  | x = tevar ;
    { Pattern.(Var (x, L.of_lex ($startpos, $endpos))) }
  | (tup, termin) = tuple_etc(implicit_label(located(pat))) ;
    { Pattern.(Tuple Unsized.(enumerate @@ of_list termin tup)) }
  | constructor = CAPIDENT ; p = located(pat_atom) ;
    { Opaque(constructor, L.of_lex ($startpos(constructor), $endpos(constructor)), p) }
  | ((tup, termin), loc) = with_loc(nonempty_tuple_etc(binding(tuplabel, located(pat)))) ;
    { Pattern.(STuple Unsized.(sort loc Printer.label @@ of_list termin tup)) }
  | "(" ; ~ = pat ; ")" ;
    <>

let recursive :=
    | "rec" ; { Rec }
    | { Strict }

let typedef :=
    | "type" ; (alias, params) = typ_params(tyvar) ; "of" ; ty = located(typ) ;
      { (alias, params, ty) }

let term_abs :=
  | ty = typedef ; "in" ; rest = located(term_abs) ;
    { Typedef (ty, rest) }
  | "lambda" ; xs = list (located(pat_atom)) ; "." ; t = located(term_abs) ;
    { L.destruct @@ List.fold_right (fun x t -> L.transform x ~fn:(fun here _ -> L.at here @@ Abs (x, t))) xs t }
  | "let" ; recur = recursive ; (pat, t1) = binding(located(pat_atom), located(term)) ; "in" ; t2 = located(term) ;
    { LetPat (recur, pat, t1, t2) }
  | (t, arms, default) = case(implicit_inj_arms) ;
    { let arms = enumerate_implicit_3 arms in
      IAdt (Case (t, arms, default)) }
  | (t, arms, default) = case(named_inj_arms) ;
    { SAdt (Case (t, arms, default)) }
  | adt = iflet(NUM, located(elidable_pat)) ;
    { IAdt adt }
  | adt = iflet(variantlabel, located(elidable_pat)) ;
    { SAdt adt }
  | (constructor, loc) = with_loc(CAPIDENT) ; SLASH ; inner = located(term_abs) ;
    { Unfold (constructor, loc, inner) }
  | (constructor, loc) = with_loc(CAPIDENT) ; inner = located(term_abs) ;
    { Opaque (constructor, loc, inner) }
  | ~ = term_app ; <>
  | ~ = term_inj ; <>

let iflet(I, P) :=
  | "if" ; "let" ; (inj, pat) = injected(located(I), P) ; "=" ; term = located(term) ;
    "then" ; yes = located(term) ;
    "else" ; no = located(term) ;
    { IfLet (inj, pat, term, yes, no) }

let term_app :=
  | t1 = located(term_app) ; t2 = located(term_field) ;
    { App (t1, t2) }
  | ~ = term_field ; <>

let term_inj :=
  | "'" ; id = located(NUM) ; t = located(elided_term) ;
    { IAdt (Sum (id, t)) }
  | "'" ; id = located(CAPIDENT) ; t = located(elided_term) ;
    { SAdt (Sum (id, t)) }

let elided_term :=
    |
      { unit_term }
    | ~ = term_field ; <>

let field(access) :=
    | (t, acc) = dotted(located(term_field), located(access)) ;
      { Field (t, acc) }

let term_field :=
  | field = field(NUM) ;
    { IAdt field }
  | field = field(LIDENT) ;
    { SAdt field }
  | ~ = term_atom ; <>


let term_with(L) :=
  | "(" ; t = located(term_atom) ; "with" ; subst = nonempty_item_sequence(binding(L, located(term)), ",") ; ")" ;
    { With (t, subst) }

let term_atom :=
  | x = tevar ;
    { Var (x, L.of_lex ($startpos, $endpos)) }
  | ts = tuple(implicit_label(located(term))) ;
    { IAdt (Tuple (enumerate_implicit_2 ts)) }
  | ts = nonempty_tuple(binding(tuplabel, located(term))) ;
    { SAdt (Tuple ts) }
  | w = term_with(NUM) ;
    { IAdt w }
  | w = term_with(tuplabel) ;
    { SAdt w }
  | "(" ; t = located(term) ; ":" ; ty = located(typ) ; ")" ;
    { Annot (t, ty) }
  | "(" ; ~ = term ; ")" ; <>


let nonempty_item_sequence(X, Sep) :=
    | x = X ;
      { [x] }
    | x = X ; Sep ; xs = item_sequence(X, Sep) ;
      { x :: xs }

let tevar :=
  | v = LIDENT ;
    { v }

let tuplabel :=
  | ~ = LIDENT ; <>

let variantlabel :=
  | ~ = CAPIDENT ; <>

let tuple (X) :=
  | "(" ; ")" ;
    { [] }
  (* note: the rule below enforces that one-element lists always
     end with a trailing comma *)
  | "(" ; x = X ; "," ; xs = item_sequence(X, ",") ; ")";
    { x :: xs }

let nonempty_tuple (X) :=
  | "(" ; x = X ; "," ; xs = item_sequence(X, ",") ; ")";
    { x :: xs }

let size_etc :=
    | "..." ;
        { Unsized.Approx }
    |
        { Unsized.Exact }

let tuple_etc (X) :=
  | "(" ; termin = size_etc ; ")" ;
    { ([], termin) }
  | "(" ; x = X ; COMMA ; xs = item_sequence(X, COMMA) ; termin = size_etc ; ")";
    { (x :: xs, termin) }

let nonempty_tuple_etc (X) :=
  | "(" ; x = X ; COMMA ; xs = item_sequence(X, COMMA) ; termin = size_etc ; ")";
    { (x :: xs, termin) }


let implicit_inj_arm := | ~ = arm(implicit_label(located(pat))) ; <>
let named_inj_arm := | ~ = arm(injected(CAPIDENT, located(elidable_pat))) ; <>
let implicit_inj_arms := | ~ = arm_seq(implicit_inj_arm) ; <>
let named_inj_arms := | ~ = nonempty_arm_seq(named_inj_arm) ; <>

let case(arms) :=
    | "case" ; x = located(term) ; "of" ;
      (arms, default) = arms ;
      "esac" ;
      { (x, arms, default) }

let default_case :=
    | "|" ; "..." ; "=>" ; default = located(term) ;
      { Some default }
    |
      { None }

let arm(binder) :=
    | "|" ; (inj, pat) = binder ; "=>" ; t = located(term) ;
      { (inj, pat, t) }

let arm_seq(arm) :=
    | default = default_case ;
      { ([], default) }
    | arm = arm ;
      (rest, default) = arm_seq(arm) ;
      { arm::rest, default }

let nonempty_arm_seq(arm) :=
    | arm = arm ; (rest, default) = arm_seq(arm) ;
      { (arm::rest, default) }

(* item sequence with optional trailing separator *)
let item_sequence(X, Sep) :=
  |
    { [] }
  | x = X ;
    { [x] }
  | x = X ; () = Sep ; xs = item_sequence(X, Sep) ;
    { x :: xs }

(*************** TYPES ***************)

let typ :=
  | ~ = typ_arrow ; <>

let typ_arrow :=
  | ty1 = located(typ_atom) ; "->" ; ty2 = located(typ_arrow) ;
    { STLC.Constr (Structure.Arrow (ty1, ty2)) }
  | ~ = typ_atom ; <>

(* By default we treat {} as the indexed empty tuple, not the labeled empty
   tuple. Types that are *syntactically* labeled must be nonempty.
   This is ok because the inference will coerce them to each other. *)
let typ_adt :=
  | tys = typ_seq("{", implicit_label(located(typ)), "*", "}") ;
    { IAdt (Prod (Unsized.enumerate tys)) }
  | tys = nonempty_typ_seq("{", labeled(tuplabel, located(typ)), "*", "}") ;
    { SAdt (Prod (Unsized.sort (L.of_lex ($startpos(tys), $endpos(tys))) Printer.label tys)) }
  | tys = typ_seq("[", implicit_label(located(typ)), "+", "]") ;
    { IAdt (Sum (Unsized.enumerate tys)) }
  | tys = nonempty_typ_seq("[", elidable_labeled(quoted(variantlabel), located(typ)), "+", "]") ;
    { let tys = Unsized.raw_map (lazy_located_default unit_ty) tys in
      SAdt (Sum (Unsized.sort (L.of_lex ($startpos(tys), $endpos(tys))) Printer.label tys)) }

let typ_params(P) :=
  | "<" ; params = item_sequence(P, ",") ; ">" ; alias = CAPIDENT ;
    { (alias, params) }

let typ_atom :=
  | "_" ;
    { STLC.Constr Wildcard }
  | x = tyvar ;
    { STLC.Constr (Var x) }
  | (alias, params) = typ_params(located(typ)) ;
    { STLC.Constr (Alias (alias, params)) }
  | adt = typ_adt ;
    { STLC.Constr adt }
  | "(" ; ~ = typ ; ")" ; <>

let tyvar :=
  | ~ = LIDENT ; <>

(* Accepts a sequence of Ty punctuated by Sep, with no trailing separator
   but maybe a trailing [Sep ...]
   This gives our syntax of composite types such as
   - {}
   - {...}
   - {a * b}
   - {a * b * ...}
   *)
let typ_seq (Beg, Ty, Sep, End) :=
  | Beg ; End ;
    { Unsized.(nil Exact) }
  | Beg ; "..." ; End ;
    { Unsized.(nil Approx) }
  | ~ = nonempty_typ_seq(Beg, Ty, Sep, End) ; <>

let nonempty_typ_seq(Beg, Ty, Sep, End) :=
  | Beg ; (l, t) = Ty ; ts = typ_seq_suffix(Ty, Sep, End) ;
    { Unsized.(cons l t ts) }

let typ_seq_suffix (Ty, Sep, End) :=
  | Sep ; "..." ; End ;
    { Unsized.(nil Approx) }
  | End ;
    { Unsized.(nil Exact) }
  | Sep ; (l, t) = Ty ; ts = typ_seq_suffix(Ty, Sep, End) ;
    { Unsized.(cons l t ts) }

