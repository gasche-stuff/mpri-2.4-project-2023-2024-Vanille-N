type ('a, 'e) t

val map : ('a1 -> 'a2) -> finish:('e1 -> 'e2) -> ('a1, 'e1) t -> ('a2, 'e2) t

type seq_take_termin = Truncated of int | Finished of int

val take_from_seq : int -> 'a Seq.t -> ('a, seq_take_termin) t
val iter : ('a -> unit) -> finish:('e -> 'o) -> ('a, 'e) t -> 'o
val punctuate : (unit -> 'a) -> ('a, 'e) t -> ('a, 'e) t
val seq_take_termin_pp : seq_take_termin -> PPrint.document
val of_seq : 'a Seq.t -> ('a, unit) t
