{
  open UntypedParser

  exception Malformed_token of string

  let keyword_table =
    Hashtbl.create 17

  let keywords = [
      "let", LET;
      "in", IN;
      "as", AS;
      "lambda", LAMBDA;
      "case", CASE;
      "esac", ESAC;
      "of", OF;
      "if", IF;
      "then", THEN;
      "else", ELSE;
      "with", WITH;
      "type", TYPE;
      "rec", REC;
    ]

  let _ =
    List.iter
      (fun (kwd, tok) -> Hashtbl.add keyword_table kwd tok)
      keywords

  let new_line lexbuf =
    Lexing.new_line lexbuf
}

let identchar = [ 'a'-'z' 'A'-'Z' '0'-'9' '_' ]
let lident = [ 'a'-'z' ] identchar* | '_' identchar+
let capident = [ 'A'-'Z' ] identchar*
let numchar = [ '0'-'9' ]
let numident = numchar*

let blank = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

rule read = parse
  | eof      { EOF }
  | "--%"    { BATCH_SEP }
  | newline      { new_line lexbuf; read lexbuf }
  | blank        { read lexbuf }
  | lident as id { try Hashtbl.find keyword_table id
                   with Not_found -> LIDENT id }
  | capident as id { (* keywords never capitalized *) CAPIDENT id }
  | numident as id { NUM (int_of_string id) }
  | '_'      { WILDCARD }
  | "->" | "→"        { ARROW }
  | "=>" | "⇒"        { DOUBLE }
  | "..." | "…"       { ETC }
  | "λ"      { LAMBDA }
  | '('      { LPAR }
  | ')'      { RPAR }
  | '{'      { LBRACE }
  | '}'      { RBRACE }
  | '['      { LBRACK }
  | ']'      { RBRACK }
  | '*'      { STAR }
  | '+'      { PLUS }
  | "'"      { QUOTE }
  | ','      { COMMA }
  | '='      { EQ }
  | ":"      { COLON }
  | '|'      { DISJ }
  | '.'      { PERIOD }
  | '<'      { LANGLE }
  | '>'      { RANGLE }
  | '/'      { SLASH }
  | "--"     { line_comment lexbuf; read lexbuf }
  | _ as c
    {
        let suggestion_list = match c with
            | '-' -> Some ["->"; "--%"; "--"]
            | '=' -> Some ["=>"]
            | '.' -> Some ["..."]
            | _ -> None
        in
        raise @@ Malformed_token (match suggestion_list with
            | None -> "invalid character."
            | Some syms -> Format.sprintf "unfinished token. Did you mean %s ?" (String.concat " or " syms))
    }

and line_comment = parse
  | newline
      { new_line lexbuf; () }
  | eof
      { raise (Malformed_token "Unterminated OCaml comment: \
                                no newline at end of file.")  }
  | _
      { line_comment lexbuf }

and skip_until_batch_sep = parse
  | eof      { false }
  | "--%"    { true }
  | newline  { new_line lexbuf; skip_until_batch_sep lexbuf }
  | blank    { skip_until_batch_sep lexbuf }
  | "--"     { line_comment lexbuf; skip_until_batch_sep lexbuf }
  | _        { skip_until_batch_sep lexbuf }

