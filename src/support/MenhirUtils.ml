open Lexing

type parsed_term = Untyped.Make(Utils.Empty).raw_term L.t

let whole_span lexbuf = (lexbuf.lex_start_p, lexbuf.lex_curr_p)

type 'a attempted_term =
  | Continue of parsed_term * 'a
      (** Syntactically correct term followed by a separator
      that allows a sequence of terms to follow.
      The blank is intended to be filled with the location at which we can continue parsing. *)
  | Final of parsed_term
      (** Syntactically correct term, but no token that would allow us to continue parsing. *)
  | Eof  (** EOF reached without parsing anything. *)
