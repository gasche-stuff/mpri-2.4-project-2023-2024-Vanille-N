module Make (T : Utils.Functor) = struct
  open Untyped.Make (T)

  let print_term : term -> PPrint.document =
      let print_pattern pat =
          Pattern.pp Var.print STLCPrinter.print_ty (L.destruct pat)
      in
      let rec print_top t = print_left_open (L.destruct t)
      and print_left_open t =
          let print_self t = print_left_open (L.destruct t)
          and print_next = print_app in
          let print_arm variant_pp pat body =
              Printer.(arm (variant_pp (print_pattern pat)) (print_self body))
          in
          PPrint.group
          @@
          match t with
          | Abs (pat, t) ->
              Printer.lambda ~input:(print_pattern pat) ~body:(print_self t)
          | LetPat (recur, pat, t, u) ->
              Printer.let_ ~recur:(Utils.recursive_pp recur)
                ~var:(print_pattern pat) ~def:(print_self t)
                ~body:(print_self u)
          | Typedef ((name, params, inner), term) ->
              Printer.typedef name
                (List.map STLC.TyVar.print params)
                (L.map_destruct STLCPrinter.print_ty inner)
                (print_self term)
          | IAdt (Case (t, arms, default)) ->
              Printer.case ~term:(print_self t) ~arms
                ~arm_pp:(fun i (inj, pat, body) ->
                  print_arm Printer.(variant_case elided_integer inj i) pat body)
                ~default:(Option.map print_self default)
          | SAdt (Case (t, arms, default)) ->
              Printer.case ~term:(print_self t) ~arms
                ~arm_pp:(fun i (inj, pat, body) ->
                  print_arm Printer.(variant_case elided_string inj i) pat body)
                ~default:(Option.map print_self default)
          | IAdt (IfLet (id, pat, t, y, n)) ->
              Printer.iflet
                ~id:(L.map_destruct Printer.integer id)
                ~pat:(print_pattern pat) ~term:(print_self t)
                ~yes:(print_self y) ~no:(print_self n)
          | SAdt (IfLet (id, pat, t, y, n)) ->
              Printer.iflet
                ~id:(L.map_destruct Printer.string id)
                ~pat:(print_pattern pat) ~term:(print_self t)
                ~yes:(print_self y) ~no:(print_self n)
          | other -> print_next other
      and print_app t =
          let print_self t = print_app (L.destruct t)
          and print_next t = print_atom (L.destruct t) in
          let print_variant id_pp id term =
              Printer.(variant (id_pp id) (print_next term))
          in
          PPrint.group
          @@
          match t with
          | App (t, u) -> Printer.app (print_self t) (print_next u)
          | IAdt (Sum (id, t)) ->
              print_variant (L.map_destruct Printer.integer) id t
          | SAdt (Sum (id, t)) ->
              print_variant (L.map_destruct Printer.string) id t
          | other -> print_atom other
      and print_atom t =
          let print_self t = print_atom (L.destruct t) in
          PPrint.group
          @@
          match t with
          | Var (x, _loc) -> Var.print x
          | Annot (t, ty) ->
              Printer.annot (print_top t)
                (L.map_destruct STLCPrinter.print_ty ty)
          | Opaque (name, _loc, inner) -> Printer.opaque name (print_top inner)
          | Unfold (name, _loc, inner) -> Printer.unfold name (print_top inner)
          | IAdt (Tuple ts) ->
              Printer.(tuple (tup_position elided_integer) print_top ts)
          | SAdt (Tuple ts) ->
              Printer.(tuple (tup_position elided_string) print_top ts)
          | IAdt (Field (t, nth)) ->
              Printer.(field (print_self t) (L.map_destruct integer nth))
          | SAdt (Field (t, nth)) ->
              Printer.(field (print_self t) (L.map_destruct string nth))
          | IAdt (With (t, repl)) ->
              Printer.with_ (print_self t)
                Printer.(tup_position elided_integer)
                print_top repl
          | SAdt (With (t, repl)) ->
              Printer.with_ (print_self t)
                Printer.(tup_position elided_string)
                print_top repl
          | ( App _ | Abs _ | LetPat _ | Typedef _
            | IAdt (Sum _)
            | SAdt (Sum _)
            | IAdt (Case _)
            | SAdt (Case _)
            | IAdt (IfLet _)
            | SAdt (IfLet _) ) as other ->
              PPrint.parens (print_left_open other)
          | Do _p -> Printer.do_
      in

      print_left_open
end
