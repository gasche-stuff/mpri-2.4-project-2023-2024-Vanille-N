open STLC

let print_ty : ty -> PPrint.document =
    Structure.deep_pp
      ~lift:(fun deep_pp (Constr inner) -> deep_pp inner)
      ~unfold:L.destruct

let print_term : term -> PPrint.document =
    let print_binding b =
        Printer.annot (TeVar.print b.var) (print_ty (L.destruct b.typ))
    in
    let print_pattern (pat : pattern L.t) : PPrint.document =
        Pattern.pp print_binding print_ty (L.destruct pat)
    in
    let rec print_top t = L.map_destruct print_left_open t
    and print_left_open t =
        let print_self = L.map_destruct print_left_open
        and print_next = print_app in
        let print_arm variant_pp pat body =
            Printer.(arm (variant_pp (print_pattern pat)) (print_self body))
        in
        PPrint.group
        @@
        match t with
        | Abs (pat, t) ->
            Printer.lambda ~input:(print_pattern pat) ~body:(print_self t)
        | LetPat (recur, pat, t, u) ->
            Printer.let_ ~recur:(Utils.recursive_pp recur)
              ~var:(print_pattern pat) ~def:(print_top t) ~body:(print_self u)
        | Typedef ((name, params, inner), term) ->
            Printer.typedef name
              (List.map TyVar.print params)
              (print_ty (L.destruct inner))
              (print_self term)
        | IAdt (Case (t, arms, default)) ->
            Printer.case ~term:(print_self t) ~arms
              ~arm_pp:(fun i (inj, pat, body) ->
                print_arm Printer.(variant_case elided_integer inj i) pat body)
              ~default:(Option.map print_self default)
        | SAdt (Case (t, arms, default)) ->
            Printer.case ~term:(print_self t) ~arms
              ~arm_pp:(fun i (inj, pat, body) ->
                print_arm Printer.(variant_case elided_string inj i) pat body)
              ~default:(Option.map print_self default)
        | IAdt (IfLet (id, pat, t, y, n)) ->
            Printer.iflet ~id:(Printer.integer id) ~pat:(print_pattern pat)
              ~term:(print_self t) ~yes:(print_self y) ~no:(print_self n)
        | SAdt (IfLet (id, pat, t, y, n)) ->
            Printer.iflet ~id:(Printer.string id) ~pat:(print_pattern pat)
              ~term:(print_self t) ~yes:(print_self y) ~no:(print_self n)
        | other -> print_next other
    and print_app t =
        let print_self = L.map_destruct print_app
        and print_next = L.map_destruct print_atom in
        let print_variant id_pp id term =
            Printer.(variant (id_pp id) (print_next term))
        in
        PPrint.group
        @@
        match t with
        | App (t, u) -> Printer.app (print_self t) (print_next u)
        | IAdt (Sum (id, t)) -> print_variant Printer.integer id t
        | SAdt (Sum (id, t)) -> print_variant Printer.string id t
        | other -> print_atom other
    and print_atom (t : term) =
        let print_self = L.map_destruct print_atom in
        PPrint.group
        @@
        match t with
        | Var x -> TeVar.print x
        | Annot (t, ty) ->
            Printer.annot (print_top t) (print_ty (L.destruct ty))
        | Opaque (name, inner) -> Printer.opaque name (print_top inner)
        | Unfold (name, inner) -> Printer.unfold name (print_top inner)
        | IAdt (Tuple ts) ->
            Printer.(tuple (tup_position elided_integer) print_top ts)
        | SAdt (Tuple ts) ->
            Printer.(tuple (tup_position elided_string) print_top ts)
        | IAdt (Field (t, nth)) -> Printer.(field (print_self t) (integer nth))
        | SAdt (Field (t, nth)) -> Printer.(field (print_self t) (string nth))
        | IAdt (With (t, subst)) ->
            Printer.(
              with_ (print_self t)
                Printer.(tup_position elided_integer)
                print_top subst)
        | SAdt (With (t, subst)) ->
            Printer.(
              with_ (print_self t)
                Printer.(tup_position elided_string)
                print_top subst)
        | ( App _ | Abs _ | LetPat _ | Typedef _
          | IAdt (Sum _)
          | SAdt (Sum _)
          | IAdt (Case _)
          | SAdt (Case _)
          | IAdt (IfLet _)
          | SAdt (IfLet _) ) as other ->
            PPrint.parens (print_left_open other)
    in

    print_left_open
