type ('a, 'e) t = unit -> ('a, 'e) node
and ('a, 'e) node = Continue of 'a * ('a, 'e) t | Finish of 'e

let map fa ~finish t =
    let rec aux t () =
        match t () with
        | Continue (a, rest) -> Continue (fa a, aux rest)
        | Finish e -> Finish (finish e)
    in
    aux t

type seq_take_termin = Truncated of int | Finished of int

let seq_take_termin_pp = function
    | Truncated n ->
        PPrint.string (Printf.sprintf "-- Truncated after %d elements" n)
    | Finished n ->
        PPrint.string (Printf.sprintf "-- Finished after %d elements" n)

let take_from_seq max_count s =
    let rec aux count (s : _ Seq.t) () =
        match s () with
        | Nil -> Finish (Finished count)
        | _ when count = max_count -> Finish (Truncated count)
        | Cons (a, rest) -> Continue (a, aux (count + 1) rest)
    in
    aux 0 s

let iter f ~finish t =
    let rec aux t =
        match t () with
        | Continue (a, rest) ->
            f a;
            aux rest
        | Finish e -> finish e
    in
    aux t

let punctuate p t =
    let rec aux active t () =
        match t () with
        | Finish e -> Finish e
        | Continue (a, rest) ->
            let rest = aux true rest in
            if active then Continue (p (), fun () -> Continue (a, rest))
            else Continue (a, rest)
    in
    aux false t

let of_seq s =
    let rec aux (s : _ Seq.t) () =
        match s () with
        | Nil -> Finish ()
        | Cons (a, rest) -> Continue (a, aux rest)
    in
    aux s
