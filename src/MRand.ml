(* The internal representation is a bit subtle here, in
   an effort to get some good performance out of the generator.

   [finished] contains instantiated ground terms, and [later] contains
   lazily computed ones, with holes. As such when we do a random
   sampling if we pick an item from [later] it may be that there
   does not exist a way to fill in the hole. In that case we will
   implement a way to remove the term from [later] so that this
   dead-end will not be picked again. *)
type 'a t = {
  (* WF invariant :
      len = List.length finished
      && len' = List.length later *)
  len : int;
  len' : int;
  finished : 'a list;
  later : (unit -> 'a t) list;
}

let wf (s : 'a t) : 'a t =
    Utils.wf "MRand: s.len = List.length s.finished" (fun () ->
        s.len = List.length s.finished);
    Utils.wf "MRand: s.len' = List.length s.later" (fun () ->
        s.len' < List.length s.later);
    s

let of_finished_unchecked (a : 'a list) : 'a t =
    { len = List.length a; len' = 0; finished = a; later = [] }

let of_finished (a : 'a list) : 'a t = wf (of_finished_unchecked a)

let of_later (a : (unit -> 'a t) list) : 'a t =
    wf { len = 0; len' = List.length a; finished = []; later = a }

let empty : 'a t = of_finished_unchecked []

let rec map (f : 'a -> 'b) (s : 'a t) : 'b t =
    wf
      {
        s with
        finished = s.finished |> List.map f;
        later = s.later |> List.map (fun gen () -> gen () |> map f);
      }

let return (x : 'a) : 'a t = of_finished [ x ]
let delay (f : unit -> 'a t) : 'a t = of_later [ f ]

let concat (s : 'a t) (s' : 'a t) : 'a t =
    wf
      {
        len = s.len + s'.len;
        len' = s.len' + s'.len';
        finished = s.finished @ s'.finished;
        later = s.later @ s'.later;
      }

let rec bind (sa : 'a t) (f : 'a -> 'b t) : 'b t =
    sa.finished |> List.map f |> List.fold_left concat (sa |> bind_later f)

and bind_later (f : 'a -> 'b t) (s : 'a t) =
    of_later (s.later |> List.map (fun s () -> bind (s ()) f))

let sum (li : 'a t list) : 'a t = li |> List.fold_left concat empty
let fail : 'a t = empty
let one_of (vs : 'a array) : 'a t = vs |> Array.to_list |> of_finished

type 'a chosen = Picked of 'a | Retry | Empty

(* Takes the nth element out of the list and returns
   it and the list shorter by one.

   @raise: [Not_found] if [i > List.length l] *)
let rec take_nth (l : 'a list) (n : int) : 'a * 'a list =
    match (l, n) with
    | h :: t, i when i <= 0 -> (h, t)
    | h :: t, i ->
        let out, rem = take_nth t (i - 1) in
        (out, h :: rem)
    | [], _ -> raise Not_found

let run (s : 'a t) : 'a Seq.t =
    (* Attempt to pick an element.
       This might suceed with `Picked x` with `x` the element,
       and it might fail in two different ways:
       - `Retry` is a recoverable failure.
         We found a dead-end and we're asking to restart to not
         skew the probabilities. The generator might be reentered later.
       - `Empty` is a fatal error.
         This generator is provably empty.
         The parent should completely delete this generator
         and never invoke it again. *)
    let rec try_pick (sampler : 'a t) : 'a chosen * 'a t =
        let len = sampler.len + sampler.len' in
        if sampler.len + sampler.len' = 0 then (Empty, sampler)
        else
          let idx = Random.int len in
          if idx < sampler.len then
            ( (* Pick from the finished terms *)
              Picked (List.nth sampler.finished idx),
              sampler )
          else
            (* Pick from the lazy terms.
               This is not guaranteed to suceed. *)
            let idx = idx - sampler.len in
            let gen, trimmed = take_nth sampler.later idx in
            let res, gen = try_pick (gen ()) in
            match res with
            | Picked x ->
                (* Success. Put the generator back in the pool. *)
                ( Picked x,
                  wf { sampler with later = (fun () -> gen) :: trimmed } )
            | Retry ->
                (* Temporary failure. Put the generator back in the pool. *)
                (Retry, wf { sampler with later = (fun () -> gen) :: trimmed })
            | Empty ->
                (* Definitive failure. Remove the generator entirely. *)
                ( Retry,
                  wf { sampler with later = trimmed; len' = sampler.len' - 1 }
                )
    in
    let rec pick (miss : int) (sampler : 'a t) : 'a * 'a t =
        let res, trimmed = try_pick sampler in
        match res with
        | Picked t ->
            (* Successful sampling. This may have had the side-effect
               of removing some dead-ends from the generator. *)
            (t, trimmed)
        | Retry ->
            (* Oh no, we reached a dead-end. Try again. *)
            pick (miss + 1) trimmed
        | Empty ->
            (* A toplevel empty means there are no well-typed terms *)
            failwith "This generator is empty; no such term exists"
    in
    let rec seq (sampler : 'a t) () =
        let res, trimmed = pick 0 sampler in
        Seq.Cons (res, seq trimmed)
    in
    seq s
