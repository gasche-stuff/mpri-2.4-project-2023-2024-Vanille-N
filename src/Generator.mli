module Make (M : Utils.MonadPlus) : sig
  module Untyped := Untyped.Make(M)
  module Constraint := Constraint.Make(M)
  module Infer := Infer.Make(M)

  val untyped : Untyped.term L.t
  (** Lazy representation of all well-formed terms in the language. *)

  val constraint_ : (STLC.term L.t * STLC.ty L.t, Infer.err) Constraint.t

  val typed : depth:int -> (STLC.term L.t * STLC.ty L.t) M.t
  (** Filters out ill-typed terms from [untyped] and truncates it
      at a specific chosen depth, to produce a generator of all well-typed
      terms of size [depth]. *)
end
