(* A type of explicitly-typed terms. *)

module TyVar = Structure.TyVar

type 'v ty_ = Constr of ('v, 'v ty_ L.t) Structure.t_
type raw_ty = string ty_
type ty = TyVar.t ty_

let rec freshen_ty (Constr s) = Constr (Structure.freshen (L.map freshen_ty) s)

module TeVar = Utils.Variables ()

type term =
  | Var of TeVar.t
  | App of term L.t * term L.t
  | Abs of pattern L.t * term L.t
  | Annot of term L.t * ty L.t
  | LetPat of let_
  | IAdt of int adt
  | SAdt of string adt
  | Opaque of string * term L.t
  | Unfold of string * term L.t
  | Typedef of typedef * term L.t

and let_ = Utils.recursive * pattern L.t * term L.t * term L.t
and 'a tuple = ('a * term L.t) list
and 'a arms = ('a * pattern L.t * term L.t) list
and 'a inj = 'a * term L.t
and 'a field = term L.t * 'a
and 'a case = term L.t * 'a arms * term L.t option
and typedef = string * TyVar.t list * ty L.t

and 'a adt =
  | Tuple of 'a tuple
  | Field of 'a field
  | Sum of 'a inj
  | Case of 'a case
  | IfLet of 'a * pattern L.t * term L.t * term L.t * term L.t
  | With of term L.t * 'a tuple

and binding = { var : TeVar.t; typ : ty L.t }
and pattern = (binding, ty) Pattern.t
