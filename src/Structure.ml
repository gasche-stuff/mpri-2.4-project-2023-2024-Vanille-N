(** Type-formers are defined explicit as type "structures".

    Type structures ['a t] are parametric over the type of
    their leaves. Typical tree-shaped representation of
    types would use [ty t], a structure carrying types as
    sub-expressions, but the types manipulated by the
    constraint solver are so-called "shallow types" that
    always use inference variables at the leaves. We cannot
    write, say, [?w = α -> (β * γ)], one has to write
    [∃?w1 ?w2 ?w3 ?w4.
       ?w = ?w1 -> ?w2
     ∧ ?w1 = α
     ∧ ?w2 = ?w3 * ?w4
     ∧ ?w3 = β
     ∧ ?w4 = γ] instead.

    (The implementation goes through a first step [('v, 'a) t_]
    that is also parametrized over a notion of type variable,
    just like ['v Untyped.term] -- see the documentation there.)
*)

module TyVar = Utils.Variables ()

type ('v, 'a) t_ =
  | Wildcard
  | Var of 'v
      (** Note: a type variable here represents a rigid/opaque/abstract type [α, β...],
        not a flexible inference variable like [?w] in constraints.

        For example, for two distinct type variables [α, β]
        the term [(lambda x. x : α → α) (y : β)] is always
        ill-typed. *)
  | Arrow of 'a * 'a  (** A function type [a → a] *)
  | IAdt of (int, 'a) adt
      (** A composite type with integer labels, [{a * a}] or [[a + a]] *)
  | SAdt of (string, 'a) adt
      (** A composite type with string labels, [{x=a * y=a}] or [[X:a + Y:a]] *)
  | Alias of 'a alias  (** A named inductive type [<a,a>Ty]. *)

and ('l, 'a) adt = Prod of ('l, 'a) Unsized.t | Sum of ('l, 'a) Unsized.t
and 'a alias = string * 'a list

type 'a raw = (string, 'a) t_
type 'a t = (TyVar.t, 'a) t_
type cyclic_ty = Loop | Any | Constr of (TyVar.t, cyclic_ty) t_
type cyclic_ty_data = { ty : cyclic_ty; user_vars : Ls.source list }

let iter_adt f = function
    | Prod ts -> Unsized.iter f ts
    | Sum ts -> Unsized.iter f ts

let iter f = function
    | Wildcard -> ()
    | Var _alpha -> ()
    | Arrow (t1, t2) ->
        f t1;
        f t2
    | IAdt adt -> iter_adt f adt
    | SAdt adt -> iter_adt f adt
    | Alias (_name, args) -> List.iter f args

let map_adt f = function
    | Prod ts -> Prod Unsized.(map f ts)
    | Sum ts -> Sum Unsized.(map f ts)

let map f = function
    | Wildcard -> Wildcard
    | Var alpha -> Var alpha
    | Arrow (t1, t2) ->
        let r1 = f t1 in
        let r2 = f t2 in
        Arrow (r1, r2)
    | IAdt adt -> IAdt (map_adt f adt)
    | SAdt adt -> SAdt (map_adt f adt)
    | Alias (name, args) -> Alias (name, List.map f args)

let result_prod :
    type label a.
    (label, a) Unsized.t Utils.doc_result -> (label, a) adt Utils.doc_result =
   fun o -> Result.map (fun l -> Prod l) o

let result_sum :
    type label a.
    (label, a) Unsized.t Utils.doc_result -> (label, a) adt Utils.doc_result =
   fun o -> Result.map (fun l -> Sum l) o

let merge_adt (type label) (f : ('a, 'b) Utils.either_or_both -> 'c)
    (s1 : (label, 'a) adt) (s2 : (label, 'b) adt) :
    (label, 'c) adt Utils.doc_result =
    match (s1, s2) with
    | Prod tup, Prod tup' -> Unsized.map2 f tup tup' |> result_prod
    | Sum case, Sum case' -> Unsized.map2 f case case' |> result_sum
    | Prod _, Sum _ ->
        Error (fun () -> PPrint.string "cannot coerce tuple to union")
    | Sum _, Prod _ ->
        Error (fun () -> PPrint.string "cannot coerce union to tuple")

let merge_empty_adt (type l1 a1 l2 a2 out) (s1 : (l1, a1) adt)
    (s2 : (l2, a2) adt) (build1 : (l1, a1) adt -> out)
    (build2 : (l2, a2) adt -> out) : out Utils.doc_result =
    (* output *)
    match (s1, s2) with
    | Prod tup, Prod tup' ->
        Unsized.merge_empty tup tup'
          (fun t -> build1 (Prod t))
          (fun t -> build2 (Prod t))
    | Sum tup, Sum tup' ->
        Unsized.merge_empty tup tup'
          (fun t -> build1 (Sum t))
          (fun t -> build2 (Sum t))
    | Prod _, Sum _ ->
        Error (fun () -> PPrint.string "cannot coerce tuple to union")
    | Sum _, Prod _ ->
        Error (fun () -> PPrint.string "cannot coerce union to tuple")

let merge (type a b c) (f : (a, b) Utils.either_or_both -> c) (s1 : a t)
    (s2 : b t) : c t Utils.doc_result =
    match (s1, s2) with
    (* Wildcard can be unified with anything and disappears in the process. *)
    | Wildcard, x -> Ok (map (Utils.right_of f) x)
    | x, Wildcard -> Ok (map (Utils.left_of f) x)
    (* Unchanged *)
    | Var x, Var y ->
        if x = y then Ok (Var x)
        else
          Error
            (fun () ->
              PPrint.string
                (Format.sprintf "rigid type variables `%s` and `%s` are unequal"
                   (TyVar.name x) (TyVar.name y)))
    | Arrow (x, y), Arrow (x', y') ->
        Ok Utils.(Arrow (f (Both (x, x')), f (Both (y, y'))))
    (* Even though they have the same [Sum]/[Prod] general structure,
       this merge might still fail if the lengths are incompatible.
       See [list_map2_resized] for the details of length restrictions
       between [Sum]/[Prod] types. *)
    | IAdt adt, IAdt adt' ->
        merge_adt f adt adt' |> Result.map (fun l -> IAdt l)
    | SAdt adt, SAdt adt' ->
        merge_adt f adt adt' |> Result.map (fun l -> SAdt l)
    | IAdt adt, SAdt adt' ->
        merge_empty_adt adt adt'
          (fun a -> IAdt (map_adt (Utils.left_of f) a))
          (fun a -> SAdt (map_adt (Utils.right_of f) a))
    | SAdt adt, IAdt adt' ->
        (* Swapping [adt] and [adt'] so that [IAdt] takes precedence. *)
        merge_empty_adt adt' adt
          (fun a -> IAdt (map_adt (Utils.right_of f) a))
          (fun a -> SAdt (map_adt (Utils.left_of f) a))
    | Alias (name, args), Alias (name', args') ->
        if name = name' then
          if List.length args = List.length args' then
            Ok (Alias (name, List.map2 (fun x y -> f (Both (x, y))) args args'))
          else
            Error
              (fun () ->
                PPrint.string
                  (Format.sprintf "expected %d generic parameters, found %d"
                     (List.length args) (List.length args')))
        else
          Error
            (fun () ->
              PPrint.string
                (Format.sprintf
                   "inductive types `%s` and `%s` are not identical" name name'))
          (* FIXME: test *)
    | Var x, _ | _, Var x ->
        Error
          (fun () ->
            PPrint.string
              (Format.sprintf
                 "rigid type variable `%s` is only compatible with itself"
                 (TyVar.name x)))
        (* FIXME: design a test *)
    | Alias _, _ | _, Alias _ ->
        Error
          (fun () ->
            PPrint.string
              "inductive type constructor cannot match primitive type.")
        (* FIXME: design a test *)
    | Arrow _, _ | _, Arrow _ ->
        Error
          (fun () ->
            PPrint.string "an arrow type can only match another arrow type")
(* FIXME: design a test *)

let global_tyvar : string -> TyVar.t =
    (* There are no binders for type variables, which are scoped
       globally for the whole term. *)
    let tenv = Hashtbl.create 5 in
    fun alpha ->
      match Hashtbl.find tenv alpha with
      | alpha_var -> alpha_var
      | exception Not_found ->
          let alpha_var = TyVar.fresh alpha in
          Hashtbl.add tenv alpha alpha_var;
          alpha_var

let freshen_adt freshen = function
    | Prod ts -> Prod Unsized.(map freshen ts)
    | Sum ts -> Sum Unsized.(map freshen ts)

let freshen freshen = function
    | Wildcard -> Wildcard
    | Var alpha -> Var (global_tyvar alpha)
    | Arrow (t1, t2) -> Arrow (freshen t1, freshen t2)
    | IAdt adt -> IAdt (freshen_adt freshen adt)
    | SAdt adt -> SAdt (freshen_adt freshen adt)
    | Alias (name, args) -> Alias (name, List.map freshen args)

let sep_seq_etc ~seq ~obj_pp ~sep ~surround =
    let open Printer in
    group
    @@ surround
         ( Unsized.fold_right_enumerate
             (fun idx lab ty (hd, doc) ->
               ( Some (obj_pp lab idx ty),
                 match (hd, doc) with
                 | Some hd, Some doc -> Some (group (sep ^/^ hd) ^/^ doc)
                 | None, Some doc -> Some (group (sep ^/^ doc))
                 | None, None -> None
                 | Some hd, None -> Some (group (sep ^/^ hd)) ))
             Unsized.(
               function Exact -> (None, None) | Approx -> (Some etc, None))
             seq
         |> fun (hd, doc) ->
           match (hd, doc) with
           | Some hd, Some doc -> hd ^/^ doc
           | None, Some doc -> doc
           | Some hd, None -> hd
           | None, None -> string "" )

(** [{$t1 * $t2 * ... $tn}] *)
let product_pp ~seq ~obj_pp =
    sep_seq_etc ~seq ~obj_pp ~sep:PPrint.star ~surround:PPrint.braces

(** [{$t1 + $t2 + ... $tn}] *)
let sum_pp ~seq ~obj_pp =
    sep_seq_etc ~seq ~obj_pp ~sep:PPrint.plus ~surround:PPrint.brackets

let shallow_pp p = function
    | Wildcard -> Printer.wildcard
    | Var v -> TyVar.print v
    | IAdt (Prod ts) ->
        product_pp ~seq:(Unsized.map p ts)
          ~obj_pp:Printer.(named_ty elided_integer)
    | IAdt (Sum ts) ->
        sum_pp ~seq:(Unsized.map p ts) ~obj_pp:Printer.(named_ty elided_integer)
    | SAdt (Prod ts) ->
        product_pp ~seq:(Unsized.map p ts)
          ~obj_pp:Printer.(named_ty elided_string)
    | SAdt (Sum ts) ->
        sum_pp ~seq:(Unsized.map p ts)
          ~obj_pp:Printer.(quote_named_ty elided_string)
    | Arrow (t1, t2) -> Printer.arrow (p t1) (p t2)
    | Alias (name, args) -> Printer.alias name (List.map p args)

let deep_pp (type a b)
    ~(lift : ((TyVar.t, b) t_ -> PPrint.document) -> a -> PPrint.document)
    ~(unfold : b -> a) (t : a) : PPrint.document =
    let rec top_pp (t : a) =
        lift
          (function
            | Arrow (t1, t2) ->
                Printer.arrow (atom_pp (unfold t1)) (top_pp (unfold t2))
            | _ -> atom_pp t)
          t
    and atom_pp (t : a) =
        lift
          (function
            | Arrow _ -> Printer.parens (top_pp t)
            | other -> shallow_pp top_pp (map unfold other))
          t
    in
    top_pp t

let cycle_pp t =
    deep_pp t
      ~unfold:(fun t -> t)
      ~lift:(fun deep_pp -> function
        | Loop -> Printer.omega
        | Any -> Printer.wildcard
        | Constr st -> deep_pp st)
