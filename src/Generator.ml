module Make (M : Utils.MonadPlus) = struct
  module Untyped = Untyped.Make (M)
  module Constraint = Constraint.Make (M)
  module Infer = Infer.Make (M)
  module Solver = Solver.Make (M)

  (* just in case... *)
  module TeVar = Untyped.Var
  module TyVar = STLC.TyVar

  let dummy = L.dummy __FILE__
  let at_dummy x = L.at dummy x

  let untyped : Untyped.term L.t =
      let basic_feature t = M.return t in
      let ext_feature t =
          if Config.(OnceCell.get generator_ext_features) then M.return t
          else M.fail
      in
      let rec gen (fv : TeVar.t list) : Untyped.term L.t =
          (* The partial terms constructed at this iteration will occur in distinct
             complete terms. Thus it's fine if we generate fresh variables once and
             use them several times in different terms, it cannot produce collisions. *)
          let x = TeVar.fresh "x" in
          let y = TeVar.fresh "y" in
          Untyped.(
            at_dummy
            @@ Do
                 ( M.delay @@ fun () ->
                   M.sum
                     [
                       (* One of the existing available variables *)
                       M.sum
                         (fv |> List.map (fun v -> M.return (Var (v, dummy))));
                       (* or any term constructor recursively filled in. *)
                       (* FIXME: adjust the probabilities ? *)
                       M.sum
                         [
                           (* [?t ?t] *)
                           basic_feature @@ App (gen fv, gen fv);
                           (* [lambda x. ?t] *)
                           basic_feature
                           @@ Abs
                                ( at_dummy @@ Pattern.Var (x, dummy),
                                  gen (x :: fv) );
                           (* [let x = ?t in ?t] *)
                           basic_feature
                           @@ LetPat
                                ( Strict,
                                  at_dummy @@ Pattern.Var (x, dummy),
                                  gen fv,
                                  gen (x :: fv) );
                           (* [let () = ?t in ?t] *)
                           ext_feature
                           @@ LetPat
                                ( Strict,
                                  at_dummy
                                  @@ Pattern.Tuple
                                       Unsized.(
                                         sort dummy Printer.index (nil Exact)),
                                  gen fv,
                                  gen fv );
                           (* [let (x,) = ?t in ?t] *)
                           ext_feature
                           @@ LetPat
                                ( Strict,
                                  (at_dummy
                                  @@ Pattern.(
                                       Tuple
                                         Unsized.(
                                           sort dummy Printer.index
                                             (cons 0
                                                (at_dummy @@ Var (x, dummy))
                                                (nil Exact))))),
                                  gen fv,
                                  gen (x :: fv) );
                           (* [let (x, y) = ?t in ?t *)
                           basic_feature
                           @@ LetPat
                                ( Strict,
                                  (at_dummy
                                  @@ Pattern.(
                                       Tuple
                                         Unsized.(
                                           sort dummy Printer.index
                                             (cons 0
                                                (at_dummy @@ Var (x, dummy))
                                                (cons 1
                                                   (at_dummy @@ Var (y, dummy))
                                                   (nil Exact)))))),
                                  gen fv,
                                  gen (x :: y :: fv) );
                           (* [()] *)
                           ext_feature @@ IAdt (Tuple []);
                           (* [(?t,)] *)
                           ext_feature @@ IAdt (Tuple [ (0, gen fv) ]);
                           (* [(?t, ?t)] *)
                           basic_feature
                           @@ IAdt (Tuple [ (0, gen fv); (1, gen fv) ]);
                           (* ['0 ?t] *)
                           ext_feature @@ IAdt (Sum (at_dummy 0, gen fv));
                           (* ['1 ?t] *)
                           ext_feature @@ IAdt (Sum (at_dummy 1, gen fv));
                           (* [?t.0] *)
                           ext_feature @@ IAdt (Field (gen fv, at_dummy 0));
                           (* [?t.1] *)
                           ext_feature @@ IAdt (Field (gen fv, at_dummy 1));
                           (* [case ?t of esac] *)
                           ext_feature @@ IAdt (Case (gen fv, [], None));
                           (* [case ?t of | x => ?t esac] *)
                           ext_feature
                           @@ IAdt
                                (Case
                                   ( gen fv,
                                     [
                                       ( 0,
                                         at_dummy @@ Pattern.Var (x, dummy),
                                         gen (x :: fv) );
                                     ],
                                     None ));
                           (* [case ?t of | x => ?t | y => ?t esac] *)
                           ext_feature
                           @@ IAdt
                                (Case
                                   ( gen fv,
                                     [
                                       ( 0,
                                         at_dummy @@ Pattern.Var (x, dummy),
                                         gen (x :: fv) );
                                       ( 1,
                                         at_dummy @@ Pattern.Var (y, dummy),
                                         gen (y :: fv) );
                                     ],
                                     None (* No default case *) ));
                         ];
                     ] ))
      in
      gen []

  let constraint_ : (STLC.term L.t * STLC.ty L.t, Infer.err) Constraint.t =
      let w = Constraint.Var.fresh "final_type" in
      Constraint.(
        Exist
          ( w,
            L.toplevel,
            None,
            Top,
            Conj
              ( Infer.has_type Untyped.Var.Map.empty Infer.AliasEnv.empty
                  untyped w,
                Infer.decode w ) ))

  let typed ~(depth : int) : (STLC.term L.t * STLC.ty L.t) M.t =
      (* There is an off-by-one error because of the outer `Do` of `untyped`. *)
      let depth = depth + 1 in
      let rec expand (depth : int) (env : Unif.Env.t)
          (constr : (STLC.term L.t * STLC.ty L.t, Infer.err) Constraint.t) :
          (STLC.term L.t * STLC.ty L.t) M.t =
          (* [Solver.eval] will push [NDo] nodes towards the root.
             depending on the depth and the status we determine when to stop. *)
          let _log, env', res = Solver.eval ~log:false env constr in
          match res with
          (* Do-free term at the correct depth: this is a solution *)
          | NRet map when depth = 1 ->
              let decode = Decode.make_decoder env' () in
              M.return (map decode)
          (* Do-expandable term with insufficient depth: we can expand further *)
          | NDo ts when depth > 1 -> M.bind ts (expand (depth - 1) env')
          (* Anything else (terms that are
             - ill-typed, or
             - do-free and too shallow, or
             - incompletely expanded) are dropped *)
          | _ -> M.fail
      in
      expand depth Unif.Env.empty constraint_
end
