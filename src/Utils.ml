open Config

exception Not_found_at of string * (string -> PPrint.document)
exception Invalid_argument_at of string * (string -> PPrint.document)

type lazy_doc = unit -> PPrint.document
type 'a doc_result = ('a, lazy_doc) result
type 'a clash = 'a * 'a * lazy_doc
type void = |

let destruct_void (v : void) = match v with _ -> .

let string_of_doc doc =
    let buf = Buffer.create 128 in
    PPrint.ToBuffer.pretty 0.9 80 buf doc;
    Buffer.contents buf

(** [wf] can be called from any module to run a well-formedness check.

    I.e. [wf "error" (fun () -> wf_condition obj)] is an [assert]...
    - with a nice error message
    - that is guaranteed to run if the flag [--wf] is passed at the command line
    - that is guaranteed to be erased otherwise.

    As such it is fine to run a [wf_condition] check that has poor performance
    because as long as [--wf] is not passed it is fully lazy. *)
let wf msg condition =
    if OnceCell.get enable_wf_check then
      if not (condition ()) then
        failwith ("Well-formedness check failed: " ^ msg)

module Variables () : sig
  type t = private { name : string; stamp : int }

  val compare : t -> t -> int
  val eq : t -> t -> bool
  val fresh : string -> t
  val namegen : string array -> unit -> t
  val name : t -> string
  val print : t -> PPrint.document

  module Set : Set.S with type elt = t
  module Map : Map.S with type key = t
end = struct
  type t = { name : string; stamp : int }

  let name v = v.name
  let compare = Stdlib.compare
  let eq n1 n2 = compare n1 n2 = 0
  let stamps = Hashtbl.create 42

  let fresh name =
      let stamp =
          match Hashtbl.find_opt stamps name with None -> 0 | Some n -> n
      in
      Hashtbl.replace stamps name (stamp + 1);
      { name; stamp }

  let namegen names =
      if names = [||] then failwith "namegen: empty names array";
      let counter = ref 0 in
      let wrap n = n mod Array.length names in
      fun () ->
        let idx = !counter in
        counter := wrap (!counter + 1);
        fresh names.(idx)

  let subscript i =
      String.fold_right
        (fun c acc ->
          match c with
          | '0' -> "₀" ^ acc
          | '1' -> "₁" ^ acc
          | '2' -> "₂" ^ acc
          | '3' -> "₃" ^ acc
          | '4' -> "₄" ^ acc
          | '5' -> "₅" ^ acc
          | '6' -> "₆" ^ acc
          | '7' -> "₇" ^ acc
          | '8' -> "₈" ^ acc
          | '9' -> "₉" ^ acc
          | c -> String.(make 1 c) ^ acc)
        (string_of_int i) ""

  let print { name; stamp } =
      if stamp = 0 then PPrint.string name
      else if Config.(OnceCell.get parseable_elaboration) then
        Printf.ksprintf PPrint.string "%s__%d" name stamp
      else Printf.ksprintf PPrint.string "%s%s" name (subscript stamp)

  module Key = struct
    type nonrec t = t

    let compare = compare
  end

  module Set = Set.Make (Key)
  module Map = Map.Make (Key)
end

module type Functor = sig
  type 'a t

  val map : ('a -> 'b) -> 'a t -> 'b t
end

(** A signature for search monads, that represent
    computations that enumerate zero, one or several
    values. *)
module type MonadPlus = sig
  include Functor

  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
  val sum : 'a t list -> 'a t
  val fail : 'a t

  val one_of : 'a array -> 'a t
  (** [fail] and [one_of] can be derived from [sum], but
      they typically have simpler and more efficient
      specialized implementations. *)

  val delay : (unit -> 'a t) -> 'a t
  (** Many search monad implementations perform their computation
      on-demand, when elements are requested, instead of forcing
      computation already to produce the ['a t] value.

      In a strict language, it is easy to perform computation
      too early in this case, for example
      [M.sum [foo; bar]] will compute [foo] and [bar] eagerly
      even though [bar] may not be needed if we only observe
      the first element.

      The [delay] combinator makes this on-demand nature
      explicit, for example one can write [M.delay
      (fun () -> M.sum [foo; bar])] to avoid computing [foo]
      and [bar] too early. Of course, if the underlying
      implementation is in fact eager, then this may apply
      the function right away.*)

  val run : 'a t -> 'a Seq.t
  (** ['a Seq.t] is a type of on-demand sequences from the
      OCaml standard library:
        https://v2.ocaml.org/api/Seq.html
  *)
end

module Empty = struct
  type 'a t = |

  (* the empty type *)
  let map (_ : 'a -> 'b) : 'a t -> 'b t = function _ -> .
end

module _ : Functor = Empty

let not_yet fname _ = failwith (fname ^ ": not implemented yet")

module Ord = struct
  (* I hate [compare]. Here's a small fix. *)

  type t = Less | Equal | Greater

  let cmp_ (type l) (x : l) (x' : l) : t =
      match Stdlib.compare x x' with
      | 0 -> Equal
      | n when n > 0 -> Greater
      | _ -> Less

  let max_by ?(tiebreaker : (unit -> t) option) cmp x x' =
      match cmp x x' with
      | Less -> x'
      | Greater -> x
      | Equal -> (
          match tiebreaker with
          | None -> x
          | Some f -> (
              match f () with Less -> x' | Greater -> x | Equal -> x))

  let into (type l) (f : l -> l -> t) : l -> l -> int =
     fun x y -> match f x y with Equal -> 0 | Greater -> 1 | Less -> -1
end

type ('l, 'r) either_or_both =
  | Left of 'l
  | Right of 'r
  | Both of 'l * 'r  (** The elements of a [zip_longest]-like operation *)

let left x = Left x
let right y = Right y
let both x y = Both (x, y)
let left_of f v = f (Left v)
let right_of f v = f (Right v)

(** [List.fold_right] for faillible operations.
    Returns [None] iff any of the elements returns [None] through [f] *)
let rec try_fold_right (f : _ -> (_, _) result) : _ list -> (_ list, _) result =
  function
    | [] -> Ok []
    | x :: xs ->
        Result.bind (f x) (fun y ->
            Result.bind (try_fold_right f xs) (fun ys -> Ok (y :: ys)))

(** [Env.find] with a more palatable failure message than [Not_found].
    Asks for [fmt] that formats the key and gives a hint. *)
let try_find (type env k v) (find : k -> env -> v) (key : k) (env : env)
    (fmt : k -> string * (string -> PPrint.document)) : v =
    match find key env with
    | v -> v
    | exception Not_found ->
        let name, localizer = fmt key in
        raise
          (Not_found_at
             (Format.sprintf "Find failed: %s not found." name, localizer))

(* [take_until_second f s] assumes that [s0] satisfies [f] and returns
   the prefix of [s] from [s0] (included)
   until the second element that satisfies [f] (excluded).

   For example [take_until_second (( = ) 0) [0; 1; 4; 0; 5] = [0; 1; 4]]

   Use-case: If you have a sequence that contains a cycle you can extract
   exactly one loop of the cycle with [take_until_second (( = ) (hd s)) s]. *)
let take_until_second (f : 'i -> bool) (s : 'i Seq.t) : 'i Seq.t =
   fun () ->
    match s () with
    | Nil -> Nil
    | Cons (a, rest) -> Cons (a, Seq.take_while (Fun.negate f) rest)

type recursive = Rec | Strict

let recursive_pp =
    PPrint.(function Rec -> space ^^ string "rec" | Strict -> empty)

(* True length of a string, required for pretty-printing special characters *)
let string_utf8_length =
    Uuseg_string.fold_utf_8 `Grapheme_cluster (fun x _ -> x + 1) 0

(* Given two indexed lists (that we assume are sorted), extract the indices
   present in both and combine the associated data.
   [zip_matching [(0,'a'); (1,'b'); (2,'c')] [(0,'A'); (2,'C')] = [(0,'a','A'); (2,'b','B')]]. *)
let rec zip_matching l l' =
    match (l, l') with
    | [], _ -> []
    | _, [] -> []
    | a :: l, a' :: l' when fst a < fst a' -> zip_matching l (a' :: l')
    | a :: l, a' :: l' when fst a > fst a' -> zip_matching (a :: l) l'
    | a :: l, a' :: l' -> (fst a, snd a, snd a') :: zip_matching l l'

(* Extract the values and group by identical key.
   In the output list each [(k, [v1; v2; ...; vn])] is obtained from each of the
   [vi = value xi] for all the consecutive [xi] of the input list that had
   [k = key xi].

   [segment_map ~key:fst ~value:snd [(0,'a');(0,'b');(1,'c');(3,'d');(3,'e')]
        = [(0,['a';'b']); (1,['c']); (3,['d';'e'])]]
*)
let segment_map ~(key : 'x -> 'k) ~(value : 'x -> 'v) (xs : 'a list) :
    ('k * 'v list) list =
    let add_if_nonempty cur acc glob =
        match cur with Some cur -> (cur, List.rev acc) :: glob | None -> glob
    in
    let rec aux cur acc glob = function
        | [] -> add_if_nonempty cur acc glob |> List.rev
        | x :: xs ->
            let k = key x in
            if cur = Some k then aux cur (value x :: acc) glob xs
            else aux (Some k) [ value x ] (add_if_nonempty cur acc glob) xs
    in
    aux None [] [] xs

let list_uniq fn l =
    let rec aux prev acc = function
        | [] -> Ok (List.rev acc)
        | h :: tl ->
            let fnh = fn h in
            if prev = Some fnh then Error fnh else aux (Some fnh) (h :: acc) tl
    in
    aux None [] l
