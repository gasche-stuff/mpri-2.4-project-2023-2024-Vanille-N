# Critical bugfix: impure `decode`

At the last moment I have discovered a major bug in the generator.
This has been reported under [#3](https://gitlab.com/gasche/mpri-2.4-project-2023-2024/-/issues/3).

I am not the one who originally discovered this bug.
It is very subtle, and several of us have
- independently introduced the same bug in our programs
- not noticed it for several weeks
- still not noticed it even after we were told that other people had this bug


## The bug

The persistent `Hashtbl.t` in `Decode.ml` has two purposes
1. remember human-readable names given to inference variables
2. memoize the reconstruction of types from variables

We have found that this can result in
- `Infer` correctly accepts only typeable terms
- `Generator` correctly produces only typeable terms
- `Infer` correctly elaborates typeable terms
- `Generator` sometimes **in**correctly elaborates typeable terms

This is unreliable on several counts:
- the first generated term is *never* incorrectly elaborated
- correct elaboration depends on internal details of the sequence of UnionFind
operations
- random generation might not be affected
- terms that exhibit this phenomenon do not appear before sufficient depth


## The fix

Move the global `Hashtbl.t` in `Decode.ml` to be more local.


## Validating the fix

Specifically following the discovery of this bug I decided to stop taking
it for granted that once `Infer` produces well-typed terms, `Generator` would
also produce well-typed terms.

In order to be able to test more thoroughly that the fix is correct,
and also in order to detect more instances of this bug I have implemented
the following features

- on `minigen`
** `--parseable-output` disables pretty-printing conventions to ensure that the
terms printed by the generator can be parsed by the typer
** `--full-type` makes it print not only the generated term but also its type

- on `minihell`
** batch mode


Then by combining `minihell`'s ability to batch parse...

  $ cat demo.batch.test
  lambda x y. x y
  --%
  lambda x y. x z
  --%
  lambda x y. x x


  $ minihell demo.batch.test
  Inferred type:
    (α → β) → α → β
  
  Elaborated term:
    λ (x : α → β). λ (y : α). x y
  ----------
  
  Fatal error:
    Find failed: z not found.
       3| lambda x y. x z
                        ^this variable is unbound in the current term
  ----------
  
  Error:
    Cycle detected: ω occurs in
    ω = ω → _
    without any inductive constructor in-between.
    The cycle goes through the following variables:
    x (ll. 5), x (ll. 5), x (ll. 5)
    help:
       5| lambda x y. x x
                 ^    ^ ^these variables are involved in the cycle
  [102]

Note: the final [102] means that there are 2 failures (caps at 99).
In batch mode to just validate terms we can pipe the output to /dev/null
and just look at the return code.


... with `minigen`'s parseable output and exhaustive annotations...

  $ minigen --depth 3 --parseable-output --annot-level=2 --exhaustive --count 100
  (λ (x : ig). (λ (x__22 : t). (x__22 : t) : t → t) : ig → t → t)
  
  --%
  
  (
    λ (x : u). (λ (x__22 : ig__1). (x : u) : ig__1 → u) :
      u → ig__1 → u
  )
  
  -- Finished after 2 elements



...we can do a big check that all generated terms are correctly elaborated!

This is our pipeline:
  $ cat pipeline.sh
  #! /usr/bin/env bash
  
  FILE=exhaustive.$1.batch.test
  
  # Run exhaustive generation at the given depth.
  # Make sure to enable the flags that make the printed output
  # pipeable to `minihell`.
  dune exec minigen -- \
      --exhaustive \
      --parseable-output \
      --annot-level=2 \
      --depth $1 \
      --count 100 \
      > $FILE
  
  wc -l $FILE
  
  # Now run inference
  # (there are annotations everywhere so this is actually just typechecking)
  dune exec minihell -- \
      $FILE \
      1>/dev/null

  $ ./pipeline.sh 3
  10 exhaustive.3.batch.test
  $ ./pipeline.sh 4
  88 exhaustive.4.batch.test
  $ ./pipeline.sh 5
  548 exhaustive.5.batch.test
  $ ./pipeline.sh 6
  1475 exhaustive.6.batch.test
  $ ./pipeline.sh 7
  1559 exhaustive.7.batch.test

Error code 0 means success !

