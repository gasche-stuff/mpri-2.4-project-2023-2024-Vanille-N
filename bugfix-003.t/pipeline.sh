#! /usr/bin/env bash

FILE=exhaustive.$1.batch.test

# Run exhaustive generation at the given depth.
# Make sure to enable the flags that make the printed output
# pipeable to `minihell`.
dune exec minigen -- \
    --exhaustive \
    --parseable-output \
    --annot-level=2 \
    --depth $1 \
    --count 100 \
    > $FILE

wc -l $FILE

# Now run inference
# (there are annotations everywhere so this is actually just typechecking)
dune exec minihell -- \
    $FILE \
    1>/dev/null
