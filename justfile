BROWSER := "firefox"
ROOT := "."
ARCHIVE := "/tmp/nvillani-mpri-2.4"


# Dependency management

install:
  opam switch create . --deps-only --with-doc --with-test

update:
  opam update
  opam upgrade


# Building and testing

build:
  dune build

test-gen: build
  dune build @gen

test-infer: build
  dune build @infer

test-lib: build
  dune build @library

test-bugfix-003: build
  dune build @bugfix-003

test: test-infer test-lib test-bugfix-003 test-gen

bless:
  dune runtest || true
  dune promote


# Other dev tools

doc:
  dune build @doc

doc-open: doc
  {{BROWSER}} {{ROOT}}/_build/default/_doc/_html/index.html &

fmt:
  dune build @fmt || dune promote

cov:
  ./coverage.sh

cov-open:
  {{BROWSER}} _coverage/index.html


# Handout

# Important files that are not checked by build
stat:
  stat README.md
  stat REPORT.md
  stat CHANGELOG.md

everything: stat fmt doc test cov

archive: everything
  ./fresh.sh {{ARCHIVE}}
