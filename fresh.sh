#! /usr/bin/env bash

# This script rebuilds the entire project in a fresh
# workspace to ensure that there are no hidden dependencies
# and that the archive for handout is self-contained.

print_help() {
    echo "$0 -- End-to-end test in a fresh environment"
    echo "USAGE:"
    echo "  $0 TARGET"
    echo "  where"
    echo "     TARGET is the directory in which to rebuild."
    echo "            as a safety measure, this is *required* to be a subdirectory of /tmp"
    echo "            (source is assumed to be the working directory)"
}

precondition_target_is_tmp() {
    if ! [[ $1 == /tmp/* ]]; then
        echo "Not allowed to write files outside of /tmp/"
        exit 44
    fi
}

reset_directory() {
    precondition_target_is_tmp "$1"
    rm -rf "$1"
    mkdir -p "$1"
}

copy_project() {
    precondition_target_is_tmp "$1"
    cp -r \
        justfile \
        constrained_generation.opam \
        bin \
        src \
        infer.t \
        library.t \
        gen.t \
        bugfix-003.t \
        README.md \
        CHANGELOG.md \
        REPORT.md \
        .ocamlformat \
        environment \
        coverage.sh \
        fresh.sh \
        dune \
        dune-project \
        "$1"
}

compress_archive() {
    local BASENAME="$(dirname "$1")/$(basename "$1")"
    local FULLNAME="$BASENAME.tar.gz"
    rm -rf "$FULLNAME"
    tar czf "$FULLNAME" "$BASENAME"
}

main() {
    local RESET_SWITCH=
    local ARCHIVE=
    case $1 in
        ('')
            echo "Expects an argument (the path to the fresh directory)"
            print_help
            exit 1
            ;;
        (*"/")
            echo "Must not include trailing /"
            print_help
            exit 1
            ;;
        ("/tmp/"*)
            reset_directory "$1" &&
            copy_project "$1" &&
            compress_archive "$1" &&
            cd "$1" && (
                just install &&
                eval $(opam env) &&
                just update &&
                just everything
            ) && cd -
            ;;
        (--*)
            echo "Unexpected argument $1"
            print_help
            exit 1
            ;;
        (*)
            echo "Expected a subpath of /tmp"
            print_help
            exit 1
            ;;
    esac
}

main "$@"


